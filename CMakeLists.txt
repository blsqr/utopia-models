# Set minimum CMake version to the one supplied with Ubuntu 20.04
cmake_minimum_required(VERSION 3.16)
include(FeatureSummary)
include(CMakeDependentOption)

# Start the project
project(EvoEcoModels
    DESCRIPTION "Evolutionary Ecosystem Models, implemented in Utopia"
    LANGUAGES C CXX
    VERSION 1.0
)

# Parse CMake options
# Option is only presented if CMAKE_BUILD_TYPE is not set to 'Release'.
# It always defaults to OFF and will always be OFF for 'Release' builds.
cmake_dependent_option(
    CPP_COVERAGE "Compile C++ code with coverage flags" OFF
        "NOT CMAKE_BUILD_TYPE STREQUAL Release" OFF
)


# --- CMake Modules ---
# Insert macros on top of the module path list
set(module_path ${PROJECT_SOURCE_DIR}/cmake/modules)
list(INSERT CMAKE_MODULE_PATH 0 ${module_path})

# Load the macros and execute them here
# (This checks dependencies, enables CMake functions, etc.)
include(UtopiaModelsMacros)


# --- Dependencies based on git submodules
# ... added as SYSTEM includes there
include(GitSubmodules)
include_package_from_submodule(
    NAME UtopiaUtils
    DIRECTORY external/model-building-utils
    RELATIVE_INCLUDE_DIR include
)



# --- Utopia Virtual Environment
# Create symlinks to the Utopia Python virtual environment, if not existing yet
if (NOT    IS_SYMLINK ${PROJECT_BINARY_DIR}/run-in-utopia-env
    OR NOT IS_SYMLINK ${PROJECT_BINARY_DIR}/activate)

    include(CreateSymlink)
    message(STATUS "Creating symlinks to the Utopia Python virtual "
                   "environment located at:\n\t${UTOPIA_ENV_DIR}")
    create_symlink(${RUN_IN_UTOPIA_ENV}
                   ${PROJECT_BINARY_DIR}/run-in-utopia-env)
    create_symlink(${UTOPIA_ENV_DIR}/bin/activate
                   ${PROJECT_BINARY_DIR}/activate)
endif()


# --- Compiler options ---
# add extra flags to debug compiler flags
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall -Wextra")

# support for armadillo high-speed libraries
# TODO Set dynamically, depending on LAPACK and OpenBLAS being installed ...
# NOTE It's unclear whether these are actually faster ...
option(USE_BLAS "Use OpenBLAS and LAPACK for armadillo" $ENV{UTOPIA_USE_BLAS})

if (${USE_BLAS})
    message(STATUS "Configuring armadillo to use LAPACK and OpenBLAS ...\n")
    add_compile_definitions(ARMA_USE_LAPACK ARMA_USE_BLAS)
    set(USING_BLAS True)
endif()

add_feature_info(use_blas USING_BLAS "use OpenBLAS and LAPACK in armadillo")



# --- Extra linking ---
# Depending on how fmt is installed, it MIGHT need to be linked. This cannot
# be easily decided via cmake, so the following workaround uses a cmake option
# to control whether it should be linked explictly.
# The default behaviour depends on an environment variable.
option(LINK_TO_FMT "Explicitly link targets to fmt library" $ENV{UTOPIA_LINK_TO_FMT})

if (${LINK_TO_FMT})
    message(STATUS "Explicitly linking fmt library ...")
    target_link_libraries(Utopia::utopia INTERFACE fmt::fmt)
    set(FMT_LINKED True)
endif()

add_feature_info(
    link2fmt
    FMT_LINKED "explicit linking of fmt library (LINK_TO_FMT option)"
)





# --- Include Config Tree ---

# Enable testing via CTest engine
enable_testing()

# Include subdirectories
add_subdirectory(doc)
add_subdirectory(src/utils)
add_subdirectory(src/models)
add_subdirectory(python)


# --- Custom targets ---
# Add test targets to rule them all
add_custom_target(build_tests_all)
add_dependencies(build_tests_all
                    build_tests_utils
                    build_tests_models)

add_custom_target(test_all)
add_dependencies(test_all
                    test_utils
                    test_models)


# --- Finally, show the feature summary and some status info ---
feature_summary(QUIET_ON_EMPTY
                WHAT REQUIRED_PACKAGES_FOUND
                     OPTIONAL_PACKAGES_FOUND
                     OPTIONAL_PACKAGES_NOT_FOUND
                     ENABLED_FEATURES
                     DISABLED_FEATURES
                     REQUIRED_PACKAGES_NOT_FOUND)

message(STATUS "Currently selected build type:  ${CMAKE_BUILD_TYPE}\n")
