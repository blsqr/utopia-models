# Extensions to `Utopia::Utils`

This module includes some general utilities that are too specific for [`UtopiaUtils`](https://gitlab.com/utopia-project/model-building-utils) yet are independent enough from model specifics such that they can be shared between models.
