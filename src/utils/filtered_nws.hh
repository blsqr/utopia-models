#ifndef UTOPIA_UTILS_FILTERED_NWS_HH
#define UTOPIA_UTILS_FILTERED_NWS_HH

#include <functional>
#include <type_traits>
#include <unordered_map>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/filtered_graph.hpp>

#include "filtered_nw.hh"


namespace Utopia::Utils::NW {


/// Aggregates FilteredNetworks
/** This uses boost::filtered_graph, which has notoriously slow iteration!
  *
  * Additionally allows to dynamically create filtered networks and, if
  * desired, store them into a map. If storing is desired, make sure to provide
  * a reasonable ``MapKeyT``, e.g. an enum.
  *
  * \TODO Consider changing such that it can use both FilteredNetwork and
  *       boost::filtered_graph, controlled by template argument. That way, it
  *       would be easy to benchmark the two against each other ...
  */
template<class NW, class MapKeyT=int>
struct FilteredNetworks {
    /// The vertex descriptor type
    using VertexDesc = typename boost::graph_traits<NW>::vertex_descriptor;

    /// The edge descriptor type
    using EdgeDesc = typename boost::graph_traits<NW>::edge_descriptor;

    /// Vertex predicate type
    using VertexPred = std::function<bool(VertexDesc)>;

    /// Edge predicate type
    using EdgePred = std::function<bool(EdgeDesc)>;

    /// Type of the filtered network; shared by all members of this class
    using FilteredNW = boost::filtered_graph<NW, EdgePred, VertexPred>;

    /// A reference to the underlying network, used for creating new filters
    const NW& base;

    /// A map of filtered networks that can be added dynamically
    std::unordered_map<MapKeyT, FilteredNW> filter_map;

    /// A filtered network without any filters
    const FilteredNW unfiltered;

    /// Construct the FilteredNetworks from an existing network
    FilteredNetworks (const NW& nw)
    :   base(nw)
    ,   filter_map{}
    ,   unfiltered(base, boost::keep_all(), boost::keep_all())
    {}

    /// Access or set one of the mapped filters
    FilteredNW& operator[] (MapKeyT key) const {
        return filter_map.at(key);
    }

    /// Access to members (not in the ``filter_map``)
    /** \note This is comparably slow due to string comparisons; only use it if
      *       there is no other choice.
      */
    virtual const FilteredNW& get (const std::string& name) const {
        if (name == "unfiltered") {
            return unfiltered;
        }
        throw std::invalid_argument(fmt::format(
            "No filtered network member named '{}' available!", name
        ));
    }

    /// Create a new filtered network with the given predicate
    /** If the predicate types are non-convertible, this allows to pass a
      * vertex predicate. Otherwise, only edge predicates are allowed.
      */
    template<class Pred=EdgePred>
    FilteredNW operator() (Pred&& pred) const {
        static_assert(
            std::is_same<Pred, EdgePred>() or std::is_same<Pred, VertexPred>(),
            "Predicate needs to be either an edge or vertex predicate type!"
        );
        static_assert(
            not std::is_convertible<EdgePred, VertexPred>()
            and std::is_same<Pred, EdgePred>(),
            "For convertible predicate types, this method is ambiguous. For "
            "your own safety, use operator()(EdgePred, VertexPred)!"
        );

        if constexpr (std::is_same<Pred, EdgePred>()) {
            return FilteredNW(base, pred);
        }
        else {
            return FilteredNW(base, boost::keep_all(), pred);
        }
    }

    /// Create a new filtered network with the given edge and vertex predicates
    FilteredNW operator() (EdgePred edge_pred, VertexPred vertex_pred) const {
        return FilteredNW(base, std::move(edge_pred), std::move(vertex_pred));
    }

    // TODO add method to directly add a filtered network

    // TODO add predicate map and predicate composer
};

} // namespace Utopia::Utils::NW

#endif // UTOPIA_UTILS_FILTERED_NWS_HH
