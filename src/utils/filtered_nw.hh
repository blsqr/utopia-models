#ifndef UTOPIA_UTILS_FILTERED_NW_HH
#define UTOPIA_UTILS_FILTERED_NW_HH

#include <functional>
#include <type_traits>
#include <unordered_map>

#include <boost/detail/iterator.hpp>
#include <boost/graph/adjacency_iterator.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/filtered_graph.hpp>  // for filtered_graph_tag

#include <UtopiaUtils/utils/nw_filter.hh>
#include <UtopiaUtils/utils/graph_iterators.hh>


namespace Utopia::Utils::NW {

/// An improved version (with limited interface) of boost::filtered_graph
/** Unlike boost::filtered_graph, this uses the NetworkFilter construct to
  * store not only predicates, but also a filtering mode.
  * Furthermore, the iterator overloads do not copy the predicates but work on
  * references, thus making the iteration considerably faster; filtered
  * iteration is notoriously slow for boost::filtered_graph ...
  */
template<class NW>
class FilteredNetwork {
public:
    // .. Exported types ......................................................
    /// This filtered network
    using self = FilteredNetwork<NW>;

    /// The filtering helper type
    using Filter = NetworkFilter<NW>;


    // .. Types for compatibility with BGL . . . . . . . . . . . . . . . . . .

    using Traits = boost::graph_traits<NW>;

    using graph_type = NW;

    using OutEdgePred = typename Filter::EdgePred;
    using InEdgePred = typename Filter::EdgePred;
    using EdgePred = typename Filter::EdgePred;
    using VertexPred = typename Filter::VertexPred;

    // Graph requirements
    using vertex_descriptor = typename Traits::vertex_descriptor;
    using edge_descriptor = typename Traits::edge_descriptor;
    using directed_category = typename Traits::directed_category;
    using edge_parallel_category = typename Traits::edge_parallel_category;
    using traversal_category = typename Traits::traversal_category;

    // IncidenceGraph requirements
    using degree_size_type = typename Traits::degree_size_type ;
    using out_edge_iterator =
        FilteredRange<OutEdgePred, typename Traits::out_edge_iterator>;

    // AdjacencyGraph requirements
    using adjacency_iterator =
        boost::adjacency_iterator<
            self, vertex_descriptor, out_edge_iterator,
            typename boost::detail::iterator_traits<out_edge_iterator>::difference_type
        >;

    // BidirectionalGraph requirements
    using in_edge_iterator =
        FilteredRange<InEdgePred, typename Traits::in_edge_iterator>;
    using inv_adjacency_iterator =
        boost::inv_adjacency_iterator<
            self, vertex_descriptor, in_edge_iterator,
            typename boost::detail::iterator_traits<in_edge_iterator>::difference_type
        >;

    // VertexListGraph requirements
    using vertices_size_type = typename Traits::vertices_size_type;
    using vertex_iterator =
        FilteredRange<VertexPred, typename Traits::vertex_iterator>;

    // EdgeListGraph requirements
    using edges_size_type = typename Traits::edges_size_type;
    using edge_iterator =
        FilteredRange<EdgePred, typename Traits::edge_iterator>;

    using graph_tag = boost::filtered_graph_tag;

    static vertex_descriptor null_vertex() { return Traits::null_vertex(); }

private:
    // .. Members .............................................................
    /// The underlying network
    const NW& _base;

    /// The filter
    const Filter _filter;


public:
    // .. Constructors ........................................................

    FilteredNetwork (const NW& nw, const Filter& filter)
    :   _base(nw)
    ,   _filter(filter)
    {}

    FilteredNetwork (const NW& nw,
                     const EdgePred& edge_pred,
                     const VertexPred& vertex_pred = keep_all,
                     const FilterMode mode = FilterMode::edges_and_vertices)
    :   _base(nw)
    ,   _filter{_base, edge_pred, vertex_pred, mode}
    {}


    // .. Getters .............................................................

    /// The underlying base network
    const NW& base () const { return _base; }

    /// The filters
    const Filter& filter () const { return _filter; }

    /// Shortcut for filter().mode
    FilterMode mode () const { return _filter.mode; }


    // .. Bundled Property Access .............................................

    template<class Descriptor>
    auto& operator[](Descriptor x) {
        return const_cast<NW&>(this->_base)[x];
    }

    template<class Descriptor>
    const auto& operator[](Descriptor x) const {
        return this->_base[x];
    }
};

} // namespace Utopia::Utils::NW


// ============================================================================
// Non-member function overloads for FilteredNetwork

namespace boost {

using Utopia::Utils::NW::FilterMode;
using Utopia::Utils::NW::NetworkFilter;
using Utopia::Utils::NW::FilteredNetwork;
using Utopia::Utils::make_filtered_range;
using Utopia::Utils::FilteredRange;


// -- Factory functions -------------------------------------------------------

template<class NW>
FilteredNetwork<NW>
make_filtered_network (const NW& base, const NetworkFilter<NW>& filter) {
    return FilteredNetwork(base, filter);
}

template<class NW>
FilteredNetwork<NW>
make_filtered_network (
    const NW& base,
    const typename FilteredNetwork<NW>::EdgePred& edge_pred
){
    return FilteredNetwork(base, edge_pred, [](auto){ return true; },
                           FilterMode::edges_only);
}

template<class NW>
FilteredNetwork<NW>
make_filtered_network (
    const NW& base,
    const typename FilteredNetwork<NW>::VertexPred& vertex_pred
){
    return FilteredNetwork(base, [](auto){ return true; }, vertex_pred,
                           FilterMode::vertices_only);
}

template<class NW>
FilteredNetwork<NW>
make_filtered_network (
   const NW& base,
   const typename FilteredNetwork<NW>::EdgePred& edge_pred,
   const typename FilteredNetwork<NW>::VertexPred& vertex_pred,
   const FilterMode mode = FilterMode::edges_and_vertices
){
    return FilteredNetwork(base, edge_pred, vertex_pred, mode);
}


// -- Iterator-pairs returning functions --------------------------------------

/// Iterator pair over all vertices (that match the filter predicate)
template<class NW, class FNW = FilteredNetwork<NW>>
std::pair<typename FNW::vertex_iterator, typename FNW::vertex_iterator>
vertices (const FilteredNetwork<NW>& fnw) {
    auto [v, v_end] = vertices(fnw.base());
    auto frg = make_filtered_range(fnw.filter(),
                                   std::move(v), std::move(v_end));
    return std::make_pair(frg.begin(), frg.end());
}

/// Iterator pair over all edges (that match the filter predicate)
template<class NW, class FNW = FilteredNetwork<NW>>
std::pair<typename FNW::edge_iterator, typename FNW::edge_iterator>
edges (const FilteredNetwork<NW>& fnw) {
    auto [e, e_end] = edges(fnw.base());
    auto frg = make_filtered_range(fnw.filter(),
                                   std::move(e), std::move(e_end));
    return std::make_pair(frg.begin(), frg.end());
}

/// Iterator pair over all out-edges of a certain vertex
template<class NW, class FNW = FilteredNetwork<NW>>
std::pair<typename FNW::out_edge_iterator, typename FNW::out_edge_iterator>
out_edges (typename FNW::vertex_descriptor u, const FilteredNetwork<NW>& fnw) {
    auto [e, e_end] = out_edges(std::move(u), fnw.base());
    auto frg = make_filtered_range(fnw.filter(),
                                   std::move(e), std::move(e_end));
    return std::make_pair(frg.begin(), frg.end());
}

/// Iterator pair over all in-edges of a certain vertex
template<class NW, class FNW = FilteredNetwork<NW>>
std::pair<typename FNW::in_edge_iterator, typename FNW::in_edge_iterator>
in_edges (typename FNW::vertex_descriptor u, const FilteredNetwork<NW>& fnw) {
    auto [e, e_end] = in_edges(std::move(u), fnw.base());
    auto frg = make_filtered_range(fnw.filter(),
                                   std::move(e), std::move(e_end));
    return std::make_pair(frg.begin(), frg.end());
}

/// Iterator pair for all edges between `u` and `v`
// TODO Check if docstring is correct
template<class NW, class FNW = FilteredNetwork<NW>>
std::pair<typename FNW::out_edge_iterator, typename FNW::out_edge_iterator>
edge_range (typename FNW::vertex_descriptor u,
            typename FNW::vertex_descriptor v,
            const FilteredNetwork<NW>& fnw)
{
    auto [e, e_end] = edge_range(std::move(u), std::move(v), fnw.base());
    auto frg = make_filtered_range(fnw.filter(),
                                   std::move(e), std::move(e_end));
    return std::make_pair(frg.begin(), frg.end());
}

/// Iterator pair over all adjacent vertices of an index vertex
/** A vertex is seen as adjacent only if *both* the vertex and edge predicate
  * evaluate to true.
  *
  * This first iterates over the out-edges using the edge filter, uses the
  * adjacency_iterator to resolve the corresponding target vertices, and
  * then filters that iteration using the vertex filter.
  *
  * \warning This function behaves differently than the adjacent_vertices
  *          function for boost::filtered_graph objects!
  */
template<class NW, class FNW = FilteredNetwork<NW>>
auto
adjacent_vertices (typename FNW::vertex_descriptor u,
                   const FilteredNetwork<NW>& fnw)
{
    static_assert(
        std::is_same<typename graph_traits<NW>::traversal_category,
                     bidirectional_graph_tag>(),
        "adjacent_vertices is only tested to work for bidirectionalS!"
    );
    using adjacency_iterator = typename FNW::adjacency_iterator;

    // The edge iteration, filtered by edge predicate
    auto [e, e_end] = boost::out_edges(std::move(u), fnw);

    // The adjacency iterator is now used to transform the iterator range into
    // an iteration over the target vertices of the filtered edges
    auto [v, v_end] =
        std::make_pair(
            adjacency_iterator(e, const_cast<FNW*>(&fnw)),
            adjacency_iterator(e_end, const_cast<FNW*>(&fnw))
        );

    // Now filter using the vertex filter; have to manually construct it here
    auto frg = make_filtered_range(fnw.filter().vertex_filter,
                                   std::move(v), std::move(v_end),
                                   fnw.filter().has_vertex_filter());
    return std::make_pair(frg.begin(), frg.end());
}

/// Iterator pair over all inverse adjacent vertices of an index vertex
/** A vertex is seen as adjacent only if *both* the vertex and edge predicate
  * evaluate to true.
  *
  * This first iterates over the in-edges using the edge filter, uses the
  * inv_adjacency_iterator to resolve the corresponding source vertices, and
  * then filters that iteration using the vertex filter.
  *
  * \warning This function behaves differently than the adjacent_vertices
  *          function for boost::filtered_graph objects!
  */
template<class NW, class FNW = FilteredNetwork<NW>>
auto
inv_adjacent_vertices (typename FNW::vertex_descriptor u,
                       const FilteredNetwork<NW>& fnw)
{
    static_assert(
        std::is_same<typename graph_traits<NW>::traversal_category,
                     bidirectional_graph_tag>(),
        "inv_adjacent_vertices is only tested to work for bidirectionalS!"
    );
    using inv_adjacency_iterator = typename FNW::inv_adjacency_iterator;

    // The edge iteration, filtered by edge predicate
    auto [e, e_end] = boost::in_edges(std::move(u), fnw);

    // The adjacency iterator is now used to transform the iterator range into
    // an iteration over the source vertices of the filtered edges
    auto [v, v_end] =
        std::make_pair(
            inv_adjacency_iterator(e, const_cast<FNW*>(&fnw)),
            inv_adjacency_iterator(e_end, const_cast<FNW*>(&fnw))
        );

    // Now filter using the vertex filter; have to manually construct it here
    auto frg = make_filtered_range(fnw.filter().vertex_filter,
                                   std::move(v), std::move(v_end),
                                   fnw.filter().has_vertex_filter());
    return std::make_pair(frg.begin(), frg.end());
}


// -- Returning descriptors ---------------------------------------------------

/// Given an edge descriptor, return the source vertex descriptor
template<class NW>
typename FilteredNetwork<NW>::vertex_descriptor
source (typename NW::edge_descriptor e, const FilteredNetwork<NW>& fnw) {
    return source(e, fnw.base());
}

/// Given an edge descriptor, return the target vertex descriptor
template<class NW>
typename FilteredNetwork<NW>::vertex_descriptor
target (typename NW::edge_descriptor e, const FilteredNetwork<NW>& fnw) {
    return target(e, fnw.base());
}



// -- Returning scalars -------------------------------------------------------

/// Number of vertices *with* filters applied
/** This is unlike the boost::filtered_graph behaviour, which returns the
  * number of vertices of the underlying graph instead.
  */
template<class NW>
typename FilteredNetwork<NW>::vertices_size_type
num_vertices (const FilteredNetwork<NW>& fnw) {
    if (not fnw.filter().has_vertex_filter()) {
        return num_vertices(fnw.base());
    }
    auto [v, v_end] = vertices(fnw);
    return std::distance(v, v_end);
}

/// Number of edges *with* filters applied
/** This is unlike the boost::filtered_graph behaviour, which returns the
  * number of edges of the underlying graph instead.
  */
template<class NW>
typename FilteredNetwork<NW>::edges_size_type
num_edges (const FilteredNetwork<NW>& fnw) {
    if (not fnw.filter().has_edge_filter()) {
        return num_edges(fnw.base());
    }
    auto [e, e_end] = edges(fnw);
    return std::distance(e, e_end);
}

// template < typename G, typename EP, typename VP >
// typename filtered_graph< G, EP, VP >::degree_size_type out_degree(
//     typename filtered_graph< G, EP, VP >::vertex_descriptor u,
//     const filtered_graph< G, EP, VP >& g)
// {
//     typename filtered_graph< G, EP, VP >::degree_size_type n = 0;
//     typename filtered_graph< G, EP, VP >::out_edge_iterator f, l;
//     for (boost::tie(f, l) = out_edges(u, g); f != l; ++f)
//         ++n;
//     return n;
// }

// template < typename G, typename EP, typename VP >
// typename filtered_graph< G, EP, VP >::degree_size_type in_degree(
//     typename filtered_graph< G, EP, VP >::vertex_descriptor u,
//     const filtered_graph< G, EP, VP >& g)
// {
//     typename filtered_graph< G, EP, VP >::degree_size_type n = 0;
//     typename filtered_graph< G, EP, VP >::in_edge_iterator f, l;
//     for (boost::tie(f, l) = in_edges(u, g); f != l; ++f)
//         ++n;
//     return n;
// }

// template < typename G, typename EP, typename VP >
// typename enable_if< typename is_directed_graph< G >::type,
//     typename filtered_graph< G, EP, VP >::degree_size_type >::type
// degree(typename filtered_graph< G, EP, VP >::vertex_descriptor u,
//     const filtered_graph< G, EP, VP >& g)
// {
//     return out_degree(u, g) + in_degree(u, g);
// }

// template < typename G, typename EP, typename VP >
// typename disable_if< typename is_directed_graph< G >::type,
//     typename filtered_graph< G, EP, VP >::degree_size_type >::type
// degree(typename filtered_graph< G, EP, VP >::vertex_descriptor u,
//     const filtered_graph< G, EP, VP >& g)
// {
//     return out_degree(u, g);
// }

// template < typename G, typename EP, typename VP >
// std::pair< typename filtered_graph< G, EP, VP >::edge_descriptor, bool > edge(
//     typename filtered_graph< G, EP, VP >::vertex_descriptor u,
//     typename filtered_graph< G, EP, VP >::vertex_descriptor v,
//     const filtered_graph< G, EP, VP >& g)
// {
//     typename graph_traits< G >::edge_descriptor e;
//     bool exists;
//     boost::tie(e, exists) = edge(u, v, g.m_g);
//     return std::make_pair(e, exists && g.m_edge_pred(e));
// }




} // namespace boost

#endif // UTOPIA_UTILS_FILTERED_NW_HH
