#ifndef UTOPIA_MODELS_COPYMEESSENTIALS_UTILS_HH
#define UTOPIA_MODELS_COPYMEESSENTIALS_UTILS_HH

// Include selected utilities
#include <UtopiaUtils/utils/cfg.hh>
#include <UtopiaUtils/utils/formatters.hh>
#include <UtopiaUtils/utils/rng.hh>
#include <UtopiaUtils/utils/tags.hh>
#include <UtopiaUtils/utils/types.hh>
#include <UtopiaUtils/utils/math.hh>

// Include _all_ CopyMeEssentials model-specific utilities
#include "utils/formatters.hh"
#include "utils/yaml.hh"


namespace Utopia::Models::CopyMeEssentials {

using namespace Utopia::Utils;

// -- Various aliases ---------------------------------------------------------

/// Floating point precision to use in this model
using Utopia::Utils::PREC;

}

#endif // UTOPIA_MODELS_COPYMEESSENTIALS_UTILS_HH
