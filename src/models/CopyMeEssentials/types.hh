#ifndef UTOPIA_MODELS_COPYMEESSENTIALS_TYPES_HH
#define UTOPIA_MODELS_COPYMEESSENTIALS_TYPES_HH

#include <memory>
#include <vector>
#include <tuple>
#include <type_traits>

#include <armadillo>

#include <utopia/core/types.hh>
#include <UtopiaUtils/utils/rng.hh>


namespace Utopia::Models::CopyMeEssentials {

/// The RNG type to use: the default RNG from the Utopia::Utils module
using ModelRNG = Utopia::Utils::DefaultRNG;



// -- General type definitions ------------------------------------------------

/// The configuration type used throughout this model
using Config = Utopia::DataIO::Config;

/// Type of the ID used for identification of entities
using IDType = unsigned int;

/// Container holding shared pointers to some object
template<class T>
using PtrContainer = std::vector<std::shared_ptr<T>>;


// -- Enums & Tags ------------------------------------------------------------



// -- Forward Declarations ----------------------------------------------------



// -- Misc --------------------------------------------------------------------


} // namespace Utopia::Models::CopyMeEssentials

#endif // UTOPIA_MODELS_COPYMEESSENTIALS_TYPES_HH
