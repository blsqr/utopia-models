# Add EEcoy model tests.
# They will be available as a custom target:  test_model_CopyMeEssentials

add_model_tests(# Use consistent capitalization for the model name!
                MODEL_NAME CopyMeEssentials
                # The sources of the model tests to carry out. Each of these
                # will become a test target with the same name.
                SOURCES
                    "test_basics.cc"
                # Optional: Files to be copied to the build directory
                AUX_FILES
                    "basics.yml"
                )
