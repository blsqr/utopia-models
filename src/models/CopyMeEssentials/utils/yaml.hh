#ifndef UTOPIA_MODELS_COPYMEESSENTIALS_UTILS_YAML_HH
#define UTOPIA_MODELS_COPYMEESSENTIALS_UTILS_YAML_HH

#include <yaml-cpp/yaml.h>

#include "../types.hh"


namespace YAML {

/** Can put YAML conversions for custom types here.
  * See: https://github.com/jbeder/yaml-cpp/wiki/Tutorial#converting-tofrom-native-data-types
  * or other models for more information.
  */


} // namespace YAML



namespace Utopia::Models::CopyMeEssentials {

/** Can put other YAML-related tools or conversions here. */


} // namespace Utopia::Models::CopyMeEssentials

#endif // UTOPIA_MODELS_COPYMEESSENTIALS_UTILS_YAML_HH
