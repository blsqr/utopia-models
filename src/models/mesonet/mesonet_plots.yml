# Default plots for the mesonet model
#
# These make heavy use of multiple inheritance and yaml features to ideally
# configure all variables in one place. Have a look at the base configuration.
---
# Shared definitions ..........................................................
# Toggles, determining which plots are being carried out
#
# NOTE Basically ALL plots need to be migrated to DAG-based plot functions.
#
_toggles:
  default:            &_default               false  # true
  specific:           &_specific              false
  generic:            &_generic               false
  experimental:       &_experimental          false

  uni:                &_uni                   false  # true
  mv:                 &_mv                    false
  mv_adaptive:        &_mv_adaptive           false  # true

  res_pri:            &_res_pri               false  # not implemented
  res_sec:            &_res_sec               false  # true
  consumer:           &_consumer              false  # true
  network:            &_network               false  # true

  # TODO Consider evaluating them here?

# Sweep values for generic plots
_generic_sweep_values:
  res_primary: &pdim_res_primary
    name: field
    default: energy
    values:
      - energy
      - energy_out
      - scaling_factor
      - num_outflows

  res_secondary: &pdim_res_secondary
    name: field
    default: energy
    values:
      - energy
      - energy_in
      - energy_out
      - scaling_factor
      - num_outflows

  consumer: &pdim_consumer
    name: field
    default: energy
    values:
      - energy
      - energy_in
      - sizes
      - count
      - num_innovations_primary
      - num_innovations_secondary
      - cc_frac


# Label values
_labels:
  energy:             &label_energy           Energy [a.u.]
  size:               &label_size             Size [-]
  cc_frac:            &label_cc_frac          Carrying Capacity Fraction [-]
  scaling_factor:     &label_scaling_factor   Scaling Factor [-]
  num_outflows:       &label_num_outflows     Number of Outflows [-]


_aesthetics:
  hvline: &style_hvline
    linestyle: solid
    color: grey
    alpha: .4
    linewidth: 3.
    zorder: -42


# -----------------------------------------------------------------------------
# Generic plots ---------------------------------------------------------------
# -----------------------------------------------------------------------------
# ... just to get an overview

generic/res_primary: !pspace &plt_generic
  enabled: !all [*_generic, *_uni, *_res_pri]
  based_on: [uni.generic, hlpr.limits.y_from_zero]

  process:
    base_path: data/mesonet/res_primary
    select_fields:
      data:
        path: !sweep
          <<: *pdim_res_primary

  helpers:
    set_labels:
      y: !coupled-sweep
        target_name: field
    set_title:
      title: !coupled-sweep
        target_name: field


generic/res_secondary: !pspace
  <<: *plt_generic
  enabled: !all [*_generic, *_uni, *_res_sec]

  process:
    base_path: data/mesonet/res_secondary
    select_fields:
      data:
        path: !sweep
          <<: *pdim_res_secondary


generic/consumer: !pspace
  <<: *plt_generic
  enabled: !all [*_generic, *_uni, *_consumer]

  process:
    base_path: data/mesonet/consumer
    select_fields:
      data:
        path: !sweep
          <<: *pdim_consumer


# -----------------------------------------------------------------------------
# Secondary resources ---------------------------------------------------------
# -----------------------------------------------------------------------------
# Universe plots ..............................................................
res_secondary/energy: &plt_res_secondary_energy
  enabled: !any
    - *_default
    - !all [*_specific, *_uni, *_res_sec]
  based_on: [uni.via_xr, kind.time_series, kind.uni.plot_sum_over_id]

  process:
    select_fields:
      data:
        path: res_secondary/energy

  helpers:
    set_labels:
      y: *label_energy


res_secondary/energy_in:
  <<: *plt_res_secondary_energy

  process:
    select_fields:
      data:
        path: res_secondary/energy_in


res_secondary/energy_out:
  <<: *plt_res_secondary_energy

  process:
    select_fields:
      data:
        path: res_secondary/energy_out


res_secondary/scaling_factor:
  enabled: !any
    - *_default
    - !all [*_specific, *_uni, *_res_sec]
  based_on: [uni.via_xr, kind.time_series]

  process:
    select_fields:
      data:
        path: res_secondary/scaling_factor
        transform:
          - sel: {}

  helpers:
    set_labels:
      y: *label_scaling_factor

    set_limits:
      y: [0., 1.1]

    set_hv_lines:
      hlines:
        - pos: 1.
          <<: *style_hvline


res_secondary/num_outflows:
  enabled: !any
    - *_default
    - !all [*_specific, *_uni, *_res_sec]
  based_on: [uni.via_xr, kind.time_series]

  process:
    select_fields:
      data:
        path: res_secondary/num_outflows
        transform:
          - sel: {}

  helpers:
    set_labels:
      y: *label_num_outflows

    set_limits:
      y: [-0.2, ~]




# Multiverse plots ............................................................
res_secondary/total_energy:
  enabled: !any
    - *_default
    - !all [*_specific, *_mv_adaptive, *_res_sec]
  based_on: [mv.via_xr]

  select:
    fields:
      total_energy:
        path: res_secondary/energy
        transform:
          - sel: {}
          - sum: id




# -----------------------------------------------------------------------------
# Consumers -------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Universe plots ..............................................................
consumer/energy:
  enabled: !any
    - *_default
    - !all [*_specific, *_uni, *_consumer]
  based_on: [uni.via_xr, kind.time_series, kind.uni.plot_sum_over_id]

  process:
    select_fields:
      data:
        path: consumer/energy

  helpers:
    set_labels:
      y: *label_energy


consumer/sizes:
  enabled: !any
    - *_default
    - !all [*_specific, *_uni, *_consumer]
  based_on: [uni.via_xr, kind.time_series, kind.uni.plot_sum_over_id]

  process:
    select_fields:
      data:
        path: consumer/sizes

  helpers:
    set_labels:
      y: *label_size


consumer/cc_frac:
  enabled: !any
    - *_default
    - !all [*_specific, *_uni, *_consumer]
  based_on: [uni.via_xr, kind.time_series]

  process:
    select_fields:
      data:
        path: consumer/cc_frac
        transform:
          - sel: {}

  helpers:
    set_labels:
      y: *label_cc_frac

    set_hv_lines:
      hlines:
        - pos: 1.
          <<: *style_hvline


# Multiverse plots ............................................................
consumer/total_energy:
  enabled: !any
    - *_default
    - !all [*_specific, *_mv_adaptive, *_consumer]
  based_on: [mv.via_xr]

  select:
    fields:
      total_energy:
        path: consumer/energy
        transform:
          - sel: {}
          - sum: id

  helpers:
    set_title:
      title: Total Energy


consumer/num_alive:
  enabled: !any
    - *_default
    - !all [*_specific, *_mv_adaptive, *_consumer]
  based_on: [mv.via_xr]

  select:
    fields:
      num_alive:
        path: consumer/count
        transform:
          - sel: {}

  helpers:
    set_title:
      title: "# Consumer Species Alive"



# -----------------------------------------------------------------------------
# Networks --------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Universe plots ..............................................................
ia_network/snapshot:
  enabled: !any
    - *_default
    - !all [*_specific, *_uni, *_network]
  based_on: [uni.network.interaction]

genealogy/snapshot:
  enabled: !all [*_specific, *_uni, *_network, *_experimental]
  based_on: [uni.network.genealogy]
