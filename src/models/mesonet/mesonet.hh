#ifndef UTOPIA_MODELS_MESONET_HH
#define UTOPIA_MODELS_MESONET_HH

#include <sstream>
#include <random>
#include <algorithm>

#include <yaml-cpp/yaml.h>

#include <utopia/core/model.hh>
#include <UtopiaUtils/data_io/data_writer.hh>

#include "cria.hh"


namespace Utopia::Models::mesonet {


// ++ Type definitions ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Typehelper to define types of mesonet model
using mesonetTypes = ModelTypes<>;


// ++ Model definition ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// The mesonet model class
class mesonet: public Model<mesonet, mesonetTypes> {
public:
    /// Base model type
    using Base = Model<mesonet, mesonetTypes>;

    /// Export Base model's time type
    using Time = typename Base::Time;

    /// Mesonet data writer type
    using DataWriter = Utopia::DataIO::DataWriter::DataWriter;

private:
    // -- Members -------------------------------------------------------------
    /// The interaction network
    ConsumerResourceIA<Base::RNG> _cria;

    /// The data manager for all output data
    DataWriter _dw;

    /// Uniform real distribution for evaluating probabilities
    std::uniform_real_distribution<double> _prob_distr;

    /// Flag: Is set when a clean up of the containers is required
    bool _need_cleanup;


public:
    // -- Model Setup ---------------------------------------------------------
    /// Construct the mesonet model
    /** \param name     Name of this model instance
     *  \param parent   The parent model this model instance resides in
     */
    template<class ParentModel>
    mesonet (const std::string name, ParentModel& parent)
    :
        // Initialize first via base model
        Base(name, parent),

        // Initialize the consumer resource interaction object
        _cria(this->_cfg["setup"], this->_log, this->_rng),

        // The data writer instance to use
        _dw(*this),

        // Statistical distributions
        _prob_distr(0., 1.),

        // Temporary variables
        _need_cleanup(false)

    {
        register_properties();
        this->_log->info("{} is all set up now :)", this->_name);
    }

private:
    // .. Helper methods ......................................................


public:
    // -- Public Interface ----------------------------------------------------
    // .. Simulation Control ..................................................

    /// Iterate a single step
    void perform_step () {
        this->_log->debug("Beginning iteration step ...");

        // Might need some cleaning up ...
        if (_need_cleanup) {
            this->_log->debug("Performing cleanup operations ...");
            _cria.remove_deactivated_flows();
            _cria.remove_deceased_consumers();
            this->_log->debug("Cleanup finished.");

            // Set flags: With cleanup done, the ID map is no longer up to date
            _need_cleanup = false;
            _dw.mark_mapping_invalidated("consumer/ids");
        }

        // Prepare consumers for energy propagation
        _cria.prepare_consumers();

        // Refill secondary resources' energy reservoir
        _cria.refill_secondary_resources();

        // Calculate the energy that will flow out of the resources
        _cria.propagate_energy_from_resources();

        this->_log->debug("Commencing consumer dynamics on {} entities ...",
                          _cria.consumers.size());
        // Go over all consumers and perform their population dynamics
        _cria.consumer_population_dynamics();

        // Carry out the splitting; returns true if splitting occured
        if (_cria.split_consumers()) {
            // This has extended _cria.consumers container, which invalidates
            // the id mapping. Cannot decide on cleanup yet, because this does
            // not say anything about agents deceasing.
            _dw.mark_mapping_invalidated("consumer/ids");
        }

        // Let consumers gain innovations and then expend their energy
        for (auto& consumer : _cria.consumers) {
            // Allow the consumer to gain an innovation; managed by CRIA
            if (_prob_distr(*this->_rng) < consumer->p_innovation) {
                _cria.gain_innovation(consumer);
            }

            // Expend the energy needed to survive
            consumer->expend_energy(this->_rng);

            // Check if the above led to the consumer being deceased; will
            // trigger clean up at the beginning of the next time step
            if (consumer->is_deceased()) {
                _need_cleanup = true;
            }
        }

        this->_log->debug("Consumer dynamics finished.");
    }


    /// Supply monitoring values
    /** Keys provided by this monitor:
      *    - num_consumers
      *    - num_innovations_primary
      *    - num_innovations_secondary
      */
    void monitor () {
        this->_monitor.set_entry("num_consumers", _cria.consumers.size());
        this->_monitor.set_entry("num_innovations_primary",
                                 _cria.num_innovations<PrimaryResource>());
        this->_monitor.set_entry("num_innovations_secondary",
                                 _cria.num_innovations<SecondaryResource>());
    }


    /// Write data
    /** As the number of alive consumer differs, this work is carried out by
      * the DataWriter.
      * It does so by taking into account the time of the last ID change and
      * only creates a new ID map if the dataset size changed.
      */
    void write_data () {
        // Invoke the data writer
        _dw.write(this->get_time());
    }


    // Getters and setters ....................................................
    // Add getters and setters here to interface with other model


private:
    // .. Property registration ...............................................

    /// Adds all conceivable output properties to the data manager
    void register_properties() {
        using namespace Utopia::DataIO::DataWriter;

        this->_log->info("Registering properties ...");

        // .. Re-usable adaptor functions . . . . . . . . . . . . . . . . . . .
        const auto adptr_id =
            [](const auto& r){ return r->id; };
        const auto adptr_E =
            [](const auto& r){ return r->get_energy(); };
        const auto adptr_Ein =
            [](const auto& r){ return r->get_energy_in(); };
        const auto adptr_Eout =
            [](const auto& r){ return r->get_energy_out(); };
        const auto adptr_scaling =
            [](const auto& r){ return r->get_scaling_factor(); };
        const auto adptr_num_outflows =
            [](const auto& r){
                return static_cast<unsigned short>(r->num_outflows());
            };
        const auto adptr_props =
            [](const auto& r){
                // Get (a clone) of the property node, including config info
                auto props = r->get_properties();
                props.SetStyle(YAML::EmitterStyle::Flow);
                // NOTE Using flow style as it can represent the data with
                //      shorter strings. yamlcpp API is marked "might change"!

                // Write to string stream
                std::stringstream s;
                s << props;
                return s.str();
            };
        const auto adptr_props_full =  // Like above but with full detail
            [](const auto& r){
                auto props = r->template get_properties<Detail::Kind::full>();
                props.SetStyle(YAML::EmitterStyle::Flow);

                std::stringstream s;
                s << props;
                return s.str();
            };
        const auto adptr_topology =  // Like above but only topology
            [](const auto& r){
                auto props = r->template get_properties<Detail::Kind::topology,
                                                        Detail::Mode::exact>();
                props.SetStyle(YAML::EmitterStyle::Flow);

                std::stringstream s;
                s << props;
                return s.str();
            };

        // Helper to build the write function lambda for container iteration
        const auto write_from_container =
            [&](const auto& cont, const auto& adptr) {
                return
                    [&](const auto& dset){
                        dset->write(cont.begin(), cont.end(), adptr);
                    };
            };

        // .. Max extent calculation . . . . . . . . . . . . . . . . . . . . .
        // 1D Max extent calculation functions
        const auto max_extent_1d_res_primary =
            [&, this](const auto&) -> std::vector<hsize_t> {
                return {this->_cria.res_primary.size()};
            };
        const auto max_extent_1d_res_secondary =
            [&, this](const auto&) -> std::vector<hsize_t> {
                return {this->_cria.res_secondary.size()};
            };
        const auto max_extent_1d_consumers =
            [&, this](const auto&) -> std::vector<hsize_t> {
                return {this->_cria.consumers.size()};
            };

        // 2D Max extent calculation functions
        const auto max_extent_2d_res_primary =
            [&, this](const auto& num_write_ops) -> std::vector<hsize_t> {
                return {num_write_ops, this->_cria.res_primary.size()};
            };
        const auto max_extent_2d_res_secondary =
            [&, this](const auto& num_write_ops) -> std::vector<hsize_t> {
                return {num_write_ops, this->_cria.res_secondary.size()};
            };
        const auto max_extent_2d_consumers =
            [&, this](const auto& num_write_ops) -> std::vector<hsize_t> {
                return {num_write_ops, this->_cria.consumers.size()};
            };


        // Now, register all the properties for the three groups.
        // NOTE As neither resources nor consumers can emerge spontaneously,
        //      the registration is only activated if at least one entity of
        //      the respective group is available at this point.

        // .. Primary Resources . . . . . . . . . . . . . . . . . . . . . . . .
        if (this->_cria.res_primary.size() > 0) {

            // Register an ID mapping
            _dw.register_property("res_primary/ids",
                // The mode of this property
                PropMode::mapping,
                // The write function, here generated by a helper function
                write_from_container(this->_cria.res_primary, adptr_id),
                // The (required) max extent function, 1D: {size of container}
                max_extent_1d_res_primary
                // Optional: lambda to set additional attributes
                // Optional: lambda to calculate chunk sizes; not given -> auto
            );

            // Register a single-write property attached to an ID mapping
            _dw.register_property("res_primary/properties",
                // The mode and the mapping property this is attached to
                PropMode::attached_to_mapping, "res_primary/ids",
                write_from_container(this->_cria.res_primary, adptr_props),
                max_extent_1d_res_primary,
                // Pass custom attributes function, denoting it as yaml data
                set_attrs_contains_yaml_data
            );

            // Register a standalone dataset
            _dw.register_property("res_primary/count",
                PropMode::standalone,
                // Define a write function ad hoc (need no container iteration)
                [this](const auto& dset){
                    dset->write(
                        static_cast<unsigned short>(
                            this->_cria.res_primary.size()
                        )
                    );
                },
                // 1D shape: {num_write_operations}
                shape_1d
            );

            // Register a mapped dataset, using indices from an ID mapping
            _dw.register_property("res_primary/energy",
                // In mapped mode, specify the name of the associated ID map
                PropMode::mapped, "res_primary/ids",
                write_from_container(this->_cria.res_primary, adptr_E),
                // The required max extent function, 2D: {num writes, size}
                max_extent_2d_res_primary
            );

            _dw.register_property("res_primary/energy_out",
                PropMode::mapped, "res_primary/ids",
                write_from_container(this->_cria.res_primary, adptr_Eout),
                max_extent_2d_res_primary
            );

            _dw.register_property("res_primary/scaling_factor",
                PropMode::mapped, "res_primary/ids",
                write_from_container(this->_cria.res_primary, adptr_scaling),
                max_extent_2d_res_primary
            );

            _dw.register_property("res_primary/num_outflows",
                PropMode::mapped, "res_primary/ids",
                write_from_container(this->_cria.res_primary,
                                     adptr_num_outflows),
                max_extent_2d_res_primary
            );
        }


        // .. Secondary Resources . . . . . . . . . . . . . . . . . . . . . . .
        if (this->_cria.res_secondary.size() > 0) {

            _dw.register_property("res_secondary/ids",
                PropMode::mapping,
                write_from_container(this->_cria.res_secondary, adptr_id),
                max_extent_1d_res_secondary
            );

            _dw.register_property("res_secondary/properties",
                PropMode::attached_to_mapping, "res_secondary/ids",
                write_from_container(this->_cria.res_secondary, adptr_props),
                max_extent_1d_res_secondary,
                set_attrs_contains_yaml_data
            );

            _dw.register_property("res_secondary/count",
                PropMode::standalone,
                [this](const auto& dset){
                    dset->write(
                        static_cast<unsigned short>(
                            this->_cria.res_secondary.size()
                        )
                    );
                },
                shape_1d
            );

            _dw.register_property("res_secondary/energy",
                PropMode::mapped, "res_secondary/ids",
                write_from_container(this->_cria.res_secondary, adptr_E),
                max_extent_2d_res_secondary
            );

            _dw.register_property("res_secondary/energy_in",
                PropMode::mapped, "res_secondary/ids",
                write_from_container(this->_cria.res_secondary, adptr_Ein),
                max_extent_2d_res_secondary
            );

            _dw.register_property("res_secondary/energy_out",
                PropMode::mapped, "res_secondary/ids",
                write_from_container(this->_cria.res_secondary, adptr_Eout),
                max_extent_2d_res_secondary
            );

            _dw.register_property("res_secondary/scaling_factor",
                PropMode::mapped, "res_secondary/ids",
                write_from_container(this->_cria.res_secondary, adptr_scaling),
                max_extent_2d_res_secondary
            );

            _dw.register_property("res_secondary/num_outflows",
                PropMode::mapped, "res_secondary/ids",
                write_from_container(this->_cria.res_secondary,
                                     adptr_num_outflows),
                max_extent_2d_res_secondary
            );
        }


        // .. Consumer . . . . . . . . . . . . . . . . . . . . . . . . . . . .
        if (this->_cria.consumers.size() > 0) {

            _dw.register_property("consumer/ids",
                PropMode::mapping,
                write_from_container(this->_cria.consumers, adptr_id),
                max_extent_1d_consumers
            );

            _dw.register_property("consumer/properties",
                PropMode::attached_to_mapping, "consumer/ids",
                write_from_container(this->_cria.consumers, adptr_props),
                max_extent_1d_consumers,
                set_attrs_contains_yaml_data
            );

            _dw.register_property("consumer/properties_full",
                PropMode::attached_to_mapping, "consumer/ids",
                write_from_container(this->_cria.consumers, adptr_props_full),
                max_extent_1d_consumers,
                set_attrs_contains_yaml_data
            );

            _dw.register_property("consumer/topology",
                PropMode::mapped, "consumer/ids",
                write_from_container(this->_cria.consumers, adptr_topology),
                max_extent_2d_consumers,
                set_attrs_contains_yaml_data
            );

            _dw.register_property("consumer/parents",
                PropMode::attached_to_mapping, "consumer/ids",
                [this](const auto& dset){
                    dset->write(this->_cria.parents);
                },
                shape_0d,
                // Custom dataset attributes function
                [](const auto& dset){
                    // As no default attributes are written, write all manually
                    dset->add_attribute("dim_name__0", "id");
                    dset->add_attribute("coords_mode__id", "start_and_step");
                    dset->add_attribute("coords__id",
                                        std::array<IDType, 2>{1, 1});
                },
                false, // disables writing of default dataset attributes
                // Custom group attributes function
                [](const auto& grp){
                    grp->add_attribute("content", "time_series");
                }
            );

            _dw.register_property("consumer/count",
                PropMode::standalone,
                [this](const auto& dset){
                    dset->write(
                        static_cast<unsigned short>(
                            this->_cria.consumers.size()
                        )
                    );
                },
                shape_1d
            );

            _dw.register_property("consumer/energy",
                PropMode::mapped, "consumer/ids",
                write_from_container(this->_cria.consumers, adptr_E),
                max_extent_2d_consumers
            );

            _dw.register_property("consumer/energy_in",
                PropMode::mapped, "consumer/ids",
                write_from_container(this->_cria.consumers, adptr_Ein),
                max_extent_2d_consumers
            );

            _dw.register_property("consumer/sizes",
                PropMode::mapped, "consumer/ids",
                write_from_container(this->_cria.consumers,
                    // Define ad hoc adaptor
                    [](const auto& c){
                        return c->get_size();
                    }),
                max_extent_2d_consumers
            );

            _dw.register_property("consumer/alive",
                PropMode::mapped, "consumer/ids",
                write_from_container(this->_cria.consumers,
                    [](const auto& c){
                        return static_cast<char>(not c->is_deceased());
                    }),
                max_extent_2d_consumers
            );

            _dw.register_property("consumer/cc_frac",
                PropMode::mapped, "consumer/ids",
                write_from_container(this->_cria.consumers,
                    [](const auto& c){
                        return c->get_K() / c->get_size();
                    }),
                max_extent_2d_consumers
            );

            _dw.register_property("consumer/num_innovations_primary",
                PropMode::mapped, "consumer/ids",
                write_from_container(this->_cria.consumers,
                    [](const auto& c){
                        return static_cast<unsigned short>(
                            c->num_innovations_primary()
                        );
                    }),
                max_extent_2d_consumers
            );

            _dw.register_property("consumer/num_innovations_secondary",
                PropMode::mapped, "consumer/ids",
                write_from_container(this->_cria.consumers,
                    [](const auto& c){
                        return static_cast<unsigned short>(
                            c->num_innovations_secondary()
                        );
                    }),
                max_extent_2d_consumers
            );
        }

        // .. Finished. . . . . . . . . . . . . . . . . . . . . . . . . . . . .
        this->_log->info("Property registration finished.");
    }
};


} // namespace Utopia::Models::mesonet

#endif // UTOPIA_MODELS_MESONET_HH
