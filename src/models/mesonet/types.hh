#ifndef UTOPIA_MODELS_MESONET_TYPES_HH
#define UTOPIA_MODELS_MESONET_TYPES_HH

#include <memory>
#include <vector>

#include <utopia/data_io/cfg_utils.hh>


namespace Utopia::Models::mesonet {

// Make Config type available in this namespace
using Config = Utopia::DataIO::Config;

/// Type of the ID used for identification of resources
using IDType = unsigned int; // TODO consider choosing longer?

/// Type of consumer population size
using SizeType = unsigned int;

/// Container to use
template<typename T>
using ContType = std::vector<T>;

/// Container of strong pointers
template<typename T>
using PtrCont = ContType<std::shared_ptr<T>>;

/// Container of weak pointers
template<typename T>
using WkPtrCont = ContType<std::weak_ptr<T>>;

/// Forward declaration of ConsumerResourceIA
/** \note Needed because classes in namespace ::Resources can otherwise not
  *       declare this as friend, because template friend forward declarations
  *       do not allow friends from other namespaces, even if the namespace is
  *       an outer namespace.
  */
template<class RNGType>
class ConsumerResourceIA;


} // namespace Utopia::Models::mesonet

#endif // UTOPIA_MODELS_MESONET_TYPES_HH
