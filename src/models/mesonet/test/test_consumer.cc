#define BOOST_TEST_MODULE test consumer
#include <boost/test/unit_test.hpp>

#include <utopia/data_io/cfg_utils.hh>

#include "../resources/consumer.hh"


// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using Utopia::DataIO::Config;
using namespace Utopia::Models::mesonet;

/// A fixture that provides the information necessary to setup consumers
/** \detail All members of this are available in the test cases.
  */
struct ConsumerSetupEnv {
    Config cfg;

    ConsumerSetupEnv () :
        cfg(YAML::LoadFile("consumer.yml"))
    {}
};

// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Test the apply_noise_to_value function
BOOST_FIXTURE_TEST_CASE (test_consumer_setup, ConsumerSetupEnv)
{
    // TODO
}
