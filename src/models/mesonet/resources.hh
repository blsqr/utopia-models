#ifndef UTOPIA_MODELS_MESONET_RESOURCES_HH
#define UTOPIA_MODELS_MESONET_RESOURCES_HH

#include <string_view>

#include "resources/base.hh"
#include "resources/primary.hh"
#include "resources/secondary.hh"
#include "resources/consumer.hh"

namespace Utopia {
namespace Models {
namespace mesonet {

// -- Specializations ---------------------------------------------------------
// .. Resources ...............................................................
/// Consumer type
using Consumer = Resources::Consumer;

/// Primary resource type
using PrimaryResource = Resources::PrimaryResource<Consumer>;

/// Secondary resource type
using SecondaryResource = Resources::SecondaryResource<Consumer>;

/// Energy flows from primary resources
using FlowPrimary = Flow<PrimaryResource, Consumer>;

/// Energy flows from secondary resources
using FlowSecondary = Flow<SecondaryResource, Consumer>;


} // namespace mesonet
} // namespace Models
} // namespace Utopia

#endif // UTOPIA_MODELS_MESONET_RESOURCES_HH
