#ifndef UTOPIA_MODELS_MESONET_RESOURCES_FLOW_HH
#define UTOPIA_MODELS_MESONET_RESOURCES_FLOW_HH

#include <string_view>
#include <cmath>

#include <yaml-cpp/yaml.h>

#include "types.hh"
#include "utils.hh"

namespace Utopia::Models::mesonet {


/// Represents a connection and the energy flow from a source to a target
/** \detail It is a directed object, holding strong references in the direction
  *         of flow and weak references in the reverse direction.
  */
template<class SourceType, class TargetType>
class Flow {
public:
    // .. Types ...............................................................
    /// Type of the source of this flow
    using Source = SourceType;

    /// Type of the target of this flow
    using Target = TargetType;


private:
    // .. Members .............................................................
    /// Whether the flow is actually active
    bool active;

    /// The ID of the next member of this class; counted up in constructor
    static IDType next_id;

public:
    /// ID of this object
    const IDType id;

    /// A representation of the properties of this object at construction
    /** \detail This is set at construction. It is NOT automatically
      *         updated when parameters change. Refer to ::get_properties for
      *         more info.
      */
    const Config cfg;

    /// Weak pointer to source object
    const std::weak_ptr<Source> source;

    /// Strong pointer to target object
    const std::shared_ptr<Target> target;

    /// Maximum flow parameter
    double max_flow;

    /// Efficacy parameter
    double efficacy;

private:
    /// Energy flowing over this edge in the current time step
    double deltaE;


public:
    /// Set up a Flow using const-referenced shared pointers
    template<class RNG>
    Flow (const std::shared_ptr<Source>& src,
          const std::shared_ptr<Target>& trgt,
          const Config& init_cfg,
          const std::shared_ptr<RNG>& rng)
    :
        active(true),
        id(next_id++),
        cfg(apply_noise(init_cfg, rng)),
        source(src),
        target(trgt),
        max_flow(get_as<double>("max_flow", cfg)),
        efficacy(get_as<double>("efficacy", cfg)),
        deltaE(std::numeric_limits<double>::quiet_NaN())
    {}

    /// Whether the flow is currently activated
    bool is_active() const {
        return active;
    }

    /// Deactivate this flow; cannot be re-activated
    void deactivate() {
        active = false;
    }

    // .. Getters .............................................................

    /// Get the current energy flow value
    double get_flow() const {
        return deltaE;
    }

    IDType get_source_id() const {
        if (auto src = source.lock()) {
            return src->id;
        }
        else {
            throw std::runtime_error("Source was already destructed!");
        }
    }

    IDType get_target_id() const {
        return target->id;
    }

    /// Get the config node representing the properties of this object
    /** \ref See ::Resource::get_properties
      */
    template<Detail::Kind lvl = Detail::Kind::minimal,
             Detail::Mode mode = Detail::Mode::include_below>
    Config get_properties() const {
        using Kind = Detail::Kind;

        // Work on a deep copy of the initial configuration. This allows the
        // returned node to be changed without side effecs.
        Config props = YAML::Clone(cfg);

        // Depending on desired detail level and level options, add further
        // information to the node.
        if constexpr (Detail::should_emit<lvl, mode>(Kind::info)) {
            props["id"] = id;

            // Information on source
            if (auto src = source.lock()) {
                props["source_id"] = src->id;
                props["source_type"] = src->desc();
            }
            else {
                props["source_id"] = -1;
                props["source_type"] = "(invalidated)";
            }

            // Information on target
            props["target_id"] = target->id;
            props["target_type"] = target->desc();
        }

        // Add non-const member variables that don't change frequently
        if constexpr (Detail::should_emit<lvl, mode>(Kind::basics)) {
            props["active"] = active;
            props["max_flow"] = max_flow;
            props["efficacy"] = efficacy;
        }

        // Frequently changing information
        if constexpr (Detail::should_emit<lvl, mode>(Kind::dynamics)) {
            props["deltaE"] = deltaE;
        }

        // Topology
        if constexpr (Detail::should_emit<lvl, mode>(Kind::topology)) {
            // Already included in info level
        }

        return props;
    }

    // .. Manipulating the flow value and the target object ...................
    /// Set the flow value for a single time step
    void set_flow(const double& flow) {
        deltaE = flow;
    }

    /// Scale the flow by a certain factor
    void scale_flow(const double& factor) {
        deltaE *= factor;
    }

    /// Apply the flow to the target (not the source!)
    /** \detail This calls the target's apply_inflow method
      */
    void apply_flow() const {
        target->apply_inflow(deltaE);
    }
};


// -- Declare static member values -------------------------------------------

/// IDs start at 1 such that there is 0 to signify something else
template<class SourceType, class TargetType>
inline IDType Flow<SourceType, TargetType>::next_id = 1;


} // namespace Utopia::Models::mesonet

#endif // UTOPIA_MODELS_MESONET_RESOURCES_FLOW_HH
