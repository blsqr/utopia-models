#ifndef UTOPIA_MODELS_MESONET_STRATEGY_FUNCS_HH
#define UTOPIA_MODELS_MESONET_STRATEGY_FUNCS_HH

#include <algorithm>
#include <limits>

#include "types.hh"


namespace Utopia {
namespace Models {
namespace mesonet {


/// Type of the selector functions
/** \detail The job of such a function is to return a container of pointers
  *         to certain objects T. These objects are selected from using
  *         different strategies, e.g. randomly.
  */
template<class N, class T, class RNG>
using Selector = std::function<PtrCont<T>(const N&&,
                                          const PtrCont<T>&,
                                          const std::shared_ptr<RNG>&)>;

namespace selectors {

/// Reservoir sampling (via std::sample)
/** \note With ``num`` exceeding the number of available elements, only
  *       ``available.size()`` elements are selected. See std::sample docs.
  */
template<class N, class T, class RNG>
Selector<N, T, RNG>
    reservoir_sampling = [](const N&& num,
                            const PtrCont<T>& available,
                            const std::shared_ptr<RNG>& rng)
{
    PtrCont<T> selected;
    std::sample(available.begin(), available.end(),
                std::back_inserter(selected), num, *rng);
    return selected;
};


/// Reservoir sampling (via std::sample) among available with least outflows
/** \note With ``num`` exceeding the number of available elements, only
  *       ``available.size()`` elements are selected. See std::sample docs.
  */
template<class N, class T, class RNG>
Selector<N, T, RNG>
    least_num_outflows = [](const N&& num,
                            const PtrCont<T>& available,
                            const std::shared_ptr<RNG>& rng)
{
    // Among the available ones, find the minimum value of number of outflows
    std::size_t min_num_outflows = std::numeric_limits<std::size_t>::max();
    for (const auto& entity : available) {
        if (entity->num_outflows() < min_num_outflows) {
            min_num_outflows = entity->num_outflows();
        }
        if (min_num_outflows == 0) {
            break;
        }
    }

    // Populate a new container with those that have this minimum num outflows
    PtrCont<T> least_outflows;
    for (const auto& entity : available) {
        if (entity->num_outflows() == min_num_outflows) {
            least_outflows.push_back(entity);
        }
    }

    return reservoir_sampling<N, T, RNG>(std::move(num),
                                         least_outflows, rng);
};

} // namespace selectors



/// Returns a selector function from the mesonet::selectors namespace
/** \detail The template parameters are used to specialize the selectors
  *         depending on the expected return type and the chosen RNG
  *
  * \param  mode  By which method to choose the elements
  *
  * \tparam T     The type of object to select
  * \tparam RNG   The RNG to use
  * \tparam N     The type of the ``num`` argument expected by the selector
  *               function.
  */
template<class T, class RNG, class N=unsigned short>
Selector<N, T, RNG> get_selector(const std::string& mode) {
    // TODO Make type-specific selectors only available if T is supported!
    if (mode == "random_sample" or mode == "reservoir_sampling") {
        return selectors::reservoir_sampling<N, T, RNG>;
    }
    if (mode == "least_num_outflows") {
        return selectors::least_num_outflows<N, T, RNG>;
    }
    else {
        throw std::invalid_argument("Invalid argument 'mode': " + mode);
    }
}


} // namespace mesonet
} // namespace Models
} // namespace Utopia

#endif // UTOPIA_MODELS_MESONET_STRATEGY_FUNCS_HH
