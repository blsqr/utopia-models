#ifndef UTOPIA_MODELS_MESONET_UTILS_DETAIL_HH
#define UTOPIA_MODELS_MESONET_UTILS_DETAIL_HH

#include <limits>


namespace Utopia::Models::mesonet {

/// Constexpr detail level controller
struct Detail {
    using uchar = unsigned char;

    /// The available kinds of informations
    /** \detail These are sorted such that they can be interpreted as detail
      *         levels if the Mode::include_below option is used.
      */
    enum class Kind : uchar {
        /// Minimal detail, e.g. only the initial configuration options
        minimal = 0,
        /// Include all configuration options
        cfg = 1,
        /// Add some object information, e.g. ID and type
        info = 2,
        /// Add basic members' values
        basics = 3,
        /// Add dynamically changing values
        dynamics = 4,
        /// Add topology information
        topology = 8,
        /// Extended information, e.g. including those of connected entities
        extended = 16,
        /// Add all available information
        full = std::numeric_limits<uchar>::max()
    };

    enum class Mode { include_below, exact };

    /// Returns true if this kind of information is to be emitted
    template<Kind lvl, Mode mode>
    static constexpr bool should_emit(Kind kind) {
        if constexpr (mode == Mode::include_below) {

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wtype-limits"
            // NOTE ignoring the gcc warning here ... because (if I'm not
            //      totally messing this up) it's wrong in this case ...
            return static_cast<uchar>(lvl) >= static_cast<uchar>(kind);
#pragma GCC diagnostic pop

        }
        else if constexpr (mode == Mode::exact) {
            return static_cast<uchar>(lvl) == static_cast<uchar>(kind);
        }
        return false;
    }
};


} // namespace Utopia::Models::mesonet

#endif // UTOPIA_MODELS_MESONET_UTILS_DETAIL_HH
