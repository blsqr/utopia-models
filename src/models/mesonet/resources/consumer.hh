#ifndef UTOPIA_MODELS_MESONET_RESOURCES_CONSUMER_HH
#define UTOPIA_MODELS_MESONET_RESOURCES_CONSUMER_HH

#include <random>
#include <algorithm>

#include "../types.hh"
#include "../flow.hh"

#include "primary.hh"
#include "secondary.hh"


namespace Utopia {
namespace Models {
namespace mesonet {
namespace Resources {


/// The consumer class; also derived from the Resource base class!
class Consumer : public Resource<Consumer> {
public:
    /// Make CRIA a template friend
    template<class T>
    friend class mesonet::ConsumerResourceIA;

    // .. Typedefs ............................................................
    /// This class's type
    using Self = Consumer;

    /// The base class
    using Base = Resource<Self>;

    // Declare base class as friend to allow it to call protected functions
    friend Resource;

    /// Energy flows from primary resources
    using FlowPrimary = Flow<PrimaryResource<Consumer>, Consumer>;

    /// Energy flows from secondary resources
    using FlowSecondary = Flow<SecondaryResource<Consumer>, Consumer>;

    /// Population dynamic functions
    using PDFunc = std::function<double(const SizeType&, const double&)>;

    /// The population dynamics mode
    enum class PopDynMode { DTLG, BevertonHolt, None };


    // .. Static methods ......................................................
    /// A type description of this class to use in constexpr
    static constexpr std::string_view type() {
        return "consumer";
    };

    /// A type description of this class to use at runtime
    static std::string desc() {
        return "consumer";
    }

    /// Whether the resource can propagate energy
    static constexpr bool can_propagate() {
        return false;
    };


    // .. Consumer-specific members, extending Resource .......................
    /// The ID of the parent; set to 0 to signify "no parent"
    /** \note ID 0 is not used for any other instantiations. IDs start at 1.
      */
    const IDType parent_id;

protected:
    /// The IDs of the offspring of this consumer
    std::vector<IDType> offspring_ids;

    /// The consumer population's size
    SizeType size;

public:
    /// The size at which a consumer will split
    const SizeType split_size;

    /// Other splitting parameters
    const Config split_params;

    /// The energy need per individual
    const double energy_need;

    /// The population dynamics to use
    const PopDynMode pop_dyn_mode;

    /// The birht (and death) rate for DTLG population dynamics
    const double birth_rate;

    /// The proliferation rate for the Beverton-Holt dynamics
    const double proliferation_rate;

    /// The innovation probability
    const double p_innovation;

    /// Other innovation parameters
    const Config innov_params;

protected:
    /// The carrying capacity that was felt by this consumer
    double K;

    /// The function to calculate the change in population size
    const PDFunc calc_deltaN;

    /// Whether this consumer is deceased
    bool deceased;

    /// Normal distribution for population dynamics
    std::normal_distribution<double> normal_dist;

    /// Innovations of primary resources, i.e. flow from these resources
    WkPtrCont<FlowPrimary> innovations_primary;

    /// Innovations of secondary resources, i.e. flow from these resources
    WkPtrCont<FlowSecondary> innovations_secondary;


public:
    /// Construct a consumer object
    template<class RNG>
    Consumer(const Config& cfg,
             std::shared_ptr<spdlog::logger>& parent_logger,
             std::shared_ptr<RNG>& rng)
    :
        // Delegate basic parameter setup to the base class
        Base(cfg, parent_logger, rng),

        // Carry over parent ID, if given; is part of cfg when splitting
        parent_id(get_as<IDType>("parent_id", this->cfg, 0)),
        offspring_ids{},

        // Extract parameters from configuration
        size(get_as<SizeType>("size", this->cfg)),
        split_size(get_as<SizeType>("split_size", this->cfg)),
        split_params(get_as<Config>("split_params", this->cfg)),
        energy_need(get_as<double>("energy_need", this->cfg)),

        pop_dyn_mode([this](){
            const auto pdm = get_as<std::string>("pop_dyn_mode", this->cfg);
            if (pdm == "DTLG") {
                return PopDynMode::DTLG;
            }
            else if (pdm == "BevertonHolt") {
                return PopDynMode::BevertonHolt;
            }
            else if (pdm == "None") {
                return PopDynMode::None;
            }
            else {
                throw std::invalid_argument("Invalid `pop_dyn_mode`: " + pdm +
                    "! Choose from: DTLG, BevertonHolt, None.");
            }
        }()),
        birth_rate(get_as<double>("birth_rate", this->cfg)),
        proliferation_rate(get_as<double>("proliferation_rate", this->cfg)),

        p_innovation(get_as<double>("p_innovation", this->cfg)),
        innov_params(get_as<Config>("innov_params", this->cfg)),

        // Cache variables, lambdas, flags, ...
        K(std::numeric_limits<double>::quiet_NaN()),
        calc_deltaN(construct_pop_dyn_func()),
        deceased(false),
        normal_dist(0., get_as<double>("pd_update_sigma", this->cfg)),

        // Initialize innovations containers empty; it's CRIA's job to add them
        innovations_primary{},
        innovations_secondary{}
    {}



protected:
    // .. Setup functions called via Resource constructor .....................
    /// Energy scale of a consumer is always zero; called from base class
    double __init_energy_scale(const Config&) {
        return 0.;
    }

    // .. Specialized setup functions .........................................

    /// Build the population dynamics function depending on pop_dyn_mode
    PDFunc construct_pop_dyn_func() const {
        // Create a new normal distribution that can be captured by the lambda

        // Distinguish by mode, then construct the update lambda
        if (pop_dyn_mode == PopDynMode::DTLG) {
            this->log->debug("Using discrete-time logistic growth population "
                             "dynamics.");
            return
                [this](const auto& size, const auto& K) {
                    return this->birth_rate * size * (1. - size/K);
            }   ;
        }
        else if (pop_dyn_mode == PopDynMode::BevertonHolt) {
            this->log->debug("Using Beverton-Holt population dynamics.");

            return
                [this](const auto& size, const auto& K){
                    // FIXME something fishy here!
                    return   (this->proliferation_rate * size)
                           / (1. + (  (size * (proliferation_rate - 1.))
                                    / K));
                };
        }
        else if (pop_dyn_mode == PopDynMode::None) {
            this->log->debug("Not performing any population dynamics.");

            return [](const auto&, const auto&){
                // Zero change in population size
                return 0.;
            };
        }
        else {
            throw std::invalid_argument("Invalid pop_dyn_mode enum value!");
        }
    }

    // .. Other specializations ...............................................
    /// Updates the config node to contain the current consumer properties
    /** \ref See ::Resource::get_properties
      */
    template<Detail::Kind lvl, Detail::Mode mode>
    void __update_properties(Config& props) const {
        using Kind = Detail::Kind;

        if constexpr (Detail::should_emit<lvl, mode>(Kind::basics)) {
            props["size"] = size;
        }

        if constexpr (Detail::should_emit<lvl, mode>(Kind::dynamics)) {
            props["offspring_ids"] = offspring_ids;

            props["deceased"] = deceased;
            props["K"] = K;
            props["num_innovations_primary"] = num_innovations_primary();
            props["num_innovations_secondary"] = num_innovations_secondary();
        }

        if constexpr (Detail::should_emit<lvl, mode>(Kind::topology)) {
            props["innovations_primary_source_ids"] =
                get_innovation_source_ids<FlowPrimary>();
            props["innovations_secondary_source_ids"] =
                get_innovation_source_ids<FlowSecondary>();
        }

        if constexpr (Detail::should_emit<lvl, mode>(Kind::extended)) {
            props["innovations_primary_properties"] =
                get_innovation_properties<FlowPrimary, Kind::basics>();
            props["innovations_secondary_properties"] =
                get_innovation_properties<FlowSecondary, Kind::basics>();
        }
    }


public:
    // -- Public interface ----------------------------------------------------
    // .. Getters .............................................................

    /// Returns the carrying capacity that was used during population dynamics
    double get_K() const {
        return K;
    }

    /// Return the innovation container for the given type
    template<class FlowT>
    WkPtrCont<FlowT> get_innovations() const {
        static_assert(   FlowT::Source::type() == "primary"
                      or FlowT::Source::type() == "secondary",
                      "Only have primary and secondary innovations!");

        if constexpr (FlowT::Source::type() == "primary") {
            return innovations_primary;
        }
        else {
            return innovations_secondary;
        }
    }

    /// Return a vector of the source IDs of connected resources
    template<class FlowT>
    std::vector<IDType> get_innovation_source_ids() const {
        std::vector<IDType> ids;
        for (const auto& innov_wk : get_innovations<FlowT>()) {
            if (auto innov = innov_wk.lock()) {
                ids.push_back(innov->get_source_id());
            }
            else {
                throw std::runtime_error("Innovation prematurely destructed!");
            }
        }
        return ids;
    }

    /// Return a vector of pointers to the connected resources
    template<class FlowT, class ResT = typename FlowT::Source>
    PtrCont<ResT> get_innovation_sources() const {
        PtrCont<ResT> sources;
        for (const auto& innov_wk : get_innovations<FlowT>()) {
            if (auto innov = innov_wk.lock()) {
                if (auto src = innov->source.lock()) {
                    sources.push_back(src);
                }
                else {
                    throw std::runtime_error("Innovation source prematurely "
                                             "destructed!");
                }
            }
            else {
                throw std::runtime_error("Innovation prematurely destructed!");
            }
        }
        return sources;
    }

    /// Return a Config node with all properties of the selected innovations
    /** \detail Returns a mapping where the keys correspond to the IDs of the
      *         innovations of this consumer.
      *
      * \ref See ::Resource::get_properties regarding template arguments
      */
    template<class FlowT,
             Detail::Kind lvl,
             Detail::Mode mode=Detail::Mode::include_below>
    Config get_innovation_properties() const {
        Config props;
        for (const auto& innov_wk : get_innovations<FlowT>()) {
            if (auto innov = innov_wk.lock()) {
                props[innov->id] = innov->template get_properties<lvl, mode>();
            }
            else {
                throw std::runtime_error("Innovation prematurely destructed!");
            }
        }
        return props;
    }

    /// Returns number of primary innovations
    std::size_t num_innovations_primary() const {
        return get_innovations<FlowPrimary>().size();
    }

    /// Returns number of secondary innovations
    std::size_t num_innovations_secondary() const {
        return get_innovations<FlowSecondary>().size();
    }

    /// Return const reference to the current size
    const SizeType& get_size() const {
        return size;
    }

    /// Whether this consumer is deceased
    bool is_deceased() const {
        return deceased;
    }

    /// Whether this consumer exceeded its split size
    bool will_split() const {
        return (split_size > 0 and size > split_size);
    }


    // .. Flow interface ......................................................
    /// Return reference to an inflow container, i.e. innovation storage
    template<class FlowT>
    WkPtrCont<FlowT>& inflow_container() {
        static_assert(   FlowT::Source::type() == "primary"
                      or FlowT::Source::type() == "secondary");

        // Distinguish by source type
        if constexpr(FlowT::Source::type() == "primary") {
            return innovations_primary;
        }
        else {
            return innovations_secondary;
        }
    }

    // .. Calculation functions ...............................................

    /// Calculates the carrying capacity depending on the extract resources
    /** \note This is also stored as member K and can be retrieved via get_K()
      */
    double calc_carrying_capacity() const {
        return this->energy_in / energy_need;
    }

    // .. Dynamics ............................................................

    /// Apply an energy inflow to this consumer's energy level
    /** \note This is called from the Flow objects
      */
    void apply_inflow(double dE) {
        this->energy += dE;
        this->energy_in += dE;
    }

    /// Performs population dynamics using Beverton-Holt dynamics
    template<class RNG>
    void population_dynamics(std::shared_ptr<RNG>& rng) {
        this->log->trace("  Population dynamics ...");
        this->log->trace("    Size:     {:>5d}", size);
        this->log->trace("    E:        {:>7.1f}", this->energy);
        this->log->trace("    E_in:     {:>7.1f}", this->energy_in);

        // Calculate and store the carrying capacity
        K = calc_carrying_capacity();
        this->log->trace("    K:        {:>7.1f}", K);

        if (K > 0.) {
            // Calculate the change in population size, rounded
            double deltaN = calc_deltaN(size, K);
            this->log->trace("    deltaN:   {:>+7.1f}  (calculated)",
                             deltaN);

            deltaN = std::round(deltaN + normal_dist(*rng));
            this->log->trace("    deltaN:   {:>+7.1f}  (w/ noise & rounding)",
                             deltaN);

            // Adjust the population size
            if (size + deltaN < 0.) {
                size = 0;
            }
            else {
                size += deltaN;
            }
            // TODO Improve. Should actually be able to just do += double,
            //      because arithmetics of unsigned integer types
            //      and floating point types are such that there can be no
            //      underflow. Instead, size will (SHOULD!) just become zero.
        }
        else {
            // No resources were connected; no energy input to adapt to ...
            // NOTE Consider dying with proliferation rate? :thinking:
            // TODO add probabilistic update
        }

        this->log->trace("    New size: {:>5d}", size);

        // Check if this led to the population dying out
        if (size == 0) {
            decease();
        }
    }

    // Expend the energy the consumer population needs
    template<class RNG>
    void expend_energy(const std::shared_ptr<RNG>& rng) {
        // Calculate total energy need to be deduced from reservoir
        double dE = size * energy_need;

        // Check if more was required than available
        if (dE > this->energy) {
            // Did not have enough energy. Need to reduce size! Based on
            // the carrying capacity, calculate a new size, including noise...
            double new_size = std::round(  this->energy/this->energy_need
                                         + normal_dist(*rng));

            // Does this lead to the population dying out?
            if (new_size < 1.) {
                decease();

                // Nothing more to do, return directly.
                return;
            }

            // Apply new size and recalculate dE, ensuring it is within bounds
            size = new_size;
            dE = std::clamp(size * energy_need, 0., this->energy);
            // TODO Re-evaluate whether this behaviour and the call to the
            //      normal_dist() above makes sense...
            //      With the clamping happening here, this basically allows
            //      that the effective energy consumer per agent is lower than
            //      the energy need ... for a short time period at least
        }

        // Deduce the expended energy from the available one
        this->energy_out = dE;
        this->energy -= dE;
    }

private:
    // .. Helper functions ....................................................

    /// Called when the consumer population drops to zero
    /** \detail This function makes sure a deceased consumer population is in
      *         a consistent state:
      *            - `deceased` flag is set
      *            - `size` is 0
      *            - remaining energy is propagated, such that `energy` is 0
      *            - all connected Flow objects marked deactivated
      *         The cleanup of the consumer and the Flow objects needs to
      *         happen separately.
      */
    void decease() {
        // Can only decease once ...
        if (deceased) {
            return;
        }

        this->log->trace("  Deceasing ...");

        // Mark as deceased
        deceased = true;

        // Make sure size is zero and all energy is removed
        size = 0;
        this->energy_out = energy;
        this->energy = 0.;

        // Deactivate all connected flow objects
        for (const auto& f : innovations_primary) {
            if (f.expired()) {
                throw std::runtime_error("FlowPrimary weak_ptr expired!");
            }
            f.lock()->deactivate();
        }
        for (const auto& f : innovations_secondary) {
            if (f.expired()) {
                throw std::runtime_error("FlowSecondary weak_ptr expired!");
            }
            f.lock()->deactivate();
        }
    }
};


} // namespace Resources
} // namespace mesonet
} // namespace Models
} // namespace Utopia

#endif // UTOPIA_MODELS_MESONET_RESOURCES_CONSUMER_HH
