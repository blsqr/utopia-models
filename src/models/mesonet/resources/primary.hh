#ifndef UTOPIA_MODELS_MESONET_RESOURCES_PRIMARY_HH
#define UTOPIA_MODELS_MESONET_RESOURCES_PRIMARY_HH

#include <algorithm>
#include <string_view>

#include "base.hh"
#include "../flow.hh"


namespace Utopia {
namespace Models {
namespace mesonet {
namespace Resources {


template<class OutflowT>
class PrimaryResource : public Resource<PrimaryResource<OutflowT>> {
public:
    /// Make CRIA a template friend
    template<class T>
    friend class mesonet::ConsumerResourceIA;

    // .. Typedefs ............................................................
    /// This class's type
    using Self = PrimaryResource<OutflowT>;

    /// The base class
    using Base = Resource<Self>;

    /// Declare base Resource as friend to allow it to call protected functions
    friend Base;

    /// The target of the outflows
    using OutflowTarget = OutflowT;

    /// The type of outflow of this resource
    using FlowType = Flow<Self, OutflowTarget>;


    // .. Static methods ......................................................
    /// A type description of this class to use in constexpr
    static constexpr std::string_view type() {
        return "primary";
    };

    /// A type description of this class to use at runtime
    static std::string desc() {
        return "primary";
    }

    /// Whether the resource can propagate energy; requires outflows member
    static constexpr bool can_propagate() {
        return true;
    };


protected:
    // .. PrimaryResource-specific members, extending Resource ................
    /// The outflows, i.e. Flow objects to the OutflowTarget
    PtrCont<FlowType> outflows;


public:
    /// Construct a primary resource
    template<class RNG>
    PrimaryResource(const Config& cfg,
                    std::shared_ptr<spdlog::logger>& parent_logger,
                    std::shared_ptr<RNG>& rng)
    :
        // All parameters can be set up in the base class
        Base(cfg, parent_logger, rng),

        // Initialize outflow container empty; it's CRIA's job to add them
        outflows{}
    {
        // Energy input of a primary resource is always zero; overwrite the NaN
        // that was set in the base class
        this->energy_in = 0.;
    }

protected:
    // .. Setup functions called via Resource constructor .....................

    /// For a primary resource, the scale is its initial energy
    double __init_energy_scale(const Config&) {
        // Make sure energy is positive; otherwise pointless to define scale
        if (this->energy <= 0.) {
            throw std::invalid_argument("Argument `energy` of primary "
                "resource " + std::to_string(this->id) + " needs to be "
                "positive in order to set a meaningful energy scale from it!");
        }

        return this->energy;
    }

    // .. Other specializations ...............................................
    /// Updates the config node to contain the current resource properties
    /** \ref See ::Resource::get_properties
      */
    template<Detail::Kind lvl, Detail::Mode mode>
    void __update_properties(Config& props) const {
        using Kind = Detail::Kind;

        if constexpr (Detail::should_emit<lvl, mode>(Kind::dynamics)) {
            props["num_outflows"] = num_outflows();
        }

        // TODO Topology
    }

public:
    // .. Flow interface ......................................................
    /// Return reference to the outflow container
    template<class FlowT>
    PtrCont<FlowT>& outflow_container() {
        return outflows;
    }

    /// Return the number of outgoing flows
    auto num_outflows() const {
        return outflows.size();
    }

    /// Remove deactivated Flow objects
    void remove_deactivated_flows() {
        outflows.erase(
            std::remove_if(outflows.begin(), outflows.end(),
                [](const auto& flow){
                    return (not flow->is_active());
                }),
            outflows.end()
        );
    }

    /// Returns the flow object pointing to the specified target
    std::shared_ptr<FlowType>
        get_outflow_to(const std::shared_ptr<OutflowTarget>& target)
    {
        auto it =
            std::find_if(outflows.begin(), outflows.end(),
                [&target](const auto& flow) {
                    return flow->target->id == target->id;
                }
            );

        if (it == outflows.end()) {
            throw std::runtime_error("No outflow to the given target "
                "is available! Choose has_outflow_to to check this case.");
        }

        return *it;
    }

    /// Whether this resource has an outflow to the given object
    bool has_outflow_to(const std::shared_ptr<OutflowTarget>& target) {
        return (   std::find_if(outflows.begin(), outflows.end(),
                                [&target](const auto& flow) {
                                    return flow->target->id == target->id;
                                })
                != outflows.end());
    }

    // .. Dynamics ............................................................

};


} // namespace Resources
} // namespace mesonet
} // namespace Models
} // namespace Utopia

#endif // UTOPIA_MODELS_MESONET_RESOURCES_PRIMARY_HH
