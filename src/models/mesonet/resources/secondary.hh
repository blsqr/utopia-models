#ifndef UTOPIA_MODELS_MESONET_RESOURCES_SECONDARY_HH
#define UTOPIA_MODELS_MESONET_RESOURCES_SECONDARY_HH

#include <algorithm>
#include <string_view>

#include "base.hh"
#include "../flow.hh"


namespace Utopia {
namespace Models {
namespace mesonet {
namespace Resources {


template<class OutflowT>
class SecondaryResource : public Resource<SecondaryResource<OutflowT>> {
public:
    /// Make CRIA a template friend
    template<class T>
    friend class mesonet::ConsumerResourceIA;

    // .. Typedefs ............................................................
    /// This class's type
    using Self = SecondaryResource<OutflowT>;

    /// The base class
    using Base = Resource<Self>;

    /// Declare base Resource as friend to allow it to call protected functions
    friend Base;

    /// The target type of the outflows
    using OutflowTarget = OutflowT;

    /// The type of outflow of this resource
    using FlowType = Flow<Self, OutflowTarget>;

    /// Refill functions
    using RefillFunc = std::function<double(const double&)>;

    /// The refill mode
    enum class RefillMode { StepwiseLogistic, CarryingCapacity };


    // .. Static methods ......................................................
    /// A type description of this class to use in constexpr
    static constexpr std::string_view type() {
        return "secondary";
    };

    /// A type description of this class to use at runtime
    static std::string desc() {
        return "secondary";
    }

    /// Whether the resource can propagate energy; requires outflows member
    static constexpr bool can_propagate() {
        return true;
    };


    // .. SecondaryResource-specific members, extending Resource ..............
    /// The refill mode to use
    const RefillMode refill_mode;

protected:
    /// The refill function, set depending on refill mode. Returns delta E
    const RefillFunc calc_dE;

    /// The maximum flow rate of the energy input (for StepwiseLogistic mode)
    /** \note Also used to calculate ``energy_scale`` using the additional
      *       ``energy_scale_factor`` argument.
      */
    double refill_max_flow;

    /// The refill efficacy value (for StepwiseLogistic mode)
    double refill_efficacy;

    /// The refill growth rate (for CarryingCapacity mode)
    double refill_growth_rate;

    /// The outflows, i.e. Flow objects to the OutflowTarget
    PtrCont<FlowType> outflows;


public:
    /// Construct a secondary resource
    template<class RNG>
    SecondaryResource(const Config& cfg,
                      std::shared_ptr<spdlog::logger>& parent_logger,
                      std::shared_ptr<RNG>& rng)
    :
        // Delegate basic parameter setup to the base class
        Base(cfg, parent_logger, rng),

        // Carry over parameters
        refill_mode([this](){
            const auto rfm = get_as<std::string>("refill_mode", this->cfg);
            if (rfm == "StepwiseLogistic") {
                return RefillMode::StepwiseLogistic;
            }
            else if (rfm == "CarryingCapacity") {
                return RefillMode::CarryingCapacity;
            }
            else {
                throw std::invalid_argument("Invalid `refill_mode`: " + rfm +
                    "! Choose from: StepwiseLogistic, CarryingCapacity.");
            }
        }()),
        calc_dE(setup_refill_func()),
        refill_max_flow(get_as<double>("refill_max_flow", cfg)),
        refill_efficacy(get_as<double>("refill_efficacy", cfg)),
        refill_growth_rate(get_as<double>("refill_growth_rate", cfg)),

        // Initialize outflow container empty; it's CRIA's job to add them
        outflows{}
    {
        // Issue warning if initializing with energy higher than CC
        if (    refill_mode == RefillMode::CarryingCapacity
            and this->energy > this->energy_scale)
        {
            this->log->warn("Initialized with energy {:.1f} larger than the "
                            "carrying capacity {:.1f}.");
        }
    }


protected:
    // .. Setup functions called via Resource constructor .....................

    /// Energy scale initialization; can be set via energy_scale_factor
    double __init_energy_scale(const Config& cfg) {
        // Set it as a multiple of the energy input
        return (  get_as<double>("energy_scale_factor", cfg)
                * get_as<double>("refill_max_flow", cfg));
    }

    // .. Other specializations ...............................................
    /// Updates the config node to contain the current resource properties
    /** \ref See ::Resource::get_properties
      */
    template<Detail::Kind lvl, Detail::Mode mode>
    void __update_properties(Config& props) const {
        using Kind = Detail::Kind;

        if constexpr (Detail::should_emit<lvl, mode>(Kind::basics)) {
            props["refill_growth_rate"] = refill_growth_rate;
            props["refill_max_flow"] = refill_max_flow;
            props["refill_efficacy"] = refill_efficacy;
        }

        if constexpr (Detail::should_emit<lvl, mode>(Kind::dynamics)) {
            props["num_outflows"] = num_outflows();
        }

        // TODO Topology
    }

    /// Determine which refill dynamics function to use
    RefillFunc setup_refill_func() const {
        if (refill_mode == RefillMode::StepwiseLogistic) {
            this->log->debug("Using stepwise logistic refill mode.");

            return [this](const auto& energy){
                return (  this->refill_max_flow
                        * (1. - 1.
                                / (1.
                                   + exp(  this->refill_efficacy
                                         * (this->energy_scale - energy)
                                         / this->energy_scale))));
            };
        }
        else if (refill_mode == RefillMode::CarryingCapacity) {
            this->log->debug("Using carrying capacity refill mode.");

            return [this](const auto& energy){
                // NOTE If the returned value is negative, this will NOT lead
                //      to a correction towards the carrying capacity!
                return (  this->refill_growth_rate
                        * energy
                        * (1. - energy/this->energy_scale));
            };
        }
        else {
            throw std::invalid_argument("Invalid refill_mode enum value!");
        }
    }


public:
    // .. Flow interface ......................................................
    /// Return a reference to the outflow container
    template<class FlowT>
    PtrCont<FlowT>& outflow_container() {
        return outflows;
    }

    /// Return the number of outgoing flows
    auto num_outflows() const {
        return outflows.size();
    }

    /// Remove deactivated Flow objects
    void remove_deactivated_flows() {
        outflows.erase(
            std::remove_if(outflows.begin(), outflows.end(),
                [](const auto& flow){
                    return (not flow->is_active());
                }),
            outflows.end()
        );
    }

    /// Returns the flow object pointing to the specified target
    std::shared_ptr<FlowType>
        get_outflow_to(const std::shared_ptr<OutflowTarget>& target)
    {
        auto it =
            std::find_if(outflows.begin(), outflows.end(),
                [&target](const auto& flow) {
                    return flow->target->id == target->id;
                }
            );

        if (it == outflows.end()) {
            throw std::runtime_error("No outflow to the given target "
                "is available! Choose has_outflow_to to check this case.");
        }

        return *it;
    }

    /// Whether this resource has an outflow to the given object
    bool has_outflow_to(const std::shared_ptr<OutflowTarget>& target) {
        return (   std::find_if(outflows.begin(), outflows.end(),
                                [&target](const auto& flow) {
                                    return flow->target->id == target->id;
                                })
                != outflows.end());
    }


    // .. Dynamics ............................................................

    /// Refill this resource depending on its current fill level
    /** \TODO This should and will be changed to overall logistic growth,
      *       rather than step-wise logistic growth. The former makes more
      *       sense when representing regrowing resources ...
      */
    void refill() {
        this->log->trace("  Refilling ...");
        this->log->trace("    E:     {:>7.1f}", this->energy);

        // Calculate deltaE
        double dE = calc_dE(this->energy);
        this->log->trace("    dE:    {:>7.1f}", dE);

        // TODO Add noise here (like in population dynamics)?

        // Make sure it does not become negative; this is not an outflow!
        if (dE < 0.) {
            this->log->trace("    Correcting dE ...");
            dE = 0.;
        }

        // Apply
        this->energy += dE;
        this->energy_in = dE;

        this->log->trace("    E_in:  {:>7.1f}", this->energy_in);
        this->log->trace("    E_new: {:>7.1f}", this->energy);
    }

};


} // namespace Resources
} // namespace mesonet
} // namespace Models
} // namespace Utopia

#endif // UTOPIA_MODELS_MESONET_RESOURCES_SECONDARY_HH
