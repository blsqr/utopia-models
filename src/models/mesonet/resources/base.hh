#ifndef UTOPIA_MODELS_MESONET_RESOURCES_BASE_HH
#define UTOPIA_MODELS_MESONET_RESOURCES_BASE_HH

#include <cmath>
#include <string_view>

#include <yaml-cpp/yaml.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include "../types.hh"
#include "../utils.hh"


namespace Utopia {
namespace Models {
namespace mesonet {
namespace Resources {


/// A base class for all resource-like species
template<class Derived>
class Resource {
private:
    /// The ID of the next member of this class; counted up in constructor
    static IDType next_id;

public:
    /// Make CRIA a template friend
    template<class T>
    friend class mesonet::ConsumerResourceIA;

    // .. Static methods ......................................................
    /// A type description of this class to use in constexpr
    static constexpr std::string_view type() {
        return Derived::type();
    };

    /// A type description of this class to use at runtime
    static std::string desc() {
        return Derived::desc();
    }

    /// Whether the resource can propagate energy
    static constexpr bool can_propagate() {
        return Derived::can_propagate();
    };


    // .. Members .............................................................
    /// ID of this object
    const IDType id;

protected:
    /// A representation of the properties of this object at construction
    /** \detail This is set at construction. It is NOT automatically
      *         updated when parameters change. Refer to ::get_properties for
      *         more info.
      */
    const Config cfg;

    /// The logger for this instance, passed down from the model
    std::shared_ptr<spdlog::logger> log;

    // .. Behaviour-relevant members ..........................................
    /// The amount of energy stored
    double energy;

    /// The energy scaling variable, denoting the possible reservoir saturation
    double energy_scale;

    /// Energy input (tracking variable)
    double energy_in;

    /// Energy output (tracking variable)
    double energy_out;

    /// Scaling factor of the energy flow (tracking variable)
    double scaling_factor;


public:
    /// Construct a resource from the given configuration
    template<class RNG>
    Resource(const Config& cfg,
             std::shared_ptr<spdlog::logger>& parent_logger,
             std::shared_ptr<RNG>& rng)
    :
        // Increment static ID
        id(next_id++),

        // Potentially apply noise to the given parameters, then store result
        cfg(apply_noise(cfg, rng)),

        // Create a logger
        log(spdlog::stdout_color_mt(parent_logger->name()
                                    + "." + desc()
                                    + "." + std::to_string(id))),

        // Carry over parameters, partly delegating to derived class
        energy(get_as<double>("energy", cfg)),
        energy_scale(impl().__init_energy_scale(cfg)),

        // Set tracking variables to NaN
        energy_in(std::numeric_limits<double>::quiet_NaN()),
        energy_out(std::numeric_limits<double>::quiet_NaN()),
        scaling_factor(std::numeric_limits<double>::quiet_NaN())
    {
        // Configure logger
        log->set_level(parent_logger->level());
        log->debug("Created '{}' resource with ID {}.", desc(), id);

        if (log->should_log(spdlog::level::trace)) {
            std::stringstream cfg_out;
            cfg_out << cfg;
            log->trace("Configuration:\n\n{}\n", cfg_out.str());
        }
    }


protected:
    // .. Initialization Methods ..............................................


    // .. CRTP ................................................................
    /// Cast to the derived class
    Derived& impl () {
        return static_cast<Derived&>(*this);
    }

    /// Const cast to the derived interface
    const Derived& impl () const {
        return static_cast<const Derived&>(*this);
    }


public:
    // .. Public interface ....................................................

    /// Get the config node representing the properties of this object
    /** \tparam lvl   Which kind of information to include into the properties,
      *               see \ref Detail::Kind .
      * \tparam mode  How to select detail levels, see \ref Detail::Mode .
      *
      * \note   If the Detail::Kind::cfg level is included into the properties,
      *         this creates a clone of the stored ``cfg`` in order to not
      *         introduce any mutability effects. The returned copy can be
      *         changed without having side effects on the stored object.
      */
    template<Detail::Kind lvl = Detail::Kind::cfg,
             Detail::Mode mode = Detail::Mode::include_below
             >
    Config get_properties() const {
        using Kind = Detail::Kind;

        // Start with an empty config node
        Config props{};

        // Include info on the configuration the resource was set up with
        if constexpr (Detail::should_emit<lvl, mode>(Kind::cfg)) {
            props = YAML::Clone(cfg);
            // Work on a deep copy of the initial configuration. Need this to
            // allow the returned node to be changed without side effecs.
        }

        // Depending on desired detail level and level options, add further
        // information to the node.
        if constexpr (Detail::should_emit<lvl, mode>(Kind::info)) {
            props["id"] = id;
            props["type"] = desc();
        }

        // Include some basic non-const properties
        if constexpr (Detail::should_emit<lvl, mode>(Kind::basics)) {
            props["energy"] = energy;
            props["energy_scale"] = energy_scale;
        }

        // Add dynamically changing values
        if constexpr (Detail::should_emit<lvl, mode>(Kind::dynamics)) {
            props["energy_in"] = energy_in;
            props["energy_out"] = energy_out;
            props["scaling_factor"] = scaling_factor;
        }

        // Let the implementation allow to update
        impl().template __update_properties<lvl, mode>(props);

        return props;
    }

    /// Return the energy
    double get_energy() const {
        return energy;
    }

    /// Return the energy input tracking variable
    double get_energy_in() const {
        return energy_in;
    }

    /// Return the energy output tracking variable
    double get_energy_out() const {
        return energy_out;
    }

    /// Return the scaling factor
    double get_scaling_factor() const {
        return scaling_factor;
    }

    /// Reset a the energy input tracking variable
    void reset_energy_in() {
        energy_in = 0.;
    }

    // .. Resource Dynamics ...................................................

    void propagate_energy() {
        static_assert(Derived::can_propagate(),
                      "Cannot propagate energy for this kind of Resource!");

        // Reset energy output member
        energy_out = 0.;

        // Instantiate variables needed below; partly re-used
        double dE_tot = 0.;   // Total energy outflow
        double dE = 0.;       // Outflow per outlet; re-used

        // If it can propagate, it has outflows. Iterate over them...
        for (const auto& flow : impl().outflows) {
            // Make sure the flow is not already deactivated
            if (not flow->is_active()) {
                continue;
            }

            // Let the implementation calculate the energy outflow
            dE = impl().calc_energy_outflow(flow);

            // Store in Flow's temporary variable; update total energy change
            flow->set_flow(dE);
            dE_tot += dE;
        }

        // Check if scaling is required
        if (dE_tot > this->energy) {
            // Yes. Calculate scaling factor and store in member
            scaling_factor = this->energy / dE_tot;

            // Total energy outflow is now exactly the resource's energy
            dE_tot = this->energy;

            // Iterate over flows again, scale down the outflow, then apply,
            // and update energy output of this resource
            for (const auto& flow : impl().outflows) {
                flow->scale_flow(scaling_factor);
                flow->apply_flow();
            }
        }
        else {
            // No need for scaling; set the member's value to 1.
            scaling_factor = 1.;

            // Apply flows to their targets and update energy output
            for (const auto& flow : impl().outflows) {
                flow->apply_flow();
            }
        }

        // Apply outflow to this resource
        this->energy_out = dE_tot;
        this->energy -= dE_tot;
    }

    /// Given the flow object, calculate the energy outflow
    /** \detail This function can be overwritten in the derived class.
      * \note   In the current implementation, the outflow is implemented in
      *         the base class to avoid copying the formula to both resource
      *         classes.
      */
    template<class FlowType>
    double calc_energy_outflow(const std::shared_ptr<FlowType>& flow) const {
        // Make sure this is not called in the wrong scenario
        static_assert((   FlowType::Source::type() == "primary"
                       or FlowType::Source::type() == "secondary")
                      and FlowType::Target::type() == "consumer",
            "Energy outflow only implemented from primary and secondary "
            "resources towards consumers!");

        // NOTE Exploitation efficiency is:
        //        (1. - exp(-flow.efficacy * energy / energy_scale))endl;

        return (  flow->target->get_size() * flow->max_flow
                * (1. - exp(-flow->efficacy * energy / energy_scale)));
    }
};


// -- Declare static member values --------------------------------------------

/// IDs start at 1 such that there is 0 to signify something else
template<class Derived>
inline IDType Resource<Derived>::next_id = 1;

} // namespace Resources
} // namespace mesonet
} // namespace Models
} // namespace Utopia

#endif // UTOPIA_MODELS_MESONET_RESOURCES_BASE_HH
