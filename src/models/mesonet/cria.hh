#ifndef UTOPIA_MODELS_MESONET_RNET_HH
#define UTOPIA_MODELS_MESONET_RNET_HH

#include <sstream>
#include <algorithm>
#include <random>
#include <queue>
#include <limits>

#include <spdlog/spdlog.h>

#include "types.hh"
#include "utils.hh"
#include "selectors.hh"

#include "resources.hh"


namespace Utopia {
namespace Models {
namespace mesonet {


/// This object models the consumer resource interaction
/** \detail While this mimicks a network modelling approach, a network is
  *         actually _not_ used to represent the interaction.
  */
template<class RNGType>
class ConsumerResourceIA {
public:
    // .. Infrastructure members ..............................................
    /// The shared logger, passed down from the model
    std::shared_ptr<spdlog::logger> log;

    /// The shared RNG, passed down from the model
    std::shared_ptr<RNGType> rng;

    /// A real distribution in range [0., 1.) for evaluating probabilities
    std::uniform_real_distribution<double> prob_distr;


    // .. Entity Containers ...................................................
    /// Container of shared pointers to primary resources
    PtrCont<PrimaryResource> res_primary;

    /// Container of shared pointers to secondary resources
    PtrCont<SecondaryResource> res_secondary;

    /// Container of shared pointers to consumers
    PtrCont<Consumer> consumers;


    // .. Genealogy ...........................................................
    /// Container of parent relations
    std::vector<IDType> parents;


private:
    // .. Temporary containers ................................................
    /// Container (FIFO queue) of consumers that are to be split
    std::queue<std::shared_ptr<Consumer>> consumers_to_split;

    // .. Flags ...............................................................



public:
    /// Construct an object that handles the consumer resource interactions
    ConsumerResourceIA (const Config& setup_cfg,
                        std::shared_ptr<spdlog::logger> logger,
                        std::shared_ptr<RNGType> rng)
    :
        // Infrastructure
        log(logger),
        rng(rng),
        prob_distr(0., 1.),

        // Initialize containers empty. Are populated in the body.
        res_primary{},
        res_secondary{},
        consumers{},

        // Genealogy-related containers
        parents{},

        // Temporary containers
        consumers_to_split{}

        // Flags
    {
        // Create entities
        log->info("Initializing resources and consumers ...");

        setup_<PrimaryResource>(setup_cfg);
        setup_<SecondaryResource>(setup_cfg);
        setup_<Consumer>(setup_cfg);

        log->info("Initialized {} resource(s) ({} primary, {} secondary) "
                  "and {} consumer(s).",
                  res_primary.size() + res_secondary.size(),
                  res_primary.size(), res_secondary.size(), consumers.size());

        // Connect entities
        log->info("Connecting consumers to resources ...");

        setup_innovations_<PrimaryResource>(setup_cfg["innovations"]);
        setup_innovations_<SecondaryResource>(setup_cfg["innovations"]);

        log->info("Have a total of {} primary and {} secondary innovations "
                  "set up now.",
                  num_innovations<PrimaryResource>(),
                  num_innovations<SecondaryResource>());

        log->info("Consumer Resource Interaction fully set up.");

        // Initial dynamics
        if (get_as<bool>("initial_refill", setup_cfg)) {
            log->debug("Performing initial refill ...");
            refill_secondary_resources();
        }
    }


    // .. Dynamics ............................................................

    /// Prepares consumers for propagation by resetting tracking variables
    void prepare_consumers() {
        for (const auto& consumer : consumers) {
            // Only need to reset the energy input as that is accessed by
            // multiple resources; others are only accessed once.
            consumer->reset_energy_in();
        }
        log->trace("Consumers' energy_in tracking variable were reset.");
    }

    /// Call all secondary resources' refill method
    void refill_secondary_resources() {
        for (auto& res : res_secondary) {
            res->refill();
        }
        log->debug("Secondary resources refilled.");
    }

    /// Call all resources' propagate_energy method
    void propagate_energy_from_resources() {
        for (const auto& res : res_primary) {
            res->propagate_energy();
        }

        for (const auto& res : res_secondary) {
            res->propagate_energy();
        }
        log->debug("Energy propagation from resources finished.");
    }

    /// Perform consumer population dynamics
    void consumer_population_dynamics() {
        log->debug("Carrying out consumer population dynamics ...");

        for (auto& consumer : consumers) {
            consumer->population_dynamics(rng);

            // Collect those that will split
            if (consumer->will_split()) {
                log->trace("Consumer {} will split.", consumer->id);
                consumers_to_split.push(consumer);  // add element to back
            }
        }
        log->debug("Population dynamics finished.");
    }

    /// Split those consumers that are in the buffer container
    /** \returns bool  True if one or more splittings occured.
      */
    bool split_consumers() {
        if (not consumers_to_split.size()) {
            return false;
        }
        log->debug("Splitting (at least) {} consumers ...",
                   consumers_to_split.size());

        while (not consumers_to_split.empty()) {
            // Split the first consumer in the container, then remove it
            split_consumer(consumers_to_split.front());
            consumers_to_split.pop();  // removes first element

            // NOTE The split_consumer method may add other consumers to the
            //      consumers_to_split container, thereby extending this loop.
        }
        // consumers_to_split queue is empty now

        return true;
    }


    // .. Getters .............................................................

    /// Retrieve a resource container member by type
    template<class ResT>
    PtrCont<ResT>& res_container() {
        static_assert(   ResT::type() == "primary"
                      or ResT::type() == "secondary"
                      or ResT::type() == "consumer",
                      "Only have containers for: primary, secondary, or "
                      "consumer resources available!");

        // Depending on type, emplace back in a different member
        if constexpr (ResT::type() == "primary") {
            return res_primary;
        }
        else if constexpr (ResT::type() == "secondary") {
            return res_secondary;
        }
        else {
            return consumers;
        }
    }

    /// Return the total number of innovations of all resources of one type
    template<class ResT>
    std::size_t num_innovations() const {
        static_assert(   ResT::type() == "primary"
                      or ResT::type() == "secondary",
                      "num_innovations only supports primary & secondary!");

        if constexpr(ResT::type() == "primary") {
            return std::accumulate(res_primary.begin(), res_primary.end(),
                                   0, [](std::size_t sum, const auto& res) {
                                    return sum + res->num_outflows();
                                   });
        }
        else {
            return std::accumulate(res_secondary.begin(), res_secondary.end(),
                                   0, [](std::size_t sum, const auto& res) {
                                    return sum + res->num_outflows();
                                   });
        }
    }

    /// The sum of primary and secondary resource container sizes
    std::size_t num_resources_total() const {
        return res_primary.size() + res_secondary.size();
    }

    /// Select a number of entities from a container via the specified strategy
    /**
      * \param cont      The container to select entities from
      * \param strategy  Selection / sampling strategy; see ::selectors
      * \param num       Number of entities to select. Can be negative, in
      *                  which case ``num`` is used to deduce from the number
      *                  of available entities. If it is larger than the number
      *                  of available ones, the sample _may_ contain duplicate
      *                  elements, depending on the sampling strategy.
      *
      * \tparam ResT     The resource type to select
      * \tparam strict   Whether to throw if ``num`` is larger than the number
      *                  of available entities. Setting this to true guarantees
      *                  that the size of the returned container is exactly
      *                  ``num`` or an exception is raised.
      * \tparam N        The type of the ``num`` argument as it is expected by
      *                  the ::get_selector function.
      */
    template<class ResT, bool strict=false, class N=unsigned short>
    PtrCont<ResT> select(const PtrCont<ResT>& cont,
                         const std::string& strategy,
                         int num = 1)
    {
        // Find out how many entities are available
        const int num_available = cont.size();

        if constexpr (strict) {
            if (num > num_available) {
                throw std::invalid_argument("Could not select "
                    + std::to_string(num) + " '" + ResT::desc() + "' entities "
                    "because there were only " + std::to_string(num_available)
                    + " available and strict checking was enabled!");
            }
        }

        // Handle negative values
        if (num < 0) {
            num = std::clamp(num_available + num + 1, 0, num_available);
        }

        if (num == 0) {
            // Nothing to do, return empty
            return {};
        }
        // Can be sure that num is > 0 now.

        // Get the selector and call it
        const auto& selector = get_selector<ResT, RNGType, N>(strategy);

        return selector(static_cast<N>(num), cont, rng);
    }

    /// Select a number of entities of a certain type, given a strategy
    /** Overload of existing selection function that uses the resource
      * containers to select entities from.
      */
    template<class ResT, bool strict=false, class N=unsigned short>
    auto select(const std::string& strategy, int num = 1) {
        return select(res_container<ResT>(), strategy, num);
    }


    // .. Manipulation ........................................................

    /// Create and add a new entity of the given type
    template<class ResT, class... Args>
    std::shared_ptr<ResT>& add_entity(Args&&... args) {
        static_assert(   ResT::type() == "primary"
                      or ResT::type() == "secondary"
                      or ResT::type() == "consumer",
                      "Can only add primary, secondary or consumer resource!");

        log->debug("Adding '{}' resource entity ...", ResT::desc());

        // Emplace back into the respective resource container
        auto& e = res_container<ResT>().emplace_back(
            std::make_shared<ResT>(std::forward<Args>(args)..., log, rng)
        );
        log->trace("Created and added '{}' resource with ID {}.",
                   ResT::desc(), e->id);

        // Consumers store the parent's ID such that the genealogy can be
        // reconstructed from the data
        if constexpr (ResT::type() == "consumer") {
            parents.push_back(e->parent_id);
        }

        // Return the entity
        return e;
    }

    /// Create and add multiple entities
    template<class ResT, class T, class... Args>
    PtrCont<ResT> add_entities(const T& num, Args&&... args) {
        PtrCont<ResT> created{};

        for (T i=0; i < num; i++) {
            created.push_back(add_entity<ResT>(std::forward<Args>(args)...));
        }

        return created;
    }

    /// Adds a new Flow object to the network
    template<class SourceT, class TargetT=Consumer, class... Args>
    void add_flow(const std::shared_ptr<SourceT>& source,
                  const std::shared_ptr<TargetT>& target,
                  Args&&... args)
    {
        log->trace("Adding flow between '{}' source (ID {}) "
                   "and '{}' target (ID {}) ...",
                   source->desc(), source->id, target->desc(), target->id);

        // Define the type of the flow to be created
        using FlowT = Flow<SourceT, TargetT>;

        // Retrieve the source's outflow container, then emplace the new object
        // in there as shared pointer
        auto& f = source->template outflow_container<FlowT>().emplace_back(
            std::make_shared<FlowT>(source, target,
                                    std::forward<Args>(args)...,
                                    rng)
        );

        // Use that shared pointer to create the weak pointer in the target
        // object; it needs to be created from a persistent shared pointer,
        // as the weak pointer expires after the shared pointer used to create
        // it goes out of scope ...
        target->template inflow_container<FlowT>().push_back(f);

        log->trace("Created and linked flow with ID {}.", f->id);
    }

    /// Improves an existing innovation
    template<class ResT>
    void improve_innovation(const std::shared_ptr<Consumer>&,  // consumer
                            const std::shared_ptr<Flow<ResT, Consumer>>& innov,
                            const Config& cfg)
    {
        const auto factor = get_as<double>("improvement", cfg);
        log->trace("Improving {} innovation {} by factor {} ...",
                   ResT::desc(), innov->id, factor);

        innov->max_flow = innov->max_flow * factor;
        innov->efficacy = innov->efficacy * factor;
    }


    /// Let the consumer gain a number of innovation of a certain type
    /** Gaining means that either a new innovation is added or an existing
      * innovation is improved, e.g. when the same innovation source is
      * selected or when the maximum number of innovations is reached.
      *
      * \note This does not check the innovation probability!
      *
      * \param  consumer  The consumer that should gain an innovation
      * \param  cfg       The innovation configuration for the chosen ResT
      * \param  num       How many innovations to gain
      *
      * \TODO   This method requires urgent improvement! It does too much and
      *         has too many branches and loops and everything. Ideally, let it
      *         focus on gaining a single innovation for a specific resource
      *         type, either adding a new connection (if possible) or improving
      *         an existing one.
      */
    template<class ResT, bool strict_selection=false, class N=unsigned short>
    void gain_innovations(const std::shared_ptr<Consumer>& consumer,
                          const Config& cfg,
                          const N& num = 1)
    {
        using FlowT = Flow<ResT, Consumer>;

        if (not res_container<ResT>().size()) {
            // No resources available, nothing to do.
            return;
        }

        // Find out the maximum number of innovations the consumer may have
        const auto& innov_params = consumer->innov_params;

        log->trace("Determining maximum number of innovations ...");
        const auto _max_num_innovs =
            get_as<long int>(ResT::desc(), innov_params["max_num"]);
        const std::size_t max_num_innovs =
            _max_num_innovs >= 0 ? _max_num_innovs : std::numeric_limits<std::size_t>::max();

        // Find out the strategy
        const auto strategy = get_as<std::string>("strategy", cfg);

        // Get the resources that can be innovated, i.e. potential flow sources
        const auto sources = select<ResT, strict_selection, N>(strategy, num);

        // Inform about the innovations to be added
        log->trace("Gaining {} {} innovation(s) for consumer {}:",
                   num, ResT::desc(), consumer->id);
        log->trace("  Strategy:         {}", strategy);
        log->trace("  Total available:  {}", res_container<ResT>().size());
        log->trace("  Current number:   {}  (max: {})",
                   consumer->get_innovations<FlowT>().size(), max_num_innovs);

        // Go over all selected sources and either add a new flow or improve
        // an existing one.
        N num_innovs_gained = 0;

        for (const auto& src : sources) {
            // Check if the maximum number is already reached
            if (consumer->get_innovations<FlowT>().size() >= max_num_innovs) {
                break;
            }

            // Try to get the innovation, if one exists
            std::shared_ptr<FlowT> innov;
            try {
                innov = src->get_outflow_to(consumer);
            }
            catch (std::runtime_error& e) {
                // Not connected yet and may still add a flow -> do so
                add_flow(src, consumer, cfg["new"]);

                // Can continue with next iteration
                num_innovs_gained++;
                continue;
            }

            // Is already connected to that resource via `innov`. Improve it.
            improve_innovation(consumer, innov, cfg);
            num_innovs_gained++;
        }

        if (num_innovs_gained >= num) {
            return;
        }
        // If there were fewer than `num` innovations gained, carry out some
        // improvements until a total of `num` innovations were gained.
        for (; num_innovs_gained < num; num_innovs_gained++) {
            // Select from the existing innovations, which one is to be
            // improved using the same strategy
            auto innov_sources = consumer->get_innovation_sources<FlowT>();
            auto src = select<ResT, strict_selection, N>(innov_sources,
                                                         strategy, 1)[0];

            // Get the corresponding innovation and improve it.
            auto innov = src->get_outflow_to(consumer);
            improve_innovation(consumer, innov, cfg);
        }
    }

    /// Let the consumer gain a single primary or secondary innovation
    /** \detail This method should be used during the dynamics. It selects
      *         randomly (proportional to relative number of available
      *         entities) whether to add a primary or secondary resource.
      *         Also, it uses the configuration stored in the consumer's
      *         innovation parameters.
      *
      * \note   This method does not check for the innovation probability!
      */
    void gain_innovation(const std::shared_ptr<Consumer>& consumer) {
        const auto num_res_tot = num_resources_total();
        if (not num_res_tot) {
            return;
        }

        // Determine whether this will be a primary or secondary innovation
        if (prob_distr(*rng) < (double(res_primary.size()) / num_res_tot)) {
            gain_innovations<PrimaryResource>(
                consumer, consumer->innov_params["primary"], 1u
            );
        }
        else {
            gain_innovations<SecondaryResource>(
                consumer, consumer->innov_params["secondary"], 1u
            );
        }
    }

    /// Splits a consumer
    /** \note If parent or offspring declare themselves ``will_split``, they
      *       will be added to the ``consumers_to_split`` FIFO queue again.
      */
    void split_consumer(std::shared_ptr<Consumer>& parent) {
        // For safety, check split size again
        if (not parent->will_split()) {
            throw std::runtime_error("Attempted to split consumer "
                + std::to_string(parent->id) + " although it did not declare "
                "itself as exceeding its split size!");
        }
        log->trace("Splitting consumer {} ...", parent->id);


        // Determine split parameters . . . . . . . . . . . . . . . . . . . . .
        // Get the split factor from the parent's split parameters
        const auto split_factor = get_as<double>("split_factor",
                                                 parent->split_params);

        // Calculate the initial size and energy of the offspring accordingly
        const SizeType size = parent->size * split_factor;  // int rounding!
        const double energy = parent->energy * (double(size) / parent->size);

        // Get mutation parameters
        const auto& mutations = parent->split_params["mutations"];
        const auto mutations_enabled = get_as<bool>("enabled", mutations);

        // Now, need a configuration node to construct the offspring. This can
        // (and should!) be based on the properties of the parent.
        auto props = parent->get_properties();
        // NOTE Is a clone of the parent's properties. It's ok to change it :)


        // Populate props to reflect the changes due to the splitting . . . . .
        // Set the parent ID value
        props["parent_id"] = parent->id;

        // Set the size and energy as calculated above
        props["size"] = size;
        props["energy"] = energy;
        // NOTE These still need to be deduced from the parent; done below.

        // Set the mutations key, if mutations are enabled
        if (mutations_enabled) {
            props["_apply_noise"] = mutations["properties"];
            // This will trigger apply_noise in the consumer constructor
        }


        // Create and add a new consumer . . . . . . . . . . . . . . . . . . .
        std::shared_ptr<Consumer>& offspring = add_entity<Consumer>(props);

        // Let the parent track this offspring ID
        parent->offspring_ids.push_back(offspring->id);

        // Deduce the size and energy that now resides in the offspring from
        // the size and energy of the parent
        parent->size -= offspring->size;
        parent->energy -= offspring->energy;
        // NOTE doing it this late to be sure that the actual created offspring
        //      object is used for deducing the values. That's safer.

        // The offspring does not have any innovations yet. Copy them from the
        // parent, allowing mutations (loss and gain) to occur.
        if (mutations_enabled) {
            // Pass the configuration for mutations over
            copy_innovations_with_mutations<Consumer::FlowPrimary>(
                parent, offspring, mutations["innovations"]["primary"]);
            copy_innovations_with_mutations<Consumer::FlowSecondary>(
                parent, offspring, mutations["innovations"]["secondary"]);
        }
        else {
            // Just copy. (Not passing the configuration disables mutations.)
            copy_innovations_with_mutations<Consumer::FlowPrimary>(
                parent, offspring);
            copy_innovations_with_mutations<Consumer::FlowSecondary>(
                parent, offspring);
        }

        log->trace("Splitting of consumer {} finished and resulted in "
                   "offspring consumer {}.", parent->id, offspring->id);


        // If either of the consumers remains above the split size, add them
        // to the FIFO-queue of consumers that are to be split, such that they
        // get split again until they are below their split size.
        if (parent->will_split()) {
            log->trace("Consumer {} will split again ...", parent->id);
            consumers_to_split.push(parent);
        }
        if (offspring->will_split()) {
            log->trace("Consumer {} will split again ...", offspring->id);
            consumers_to_split.push(offspring);
        }
    }


    // .. Cleanup .............................................................

    /// Remove the deactivated flow objects
    void remove_deactivated_flows() {
        for (const auto& res : res_primary) {
            res->remove_deactivated_flows();
        }

        for (const auto& res : res_secondary) {
            res->remove_deactivated_flows();
        }

        log->debug("Removed deactivated energy flow links.");
    }

    /// Removes the deceased consumers and severes the flow links
    void remove_deceased_consumers() {
        const auto num_before = consumers.size();

        consumers.erase(
            std::remove_if(
                consumers.begin(), consumers.end(),
                [](const auto& c){ return c->is_deceased(); }
            ),
            consumers.end()
        );

        log->debug("Removed {} deceased consumers.",
                   num_before - consumers.size());
    }


private:
    // .. Helper functions ....................................................

    /// Copies the innovations of one parent consumer to its offspring
    /** \detail This also allows performing mutations
      */
    template<class FlowT>
    void copy_innovations_with_mutations(std::shared_ptr<Consumer>& parent,
                                         std::shared_ptr<Consumer>& offspring,
                                         const Config& mutations = {})
    {
        // Get the type of the innovation's source
        using ResT = typename FlowT::Source;

        // Determine mutation properties
        const auto mutations_enabled = get_as<bool>("enabled", mutations,
                                                    false);
        const auto p_loss = get_as<double>("p_loss", mutations, 0.);

        log->trace("Copying up to {} {} innovation(s) of parent {} to "
                   "offspring {} ... (mutations: {}, p_loss: {})",
                   parent->get_innovations<FlowT>().size(),
                   ResT::type(),
                   parent->id, offspring->id,
                   mutations_enabled ? "yes" : "no", p_loss);

        // Iterate over the parent's innovations
        for (auto& wk_innov : parent->inflow_container<FlowT>()) {
            // These are shared pointers, so need to lock them.
            auto innov = wk_innov.lock();
            if (not innov) {
                throw std::runtime_error("Innovation prematurely destroyed!");
            }
            log->trace("  Candidate innovation's ID: {}", innov->id);

            // Determine whether this innovation will be lost
            if (mutations_enabled and p_loss > 0.) {
                if (p_loss == 1.) {
                    // Innovation got lost. Don't continue, i.e. don't copy.
                    log->trace("    Deterministically lost this innovation.");
                    continue;
                }
                else if (prob_distr(*rng) < p_loss) {
                    log->trace("    Randomly lost this innovation.");
                    continue;
                }
            }
            // If this point is reached, a new innovation is to be added

            // Get a copy of the (parent's) innovation's properties
            auto innov_props = innov->get_properties();

            // Mutate properties, if enabled
            if (mutations_enabled and mutations["mutate_inherited"].size()) {
                // Add the given parameters to the node that is checked by the
                // apply_noise function. Will automatically be invoked during
                // construction of the innovation object.
                innov_props["_apply_noise"] = mutations["mutate_inherited"];

                log->trace("    Inheriting with mutations ...");
            }
            else {
                log->trace("    Inheriting without mutations ...");
            }

            // Resolve the source object by locking the weak pointer
            auto innov_src = innov->source.lock();

            if (not innov_src) {
                throw std::runtime_error("Flow source prematurely destroyed!");
            }

            // Create the new innovation
            add_flow(innov_src, offspring, innov_props);
        }

        if (not mutations_enabled) {
            // Done here.
            return;
        }
        // else: May _gain_ additional innovations

        // Get parameters
        const auto p_gain = get_as<double>("p_gain", mutations, 0.);
        auto n_draw = get_as<int>("num_draws", mutations, 0);

        if (p_gain == 0. or n_draw == 0) {
            return;
        }

        // Wrap around a potentially negative n_draw
        const int num_available = res_container<ResT>().size();
        if (n_draw < 0) {
            // Set negative n_draw to number of available resources
            n_draw = std::clamp(num_available + n_draw + 1, 0, num_available);
        }

        log->trace("With a bit of luck, gaining new '{}' innovations ... "
                   "({} draw(s), {} available, p_gain {})",
                   ResT::desc(), n_draw, num_available, p_gain);

        for (int i=0; i < n_draw; i++) {
            if (prob_distr(*rng) < p_gain) {
                // Got lucky. Get a single innovation or improve an existing
                // one. All the parameters for that are already in the given
                // configuration node...
                gain_innovations<ResT>(offspring, mutations, 1u);
            }
        }
    }


    // .. Setup functions .....................................................

    /// General method to create certain resource types
    template<class ResT>
    PtrCont<ResT> setup_(const Config& cfg) {
        // Get the resource type descriptor string
        const auto res_desc = ResT::desc();

        // Check that the config entry is available
        if (not cfg["num"][res_desc] or not cfg[res_desc]) {
            throw std::invalid_argument("Missing one or more of the required "
                "config entries for setup of '" + res_desc + "'!");
        }
        const int num = get_as<int>(res_desc, cfg["num"]);

        // Find out if noisy setup is to be used; this is only for information
        // purposes; the action is evaluated elsewhere.
        const bool noisy_setup = get_as<bool>("_enabled",
                                              cfg[res_desc]["_apply_noise"],
                                              false);

        log->info("Initializing {} {} resource{} with {} setup ...",
                  num, res_desc, (num != 1 ? "s" : ""),
                  (noisy_setup ? "noisy" : "deterministic"));

        // Add the entities to a new container of pointers
        auto cont = add_entities<ResT>(num, cfg[res_desc]);

        log->debug("Initialized {} {} resource{}.",
                   cont.size(), res_desc, (cont.size() != 1 ? "s" : ""));

        return cont;
    }


    /// Setup innovations, i.e. Flow objects from resources to consumers
    template<class ResT>
    void setup_innovations_(const Config& cfg) {
        // Get the resource type descriptor string
        const auto res_desc = ResT::desc();

        // Check that the config entry is available
        if (not cfg["num"][res_desc] or not cfg[res_desc]) {
            throw std::invalid_argument("Missing one or more of the required "
                "configuration entries for the setup of '" + res_desc
                + "' innovation!");
        }

        // Determine number of innovations to add per consumer
        auto num = get_as<int>(res_desc, cfg["num"]);

        // Correct for number of resources
        num = std::clamp(num,
                         num, static_cast<int>(res_container<ResT>().size()));
        // NOTE Allowing negative values here because gain_innovations also
        //      supports them (and takes care of too negative ones)

        // If there are none to be added, return here
        if (num == 0) {
            log->debug("No {} innovations are to be added.", res_desc);
            return;
        }

        // Pass along to gain_innovation function
        log->debug("Setting up {} '{}' innovation{} per consumer ...",
                   num, res_desc, (num != 1 ? "s" : ""));

        for (auto& consumer : consumers) {
            gain_innovations<ResT, true>(consumer, cfg[res_desc], num);
            //                     ^^^^ strict checking of num
        }

        // Done. Count how many were created and inform about it
        const auto inno_cnt = num_innovations<ResT>();
        log->debug("Added {} '{}' innovation{} in total.",
                   inno_cnt, res_desc, (inno_cnt != 1 ? "s" : ""));
    }
};


} // namespace mesonet
} // namespace Models
} // namespace Utopia

#endif // UTOPIA_MODELS_MESONET_RNET_HH
