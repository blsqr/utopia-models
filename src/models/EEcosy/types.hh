#ifndef UTOPIA_MODELS_EECOSY_TYPES_HH
#define UTOPIA_MODELS_EECOSY_TYPES_HH

#include <memory>
#include <vector>
#include <tuple>
#include <type_traits>

#include <armadillo>

#include <utopia/core/types.hh>

#include <UtopiaUtils/utils/rng.hh>


namespace Utopia::Models::EEcosy {

/// The RNG type to use: the default RNG from the Utopia::Utils module
using ModelRNG = Utopia::Utils::DefaultRNG;



// -- General type definitions ------------------------------------------------

/// The configuration type used throughout this model
using Config = Utopia::DataIO::Config;

/// Type of the ID used for identification of entities
using IDType = unsigned int;

/// Container holding shared pointers to some object
template<class T>
using PtrContainer = std::vector<std::shared_ptr<T>>;


// -- Enums & Tags ------------------------------------------------------------


// .. EcoNet public interface tags ............................................

/// A tag to signify that the return value is only the entity
struct entity_only;

/// A tag to signify that the return value is only the descriptor
struct descriptor_only;

/// A tag signifying that both the entity and its descriptor should be returned
struct with_descriptor;

/// A tag to denote that a flow is outgoing from some reference vector
struct outgoing;

/// A tag to denote that a flow is incoming towards some reference vector
struct incoming;

/// A tag to denote both incoming and outgoing directions
struct both_directions;

/// A tag to denote something going alongside the flow direction
struct forward;

/// A tag to denote something going reverse to the flow direction
struct reverse;

/// A tag to control mutation behaviour: skip mutation
struct without_mutation;

/// A tag to control mutation behaviour: perform mutation
struct with_mutation;


// -- Resource Space ----------------------------------------------------------

/// The type used for representing resource (column) vectors
template<std::size_t dim>
using ResourceVectorType = arma::Col<double>::fixed<dim>;

/// The type used for representing mask (column) vectors
template<std::size_t dim>
using MaskVectorType = arma::Col<arma::uword>::fixed<dim>;

/// The type used for representing square resource transformation matrices
template<std::size_t dim>
using TransformationMatrixType = arma::Mat<double>::fixed<dim,dim>;


// -- Forward Declarations ----------------------------------------------------

/// Forward declaration of flow base class
template<class ResSpace, class RNG>
class BaseFlow;

/// Forward declaration of population base class
template<class ResSpace, class RNG>
class BasePopulation;

/// The EEcosy model
template<class ResSpace>
class EEcosy;


// -- Misc --------------------------------------------------------------------


} // namespace Utopia::Models::EEcosy

#endif // UTOPIA_MODELS_EECOSY_TYPES_HH
