#ifndef UTOPIA_MODELS_EECOSY_HH
#define UTOPIA_MODELS_EECOSY_HH

#include <sstream>
#include <random>

#include <utopia/core/model.hh>
#include <utopia/core/types.hh>
#include <utopia/data_io/graph_utils.hh>

#include <UtopiaUtils/utils/action_manager.hh>
#include <UtopiaUtils/data_io/data_writer.hh>

#include "utils.hh"
#include "econet.hh"



namespace Utopia::Models::EEcosy {

// ++ Type definitions ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Type helper to define types used by the model
using ModelTypes = Utopia::ModelTypes<ModelRNG>;


// ++ Model definition ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// The EEcosy Model
template<class ResSpace>
class EEcosy:
    public Model<EEcosy<ResSpace>, ModelTypes>
{
public:
    /// The type of the Model base class of this derived class
    using Base = Model<EEcosy, ModelTypes>;

    /// Export Base model's time type
    using Time = typename Base::Time;

    /// The data writer type to use
    using DataWriter = Utopia::DataIO::DataWriter::DataWriter;

    /// Spezialization of the EcoNet type
    using EcoNet = Models::EEcosy::EcoNet<typename Base::RNG, ResSpace>;

private:
    // -- Members -------------------------------------------------------------

    /// Entity default configurations
    const Config _defaults;

    /// The object representing the ecological interaction network
    EcoNet _econet;

    /// A manager making dynamically performing config-configurable actions
    ActionManager _am;

    /// A manager for all output data
    DataWriter _dw;

    /// Uniform real distribution for evaluating probabilities
    std::uniform_real_distribution<double> _prob_distr;

    /// Flag: Whether splitting is to be carried out
    bool _splitting_enabled;

    /// Names of dynamically mapped datasets
    static inline const std::vector<std::string> _dynamic_mappings = {
        "populations/regular/ids",
        "flows/trophic/ids",
        "network/_vertices",
        "network/edge_ids"
    };


public:
    // -- Model Setup ---------------------------------------------------------
    /// Construct the EEcosy model
    /** \param name     Name of this model instance
     *  \param parent   The parent model this model instance resides in
     */
    template<class ParentModel>
    EEcosy (const std::string& name, ParentModel& parent)
    :
        // Initialize first via base model
        Base(name, parent),

        // Retrieve entity default configuration
        _defaults(get_as<Config>("defaults", this->_cfg)),

        // Initialize an empty ecological interaction network; it will be set
        // up properly via the ActionManager
        _econet(this->_log, this->_rng,
                get_as<Config>("econet", this->_cfg),
                {}, _defaults,
                get_as<Config>("resource_space", this->_cfg)),

        // Set up DataWriter and ActionManager
        _am(this->_log, this->_cfg["action_manager"]),
        _dw(*this),

        // Statistical distributions
        _prob_distr(0., 1.),

        // Temporary variables
        // ...

        // Constants
        _splitting_enabled(get_as<bool>("splitting_enabled", this->_cfg))
    {
        setup_action_manager();

        // ... and let it set up the ecological network
        this->_am.invoke_hook("setup_econet");

        // Create a graph group, then register all write properties
        create_graph_group(_econet.nw, this->_hdfgrp, "network");
        register_properties();

        this->_log->info("{} is all set up now :)", this->_name);
        this->_log->info("  Splitting?  {}.",
                         _splitting_enabled ? "Enabled" : "Disabled");
    }


private:
    // .. Setup functions .....................................................

    // .. Helper functions ....................................................

public:
    // -- Simulation Control --------------------------------------------------

    /// Called before model iteration starts
    /** This override merely adds the ``prolog`` hook, then invokes the parent
      * method.
      */
    void prolog () override {
        this->_am.invoke_hook("prolog");
        this->__prolog();
    }

    /// Called after model iteration ends
    /** This override merely adds the ``epilog`` hook, then invokes the parent
      * method.
      */
    void epilog () override {
        this->_am.invoke_hook("epilog");
        this->__epilog();
    }

    /// Iterate a single step
    void perform_step () {
        this->_am.invoke_hook("perform_step");

        // Might need some cleaning up, i.e.: removing inactive flows and pops
        if (_econet.need_cleanup()) {
            _econet.cleanup();

            // With cleanup done, the ID map is no longer up to date
            _dw.mark_mappings_invalidated(_dynamic_mappings);
        }

        // .. Main algorithm . . . . . . . . . . . . . . . . . . . . . . . . .
        // -- 1: Network-level foraging --
        _econet.perform_foraging();

        // -- 2: Population-internal processes --
        _econet.perform_population_internal_processes();

        // -- 3: Evolution --
        if (not _splitting_enabled) return;

        if (_econet.perform_splitting()) {
            //    Splitting occured
            // => network changed => inalidate all mappers
            _dw.mark_mappings_invalidated(_dynamic_mappings);
        }
    }


    /// Supply monitoring values
    /** Keys provided by this monitor:
      *
      *    - `num_populations`
      *    - `num_flows`
      *
      * Furthermore, this is hooked to the `monitor` ActionManager hook.
      */
    void monitor () {
        this->_am.invoke_hook("monitor");

        this->_monitor.set_entry("num_populations", _econet.num_populations());
        this->_monitor.set_entry("num_flows", _econet.num_flows());
    }


    /// Write data
    /** As the number of active populations and flows differs, the data writing
      * has to store the output in differently-sized datasets.
      * This work is carried out by the mesonet::DataWriter, which handles the
      * tracking and creation of ID mappings and the associated properties.
      *
      * Furthermore, this is hooked to the ``write_data`` ActionManager hook.
      */
    void write_data () {
        this->_am.invoke_hook("write_data");

        // Invoke the DataWriter and let it do all the rest
        _dw.write(this->get_time());
    }


    // -- Getters and setters -------------------------------------------------
    // Add getters and setters here to interface with other model


private:
    // -- Manager setup -------------------------------------------------------

    /// Registers all conceivable output properties to the DataWriter
    void register_properties() {
        using namespace Utopia::DataIO::DataWriter;

        this->_log->info("Registering properties with DataWriter ...");

        // .. Helper functions ................................................
        // ... generating write functions from entities or graph iterations

        const auto from_populations =
            [this](const auto& adptr) {
                return
                    [&](const auto& dset){
                        auto pops = this->_econet.populations();
                        dset->write(pops.begin(), pops.end(), adptr);
                    };
            };

        const auto from_regular_populations =
            [this](const auto& adptr) {
                return
                    [&](const auto& dset){
                        auto& nw = this->_econet.filtered.only_regular;
                        auto pops = this->_econet.template populations<Population>(nw);
                        dset->write(pops.begin(), pops.end(), adptr);
                    };
            };

        const auto from_source_populations =
            [this](const auto& adptr) {
                return
                    [&](const auto& dset){
                        auto& nw = this->_econet.filtered.only_source;
                        auto pops = this->_econet.template populations<Source>(nw);
                        dset->write(pops.begin(), pops.end(), adptr);
                    };
            };

        const auto from_flows =
            [this](const auto& adptr) {
                return
                    [&](const auto& dset){
                        auto flows = this->_econet.flows();
                        dset->write(flows.begin(), flows.end(), adptr);
                    };
            };

        const auto from_trophic_flows =
            [this](const auto& adptr) {
                return
                    [&](const auto& dset){
                        auto& nw = this->_econet.filtered.only_trophic;
                        auto flows = this->_econet.template flows<TrophicFlow>(nw);
                        dset->write(flows.begin(), flows.end(), adptr);
                    };
            };

        [[maybe_unused]] const auto from_vertices =
            [this](const auto& adptr) {
                return
                    [&](const auto& dset){
                        auto vertex_it = this->_econet.vertices();
                        dset->write(vertex_it.begin(), vertex_it.end(), adptr);
                    };
            };

        [[maybe_unused]] const auto from_edges =
            [this](const auto& adptr) {
                return
                    [&](const auto& dset){
                        auto edge = this->_econet.edges();
                        dset->write(edge.begin(), edge.end(), adptr);
                    };
            };

        const auto from_edges_twopass =
            [this](const auto& adptr1, const auto& adptr2) {
                return
                    [&](const auto& dset){
                        auto edge = this->_econet.edges();
                        dset->write(edge.begin(), edge.end(), adptr1);
                        dset->write(edge.begin(), edge.end(), adptr2);
                    };
            };


        // .. Re-usable adaptor functions . . . . . . . . . . . . . . . . . . .
        // ... for _all_ entities (Population or Flow objects)
        const auto adptr_id =
            [](const auto& e){ return e->id; };
        const auto adptr_kind_id =
            [](const auto& e){ return e->kind_id(); };
        const auto adptr_active =
            [](const auto& e){
                return static_cast<char>(e->is_active());
            };

        // ... for graph-related objects, descriptor-based
        [[maybe_unused]] const auto adptr_passthru =
            [](const auto& d){ return d; };

        const auto adptr_source_pop_id =
            [this](const auto& e){
                return this->_econet[this->_econet.source(e)]->id;
            };
        const auto adptr_target_pop_id =
            [this](const auto& e){
                return this->_econet[this->_econet.target(e)]->id;
            };

        // ... _specifically_ for Populations
        const auto adptr_size =
            [](const auto& p){ return p->get_size(); };
        const auto adptr_rel_size =
            [](const auto& p){ return p->get_rel_size(); };
        const auto adptr_scaling_factor =
            [](const auto& p){ return p->get_scaling_factor(); };
        const auto adptr_divergence =
            [](const auto& p){ return p->get_divergence(); };
        const auto adptr_num_offspring =
            [](const auto& p){ return p->get_offspring_ids().size(); };
        const auto adptr_reservoir =
            [](const auto& p){
                // return p->get_reservoir();  // FIXME Use this approach!
                // NOTE Have to write out variably-sized due to frontend issues
                return to_vec(p->get_reservoir());
            };
        const auto adptr_reagents =
            [](const auto& p){
                // return p->get_reagents();
                return to_vec(p->get_reagents());
            };

        // ... _specifically_ for Source populations
        const auto adptr_total_refill =
            [](const auto& s){
                // return s->get_total_refill();
                return to_vec(s->get_total_refill());
            };


        // ... _specifically_ for Flows
        const auto adptr_effort =
            [](const auto& f){ return f->get_effort(); };
        const auto adptr_response =
            [](const auto& f){ return f->get_response(); };
        const auto adptr_consumption_ratio =
            [](const auto& f){ return f->get_consumption_ratio(); };
        const auto adptr_consumption =
            [](const auto& f){ return f->get_consumption(); };
        const auto adptr_resource_flow =
            [](const auto& f){ return f->get_resource_flow(); };


        // .. Max extent calculation . . . . . . . . . . . . . . . . . . . . .
        // 1D
        const auto max_extent_1d_write_ops =
            [](const auto num_write_ops) -> std::vector<hsize_t> {
                return {num_write_ops};
            };
        const auto max_extent_1d_vertices =
            [this](const auto) -> std::vector<hsize_t> {
                return {this->_econet.num_populations()};
            };
        const auto max_extent_1d_regular =
            [this](const auto) -> std::vector<hsize_t> {
                return {this->_econet.num_populations(this->_econet.filtered.only_regular)};
            };
        const auto max_extent_1d_source =
            [this](const auto) -> std::vector<hsize_t> {
                return {this->_econet.num_populations(this->_econet.filtered.only_source)};
            };

        const auto max_extent_1d_edges =
            [this](const auto) -> std::vector<hsize_t> {
                return {this->_econet.num_flows()};
            };

        // 2D
        [[maybe_unused]] const auto max_extent_2d_vertices =
            [this](const auto num_write_ops) -> std::vector<hsize_t> {
                return {num_write_ops, this->_econet.num_populations()};
            };
        const auto max_extent_2d_rspace_dims =
            [](const auto num_write_ops) -> std::vector<hsize_t> {
                return {num_write_ops, ResSpace::num_dims};
            };
        const auto max_extent_2d_regular =
            [this](const auto num_write_ops) -> std::vector<hsize_t> {
                return {num_write_ops,
                        this->_econet.num_populations(this->_econet.filtered.only_regular)};
            };
        [[maybe_unused]] const auto max_extent_2d_source =
            [this](const auto num_write_ops) -> std::vector<hsize_t> {
                return {num_write_ops,
                        this->_econet.num_populations(this->_econet.filtered.only_source)};
            };

        const auto max_extent_2d_edges =
            [this](const auto num_write_ops) -> std::vector<hsize_t> {
                return {num_write_ops, this->_econet.num_flows()};
            };
        const auto max_extent_2d_edges_width_2 =
            [this](const auto) -> std::vector<hsize_t> {
                return {2, this->_econet.num_flows()};
            };


        // .. Attribute writers . . . . . . . . . . . . . . . . . . . . . . . .

        const auto is_vertex_property_map =
            [](const auto& dset){
                dset->add_attribute("is_vertex_property", true);
            };
        const auto is_edge_property_map =
            [](const auto& dset){
                dset->add_attribute("is_edge_property", true);
            };
        const auto attrs_rspace_dims =
            [](const auto& dset){
                dset->add_attribute("dim_name__1", "resource");
                dset->add_attribute("coords_mode__resource", "trivial");
            };


        // ....................................................................
        // .. PROPERTY REGISTRATION - POPULATIONS .............................
        // .. Base . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

        // .. Regular . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

        // Register an ID mapping
        _dw.register_property("populations/regular/ids",
            // The mode of this property
            PropMode::mapping,
            // The write function, here generated by a helper function
            from_regular_populations(adptr_id),
            // The (required) max extent function, 1D: {size of container}
            max_extent_1d_regular
            // Optional: lambda to set additional attributes
            // Optional: lambda to calculate chunk sizes; not given -> auto
        );

        _dw.register_property("populations/regular/active",
            PropMode::mapped, "populations/regular/ids",
            from_regular_populations(adptr_active),
            max_extent_2d_regular
        );

        _dw.register_property("populations/regular/size",
            PropMode::mapped, "populations/regular/ids",
            from_regular_populations(adptr_size),
            max_extent_2d_regular
        );

        _dw.register_property("populations/regular/rel_size",
            PropMode::mapped, "populations/regular/ids",
            from_regular_populations(adptr_rel_size),
            max_extent_2d_regular
        );

        _dw.register_property("populations/regular/consumption_ratio",
            PropMode::mapped, "populations/regular/ids",
            from_regular_populations(adptr_consumption_ratio),
            max_extent_2d_regular
        );

        _dw.register_property("populations/regular/scaling_factor",
            PropMode::mapped, "populations/regular/ids",
            from_regular_populations(adptr_scaling_factor),
            max_extent_2d_regular
        );

        _dw.register_property("populations/regular/divergence",
            PropMode::mapped, "populations/regular/ids",
            from_regular_populations(adptr_divergence),
            max_extent_2d_regular
        );

        _dw.register_property("populations/regular/num_offspring",
            PropMode::mapped, "populations/regular/ids",
            from_regular_populations(adptr_num_offspring),
            max_extent_2d_regular
        );

        _dw.register_property("populations/regular/reservoir",
            PropMode::mapped, "populations/regular/ids",
            from_regular_populations(adptr_reservoir),
            max_extent_2d_regular
        );

        _dw.register_property("populations/regular/reagents",
            PropMode::mapped, "populations/regular/ids",
            from_regular_populations(adptr_reagents),
            max_extent_2d_regular
        );

        // .. Source . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

        if (_econet.num_populations(_econet.filtered.only_source) > 0)
        {
        _dw.register_property("populations/source/ids",
            PropMode::mapping,
            from_source_populations(adptr_id),
            max_extent_1d_source
        );

        _dw.register_property("populations/source/size",
            PropMode::mapped, "populations/source/ids",
            from_source_populations(adptr_size),
            max_extent_2d_source
        );

        _dw.register_property("populations/source/consumption_ratio",
            PropMode::mapped, "populations/source/ids",
            from_source_populations(adptr_consumption_ratio),
            max_extent_2d_source
        );

        _dw.register_property("populations/source/scaling_factor",
            PropMode::mapped, "populations/source/ids",
            from_source_populations(adptr_scaling_factor),
            max_extent_2d_source
        );

        _dw.register_property("populations/source/reservoir",
            PropMode::mapped, "populations/source/ids",
            from_source_populations(adptr_reservoir),
            max_extent_2d_source
        );

        _dw.register_property("populations/source/total_refill",
            PropMode::mapped, "populations/source/ids",
            from_source_populations(adptr_total_refill),
            max_extent_2d_source
        );
        }


        // .. PROPERTY REGISTRATION - FLOWS ...................................

        // .. Base . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

        // .. Trophic Flows . . . . . . . . . . . . . . . . . . . . . . . . . .

        _dw.register_property("flows/trophic/ids",
            PropMode::mapping,
            from_flows(adptr_id),
            max_extent_1d_edges
        );

        _dw.register_property("flows/trophic/active",
            PropMode::mapped, "flows/trophic/ids",
            from_flows(adptr_active),
            max_extent_2d_edges
        );

        _dw.register_property("flows/trophic/effort",
            PropMode::mapped, "flows/trophic/ids",
            from_trophic_flows(adptr_effort),
            max_extent_2d_edges
        );

        _dw.register_property("flows/trophic/response",
            PropMode::mapped, "flows/trophic/ids",
            from_trophic_flows(adptr_response),
            max_extent_2d_edges
        );

        _dw.register_property("flows/trophic/consumption",
            PropMode::mapped, "flows/trophic/ids",
            from_trophic_flows(adptr_consumption),
            max_extent_2d_edges
        );

        _dw.register_property("flows/trophic/resource_flow",
            PropMode::mapped, "flows/trophic/ids",
            from_trophic_flows(adptr_resource_flow),
            max_extent_2d_edges
        );


        // .. PROPERTY REGISTRATION - NETWORK STRUCTURE .......................
        // NOTE This assumes the group at path "network/" being a graph group

        _dw.register_property("network/_vertices",
            PropMode::mapping,
            from_populations(adptr_id),
            max_extent_1d_vertices
        );

        _dw.register_property("network/edge_ids",
            PropMode::mapping,
            from_flows(adptr_id),
            max_extent_1d_edges
        );

        _dw.register_property("network/_edges",
            PropMode::attached_to_mapping, "network/edge_ids",
            from_edges_twopass(adptr_source_pop_id, adptr_target_pop_id),
            max_extent_2d_edges_width_2,
            // Custom dataset attributes
            [](const auto& dset){
                // Write all manually
                dset->add_attribute("dim_name__0", "label");
                dset->add_attribute("coords_mode__label", "values");
                dset->add_attribute("coords__label",
                                    std::array<std::string, 2>{"source",
                                                               "target"});

                dset->add_attribute("dim_name__1", "idx");
                dset->add_attribute("coords_mode__idx", "trivial");
            },
            false // Disable writing of default dataset attributes
        );


        // .. PROPERTY REGISTRATION - NETWORK PROPERTY MAPS ...................

        _dw.register_property("network/vertex_kinds",
            PropMode::attached_to_mapping, "network/_vertices",
            from_populations(adptr_kind_id),
            max_extent_1d_vertices,
            // Leave dataset attributes as they are ...
            empty_attrs, true,
            // ... but need to write additional group attributes
            is_vertex_property_map
        );

        _dw.register_property("network/edge_kinds",
            PropMode::attached_to_mapping, "network/edge_ids",
            from_flows(adptr_kind_id),
            max_extent_1d_edges,
            empty_attrs, true, is_edge_property_map
        );


        // .. PROPERTY REGISTRATION - GLOBAL ECONET PROPERTIES ................

        _dw.register_property("econet/available_resources",
            PropMode::standalone,
            [this](const auto& dset){
                auto& econet = this->_econet;
                dset->write(
                    econet.available_resources(econet.filtered.only_regular)
                );
            },
            // NOTE Not using structured dtype here but writing as 2D array
            max_extent_2d_rspace_dims,
            attrs_rspace_dims
        );

        _dw.register_property("econet/total_losses",
            PropMode::standalone,
            [this](const auto& dset){
                dset->write(this->_econet.total_losses());
            },
            // NOTE Not using structured dtype here but writing as 2D array
            max_extent_2d_rspace_dims,
            attrs_rspace_dims
        );

        _dw.register_property("econet/total_mass",
            PropMode::standalone,
            [this](const auto& dset){
                auto& econet = this->_econet;
                dset->write(
                    econet.total_mass(econet.filtered.only_regular)
                );
            },
            max_extent_1d_write_ops
        );

        _dw.register_property("econet/mean_composition",
            PropMode::standalone,
            [this](const auto& dset){
                auto& econet = this->_econet;
                dset->write(
                    econet.mean_composition(econet.filtered.only_regular)
                );
            },
            // NOTE Not using structured dtype here but writing as 2D array
            max_extent_2d_rspace_dims,
            attrs_rspace_dims
        );


        // .. Network Topology . . . . . . . . . . . . . . . . . . . . . . . .

        _dw.register_property("econet/num_vertices",
            PropMode::standalone,
            [this](const auto& dset){
                dset->write(this->_econet.num_populations());
            },
            max_extent_1d_write_ops
        );

        _dw.register_property("econet/num_edges",
            PropMode::standalone,
            [&](const auto& dset){
                dset->write(this->_econet.num_flows());
            },
            max_extent_1d_write_ops
        );

        // TODO add more topology-related properties here, e.g.:
        //          - diameter?
        //          - number of connected components / clusters
        //          - ...

        // .. Finished. . . . . . . . . . . . . . . . . . . . . . . . . . . . .
        this->_log->info("Property registration finished.");
    }

    /// Registers triggers and actions with the ActionManager
    void setup_action_manager () {
        using Time = typename Base::Time;

        // .. Triggers ........................................................
        _am.register_trigger("time",
            [this](const auto& cfg){
                return get_as<Time>("time", cfg) == this->get_time();
            }
        );
        _am.register_trigger("times",
            // Very naive implementation, slow if many times are given
            [this](const auto& cfg){
                using Times = std::vector<Time>;

                return contains(get_as<Times>("times", cfg), this->get_time());
            }
        );
        _am.register_trigger("time_multiples",
            [this](const auto& cfg){
                return (this->get_time() % get_as<Time>("n", cfg) == 0);
            }
        );
        _am.register_trigger("time_slice",
            [this](const auto& cfg){
                const auto start = get_as<Time>("start", cfg, 0);
                const auto stop = get_as<Time>("stop", cfg,
                                               this->get_time_max() + 1);
                const auto step = get_as<Time>("step", cfg);
                const auto time = this->get_time();
                return
                    time >= start and (time-start) % step == 0 and time < stop;
            }
        );


        // .. Actions .........................................................
        _am.register_action("add_populations",
            [this](const auto& cfg){
                const auto num = get_as<int>("num", cfg);
                const auto kind = get_as<std::string>("kind", cfg);
                const auto pop_cfg = recursive_update(
                    get_as<Config>(kind, this->_defaults["populations"]),
                    get_as<Config>("init", cfg, {})
                );
                this->_econet.add_populations(num, kind, pop_cfg);
            }
        );
        _am.register_action("add_regular_populations",
            [this](const auto& cfg){
                const auto num = get_as<int>("num", cfg, 1);
                const auto pop_cfg = recursive_update(
                    get_as<Config>("regular", this->_defaults["populations"]),
                    get_as<Config>("init", cfg, {})
                );
                this->_econet.template add_populations<Population>(num,
                                                                   pop_cfg);
            }
        );
        _am.register_action("add_source_populations",
            [this](const auto& cfg){
                const auto num = get_as<int>("num", cfg, 1);
                const auto pop_cfg = recursive_update(
                    get_as<Config>("source", this->_defaults["populations"]),
                    get_as<Config>("init", cfg, {})
                );
                this->_econet.template add_populations<Source>(num, pop_cfg);
            }
        );

        _am.register_action("add_flows",
            [this](const auto& cfg){
                auto& econet = this->_econet;

                const auto select = get_as<Config>("select", cfg);
                const auto filter = get_as<std::string>("filter", cfg);
                const auto direction = get_as<std::string>("direction", cfg);

                const auto kind = get_as<std::string>("kind", cfg, "trophic");
                const auto flow_cfg = recursive_update(
                    get_as<Config>(kind, this->_defaults["flows"]),
                    get_as<Config>("init", cfg, {})
                );

                if (kind != "trophic") {
                    throw std::invalid_argument(fmt::format(
                        "add_flows action currently only supports 'trophic' "
                        "flows, but got kind '{}'!", kind
                    ));
                }

                if (direction == "incoming") {
                    for (const auto v
                         : econet.vertices(econet.filtered.get(filter)))
                    {
                        econet.template add_flows<incoming>(v, select,
                                                            flow_cfg);
                    }
                }
                else {
                    for (const auto v
                         : econet.vertices(econet.filtered.get(filter)))
                    {
                        econet.template add_flows<outgoing>(v, select,
                                                            flow_cfg);
                    }
                }
            }
        );

        _am.register_action("add_population_and_connect_upstream",
            [this](const auto& cfg){
                if (   cfg["num"]
                    or cfg["target_nw"]
                    or cfg["init"]
                    or cfg["kind"]
                    or cfg["select"]["target"])
                {
                    throw std::invalid_argument(
                        "Received one or more of the following invalid keys "
                        "in the `add_population_and_connect_upstream` action: "
                        "`num`, `kind`, `target_nw`, `init`, "
                        "`select -> target`."
                    );
                }

                // Add the population
                const auto pop_cfg = recursive_update(
                    get_as<Config>("regular", this->_defaults["populations"]),
                    get_as<Config>("pop_init", cfg, {})
                );
                const auto u = this->_econet.add_population(pop_cfg);

                // Connect it, restricting the target network to the sole
                // vertex that was just added.
                auto select_cfg = get_as<Config>("select", cfg);
                select_cfg["target"] = Config{};
                select_cfg["target"]["mode"] = "all";

                const auto flow_cfg = recursive_update(
                    get_as<Config>("trophic", this->_defaults["flows"]),
                    get_as<Config>("flow_init", cfg, {})
                );

                auto& econet = this->_econet;
                econet.connect(
                    econet.filtered.get(get_as<std::string>("source_nw", cfg)),
                    econet.filtered(boost::keep_all(),
                                    [u](auto d){ return (u == d); }),
                    select_cfg,
                    flow_cfg
                );
            }
        );

        _am.register_action("connect",
            [this](const auto& cfg){
                auto& econet = this->_econet;

                const auto kind = get_as<std::string>("kind", cfg);
                const auto flow_cfg = recursive_update(
                    get_as<Config>(kind, this->_defaults["flows"]),
                    get_as<Config>("init", cfg, {})
                );

                econet.connect(
                    kind,
                    econet.filtered.get(get_as<std::string>("source_nw", cfg)),
                    econet.filtered.get(get_as<std::string>("target_nw", cfg)),
                    get_as<Config>("select", cfg),
                    flow_cfg
                );
            }
        );

        _am.register_action("set_splitting",
            [this](const auto& cfg){
                this->_splitting_enabled = get_as<bool>("enabled", cfg);
                this->_log->info(
                    "Set splitting: {}.",
                    this->_splitting_enabled ? "enabled" : "disabled"
                );
            }
        );

        _am.register_action("update_foraging_efforts_and_responses",
            [this](const auto& cfg){
                auto& econet = this->_econet;

                const auto max_n = get_as<unsigned>("max_n", cfg);
                const auto rtol = get_as<double>("rtol", cfg);

                const auto n =
                    econet.update_foraging_efforts_and_responses(max_n, rtol);

                this->_log->info(
                    "Balanced foraging efforts and responses to a relative "
                    "tolerance of {:f}. Needed {:d} iterations.", rtol, n
                );
            }
        );


        // .. Procedures and Hooks ............................................
        const auto& setup_cfg = this->_cfg["setup"];
        if (setup_cfg["procedures"]) {
            for (const auto& kv : setup_cfg["procedures"]) {
                _am.register_procedure(kv.first.template as<std::string>(),
                                       kv.second);
            }
        }

        // From a bunch of scenarios, use a specific one for the setup hook.
        // This allows to easily switch between them.
        // If `use_scenario` is not given or Null, still register the hook, but
        // specify an empty procedure sequence.
        auto procedure_seq = ActionManager::ProcedureSequence {};

        if (setup_cfg["use_scenario"] and
            not setup_cfg["use_scenario"].IsNull())
        {
            const auto scenario = get_as<std::string>("use_scenario",
                                                      setup_cfg);
            this->_log->info("Using scenario '{}' for setup.", scenario);

            const auto& scenarios = get_as<Config>("scenarios", setup_cfg);
            procedure_seq =
                get_as<ActionManager::ProcedureSequence>(scenario, scenarios);
        }
        else {
            this->_log->info("No setup scenario specified.");
        }

        _am.register_hook("setup_econet", procedure_seq);
    }

};


} // namespace Utopia::Models::EEcosy

#endif // UTOPIA_MODELS_EECOSY_HH
