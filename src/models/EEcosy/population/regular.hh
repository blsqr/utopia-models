#ifndef UTOPIA_MODELS_EECOSY_POPULATION_REGULAR_HH
#define UTOPIA_MODELS_EECOSY_POPULATION_REGULAR_HH

#include <cmath>
#include <vector>
#include <limits>
#include <random>

#include <UtopiaUtils/utils/cfg.hh>
#include <UtopiaUtils/utils/math.hh>

#include "../utils/casting.hh"
#include "../utils/yaml.hh"
#include "../utils/formatters.hh"

#include "../reactions.hh"
#include "base.hh"


namespace Utopia::Models::EEcosy {


/// Regular population class, used for most entities of the ecological network
template<class _ResSpace, class _RNG>
class Population : public BasePopulation<_ResSpace, _RNG> {
public:
    using Base = BasePopulation<_ResSpace, _RNG>;

    /// Export the resource space
    using ResSpace = _ResSpace;

    /// Export the RNG type
    using RNG = _RNG;

    /// Type for resource vectors
    using ResVec = typename ResSpace::ResVec;

    /// Specialized reaction set type
    using ReactionSet = Models::EEcosy::ReactionSet<ResSpace>;

    /// A probability distribution type
    using ProbDistr = std::uniform_real_distribution<double>;


private:

    /// A string describing the kind of population type this class represents
    inline static const std::string __regular_kind = "regular";


public:
    // .. Properties (fixed during life time) .................................

    /// The resources (quantity and quality) each individual is made up of
    const ResVec composition;

    /// The composition, but unmasked (i.e.: NaNs replaced with zeros)
    const ResVec composition_unmasked;

    /// Reservoir leak factor
    const double reservoir_leak;

    /// Reservoir use factor; part of the reservoir available for reproduction
    const double reservoir_use;

    /// Mortality; evaluated probabilistically, relative to population size
    const double mortality;

    /// The size *below* which the population becomes extinct
    const double extinction_size;

    /// Parameters used to determine costs
    const Config costs;

    /// Divergence parameters
    const Config div_params;

    /// Splitting parameters
    const Config split_params;

    /// The metabolic reactions this population is capable of
    const ReactionSet reactions;

    /// Specific mass per individual; computed from composition vector
    const double mass;

    /// Size scale value; used to determine a relative population size
    const double size_scale;

    /// Sustenance cost per unit time to keep each individual alive
    const ResVec sustenance_cost;

    /// The sustenance cost, but unmasked (i.e.: NaNs replaced with zeros)
    const ResVec sustenance_cost_unmasked;

    /// ID of the parent population
    const IDType parent_id;


protected:
    // .. State variables and observables .....................................
    /// The population's size, i.e. it's number of individuals
    /** \note It's of little importance whether this is represented as an
      *       integer or a floating point value; using a float is easier in
      *       some parts of the implementation, though.
      */
    double size;

    /// Ratio of active size and actual consumption
    double consumption_ratio;

    /// Scaling factor for an interaction
    double scaling_factor;

    /// The reagents reservoir
    ResVec reagents;

    /// The reservoir of processed resources, used for reproduction
    ResVec reservoir;

    /// The current divergence potential
    double divergence;

    /// IDs of offspring populations
    std::vector<IDType> offspring_ids;


public:
    // .. Constructors ........................................................
    /// Construct a generic Population from a given configuration
    Population (const std::shared_ptr<spdlog::logger>& parent_logger,
                const std::shared_ptr<RNG>& rng,
                const ResSpace& rspace,
                const Config& setup_cfg,
                const std::string& logger_suffix = "")
    :
        // Initialize base class, specifying a custom logger suffix
        Base(parent_logger, rng, rspace, setup_cfg,
             logger_suffix.empty() ? kind() : logger_suffix),

        // Carry over explicit parameters from configuration
        composition(
            rspace.template get_as<ResVec, clean_and_mask>("composition",
                                                           this->cfg)
        ),
        composition_unmasked(
            apply_operations<unmask_NaNs>(composition)
        ),

        reservoir_leak(get_as<double>("reservoir_leak", this->cfg)),
        reservoir_use(get_as<double>("reservoir_use", this->cfg)),
        mortality(get_as<double>("mortality", this->cfg)),
        extinction_size(get_as<double>("extinction_size", this->cfg)),

        // ... and parameter bundles
        costs(get_as<Config>("costs", this->cfg)),
        div_params(get_as<Config>("div_params", this->cfg)),
        split_params(get_as<Config>("split_params", this->cfg)),

        // The reactions
        reactions(),

        // From these, compute some properties (fixed during life time)
        mass(compute_mass(rspace)),
        size_scale(compute_size_scale()),

        sustenance_cost(compute_sustenance_cost()),
        sustenance_cost_unmasked(
            apply_operations<unmask_NaNs>(sustenance_cost)
        ),

        // Keep track of the parent's ID, if one is specified
        parent_id(get_as<IDType>("parent_id", this->cfg, 0)),

        // Initialize state variables
        size(get_as<double>("size", this->cfg)),
        consumption_ratio(std::numeric_limits<double>::quiet_NaN()),
        scaling_factor(std::numeric_limits<double>::quiet_NaN()),
        reagents{ResSpace::zeros()},
        reservoir(rspace.template get_as<ResVec>("reservoir", this->cfg)),
        divergence(std::numeric_limits<double>::quiet_NaN()),

        // Genealogy-related information
        offspring_ids{}
    {}

    // .. Getters .............................................................

    /// A description of this type, mainly for log messages
    const std::string& kind () const override {
        return __regular_kind;
    }

    /// A numeric identifier of this type, mainly for writing data
    char kind_id () const override {
        return 1;
    }

    /// The population size
    double get_size () const {
        return size;
    }

    /// The relative population size, calculated using the size scale
    double get_rel_size () const {
        return size / size_scale;
    }

    /// The consumption ratio
    double get_consumption_ratio () const {
        return consumption_ratio;
    }

    /// The scaling factor
    double get_scaling_factor () const {
        return scaling_factor;
    }

    /// The current value of the reagents reservoir
    const ResVec& get_reagents () const {
        return reagents;
    }

    /// The current value of the general resource reservoir
    const ResVec& get_reservoir () const {
        return reservoir;
    }

    /// The divergence potential
    double get_divergence () const {
        return divergence;
    }

    /// The offspring IDs
    const auto& get_offspring_ids () const {
        return offspring_ids;
    }

    /// Retrieve the properties for recreating this population
    Config get_properties () const override {
        auto props = Base::get_properties();

        // Set entries that initialize tracking variables to current values
        props["size"] = size;
        props["divergence"] = divergence;

        props["reagents"] = to_vec(reagents);
        props["composition"] = to_vec(composition);
        props["reservoir"] = to_vec(reservoir);

        return props;
    }


    // .. Setters .............................................................
    // FIXME Logging armadillo vectors fails with gcc-10

    /// Adds the ID of a new offspring
    void register_offspring_id (IDType id) {
        offspring_ids.push_back(id);
    }

    /// Called when the population is deactivated
    void __deactivate () override {}

    // .. Absolute . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

    /// Set the size to the given value
    void set_size (double new_size) {
        size = new_size;
        this->log->trace("Set size:          {:.3g}", size);
        __check_size();
    }

    /// Sets the value of the scaling factor
    void set_scaling_factor (double new_value) {
        scaling_factor = new_value;
    }

    /// Sets the reagents to the given value
    void set_reagents (const ResVec& new_value) {
        reagents = new_value;
        // this->log->trace("Set reagents:      {}", reagents);
    }

    /// Sets the reservoir to the given value
    void set_reservoir (const ResVec& new_value) {
        reservoir = new_value;
        // this->log->trace("Set reservoir:     {}", reservoir);
    }


    // .. Relative . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

    /// Adjusts the size by adding the given value
    void adjust_size (double delta_size) {
        size += delta_size;
        this->log->trace("Adjusted size:     {:.3g}  (delta: {:+.3g})",
                         size, delta_size);
        __check_size();
    }

    /// Adjusts the reagents by adding the given value
    void adjust_reagents (const ResVec& delta) {
        reagents += delta;
        // this->log->trace("Adjusted reagents:      {}", reagents);
    }

    /// Adjusts the reservoir by adding the given value
    void adjust_reservoir (const ResVec& delta) {
        reservoir += delta;
        // this->log->trace("Adjusted reservoir:     {}", reservoir);
    }


    // .. Dynamics ............................................................
    // .. High-level . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

    /// Perform population-internal metabolic processes
    /** Performs the conversion reactions from the reagents to the reservoir.
      * Additionally, this is where the reservoir leak is applied.
      *
      * \TODO implement properly; currently only moves resources over from the
      *       reagents to the reservoir
      *
      * \returns The resources expended or lost as part of this process.
      */
    virtual ResVec perform_metabolic_processes (const double dt) {
        auto losses = ResVec{}.fill(0.);

        // TODO metabolic processes and their losses
        //      ... currently: directly transfers the resources.
        // FIXME Reservoir goes to NaN in some scenarios… presumably here.
        adjust_reservoir(+reagents);
        reagents *= 0.;

        // -- Reservoir leak
        if (reservoir_leak > 0.) {
            const auto leak = dt * reservoir_leak * reservoir;
            adjust_reservoir(-leak);
            losses += leak;
        }

        return losses;
    }

    /// Expend sustenance costs
    virtual ResVec expend_sustenance_costs (const double dt,
                                            std::shared_ptr<RNG>&)
    {
        const ResVec total_cost =
            ResVec(dt * size * sustenance_cost_unmasked).clean(PREC);

        if (arma::all(total_cost <= reservoir)) {
            this->log->trace("Had enough resources available in reservoir.");
            adjust_reservoir(-total_cost);
            return total_cost;
        }
        // else:    Higher costs than resources available in the reservoir
        //       => Need to reduce population size
        this->log->trace("Did not have enough resources in reservoir.");

        // Compute how many _could_ be sustained with the current reservoir
        const auto sustainable_size =
            smallest_quotient<use_absolute>(reservoir, sustenance_cost);
        this->log->trace("size:             {}", size);
        this->log->trace("sustainable size: {}", sustainable_size);

        // TODO Consider implementing some randomness here

        // Let this be the new size and adjust reservoir accordingly
        set_size(sustainable_size);
        const ResVec adjusted_cost =
            sustainable_size * sustenance_cost_unmasked;
        adjust_reservoir(-adjusted_cost);

        return adjusted_cost;
    }

    /// Perform (internal) population dynamics: reproduction, death
    /** For reproduction: Computes, how often the composition vector is
      * contained in the reservoir, using the smallest_quotient function.
      * Then, taking into account the reservoir_use parameter, computes and
      * applies the population size change.
      * Resources from the reservoir are directly deduced.
      *
      * For death: A fixed fraction of the _new_ population size is chosen to
      * die.
      *
      * \TODO    Let death be stochastic.
      * \TODO    Let birth cost something?
      * \TODO    Propagate resources away? Part of reservoir as well?
      *
      * \returns Resources lost through death
      */
    virtual ResVec perform_population_dynamics (const double dt,
                                                std::shared_ptr<RNG>&)
    {
        // -- Reproduction
        const auto r = smallest_quotient<use_absolute>(reservoir, composition);
        const auto num_born = reservoir_use * r;
        this->log->trace("    num_born:  {:>+7.1f}  (b: {}, r: {:.4g})",
                         num_born, reservoir_use, r);

        // -- Death (currently: fixed fraction, TODO: stochastic)
        const auto num_died = dt * size * mortality;
        this->log->trace("    num_died:  {:>+7.1f}", num_died);

        // Apply the changes
        adjust_size(num_born - num_died);

        if (num_born > 0.) {
            adjust_reservoir(-num_born * composition_unmasked);
        }

        return num_died * composition_unmasked;
    }

    // .. Lower level . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

    /// Computes and updates the foraging effort of all incoming trophic flows
    /** Iterates over all incoming flows and aggregates their combined foraging
      * strength. Then iterates over the flows again and normalizes the
      * foraging effort to 1 using the sum of the foraging strength.
      *
      * \param  in_flows  The incoming trophic flows to this population, i.e.
      *                   coming from all the prey of this population.
      */
    template<class EcoNet, class RangeIt>
    void update_foraging_efforts (const EcoNet& econet, RangeIt&& in_flows) {
        double total_effort = 0.;

        for (const auto& [e, flow] : in_flows) {
            const auto& prey = as<Population>(econet[econet.source(e)]);
            total_effort += (prey->mass * flow->get_response());
        }

        for (const auto& [e, flow] : in_flows) {
            const auto& prey = as<Population>(econet[econet.source(e)]);
            flow->update_foraging_effort(prey->mass, total_effort,
                                         econet.min_effort);
        }
        // TODO Improve numerics here?
    }

    /// Updates the consumption ratio, given a projected consumption
    double update_consumption_ratio (const double projected_consumption) {
        consumption_ratio = fabs(projected_consumption) / size;
        this->log->trace("  consumption_ratio:  {:.3g}", consumption_ratio);
        return consumption_ratio;
    }

    /// Updates the divergence state variable
    /**
      * \TODO more efficient implementation, e.g. as pre-evaluated lambda
      */
    virtual void update_divergence () {
        const auto base_rate = get_as<double>("base_rate", div_params);
        const auto stability = get_as<double>("stability", div_params);

        divergence = base_rate + std::sqrt(get_rel_size()) / stability;
    }

    /// Check whether the population's size is below the extinction threshold
    virtual bool check_extinction () const {
        return (size < extinction_size);
    }

    /// Check whether this population will diverge
    virtual bool check_divergence (const double dt,
                                   const std::shared_ptr<RNG>& rng,
                                   ProbDistr& prob_distr) const
    {
        const auto threshold = get_as<double>("threshold", div_params);
        if (divergence < threshold) {
            return false;
        }

        return (prob_distr(*rng) < dt * divergence);
    }


    // .. Setup methods .......................................................

    /// Compute the specific mass
    /** This is the 1-norm of the composition of this population, weighted by
      * the resource dimension weights.
      */
    double compute_mass (const ResSpace& rspace) const {
        return arma::norm(rspace.weights % composition_unmasked, 1);
    }

    /// Compute the size scale as (size_scale parameter / mass)
    double compute_size_scale () const {
        return get_as<double>("size_scale", this->cfg) / mass;
    }

    /// Compute sustenance cost
    /** The result may include NaNs, which denote that no such resource is
      * required for sustenance.
      */
    ResVec compute_sustenance_cost () const {
        ResVec sc;

        // The base cost, a multiple of the composition
        sc = get_as<double>("base", costs) * composition;

        // Depending on metabolic capabilities, additional costs
        // TODO

        return sc;
    }


    // .. Helpers .............................................................

    /// Throws if the size is negative
    /** \TODO Consider NOT calling this if NOT in debug mode
      */
    void __check_size () const {
        if (this->size < -PREC) {
            throw std::runtime_error(fmt::format(
                "Size of {} population {:d} was set to a negative value! "
                "{} < {}", this->kind(), this->id, this->size, -PREC
            ));
        }
    }
};


} // namespace Utopia::Models::EEcosy

#endif // UTOPIA_MODELS_EECOSY_POPULATION_REGULAR_HH
