#ifndef UTOPIA_MODELS_EECOSY_POPULATION_SOURCE_HH
#define UTOPIA_MODELS_EECOSY_POPULATION_SOURCE_HH

#include <cmath>
#include <limits>
#include <random>
#include <functional>

#include <yaml-cpp/yaml.h>
#include <spdlog/spdlog.h>

#include <UtopiaUtils/utils/cfg.hh>
#include <UtopiaUtils/utils/math.hh>

#include "../utils/casting.hh"

#include "regular.hh"


namespace Utopia::Models::EEcosy {


/// A source population is a special population modelling system energy input
/** Basically, it is a Population that does neither get extinct nor have any
  * particularly interesting population dynamics. Thus, it can be embedded into
  * the network just like a regular Population, but it performs dynamics that
  * model some external energy input.
  */
template<class _ResSpace, class _RNG>
class Source : public Population<_ResSpace, _RNG> {
public:
    using Base = Population<_ResSpace, _RNG>;

    /// Export the resource space
    using ResSpace = _ResSpace;

    /// Export the RNG type
    using RNG = _RNG;

    /// Type for resource vectors
    using ResVec = typename ResSpace::ResVec;

    /// Type of the functional that computes the reservoir refill
    using RefillFunc = std::function<ResVec()>;

    /// A probability distribution type
    using ProbDistr = typename Base::ProbDistr;

private:

    /// A string describing the kind of population type this class represents
    inline static const std::string __source_kind = "source";


public:
    // .. Properties (fixed during life time) .................................
    /// A functional that computes the reservoir refill
    const RefillFunc refill_func;


protected:
    // .. State variables and observables .....................................
    ResVec total_refill;


public:
    // .. Constructors ........................................................
    /// Construct a source population
    Source (const std::shared_ptr<spdlog::logger>& parent_logger,
            const std::shared_ptr<RNG>& rng,
            const ResSpace& rspace,
            const Config& setup_cfg,
            const std::string& logger_suffix = "")
    :
        // Initialize via base class, specifying a custom logger suffix, and
        // setting some parameters that are required by the base class but are
        // not strictly needed for the Source population
        Base(parent_logger, rng, rspace, prepare_params(setup_cfg),
             logger_suffix.empty() ? kind() : logger_suffix),

        // Construct the refill function
        refill_func(construct_refill_func(rspace)),

        // And an empty aggregation vector
        total_refill(rspace.zeros())
    {}


    // .. Getters .............................................................

    /// A description of this type, mainly for log messages
    const std::string& kind () const override {
        return __source_kind;
    }

    /// A numeric identifier of this type, mainly for writing data
    char kind_id () const override {
        return 2;
    }

    const ResVec& get_total_refill () const {
        return total_refill;
    }


    // .. Setters .............................................................



    // .. Dynamics ............................................................
    // .. High-level . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

    /// "Metabolism" of Source populations: refilling
    /** Invokes the refill function and adjusts the reservoir size by its
      * return value.
      * The reagents are untouched by this method.
      *
      * The losses returned here are always zero.
      */
    ResVec perform_metabolic_processes (const double dt) override {
        const auto delta = refill_func();
        this->adjust_reservoir(dt * delta);
        total_refill += dt * delta;

        return ResVec{}.fill(0.);
    }

    /// Expend sustenance costs: none for a Source population, no losses
    ResVec expend_sustenance_costs (const double,
                                    std::shared_ptr<RNG>&) override
    {
        return ResVec{}.fill(0.);
    }

    /// Perform (internal) population dynamics
    /**
      *
      * The losses returned here are always zero.
      */
    ResVec perform_population_dynamics (const double,
                                        std::shared_ptr<RNG>&) override
    {
        // -- Reproduction (same as regular population)
        const auto r = smallest_quotient<use_absolute>(this->reservoir,
                                                       this->composition);
        const auto num_born = this->reservoir_use * r;
        this->log->trace("     num_born:  {:>+7.1f}  (b: {}, r: {:.4g})",
                         num_born, this->reservoir_use, r);

        // -- NO death

        // -- Apply the changes
        this->size += num_born;
        this->reservoir -= (num_born * this->composition);

        return ResVec{}.fill(0.);
    }


    // .. Lower level . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

    /// The source population's divergence is of no importance; does nothing
    void update_divergence () override {}

    /// A source population never gets extinct; this always returns false
    bool check_extinction () const override {
        return false;
    }

    /// A source population never diverges; this always returns false
    bool check_divergence (const double,
                           const std::shared_ptr<RNG>&,
                           ProbDistr&) const override
    {
        return false;
    }


    // .. Setup methods .......................................................

    /// Sets some default parameters that make setup easier
    inline static Config prepare_params (const Config& setup_cfg) {
        // Work on a deep copy
        auto cfg = YAML::Clone(setup_cfg);

        // Set default values
        // NOTE In yaml-cpp 0.6.2, setting these via std::numeric_limits will
        //      fail when reading it back as double.
        cfg["mortality"] = ".nan";
        cfg["extinction_size"] = ".nan";
        cfg["costs"] = Config{};
        cfg["costs"]["base"] = 0.;
        cfg["div_params"] = Config{};
        cfg["split_params"] = Config{};

        return cfg;
    }

    /// Constructs and returns the refill function lambda
    /** Which function is chosen depends on the `refill_function` key in the
      * configuration.
      * The value returned by these functions is added to the reservoir when
      * the "metabolic processes" are carried out.
      *
      *     - `constant_rate`: adds a constant resource vector each time
      *     - `constant_reservoir`: refills the reservoir to a constant value
      *     - `constant_size`: refills only as many resources as are needed to
      *           grow to the specified `target_size`
      *     - `saturating`: uses the shifted, scaled, & shrunk tanh derivative
      *           and multiplies it with the current value of the reservoir to
      *           arrive at the value that is to be added
      *
      * \note  Refill functions should return a ResVec that does NOT contain
      *        NaN values.
      *
      * For a speed comparison of different sigmoidal function derivatives, see
      *     http://quick-bench.com/h_qdvCg4-TfLZRfXh-boQlGi5kc
      */
    RefillFunc construct_refill_func (const ResSpace& rspace) const {
        const auto name = get_as<std::string>("refill_function", this->cfg);
        const auto params = get_as<Config>("refill_params", this->cfg);

        this->log->info("Selected refill function:  {}", name);

        if (name == "constant_rate") {
            auto refill = ResVec{};

            if (params[name]["value"].IsScalar()) {
                refill =
                    get_as<double>("value", params[name]) * this->composition;
            }
            else {
                refill =
                    rspace.template get_as<ResVec>("value", params[name]);
            }

            return [refill{std::move(refill)}](){
                return refill;
            };
        }
        else if (name == "constant_reservoir") {
            auto target = ResVec{};

            if (params[name]["value"].IsScalar()) {
                target =
                    get_as<double>("value", params[name]) * this->composition;
            }
            else {
                target =
                    rspace.template get_as<ResVec>("value", params[name]);
            }

            return [this, target{std::move(target)}](){
                return target - this->get_reservoir();
            };
        }
        else if (name == "constant_size") {
            double target_size = get_as<double>("target_size", params[name]);

            return [this, target_size{std::move(target_size)}](){
                return (
                      std::max(0., target_size - this->get_size())
                    * this->composition_unmasked
                );
            };
        }
        else if (name == "saturating") {
            auto scale = get_as<double>("scale", params[name]);
            auto shrink = get_as<double>("shrink", params[name]);

            return [this, scale, shrink](){
                return this->reservoir * scale * (
                    1. - pow(tanh((this->get_rel_size() - 1.) * shrink), 2)
                );
            };
        }
        else {
            throw std::invalid_argument(fmt::format(
                "Invalid growth function name '{}'! Available: saturating, "
                "constant_rate, constant_reservoir, constant_size",
                name
            ));
        }
    }

};


} // namespace Utopia::Models::EEcosy

#endif // UTOPIA_MODELS_EECOSY_POPULATION_SOURCE_HH
