#ifndef UTOPIA_MODELS_EECOSY_POPULATION_BASE_HH
#define UTOPIA_MODELS_EECOSY_POPULATION_BASE_HH

#include <sstream>
#include <limits>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include "../utils.hh"
#include "../types.hh"
#include "../resource_space.hh"


namespace Utopia::Models::EEcosy {

using Utopia::Utils::apply_noise;


/// Base class for populations
/** This object is intended to be associated with the bundled vertex properties
  * of the ecological interaction network.
  * The derived classes implement the actual dynamics, while this class defines
  * the shared interface.
  *
  * \NOTE \parblock
  *       The population needs to act averse to the underlying data structure,
  *       i.e. the underlying BGL graph. It should exclusively work with other
  *       population objects.
  *
  *       This is important especially due to the nature of BGL vertex
  *       descriptors: These may change throughout the life time of the graph
  *       and thus become invalidated.
  *       Thus, a Population instance should NEVER store a vertex descriptor
  *       but do all graph-related actions with descriptors as arguments.
  *
  *       Ideally, actions that occur directly on the graph should be avoided,
  *       but instead the retrieval of a Flow or Population object should
  *       occur elsewhere.
  *
  *       \endparblock
  */
template<class _ResSpace, class _RNG>
class BasePopulation {
public:
    /// Export the resource space
    using ResSpace = _ResSpace;

    /// Export the RNG type
    using RNG = _RNG;

    /// Type for resource vectors
    using ResVec = typename ResSpace::ResVec;

    /// Shorthand for the number of dimensions in the resource space
    static constexpr std::size_t rspace_dims = ResSpace::num_dims;

private:
    /// A string describing the kind of population type this class represents
    inline static const std::string __base_kind = "base";

    /// The ID of the next member of this type; incremented in constructor
    /** \note IDs start at 1 such that there is 0 to signify something else
      */
    inline static IDType next_id = 1;

    /// Whether the population is actually active
    bool active;

public:
    /// ID of this object
    /** The ID is specified at construction and is sourced from the static
      * ``next_id`` member.
      *
      * \note The ID counter is shared among all objects derived from the
      *       ::BasePopulation
      */
    const IDType id;

    // .. Parameters ..........................................................
    /// A representation of the properties of this object at construction
    /** \detail This is set at construction. It is NOT automatically
      *         updated when parameters change. Refer to ::get_properties for
      *         more info.
      */
    const Config cfg;

protected:
    /// The logger for this instance, passed down from the model
    const std::shared_ptr<spdlog::logger> log;

public:
    // .. Constructors ........................................................
    /// Construct a BasePopulation from a given configuration
    /**
      * \NOTE During construction, derived class overrides are not available!
      *       In other words: No construction of members that require some
      *       dynamic specialization should occur here.
      */
    BasePopulation (const std::shared_ptr<spdlog::logger>& parent_logger,
                    const std::shared_ptr<RNG>& rng,
                    const ResSpace&,
                    const Config& setup_cfg,
                    const std::string& logger_suffix = "")
    :
        // By default: marked active
        active(true),

        // Increment static ID
        id(next_id++),

        // Potentially apply noise to the given parameters, then store result
        cfg(apply_noise(setup_cfg, rng)),

        // Create a population-specific logger
        // NOTE Need to use the ``logger_suffix`` as a workaround here because
        //      no dynamic binding is available during construction!
        log(spdlog::stdout_color_mt(
                parent_logger->name() + ".pop"
                + "." + (logger_suffix.size() ? logger_suffix : kind())
                + "." + std::to_string(id))
        )

        // Initialize shared parameters
        // ...
    {
        // Configure logger
        log->set_level(parent_logger->level());

        log->debug("Constructing population ...");
        log->trace("Configuration:\n\n{}\n", cfg);
    }

    /// Custom destructor, taking care of member side effects
    /** This removes the custom-created logger from the registry to avoid it to
      * aggregate too many loggers.
      */
    virtual ~BasePopulation () {
        log->trace("Destructing ...");
        spdlog::drop(log->name());
    }


protected:
    // .. Initialization Methods ..............................................




public:
    // .. Getters .............................................................

    /// A description of this type, mainly for log messages
    virtual const std::string& kind () const {
        return __base_kind;
    }

    /// A numeric identifier of this type, mainly for writing data
    virtual char kind_id () const {
        return 0;
    }

    /// The associated logger
    const auto& get_logger () const {
        return log;
    }

    /// Whether the population is active
    bool is_active () const {
        return active;
    }

    /// Retrieve the properties for recreating this population
    virtual Config get_properties () const {
        auto props = YAML::Clone(cfg);

        // TODO make more versatile, as in mesonet?

        return props;
    }


    // .. Setters .............................................................

    /// Deactivate the population; note that it cannot be re-activated
    void deactivate () {
        active = false;
        __deactivate();
        log->debug("Deactivated.");
    }

    /// Called from deactivate, allowing to add additional behaviour
    virtual void __deactivate () {}

};


} // namespace Utopia::Models::EEcosy

#endif // UTOPIA_MODELS_EECOSY_POPULATION_BASE_HH
