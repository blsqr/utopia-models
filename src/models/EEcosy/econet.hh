#ifndef UTOPIA_MODELS_EECOSY_ECONET_HH
#define UTOPIA_MODELS_EECOSY_ECONET_HH

#include <algorithm>
#include <memory>
#include <iterator>
#include <type_traits>
#include <queue>

#include <boost/range/adaptor/filtered.hpp>

#include <utopia/core/graph/iterator.hh>

#include <UtopiaUtils/utils/cfg.hh>
#include <UtopiaUtils/utils/iterators.hh>
#include <UtopiaUtils/utils/math.hh>
#include <UtopiaUtils/utils/meta.hh>
#include <UtopiaUtils/utils/select.hh>

#include "utils/casting.hh"
#include "utils/formatters.hh"
#include "utils/yaml.hh"

#include "types.hh"
#include "resource_space.hh"
#include "populations.hh"
#include "flows.hh"
#include "network.hh"


namespace Utopia::Models::EEcosy {

/// The ecological interaction network
/** This object implements the ecological interaction network. It builds on the
  * Boost Graph Library (BGL), but provides an interface that allows working
  * with the Population and Flow objects.
  *
  * The EcoNet class focusses on performing those tasks that connect the
  * imposed objects (Population, Flow, ...) with the underlying graph structure
  * (adjacency list, vertex descriptors, edge descriptors, ...) of the BGL.
  * While this approach reduces the object-oriented interface of the Population
  * and Flow classes, it liberates the Population and Flow objects from having
  * to work with the BGL interface; instead they only ever see other Flow or
  * Population objects as arguments. (\a Ideally, that is.)
  *
  * \NOTE \parblock
  *       There is a NAMING SCHEME used throughout this class implementation.
  *       It follows the BGL naming scheme and makes a distinction between
  *       objects that are strictly working on BGL data structures and objects
  *       that are superimposed on it, i.e. the populations and flows.
  *
  *       The intention of this naming scheme is to distinguish clearly between
  *       the underlying BGL data structures and the imposed structure.
  *       In situations where the scheme does not offer enough guidance, a
  *       solution should be found that follows this principle.
  *
  *       The following variable names should be used exclusively for BGL
  *       objects:
  *           - `v, u`:      vertex \a descriptors
  *           - `e`:         edge \a descriptors
  *           - `source, s`: source vertex \a descriptor for an edge
  *           - `target, t`: target vertex \a descriptor for an edge
  *
  *       These variable names should be used exclusively for EcoNet objects:
  *           - `pop`:       a ::BasePopulation object or a derived object,
  *                          i.e. a bundled vertex property
  *           - `flow`:      a ::BaseFlow object or a derived object, i.e. a
  *                          bundled edge property
  *
  *       (WIP)
  *
  *       \parblockend
  *
  */
template<
    class _RNG,
    class _ResSpace = DefaultResourceSpace,
    class _Network = NW::Network<_ResSpace, _RNG>
>
class EcoNet {
public:
    // .. Types ...............................................................

    /// The EcoNet-internal RNG type
    using RNG = _RNG;

    /// The resource space this EcoNet assumes
    using ResSpace = _ResSpace;

    /// The resource vector type
    using ResVec = typename ResSpace::ResVec;

    /// The mask vector type
    using MaskVec = typename ResSpace::MaskVec;

    /// The network type this EcoNet assumes
    using Network = _Network;

    /// The traits of the EEcosy network
    using NetworkTraits = boost::graph_traits<Network>;

    /// The vertex descriptor type
    using VertexDesc = NW::VertexDesc<Network>;

    /// The edge descriptor type
    using EdgeDesc = NW::EdgeDesc<Network>;

    /// The vertex (bundled) property type
    using VertexProp = typename Network::vertex_property_type;

    /// The edge (bundled) property type
    using EdgeProp = typename Network::edge_property_type;

    /// Aggregated filtered networks
    struct FilteredNetworks : public NW::FilteredNetworks<Network> {
        /// The base type
        using Base = NW::FilteredNetworks<Network>;

        /// The filtered network type
        using FilteredNW = typename Base::FilteredNW;

        /// The vertex predicate type
        using VertexPred = typename Base::VertexPred;

        /// The edge predicate type
        using EdgePred = typename Base::EdgePred;

        // .. Custom Filters . . . . . . . . . . . . . . . . . . . . . . . . .
        // .. by activity status ..

        const FilteredNW active;
        const FilteredNW inactive;

        // .. by kind ..

        /// Filters that include only *regular* populations; no flow filter
        const FilteredNW only_regular;

        /// Filters that include only *source* populations; no flow filter
        const FilteredNW only_source;

        /// Filters that include only *trophic* flows; no population filter
        const FilteredNW only_trophic;

        // .. by use case ..
        /// Foraging: active trophic flows, active regular or source pops
        const FilteredNW foraging;

        /// Population-internal: all flows, active regular or source pops
        const FilteredNW pop_internal;

        /// Evolution: all flows, active regular pops
        const FilteredNW evolvable;


        // .. Constructors and Getters . . . . . . . . . . . . . . . . . . . .
        /// Construct the filtered networks
        FilteredNetworks (const Network& nw)
        :   Base(nw)

        ,   active(nw,
                [&nw](auto e){ return nw[e]->is_active(); },
                [&nw](auto v){ return nw[v]->is_active(); })
        ,   inactive(nw,
                [&nw](auto e){ return not nw[e]->is_active(); },
                [&nw](auto v){ return not nw[v]->is_active(); })

        ,   only_regular(nw,
                boost::keep_all(),
                [&nw](auto v){ return nw[v]->kind_id() == 1; })
        ,   only_source(nw,
                boost::keep_all(),
                [&nw](auto v){ return nw[v]->kind_id() == 2; })
        ,   only_trophic(nw,
                [&nw](auto e){ return nw[e]->kind_id() == 1; },
                boost::keep_all())

        ,   foraging(nw,
                [&nw](auto e){
                    return (nw[e]->kind_id() == 1) and nw[e]->is_active();
                },
                [&nw](auto v){
                    return (nw[v]->kind_id() >= 1) and nw[v]->is_active();
                })
        ,   pop_internal(nw,
                boost::keep_all(),
                [&nw](auto v){
                    return (nw[v]->kind_id() >= 1) and nw[v]->is_active();
                })
        ,   evolvable(nw,
                boost::keep_all(),
                [&nw](auto v){
                    return (nw[v]->kind_id() == 1) and nw[v]->is_active();
                })
        {}

        const FilteredNW& get (const std::string& name) const override {
            // Most frequently used
            if      (name == "active")          return active;
            else if (name == "only_regular")    return only_regular;
            else if (name == "foraging")        return foraging;
            else if (name == "pop_internal")    return pop_internal;
            else if (name == "evolvable")       return evolvable;

            // Less frequently used
            else if (name == "inactive")        return inactive;
            else if (name == "only_source")     return only_source;
            else if (name == "only_trophic")    return only_trophic;

            // All other members
            return this->Base::get(name);
        }
    };

    /// The filtered network
    using FilteredNW = typename FilteredNetworks::FilteredNW;

    /// The appropriately specialized EEcosy model is a friend
    friend EEcosy<ResSpace>;


protected:
    // .. Infrastructure members ..............................................
    /// The shared logger, passed down from the model
    std::shared_ptr<spdlog::logger> log;

    /// The shared RNG, passed down from the model
    std::shared_ptr<RNG> rng;

    /// Defaults for populations and flows
    const Config defaults;

    /// A uniform distribution in range [0., 1.) for evaluating probabilities
    std::uniform_real_distribution<double> prob_distr;

    /// A normal distribution (mu=0., sigma=1.) for mutations
    std::normal_distribution<double> normal_distr;


    // .. Resource space & ecological network .................................
    /// The resource space object
    ResSpace rspace;

    /// The underlying network object
    Network nw;

public:
    /// Filtered views into the main network
    FilteredNetworks filtered;


    // .. Parameters ..........................................................
    /// Delta t
    const double dt;

    /// The maximum consumption ratio per iteration
    const double max_consumption_ratio;

    /// The minimum effort possible
    const double min_effort;

    /// The minimum strength to be viable on a resource
    const double min_strength;

    /// The competition factor
    const double competition_factor;


    // .. Temporary containers ................................................
    /// The populations that are to be split
    std::queue<std::pair<VertexDesc, VertexProp>> to_split;


protected:
    // .. Tracking Variables ..................................................
    /// The total resource losses throughout the system
    /** These are all the resources that are no longer available to the
      * network, including expended sustenance costs.
      */
    ResVec _losses;


    // .. Flags ...............................................................
    /// Whether cleanup of the network is necessary
    /** "Cleanup" refers to the removal of deactivated population and flows.
      * It is carried out separately.
      */
    bool _need_cleanup;


public:
    /// Set up a ecological interaction network and populate it
    /** If a `setup_cfg` is given, this also sets up the ecological network
      * and performs initial dynamics.
      */
    EcoNet (std::shared_ptr<spdlog::logger> logger,
            std::shared_ptr<RNG> rng,
            const Config& cfg,
            const Config& setup_cfg = {},
            const Config& defaults = {},
            const Config& rspace_cfg = {})
    :
        log(logger)
    ,   rng(rng)
    ,   defaults(defaults)
    ,   prob_distr(0., 1.)
    ,   normal_distr(0., 1.)

        // Initialize resource space object
    ,   rspace(rspace_cfg)

        // Initialize empty network (may be populated in constructor's body or
        // via public interface calls) and filtered views into that network.
    ,   nw()
    ,   filtered(nw)

        // Parameters
    ,   dt(get_as<double>("dt", cfg))
    ,   max_consumption_ratio(get_as<double>("max_consumption_ratio", cfg))
    ,   min_effort(get_as<double>("min_effort", cfg))
    ,   min_strength(get_as<double>("min_strength", cfg))
    ,   competition_factor(get_as<double>("competition_factor", cfg))

        // Temporary containers, tracking variables, flags, ...
    ,   to_split{}
    ,   _losses{ResSpace::zeros()}
    ,   _need_cleanup{false}
    {
        // Make sure that there can be no parallel edges
        using _par_cat = typename NetworkTraits::edge_parallel_category;
        static_assert(
            is_same<_par_cat, boost::disallow_parallel_edge_tag>()
        );

        // Inform about resoruce space
        log->info("Initialized {:d}-dimensional resource space.",
                  rspace.num_dims);
        // log->info("Resource dimension weights:\n{}", rspace.weights);
        // FIXME Not working on Ubuntu 20.04

        // Set up EcoNet itself, if desired
        if (not setup_cfg.size()) {
            log->info("Set up empty ecological network.");
        }
        else {
            setup_nw(setup_cfg);
            log->info(
                "Set up ecological network with {} populations and {} flows.",
                num_populations(), num_flows()
            );
        }

        // TODO Other initial dynamics?
    }


    // -- Dynamics ------------------------------------------------------------
    // (In order of appearance within EEcosy::perform_step)

    /// Balances the foraging efforts and responses with fixed population sizes
    auto update_foraging_efforts_and_responses (const unsigned max_n=1,
                                                const double rtol=0.001)
    {
        const auto& foraging_nw = filtered.foraging;
        auto n = 0u;
        auto tmp_rdelta = 0.;
        auto max_rdelta = std::numeric_limits<double>::infinity();

        log->trace("Updating foraging efforts and response ... "
                   "(max_n: {:d},  rtol: {:f})", max_n, rtol);

        while (n < max_n and max_rdelta > rtol) {
            max_rdelta = 0.;

            for (const auto& [v, pop]
                 : populations<Population, with_descriptor>(foraging_nw))
            {
                pop->update_foraging_efforts(
                    *this,
                    in_flows<TrophicFlow, with_descriptor>(v, foraging_nw)
                );

            }

            for (const auto& [e, flow]
                 : flows<TrophicFlow, with_descriptor>(foraging_nw))
            {

                const auto& prey = as<Population>((*this)[source(e)]);
                tmp_rdelta = flow->update_response(
                    min_strength, prey,
                    total_pressure(
                        e, flow,
                        out_flows<TrophicFlow, with_descriptor>(source(e),
                                                                foraging_nw)
                    )
                );

                if (tmp_rdelta > max_rdelta) {
                    max_rdelta = tmp_rdelta;
                }
            }

            n++;
            log->trace("  largest rel. change in iteration {:d}: {:.3g}",
                       n, max_rdelta);
        }

        return n;
    }

    /// Performs the foraging operations between populations
    /** \TODO document
      */
    void perform_foraging () {
        const auto& foraging_nw = filtered.foraging;

        // First, need to update the foraging efforts and responses. During
        // regular iteration, a simple update should suffice.
        // TODO Consider temporarily increasing max_n after SPLITTING occurred.
        update_foraging_efforts_and_responses(3, 0.05);

        log->trace("Computing consumption ...");
        for (const auto& [v, prey]
             : populations<Population, with_descriptor>(foraging_nw))
        {
            // Inside this loop, all function calls are formulated from the
            // perspective of prey populations, i.e. those with outgoing flows.
            // If there are no outgoing flows for a population, this loop will
            // simply not do anything.

            // Update the outgoing flows' properties accordingly
            double delta_n = 0.;
            for (const auto& [e, flow]
                 : out_flows<TrophicFlow, with_descriptor>(v, foraging_nw))
            {
                const auto& pred = as<Population>((*this)[target(e)]);
                delta_n += flow->update_consumption(dt, prey, pred);
            }

            // Potentially need the consumption correction
            const auto c = prey->update_consumption_ratio(delta_n);
            if (c > max_consumption_ratio) {
                const double s = (max_consumption_ratio / c);
                prey->set_scaling_factor(s);
                log->trace("consumption_ratio of prey {} is {:.3f} > {:.3g} "
                           "==> will be scaled by {:.3g}",
                           prey->id, c, max_consumption_ratio, s);

                for (const auto& [e, flow]
                     : out_flows<TrophicFlow, with_descriptor>(v, foraging_nw))
                {
                    flow->scale_consumption(s);
                }
            }
            else {
                prey->set_scaling_factor(
                    std::numeric_limits<double>::quiet_NaN()
                );
            }
        }

        log->trace("Applying foraging effects ...");
        for (const auto& [e, flow]
             : flows<TrophicFlow, with_descriptor>(foraging_nw))
        {
            const auto& prey = as<Population>((*this)[source(e)]);
            const auto& pred = as<Population>((*this)[target(e)]);
            flow->apply_foraging(prey, pred);
        }

        log->trace("Foraging operations finished.");
    }

    /// Performs the population-internal processes
    /** This invokes each populations' internal processes:
      *
      *    1. Metabolism
      *    2. Expending sustenance costs
      *    3. Population dynamics
      *    4. Update divergence and determine whether to split
      *
      * In steps 2 and 3, populations may become extinct. If they do, they are
      * deactivated and the remaining steps are not carried out for that
      * population.
      *
      * If it did *not* become extinct, the divergence potential will be
      * updated and it will be checked, whether splitting needs to occur. If
      * that is the case, it will be registered in the to_split queue.
      *
      * \note    The network structure is *not* changed in this method.
      *
      * \returns Number of populations that became extinct and were marked as
      *          inactive.
      */
    std::size_t perform_population_internal_processes () {
        auto num_extinct = 0u;

        log->trace("Performing population-internal procceses ...");

        // TODO Use parallel STL once available?!
        for (auto [v, pop]
             : populations<Population, with_descriptor>(filtered.pop_internal))
        {
            // -- 1: Metabolic processes --
            _losses += pop->perform_metabolic_processes(dt);

            // -- 2: Expending sustenance costs --
            _losses += pop->expend_sustenance_costs(dt, rng);

            if (pop->check_extinction()) {
                deactivate_population(v, pop);
                num_extinct++;
                continue;
            }

            // -- 3: Population dynamics: birth and random death --
            _losses += pop->perform_population_dynamics(dt, rng);

            if (pop->check_extinction()) {
                deactivate_population(v, pop);
                num_extinct++;
                continue;
            }

            // Reaching this point means: not extinct
            // -- 4: Update divergence and splitting --
            pop->update_divergence();
            if (pop->check_divergence(dt, rng, prob_distr)) {
                to_split.push({v, pop});
            }
        }

        if (num_extinct) {
            //    At least one population became extinct, was marked inactive
            // => set flag to denote that cleanup would be necessary
            log->debug("{:d} population(s) became extinct from population-"
                       "internal dynamics. Will need clean-up.", num_extinct);
            _need_cleanup = true;
        }

        return num_extinct;
    }

    /// Performs splitting operations, the evolutionary process in EEcosy
    /** This iterates over the to_split queue, which was populated with the
      * populations that were diverging during population dynamics.
      *
      * \note    Non-zero return values indicate that the network structure
      *          changed (both in terms of edges and vertices).
      *
      * \returns Number of population splittings that occurred, which is the
      *          same as the number of new populations. Some of these might
      *          not be viable and might already have been deactivated; these
      *          are counted in the same way as the viable ones here.
      */
    std::size_t perform_splitting () {
        if (not to_split.size()) {
            log->trace("No splitting operations queued.");
            return 0;
        }
        log->debug("Splitting (at least) {} populations ...", to_split.size());

        auto num_split = 0u;

        // Work through the queue of populations that need to split and let
        // them split, generating an offspring population.
        // The split_populations method takes care of the rest.
        while (not to_split.empty()) {
            auto [v, pop] = to_split.front();
            split_population(v, as<Population>(pop));
            to_split.pop();
            num_split++;
        }
        // NOTE Adding vertices and edges does, fortunately, NOT invalidate
        //      vertex descriptors, thus alleviating the need to search for the
        //      corresponding descriptors in the network

        return num_split;
    }


    // .. General helper functions for dynamics ...............................

    /// Compute or look-up the competitiveness of two predators for one prey
    /**
      * \param prey    The prey that is being competed for
      * \param pred1   One predator competing for the prey
      * \param pred2   The other predator competing for the prey
      */
    template<class Pop>
    double competitiveness (Pop&&, Pop&&, Pop&&) {
        return competition_factor;
        // FIXME Implement
        // return competition_factor + (1. - competition_factor) * similarity;
    }

    /// Computes the total competition pressure for the given flow edge
    /**
      * \param e               The edge descriptor to compute this for
      * \param flow            The flow object to compute this for
      * \param all_pred_flows  A range iterator over all competing flows
      *
      * \returns  Sum of the product of competitiveness, strength, effort, and
      *           size of the competing predator; basically: the sum in the
      *           denominator of the functional response.
      */
    template<class Flow, class RangeIt>
    double total_pressure (EdgeDesc e, Flow&& flow, RangeIt&& all_pred_flows) {
        const auto& prey = as<Population>((*this)[source(e)]);
        const auto& pred = as<Population>((*this)[target(e)]);

        double sum = 0.;

        for (const auto& [e2, flow2] : all_pred_flows) {
            const auto& pred2 = as<Population>((*this)[target(e2)]);
            sum += (
                  competitiveness(prey, pred, pred2)
                * flow2->strength * flow2->get_effort()
                * pred2->get_size()
            );
        }

        if (sum <= 0.) {
            throw std::runtime_error(fmt::format(
                "Got total_pressure of {} <= 0. for edge {:d} ({:d} -> {:d})!",
                sum, flow->id, prey->id, pred->id
            ));
        }

        return sum;
    }


    /// Checks extinction of a population; if so, deactivates it
    template<class Pop>
    void deactivate_population (const VertexDesc v,
                                const std::shared_ptr<Pop>& pop)
    {
        log->debug("Deactivating population {} and all connected flows ...",
                   pop->id);
        pop->deactivate();

        // Keep track of remaining resources in that population
        _losses += pop->get_size() * pop->composition_unmasked;
        _losses += pop->get_reagents();
        _losses += pop->get_reservoir();
        pop->set_size(0.);
        pop->set_reagents(rspace.zeros());
        pop->set_reservoir(rspace.zeros());

        // Deactivate all flows connected to that population
        for (auto flow : in_flows(v)) {
            flow->deactivate();
        }
        for (auto flow : out_flows(v)) {
            flow->deactivate();
        }
        // TODO _Might_ have to differentiate here. There MIGHT be flows
        //      that should not be deactivated, e.g. detritus flows ...?

        _need_cleanup = true;
        log->debug("Population and flows deactivated; cleanup flag set.");
    }


    // .. Splitting-related methods ...........................................

    /// Splits the population identified by the given vertex descriptor
    /** This also carries over resources and size from the parent to the
      * offspring.
      *
      * Currently, this is restricted to duplication regular Population
      * vertices.
      *
      * \tparam  mutation_mode   Whether to mutate or not; accepts the two tags
      *                          with_mutation and without_mutation
      * \param  v       The vertex descriptor of the population to duplicate
      * \param  parent  The parent Population entity
      *
      * \returns the vertex descriptor of the offspring
      * \TODO add option to return multiple
      */
    template<class to_return = descriptor_only, class Pop>
    auto split_population (const VertexDesc v,
                           const std::shared_ptr<Pop>& _parent)
    {
        // Restrict to the Population interface
        const auto parent = as<Population>(_parent);
        log->debug("Splitting {} population {} ...",
                   parent->kind(), parent->id);

        // Retrieve the parent population's properties, which will be the
        // basis for the offspring population's properties
        auto props = parent->get_properties();
        log->debug("Got properties from parent:\n{}\n", props);

        // -- Determine offspring properties
        // Enable mutations for other properties
        enable_mutations(
            props, get_as<Config>("properties", parent->split_params)
        );

        // Carry out composition mutations
        const auto composition = mutate_composition(parent);

        // Depending on split fraction, determine split-off resources and
        // population sizes, marked `so_…`.
        // These become inaccessible to the parent population and are the
        // quantities that are deduced from the parent (see below).
        const auto split_fraction = get_as<double>("split_fraction",
                                                   parent->split_params);

        const auto so_size = split_fraction * parent->get_size();
        const ResVec so_reagents = split_fraction * parent->get_reagents();
        const ResVec so_reservoir = split_fraction * parent->get_reservoir();

        // Ensure mass & energy conservation by adjusting size and reservoir
        // according to the mutated composition
        const auto [size, reservoir, losses] = apply_conservation_laws(
            parent->composition_unmasked,
            apply_operations<unmask_NaNs>(composition),
            so_size,
            so_reservoir
        );

        // Keep track of losses
        _losses += losses;

        // Inject into property node, alongside with parent's ID
        props["composition"] = to_vec(composition);
        props["reagents"] =  to_vec(so_reagents);  // stays the same
        props["reservoir"] = to_vec(reservoir);
        props["size"] = size;
        props["parent_id"] = parent->id;


        // -- Add a new population, using the paramters set above
        auto [u, offspring] =
            add_population<Population, with_descriptor>(props);
        log->debug("Offspring population added.");

        // Register offspring ID with parent
        parent->register_offspring_id(offspring->id);

        // Deduce size, reservoir, etc. from parent population
        parent->adjust_reagents(-so_reagents);
        parent->adjust_reservoir(-so_reservoir);
        parent->adjust_size(-so_size);
        // NOTE Using `so_…` here because the conservation laws can lead to
        //      a scenario where the offspring has smaller size or reservoir
        //      values. However, the "split-off" part should be regarded as
        //      inaccessible to the parent population, which is ensured by
        //      deducing exactly that split-off part here.

        // Embed offspring into the network . . . . . . . . . . . . . . . . . .
        log->debug("Embedding offspring into ecological network ...");

        embed_offspring<incoming>(
            v, parent, u, offspring,
            get_as<Config>("inflows", offspring->split_params)
        );
        embed_offspring<outgoing>(
            v, parent, u, offspring,
            get_as<Config>("outflows", offspring->split_params)
        );

        // TODO Update foraging efforts here?!! :thinking:


        // Viability, repeated splitting? . . . . . . . . . . . . . . . . . . .
        // Might already have to deactivate it, as conservation laws might have
        // reduced its size to zero ... To keep track of it, it's still useful
        // to regard it like other populations, i.e. to add it and embed it
        // into the network as done above.
        if (offspring->check_extinction()) {
            log->debug("Population is not viable! Deactivating ...");
            deactivate_population(u, offspring);
        }

        // How about repeated splitting?
        parent->update_divergence();
        if (parent->check_divergence(dt, rng, prob_distr)) {
            to_split.push({v, parent});
        }

        offspring->update_divergence();
        if (offspring->check_divergence(dt, rng, prob_distr)) {
            to_split.push({u, offspring});
        }

        // Done.
        return __build_rv<to_return>(u, offspring);
    }


    /// Adds or updates an `_apply_noise` entry in place
    /** Helps setting or updating an existing entry for the `_apply_noise`
      * functionality. Relevant configuration keys in `params` are the
      * following (all *optional*, set in that order):
      *
      *    - `mutations`: mapping to use for `_apply_noise`, defaults to an
      *      empty mapping, thus having no effect if `merge` is set
      *    - `merge`: whether to merge existing `_apply_noise` content using
      *      a recursive update with `mutations`, default: true
      *    - `enabled`: value to set `_enabled` to, default: true
      *    - `after_application`: value to set `_after_application` to,
      *      default: `disable`
      *
      */
    void enable_mutations (Config& cfg, const Config& params) {
        const auto mutations = get_as<Config>("mutations", params, {});

        // Ensure it's no zombie
        if (not cfg["_apply_noise"]) {
            cfg["_apply_noise"] = Config{};
        }

        // Merge or overwrite
        if (get_as<bool>("merge", params, true)) {
            cfg["_apply_noise"] = recursive_update(cfg["_apply_noise"],
                                                   mutations);
        }
        else {
            cfg["_apply_noise"] = YAML::Clone(mutations);
        }

        // Set flags
        cfg["_apply_noise"]["_enabled"] =
            get_as<bool>("enabled", params, true);
        cfg["_apply_noise"]["_after_application"] =
            get_as<std::string>("after_application", params, "disable");
    }

    /// Applies mutations to population's composition, returning a mutated copy
    /** Implicitly assumes that `pop` is a regular Population or adheres to
      * that interface. Typically, `pop` is a parent population.
      *
      * The returned composition is used in split_population to assign a
      * composition to the offspring population.
      */
    template<class Pop>
    ResVec mutate_composition (const std::shared_ptr<Pop>& pop) {
        ResVec comp = pop->composition;
        const auto params = get_as<Config>("composition", pop->split_params);

        if (not get_as<bool>("enabled", params)) {
            log->debug("Copying the composition vector of population {} "
                       "without mutations ...", pop->id);
            return comp;
        }

        const auto rel_sigma = get_as<double>("rel_sigma", params);
        const auto p_new = get_as<double>("p_new", params);
        const auto new_mu = get_as<double>("new_mu", params);
        const auto new_sigma = get_as<double>("new_sigma", params);

        log->debug("Creating a mutated composition vector from population {} "
                   "... (rel_sigma: {}, p_new: {}, new_mu: {}, new_sigma: {}",
                   pop->id, rel_sigma, p_new, new_mu, new_sigma);

        // Perform the mutations
        comp.transform(
            [this, rel_sigma, p_new, new_mu, new_sigma](auto v){
                if (not std::isnan(v)) {
                    return
                        (rel_sigma * v * this->normal_distr(*this->rng)) + v;
                }
                // Evaluate if a new composition component is unlocked
                if (not p_new or this->prob_distr(*rng) > p_new) {
                    // Nope.
                    return v;
                }
                // Yes.
                return (new_sigma * this->normal_distr(*this->rng)) + new_mu;
            }
        );

        // Transform very small values and negative values to NaN
        comp.transform([](auto v){
            if (v < PREC)
                return std::numeric_limits<double>::quiet_NaN();
            return v;
        });

        // log->debug("Mutated composition:\n{}\n", comp);
        // FIXME
        return comp;
    }

    /// Applies conservation laws to determine valid size and reservoir values
    /** To conserve mass, this method makes sure that the size and reservoir
      * of a newly split-off population are not requiring resources that were
      * not available before (and which were not available in the reservoir).
      *
      * This inevitably is a lossy operation if there is a composition change:
      *
      *     - With reduced composition elements, the same population size now
      *       holds fewer resources.
      *     - With growing composition elements, the viable population size is
      *       determined by the amount of resources that can be balanced out
      *       from the reservoir; if this leads to a smaller population size,
      *       resources are lost due to the size reduction.
      *
      * Note that the reservoir change is not a loss, but the movement of
      * resources from the reservoir to the individuals!
      *
      * \param  comp_old     The parent population's composition, unmasked
      * \param  comp_new     The offspring population's composition, unmasked
      * \param  size         The population size split-off from the parent
      * \param  reservoir    The reservoir split-off from the parent
      *
      * \returns the new size, new reservoir value, and the losses that arose
      *          from the size change and composition difference.
      */
    std::tuple<double, ResVec, ResVec>
    apply_conservation_laws (const ResVec& comp_old,
                             const ResVec& comp_new,
                             const double size,
                             const ResVec& reservoir) const
    {
        // Return variables
        double size_new = 0.;
        ResVec reservoir_new = rspace.zeros();
        ResVec losses = rspace.zeros();

        // -- Compute changes in composition dimensions
        const ResVec dcomp = comp_new - comp_old;
        const MaskVec smaller_comp = (dcomp <= PREC);  // TODO Compare to zero?

        // -- Determine new size and reservoir values
        if (arma::all(smaller_comp)) {
            // Composition might have changed, but no components grew. Thus,
            // don't need to balance out from the reservoir.
            size_new = size;
            reservoir_new = reservoir;
        }
        else {
            // For at least one resource dimension, population size needs to
            // be balanced out by the reservoir. Compute how much of the
            // population size can balance out the above difference via the
            // reservoir.
            // Need only the components of the composition that are larger;
            // mask all other resource dimensions such that they are not taken
            // into account for computation of the smallest quotient.
            ResVec dcomp_growth = apply_mask(dcomp, smaller_comp);
            // dcomp_growth.print("dcomp_growth");

            // The smallest quotient is now the population size that can be
            // "balanced-out from reservoir"
            const double size_bfr = smallest_quotient(reservoir, dcomp_growth);
            // log->info("size_bfr: {}", size_bfr);

            // The actual size change still cannot be larger than the
            // previously determined size of the new population
            size_new = std::min(size, size_bfr);

            // Using the determined size change, reduce the reservoir by those
            // non-negative compontents. To do that, unmask dcomp_growth such
            // that there are no further NaNs which would propagate into the
            // reservoir_new vector.
            unmask(dcomp_growth);

            reservoir_new = reservoir - (size_new * dcomp_growth);

            // To account for the balanced-out resources, already add the
            // reservoir change to the losses; these components will be
            // cancelled out by the positive components in the composition
            // change when calculating the losses below.
            losses = size_new * dcomp_growth;
            // NOTE The benefit of already assigning the losses here is that
            //      it reduces the number of problematic floating point
            //      operations, namely: `reservoir - reservoir_new`
        }

        // Compute losses from composition change
        losses += (size * comp_old) - (size_new * comp_new);
        losses.clean(PREC);

        // dcomp.print("dcomp");
        // smaller_comp.print("smaller_comp");
        // reservoir.print("reservoir");
        // reservoir_new.print("reservoir_new");
        // log->info("size: {}", size);
        // log->info("size_new: {}", size_new);
        // losses.print("losses");

        return {size_new, reservoir_new, losses};
    }

    /// Embeds an offspring population into the ecological network
    /** Copies flows from parent vertex to offspring vertex, allowing mutations
      * in properties and topology (losing flows or changing properties).
      *
      * Relevant keys in `params`:
      *
      *     - `inherit`: flow inheritance. Contains: `select` parameters,
      *       `p_loss`, and `properties` for property mutations
      *     - `gain`: gaining new flows. Contains: `enabled` flag, `preselect`,
      *       `select`, `p_gain`, `num_draws` (mapped to selection size)
      *
      * \tparam  direction       Which kind of flows to copy; incoming or
      *                          outgoing
      *
      * \param   v               Parent population vertex descriptor
      * \param   parent          Parent population entity
      * \param   u               Offspring population vertex descriptor
      * \param   offspring       Offspring population entity
      * \param   params          Selection and mutation parameters.
      *
      * \TODO reduce log levels to trace
      */
    template<class direction, class Pop, class Off>
    auto embed_offspring (const VertexDesc v,
                          const std::shared_ptr<Pop>& parent,
                          const VertexDesc u,
                          const std::shared_ptr<Off>& offspring,
                          const Config& params)
    {
        static_assert(is_any<direction, incoming, outgoing>(),
            "Flow direction may only be `incoming` or `outgoing`!"
        );

        using EdgeAndFlow = std::pair<EdgeDesc, EdgeProp>;

        log->debug("Embedding offspring {} into network of parent {} ...",
                   offspring->id, parent->id);

        // Aggregate the corresponding flows of the parent population
        auto parent_flows = std::vector<EdgeAndFlow>{};
        const auto& fnw = filtered.active;

        if constexpr (is_same<direction, incoming>()) {
            for (auto ep : in_flows<TrophicFlow, with_descriptor>(v, fnw)) {
                parent_flows.push_back(ep);
            }
            log->debug("  There are {} incoming flows available for "
                       "selection.", parent_flows.size());
        }
        else {
            for (auto ep : out_flows<TrophicFlow, with_descriptor>(v, fnw)) {
                parent_flows.push_back(ep);
            }
            log->debug("  There are {} outgoing flows available for "
                       "selection.", parent_flows.size());
        }


        // -- Apply a selection to them; then copy them
        //    This is where connections may be lost
        const auto inheritance_params = get_as<Config>("inherit", params);
        const auto p_loss = get_as<double>("p_loss", inheritance_params);
        const auto prop_mutations = get_as<Config>("properties",
                                                   inheritance_params);

        auto flows_to_copy =
            select(parent_flows, inheritance_params["select"], rng);
        log->debug("  Selected {:d} existing flows to copy from parent "
                   "(p_loss: {:f}) ...", flows_to_copy.size(), p_loss);

        VertexDesc s, t;  // source and target

        for (auto [e, flow] : flows_to_copy) {
            if (p_loss > 0. and p_loss < prob_distr(*rng)) {
                log->trace("  Not copying flow {:d} ...", flow->id);
                continue; // Lost this one
            }
            log->trace("  Copying flow {:d} ...", flow->id);

            // Extract flow properties and (potentially) mutate them
            auto props = flow->get_properties();
            enable_mutations(props, prop_mutations);

            // Determine source and target, depending on direction
            if constexpr (is_same<direction, incoming>()) {
                // incoming, i.e. flow source is the new flow's source
                s = source(e);
                t = u;
            }
            else {
                // outgoing, i.e. flow target is new flow's target
                s = u;
                t = target(e);
            }

            // Add the new flow
            add_flow<TrophicFlow>(s, t, props);
        }

        // -- Gaining new flows
        const auto gain_params = get_as<Config>("gain", params);
        if (not get_as<bool>("enabled", gain_params)) {
            log->debug("  Gaining flows was disabled.");
            return;
        }

        // Pre-selection: determine pool of possible vertices
        const auto from_nw = get_as<std::string>("from_nw", gain_params);
        auto descs =
            select_vertices(filtered.get(from_nw),
                            get_as<Config>("pre_select", gain_params),
                            u, offspring);
        log->debug("  Pre-selected {:d} populations from '{}' network as "
                   "candidates for new connections to/from offspring {}.",
                   descs.size(), from_nw, offspring->id);

        // Filter out the offspring itself and any connected population in
        // order to prevent self-loops or size-2 loops with existing neighbours
        auto not_u = [u](auto v){ return u != v; };
        auto connected_vertices = all_adjacent_vertices(v);
        auto not_connected = [&connected_vertices](const auto v){
            return not contains(connected_vertices, v);
        };

        descs =
            select(descs
                   | boost::adaptors::filtered(not_u)
                   | boost::adaptors::filtered(not_connected),
                   get_as<Config>("select", gain_params), rng);
        log->debug("  Have {:d} candidate populations left after applying "
                   "selection and filtering out already-connected ones.",
                   descs.size());

        const auto num_draws =
            map_into_range<size_like, clamp_to_bounds>(
                get_as<int>("num_draws", gain_params),
                descs.size()
            );

        // Select the actual descriptors
        descs = select(descs, num_draws, "random", rng);

        // Remaining parameters
        const auto p_gain = get_as<double>("p_gain", gain_params);
        const auto props =
            recursive_update(defaults["flows"]["trophic"],
                             get_as<Config>("init", gain_params));

        // Add the flows with a certain probability
        for (const auto w : descs) {
            if (p_gain == 0. or prob_distr(*rng) > p_gain) continue;

            // Determine flow source and target, depending on direction
            if constexpr (is_same<direction, incoming>()) {
                // offspring is target
                s = w;
                t = u;
            }
            else {
                // offspring is source
                s = u;
                t = w;
            }
            add_flow<TrophicFlow>(s, t, props);
        }
    }




    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    // -- Entity or Network Access --------------------------------------------
    // NOTE For most methods, there are multiple overloads. These allow to use
    //      the methods not only with the main network, but also with filtered
    //      views into the network.

    // .. Getters .............................................................

    /// The underlying network
    const auto& network () const {
        return nw;
    }

    // .. Lookup of descriptor or bundled property . . . . . . . . . . . . . .

    /// Descriptor-based access to the associated bundled property
    template<class Desc>
    auto operator[] (Desc d) const {
        return nw[std::move(d)];
    }

    /// Returns the source vertex descriptor of the given edge
    auto source (const EdgeDesc e) const {
        return boost::source(std::move(e), nw);
    }

    /// Returns the target vertex descriptor of the given edge
    auto target (const EdgeDesc e) const {
        return boost::target(std::move(e), nw);
    }

    // TODO Add vertex, edge ...


    // .. Scalar graph information . . . . . . . . . . . . . . . . . . . . . .

    /// The number of populations (i.e. vertices)
    std::size_t num_populations () const {
        return boost::num_vertices(this->nw);
    }

    /// The (correct) number of populations in a filtered network
    /** For filtered networks, boost::num_vertices returns the number of
      * vertices of the *underlying* network. This method, however, determines
      * the number via the iterator distance, which is O(n), but at least the
      * value is the one that would be expected from a method like this.
      */
    template<class NW>
    std::size_t num_populations (NW&& nw) const {
        auto [v, v_end] = boost::vertices(nw);
        return std::distance(v, v_end);
    }

    /// The number of flows
    std::size_t num_flows () const {
        return boost::num_edges(this->nw);
    }

    /// The (correct) number of flows in a filtered network
    /** For filtered networks, boost::num_edges returns the number of edges of
      * the *underlying* network. This method, however, determines the number
      * via the iterator distance, which is O(n), but at least the value is
      * the one that would be expected from a method like this.
      */
    template<class NW>
    std::size_t num_flows (const NW& nw) const {
        auto [e, e_end] = boost::edges(nw);
        return std::distance(e, e_end);
    }

    /// Total degree of a vertex (in- plus out-edges)
    auto degree (const VertexDesc v) const {
        return boost::degree(std::move(v), this->nw);
    }

    /// Total degree of a vertex (in- plus out-edges) in a filtered network
    /** boost::filtered_graph does not implement boost::degree, so it is
      * calculated from the in- and out-degree.
      *
      * \note Only available with graphs that are boost::bidirectionalS or
      *       boost::undirectedS.
      *
      * \TODO Make this compatible with directed graphs by distinguishing how
      *       the total degree is computed.
      */
    template<class NW>
    auto degree (const VertexDesc v, NW&& nw) const {
        return boost::in_degree(v, nw) + boost::out_degree(v, nw);
    }

    /// In-degree of a vertex
    /** \note Only available with graphs that are boost::bidirectionalS or
      *       boost::undirectedS.
      */
    auto in_degree (const VertexDesc v) const {
        return boost::in_degree(std::move(v), this->nw);
    }

    /// In-degree of a vertex in a filtered network
    /** \note Only available with graphs that are boost::bidirectionalS or
      *       boost::undirectedS.
      */
    template<class NW>
    auto in_degree (const VertexDesc v, NW&& nw) const {
        return boost::in_degree(std::move(v), nw);
    }

    /// Out-degree of a vertex
    auto out_degree (const VertexDesc v) const {
        return boost::out_degree(std::move(v), this->nw);
    }

    /// Out-degree of a vertex in a filtered network
    template<class NW>
    auto out_degree (const VertexDesc v, NW&& nw) const {
        return boost::out_degree(std::move(v), nw);
    }


    // .. Range iterators .....................................................

    // .. Descriptor-returning . . . . . . . . . . . . . . . . . . . . . . . .

    /// Iterator range over all vertex descriptors of the EcoNet network
    auto vertices () const {
        return vertices(this->nw);
    }

    /// Iterator range over all vertex descriptors of the given network
    template<class NW>
    auto vertices (NW&& nw) const {
        return range<IterateOver::vertices>(nw);
    }

    /// Iterator range over all edge descriptors of the EcoNet network
    auto edges () const {
        return edges(this->nw);
    }

    /// Iterator range over all edge descriptors of the given network
    template<class NW>
    auto edges (NW&& nw) const {
        return range<IterateOver::edges>(nw);
    }

    /// Iterator range over all in-edge descriptors of a vertex
    /** \note Only available with graphs that are boost::bidirectionalS or
      *       boost::undirectedS.
      */
    auto in_edges (const VertexDesc v) const {
        return in_edges(std::move(v), this->nw);
    }

    /// Iterator range over all in-edge descriptors of a vertex
    /** \note Only available with graphs that are boost::bidirectionalS or
      *       boost::undirectedS.
      */
    template<class NW>
    auto in_edges (const VertexDesc v, NW&& nw) const {
        return range<IterateOver::in_edges>(std::move(v), nw);
    }

    /// Iterator range over all out-edge descriptors of a vertex
    auto out_edges (const VertexDesc v) const {
        return out_edges(std::move(v), nw);
    }

    /// Iterator range over all out-edge descriptors of a vertex
    template<class NW>
    auto out_edges (const VertexDesc v, NW&& nw) const {
        return range<IterateOver::out_edges>(std::move(v), nw);
    }

    /// Iterator range over adjacent vertices (out-edge neighbors)
    auto adjacent_vertices (const VertexDesc v) const {
        return adjacent_vertices(std::move(v), this->nw);
    }

    /// Iterator range over adjacent vertices (out-edge neighbors)
    template<class NW>
    auto adjacent_vertices (const VertexDesc v, NW&& nw) const {
        return range<IterateOver::neighbors>(std::move(v), nw);
    }

    /// Iterator range over inverse adjacent vertices (in-edge neighbors)
    auto inv_adjacent_vertices (const VertexDesc v) const {
        return inv_adjacent_vertices(std::move(v), this->nw);
    }

    /// Iterator range over inverse adjacent vertices (in-edge neighbors)
    template<class NW>
    auto inv_adjacent_vertices (const VertexDesc v, NW&& nw) const {
        return range<IterateOver::inv_neighbors>(std::move(v), nw);
    }

    /// Returns a container with *all* adjacent vertices (inverse and regular)
    std::vector<VertexDesc> all_adjacent_vertices (const VertexDesc v) const {
        return all_adjacent_vertices(std::move(v), this->nw);
    }

    /// Returns a container with *all* adjacent vertices (inverse and regular)
    template<class NW>
    std::vector<VertexDesc> all_adjacent_vertices (const VertexDesc v,
                                                   NW&& nw) const
    {
        auto descs = std::vector<VertexDesc>{};

        for (const auto u : adjacent_vertices(v, nw))     descs.push_back(u);
        for (const auto u : inv_adjacent_vertices(v, nw)) descs.push_back(u);

        return descs;
    }


    // .. Population- or Flow-returning . . . . . . . . . . . . . . . . . . . .

    /// Iterator range over all populations in the EcoNet network
    template<template<class...> class cast_to = VoidT, class... opts>
    auto populations () const {
        return populations<cast_to, opts...>(this->nw);
    }

    /// Iterator range over all populations in the given network
    template<template<class...> class cast_to = VoidT, class... opts, class NW>
    auto populations (NW&& nw) const {
        return __make_trf_range<IterateOver::vertices, cast_to, opts...>(nw);
    }

    /// Iterator range over all flows in the EcoNet network
    template<template<class...> class cast_to = VoidT, class... opts>
    auto flows () const {
        return flows<cast_to, opts...>(this->nw);
    }

    /// Iterator range over all flows in the given network
    template<template<class...> class cast_to = VoidT, class... opts, class NW>
    auto flows (NW&& nw) const {
        return __make_trf_range<IterateOver::edges, cast_to, opts...>(nw);
    }

    /// Iterator range over inflows to a population (within EcoNet network)
    template<template<class...> class cast_to = VoidT, class... opts>
    auto in_flows (const VertexDesc v) const {
        return in_flows<cast_to, opts...>(v, this->nw);
    }

    /// Iterator range over inflows to a population (within given network)
    template<template<class...> class cast_to = VoidT, class... opts, class NW>
    auto in_flows (const VertexDesc v, NW&& nw) const {
        return __make_trf_range<IterateOver::in_edges, cast_to, opts...>(nw, v);
    }

    /// Iterator range over outflows from a population (within EcoNet network)
    template<template<class...> class cast_to = VoidT, class... opts>
    auto out_flows (const VertexDesc v) const {
        return out_flows<cast_to, opts...>(v, this->nw);
    }

    /// Iterator range over outflows from a population (within given network)
    template<template<class...> class cast_to = VoidT, class... opts, class NW>
    auto out_flows (const VertexDesc v, NW&& nw) const {
        return __make_trf_range<IterateOver::out_edges, cast_to, opts...>(nw, v);
    }

    /// Iterator range over downstream populations (within EcoNet network)
    /** \note Downstream populations are the neighbors in direction of the
      *       flow, i.e. those that feed on the given reference population.
      */
    template<template<class...> class cast_to = VoidT, class... opts>
    auto downstream_populations (const VertexDesc v) const {
        return downstream_populations<cast_to, opts...>(v, this->nw);
    }

    /// Iterator range over downstream populations (within given network)
    /** \note Downstream populations are the neighbors in direction of the
      *       flow, i.e. those that feed on the given reference population.
      */
    template<template<class...> class cast_to = VoidT, class... opts, class NW>
    auto downstream_populations (const VertexDesc v, NW&& nw) const {
        return __make_trf_range<IterateOver::neighbors, cast_to, opts...>(nw, v);
    }

    /// Iterator range over upstream populations (within EcoNet network)
    /** \note Upstream populations are the neighbors in the opposite direction
      *       of the flow, i.e. those the given reference population feeds on.
      */
    template<template<class...> class cast_to = VoidT, class... opts>
    auto upstream_populations (const VertexDesc v) const {
        return upstream_populations<cast_to, opts...>(v, this->nw);
    }

    /// Iterator range over upstream populations (within given network)
    /** \note Upstream populations are the neighbors in the opposite direction
      *       of the flow, i.e. those the given reference population feeds on.
      */
    template<template<class...> class cast_to = VoidT, class... opts, class NW>
    auto upstream_populations (const VertexDesc v, NW&& nw) const {
        return __make_trf_range<IterateOver::inv_neighbors, cast_to, opts...>(nw, v);
    }


    // -- Search and Select ---------------------------------------------------

    /// Aggregate vertex descriptors that match a predicate
    template<class UnaryPredicate>
    auto find_vertices (UnaryPredicate&& pred) const {
        return find_vertices(this->nw, pred);
    }

    /// Aggregate vertex descriptors that match a predicate
    template<class NW, class UnaryPredicate>
    auto find_vertices (NW&& nw, UnaryPredicate&& pred) const {
        std::vector<VertexDesc> descs;
        for (auto v : vertices(nw)) {
            if (pred(v)) {
                descs.push_back(v);
            }
        }
        return descs;
    }

    /// Aggregate edge descriptors that match a predicate
    template<class UnaryPredicate>
    auto find_edges (UnaryPredicate&& pred) const {
        return find_edges(this->nw, pred);
    }

    /// Aggregate edge descriptors that match a predicate
    template<class NW, class UnaryPredicate>
    auto find_edges (NW&& nw, UnaryPredicate&& pred) const {
        std::vector<EdgeDesc> descs;
        for (auto e : edges(nw)) {
            if (pred(e)) {
                descs.push_back(e);
            }
        }
        return descs;
    }

    /// Selects vertices using a specific strategy
    /** Invokes generate_vertex_predicate and uses that predicate to aggregate
      * a list of
      *
      * \note  Depending on whether `v` is available and used in the chosen
      *        selection strategy, the returned container *may* include `v`.
      *
      * \TODO  Support cast_to and opts
      *
      * \param nw       The network to use for selection
      * \param params   Selection parameters, used in generate_vertex_predicate
      * \param v        A sentinel vertex, required for some selection modes
      *
      * \returns        A container of vertex descriptors
      */
    template<class NW, class... Args>
    std::vector<VertexDesc> select_vertices (NW&& nw, Args&&... args) const {
        auto descs = std::vector<VertexDesc>{};

        auto pred = generate_vertex_predicate(std::forward<Args>(args)...);

        for (auto [u, pop] : populations<Population, with_descriptor>(nw)) {
            if (pred(u, pop)) descs.push_back(u);
        }

        return descs;
    }

    /// Generates a vertex predicate function
    /** The strategy is specified via the `strategy` key in `params`.
      * Available strategies:
      *
      *     - `lower_mass`
      *     - `higher_mass`
      *     - `by_mass`, expecting `min` and `max`
      *     - ... TODO
      */
    template<class Pop>
    std::function<bool(const VertexDesc, const std::shared_ptr<Pop>&)>
    generate_vertex_predicate (const Config& params,
                               const VertexDesc = {},
                               const std::shared_ptr<Pop> pop = {}) const
    {
        const auto strategy = get_as<std::string>("strategy", params);
        log->debug("Generating vertex predicate for strategy '{}' ...",
                   strategy);

        if (strategy == "lower_mass") {
            const auto mass = pop->mass;
            return [mass](const auto, const auto& p) {
                return p->mass < mass;
            };
        }
        else if (strategy == "higher_mass") {
            const auto mass = pop->mass;
            return [mass](const auto, const auto& p) {
                return p->mass > mass;
            };
        }
        else if (strategy == "by_mass") {
            auto mass_min = get_as<double>("min", params["by_mass"], 0.);
            auto mass_max = get_as<double>("max", params["by_mass"]);

            return [mass_min, mass_max](const auto, const auto& p) {
                return (p->mass >= mass_min and p->mass <= mass_max);
            };
        }

        throw std::invalid_argument(fmt::format(
            "Invalid strategy '{}'! Choose from:  lower_mass, higher_mass, "
            "by_mass ...",
            strategy
        ));
    }


    // -- Manipulating the Network Structure ----------------------------------
    // .. Adding Entities .....................................................

    // .. Populations . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

    /// Adds a population of the specified type, using the given configuration
    /** The population type is given as template template parameter; the
      * specialization with the resource space occurs within this method.
      */
    template<
        template<class...> class PopT = Population,
        class to_return = descriptor_only
    >
    auto add_population (const Config& cfg) {
        // Specialize the population with the resource space and RNG
        using Population = PopT<ResSpace, RNG>;

        // Add a new vertex, which will contain an empty shared_ptr
        const auto v = boost::add_vertex(nw);

        // Create new population and assign it to the bundled property of the
        // newly created vertex
        VertexProp pop = std::make_shared<Population>(log, rng, rspace, cfg);
        nw[v] = pop;

        return __build_rv<to_return, PopT>(v, pop);
    }

    /// Adds a population of the specified kind, passing on other arguments
    template<class to_return = descriptor_only, class... Args>
    auto add_population (const std::string& kind, Args&&... args) {
        if (kind == "regular") {
            return
                add_population<Population, to_return>(
                    std::forward<Args>(args)...
                );
        }
        else if (kind == "source") {
            return
                add_population<Source, to_return>(
                    std::forward<Args>(args)...
                );
        }
        else if (kind == "base") {
            return
                add_population<BasePopulation, to_return>(
                    std::forward<Args>(args)...
                );
        }
        throw std::invalid_argument("Invalid population kind '" + kind + "'!");
    }

    /// Adds multiple populations and returns a container of vertex descriptors
    template<template<class...> class PopT = VoidT,
             class NumT=int,
             class... Args>
    auto add_populations (const NumT num, Args&&... args) {
        std::vector<VertexDesc> vdescs{};
        vdescs.reserve(num);

        for ([[maybe_unused]] const auto i : in_range(num)) {
            if constexpr (is_same_template<PopT, VoidT>()) {
                vdescs.push_back(
                    add_population(std::forward<Args>(args)...)
                );
            }
            else {
                vdescs.push_back(
                    add_population<PopT>(std::forward<Args>(args)...)
                );
            }
        }
        return vdescs;
    }


    // .. Flows . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

    /// Add a flow of the specified type, from source to target vertex
    /**
      * \param v   The source vertex
      * \param u   The target vertex
      * \param cfg The configuration to initialize the Flow object with
      */
    template<
        template<class...> class FlowT = TrophicFlow,
        class to_return = descriptor_only,
        class directedness = forward
    >
    auto add_flow (VertexDesc v, VertexDesc u, const Config& cfg) {
        static_assert(is_any<directedness, forward, reverse>(),
            "Flow directedness may only be `forward` or `reverse`!"
        );

        // Specialize the flow with the resource space
        using Flow = FlowT<ResSpace, RNG>;

        // If adding in reverse, simply swap v and u
        if constexpr (is_same<directedness, reverse>()) {
            std::swap(v, u);
        }

        // Add a new edge, which will contain an empty shared_ptr
        const auto [e, edge_added] = boost::add_edge(v, u, nw);
        // NOTE CAREFUL! The source and target descriptors should be valid,
        //      otherwise the graph is extended and these objects are created
        // TODO Consider testing this

        if (not edge_added) {
            throw std::runtime_error(fmt::format(
                "Edge {} -> {} could not be added! Does a parallel edge "
                "already exist?", nw[v]->id, nw[u]->id)
            );
        }

        // Create new flow and assign it to the bundled property of the newly
        // created edge
        log->trace("Creating new flow from configuration:\n\n{}\n", cfg);

        EdgeProp flow = std::make_shared<Flow>(rng, rspace, cfg);
        nw[e] = flow;

        log->trace("Created and associated {} flow {:d}.",
                   flow->kind(), flow->id);

        return __build_rv<to_return, FlowT>(e, flow);
    }

    /// Add a flow with the type specified as string
    template<
        class to_return = descriptor_only,
        class directedness = forward,
        class... Args
    >
    auto add_flow (const std::string& kind, Args&&... args) {
        if (kind == "trophic") {
            return
                add_flow<TrophicFlow, to_return, directedness>(
                    std::forward<Args>(args)...
                );
        }
        else if (kind == "base") {
            return
                add_flow<BaseFlow, to_return, directedness>(
                    std::forward<Args>(args)...
                );
        }
        throw std::invalid_argument("Invalid flow kind '" + kind + "'!");
    }

    /// Add multiple pre-determined flows to the specified reference vertex
    /** All flows will be outgoing or incoming from the specified reference
      * vertex `v`.
      *
      * \tparam flow_direction The tag specifying what kind of flow is to be
      *                        added. Can be: ::outgoing or ::incoming
      */
    template<class flow_direction = void,
             template<class...> class FlowT = TrophicFlow,
             class... Args>
    auto add_flows (const VertexDesc v,
                    const std::vector<VertexDesc>& udescs,
                    Args&&... args)
    {
        static_assert(is_any<flow_direction, outgoing, incoming>(),
                      "Flow needs to be outgoing or incoming!");

        std::vector<EdgeDesc> edescs{};
        edescs.reserve(udescs.size());

        for (const auto u : udescs) {
            if constexpr (is_same<flow_direction, outgoing>()) {
                edescs.push_back(
                    add_flow<FlowT>(v, u, std::forward<Args>(args)...)
                );
            }
            else {
                edescs.push_back(
                    add_flow<FlowT>(u, v, std::forward<Args>(args)...)
                );
            }
        }

        return edescs;
    }

    /// Add a flow to the specified vertex, using a vertex selector
    /** \tparam flow_direction The tag specifying what kind of flow is to be
      *                        added. Can be: ::outgoing or ::incoming
      *
      * \TODO Fix internal copy of descriptors into container; use iterators!
      */
    template<class flow_direction = void,
             template<class...> class FlowT = TrophicFlow,
             class... Args>
    auto add_flows (const VertexDesc v,
                    const Config& select_params,
                    Args&&... args)
    {
        static_assert(is_any<flow_direction, outgoing, incoming>(),
                      "Flow needs to be outgoing or incoming!");

        // The predicate to filter by
        const auto not_v = [v](const auto u){ return u != v; };

        // Select the desired vertices, excluding v
        const auto udescs =
            select(vertices() | boost::adaptors::filtered(not_v),
                   select_params, rng);

        return add_flows<flow_direction, FlowT>(v, std::move(udescs),
                                                std::forward<Args>(args)...);
    }

    /// Connects vertices from the source network with those from the target
    /** Behaviour can be controlled by the `params` config, which allow the
      * following keys:
      *
      *     - `source`:       source selection parameters for select interface
      *     - `target`:       target selection parameters for select interface
      *     - `p_link`:       link probability (optional, default: 1.)
      *     - `exclude_self`: if true, no self-links will be created (optional,
      *                       default: true)
      *     - `exist_ok`:     if false, throws if add_flows threw an error; if
      *                       true, warns instead (optional, default: false)
      *
      * \param  source_nw     The network to select the connection sources from
      * \param  target_nw     The network to select the connection targets from
      * \param  params        Parameters for selecting entities from the
      *                       respective networks. See above for allowed keys.
      * \param  flow_args     Passed on to add_flow for setting up the Flow
      *                       object and connecting the vertices
      *
      * \tparam FlowT         The flow type to use for all connections. Has to
      *                       be a template type!
      *
      * \return A container of newly added edge descriptors
      */
    template<template<class...> class FlowT = TrophicFlow,
             class SourceNW, class TargetNW,
             class... Args>
    auto connect (SourceNW&& source_nw,
                  TargetNW&& target_nw,
                  const Config& params,
                  Args&&... flow_args)
    {
        // The container of resulting edge descriptors
        std::vector<EdgeDesc> edescs{};

        // Extract remaining parameters
        const bool exclude_self = get_as<bool>("exclude_self", params, true);
        const bool exist_ok = get_as<bool>("exist_ok", params, false);
        const double p_link = get_as<double>("p_link", params, 1.);

        if (p_link < 0. or p_link > 1.) {
            throw std::invalid_argument(fmt::format(
                "p_link needs to be in [0, 1], but was {}!", p_link
            ));
        }

        // Select the desired source and target vertex pools
        const auto source_pool = select(vertices(source_nw),
                                        params["source"], rng);
        const auto target_pool = select(vertices(target_nw),
                                        params["target"], rng);

        log->info("Connecting {} source population(s) to {} target "
                  "population(s) ...", source_pool.size(), target_pool.size());
        log->debug("p_link: {}, exclude_self: {}, exist_ok: {}",
                   p_link, exclude_self, exist_ok);

        // Iterate over all (source, target) combinations and add connections
        // with a certain probability.
        for (const auto s : source_pool) {
            for (const auto t : target_pool) {
                log->trace("Evaluating whether to add edge {} -> {} ...",
                           nw[s]->id, nw[t]->id);
                if (exclude_self and s == t) continue;
                if (p_link == 0.) continue;
                if (p_link != 1. and prob_distr(*rng) > p_link) continue;

                try {
                    edescs.push_back(
                        add_flow<FlowT>(s, t, std::forward<Args>(flow_args)...)
                    );
                }
                catch (std::runtime_error& err) {
                    if (not exist_ok) throw;
                    log->warn("Failed connecting edge! {}", err.what());
                }
            }
        }
        log->debug("Added {:d} new flows.", edescs.size());

        return edescs;
    }

    /// Connects vertices from the source network with those from the target
    /** This overload allows specifying a flow `kind` at runtime. Depending on
      * the value, the corresponding type is given to the actual connect
      * method. All further parameters are passed along.
      *
      * \param  kind          The kind of flow to add, i.e. `trophic` or `base`
      */
    template<class... Args>
    auto connect (const std::string& kind, Args&&... args) {
        if (kind == "trophic") {
            return connect<TrophicFlow>(std::forward<Args>(args)...);
        }
        else if (kind == "base") {
            return connect<BaseFlow>(std::forward<Args>(args)...);
        }
        throw std::invalid_argument("Invalid flow kind '" + kind + "'!");
    }


    // .. Removing Entities ...................................................

    /// Removes the specified vertex, clearing it of edges beforehand
    void remove_vertex (const VertexDesc v) {
        log->trace("Removing vertex with population {} ...", nw[v]->id);
        boost::clear_vertex(v, nw);
        boost::remove_vertex(std::move(v), nw);
        log->trace("Vertex cleared and removed.");
    }

    /// Remove the specified edge
    void remove_edge (const EdgeDesc e) {
        log->trace("Removing edge with flow {} (population {} -> {}) ...",
                   nw[e]->id, nw[source(e)]->id, nw[target(e)]->id);
        boost::remove_edge(std::move(e), nw);
        log->trace("Edge removed.");
    }

    /// Conditionally remove populations and the associated vertices
    /** Removes those elements where the predicate returns `true` using the
      * erase-remove idiom.
      *
      * \param   predicate  A unary predicate function that will receive the
      *                     respective Population object to decide removal of.
      *
      * \note    When vertices are removed, the connected edges are also
      *          removed! Thus, if the return value of this function is
      *          something else then 0, it has to be assumed that the topology
      *          changed *both* in terms of vertices *and* edges.
      *
      * \returns The number of removed populations
      */
    template<class UnaryPredicate>
    std::size_t remove_populations_if (const UnaryPredicate& predicate) {
        using VL = typename Network::vertex_list_selector;
        static_assert(is_same<VL, boost::listS>(),
            "Population removal is only possible with listS vertex selector!"
        );

        typename NetworkTraits::vertex_iterator vi, vi_end, next;
        boost::tie(vi, vi_end) = boost::vertices(nw);

        std::size_t num_removed = 0;

        for (next = vi; vi != vi_end; vi = next) {
            ++next;
            if (not predicate(nw[*vi])) {
                // NOT deleting this one
                continue;
            }
            remove_vertex(*vi);
            num_removed++;
        }

        return num_removed;
    }

    /// Conditionally remove flows and the associated edges
    /** Removes those elements where the predicate returns `true`, using the
      * boost::remove_edge_if function.
      *
      * Uses the boost-internal removal function to avoid having to handle
      * the edge iterator maintenance -- which isn't even possible for graphs
      * with directed edges!
      * In order to provide the same interface as remove_populations_if, a
      * new predicate lambda is constructed internally that resolves the Flow
      * object and, if configured, the corresponding edge descriptor.
      *
      * \tparam  predicate_signature  `entity_only` or `with_descriptor`
      *
      * \param   predicate  A unary or binary predicate function that will
      *                     receive the respective edge descriptor (if the
      *                     ::with_descriptor tag is given) and the Flow
      *                     object to decide removal of.
      *                     The signature hence needs to be
      *                     `bool(EdgeDesc, Flow)` when the descriptor is to
      *                     be included, or just `bool(Flow)` otherwise.
      *
      * \warning Invalidates edge iterators!
      *
      * \returns The number of removed flows
      */
    template<template<class...> class cast_to = VoidT,
             class predicate_signature=entity_only,
             class Predicate>
    std::size_t remove_flows_if (const Predicate& predicate) {
        static_assert(
            is_any<predicate_signature, entity_only, with_descriptor>(),
            "Got invalid tag! Can only be `entity_only` or `with_descriptor`."
        );

        const auto num_flows_before = num_flows();

        if constexpr (is_same<predicate_signature, entity_only>()) {
            boost::remove_edge_if([&predicate, this](const auto ed){
                return predicate(this->nw[ed]);
            }, nw);
        }
        else {
            boost::remove_edge_if([&predicate, this](const auto ed){
                return predicate(ed, this->nw[ed]);
            }, nw);
        }

        return num_flows_before - num_flows();
    }

    /// Conditionally remove inflows & associated edges for a specific vertex
    /** Removes those elements where the predicate returns `true` for the
      * specified vertices' incoming edges.
      *
      * \tparam  predicate_signature  `entity_only` or `with_descriptor`
      *
      * \param   v          The vertex to evaluate the predicate for
      * \param   predicate  A unary or binary predicate function that will
      *                     receive the respective edge descriptor (if the
      *                     ::with_descriptor tag is given) and the Flow
      *                     object to decide removal of.
      *                     The signature hence needs to be
      *                     `bool(EdgeDesc, Flow)` when the descriptor is to
      *                     be included, or just `bool(Flow)` otherwise.
      *
      * \warning Invalidates edge iterators!
      *
      * \returns The number of removed flows
      */
    template<template<class...> class cast_to = VoidT,
             class... opts,
             class Predicate>
    auto remove_in_flows_if (const Predicate& predicate, const VertexDesc v) {
        using EdgeIter = typename NetworkTraits::in_edge_iterator;
        return __remove_flows_if<EdgeIter, cast_to, opts...>(predicate, v);
    }

    /// Conditionally remove outflows & associated edges for a specific vertex
    /** Removes those elements where the predicate returns `true` for the
      * specified vertices' outgoing edges.
      *
      * \tparam  predicate_signature  `entity_only` or `with_descriptor`
      *
      * \param   v          The vertex to evaluate the predicate for
      * \param   predicate  A unary or binary predicate function that will
      *                     receive the respective edge descriptor (if the
      *                     ::with_descriptor tag is given) and the Flow
      *                     object to decide removal of.
      *                     The signature hence needs to be
      *                     `bool(EdgeDesc, Flow)` when the descriptor is to
      *                     be included, or just `bool(Flow)` otherwise.
      *
      * \warning Invalidates edge iterators!
      *
      * \returns The number of removed flows
      */
    template<template<class...> class cast_to = VoidT,
             class... opts,
             class Predicate>
    auto remove_out_flows_if (const Predicate& predicate, const VertexDesc v) {
        using EdgeIter = typename NetworkTraits::out_edge_iterator;
        return __remove_flows_if<EdgeIter, cast_to, opts...>(predicate, v);
    }


    // -- Computations --------------------------------------------------------

    /// Returns the accumulated resource losses
    /** These are the resources that are no longer part of the interaction
      * network, e.g. because populations became extinct or were not viable
      * after splitting.
      *
      * \TODO make this work via const reference
      */
    ResVec total_losses () const {
        return _losses;
    }

    /// Shortcut for available_resources, uses the full network
    ResVec available_resources () const {
        return available_resources(this->nw);
    }

    /// Available resource amount of all (regular-like) populations
    /** Includes the `reservoir`, `reagents` and `composition * size`.
      * If some of these quantities contain NaNs, they are unmasked and
      * replaced with zero.
      */
    template<class NW>
    ResVec available_resources (NW&& nw) const {
        ResVec total = ResSpace::zeros();
        for (const auto& pop : populations<Population>(nw)) {
            total += (  pop->get_size() * pop->composition_unmasked
                      + pop->get_reservoir()
                      + pop->get_reagents());
        }
        return total;
    }

    /// Shortcut for total_mass, using the full network
    double total_mass () const {
        return total_mass(this->nw);
    }

    /// The total mass of all (regular-like) populations in the network
    /** Aggregates `size * mass`, where mass is the specific mass of the
      * individuals in a population.
      */
    template<class NW>
    double total_mass (NW&& nw) const {
        auto total = 0.;
        for (const auto& pop : populations<Population>(nw)) {
            total += pop->get_size() * pop->mass;
        }
        return total;
    }

    /// Shortcut for mean_composition, using the full network
    ResVec mean_composition () const {
        return mean_composition(this->nw);
    }

    /// Computes the mean individual composition
    template<class NW>
    ResVec mean_composition (NW&& nw) const {
        auto total_individuals = 0.;
        auto sum = ResVec{};
        sum.fill(0.);

        for (const auto& pop : populations<Population>(nw)) {
            total_individuals += pop->get_size();
            sum += pop->composition_unmasked * pop->get_size();
        }

        return sum / total_individuals;
    }

    // TODO Add more here


    // -- Helper functions ----------------------------------------------------

    /// Clean up the network, i.e. remove deactivated populations and flows
    void cleanup () {
        log->debug("Performing cleanup operations ...");

        remove_flows_if([](const auto& flow){
            return not flow->is_active();
        });
        remove_populations_if([](const auto& pop){
            return not pop->is_active();
        });

        _need_cleanup = false;
        log->debug("Cleanup finished.");
    }

    /// Whether cleanup is needed
    bool need_cleanup () const {
        return _need_cleanup;
    }


private:

    /// Removes in/out edges if the predicate is fulfilled
    /** Helper function for remove_in_flows_if and remove_out_flows_if
      *
      * \tparam  predicate_signature  `entity_only` or `with_descriptor`
      *
      * \param   v          The vertex to evaluate the predicate for
      * \param   predicate  A unary or binary predicate function that will
      *                     receive the respective edge descriptor (if the
      *                     ::with_descriptor tag is given) and the Flow
      *                     object to decide removal of.
      *                     The signature hence needs to be
      *                     `bool(EdgeDesc, Flow)` when the descriptor is to
      *                     be included, or just `bool(Flow)` otherwise.
      *
      * \returns Number of removed flows
      */
    template<
        class edge_iterator,
        template<class ...> class cast_to = VoidT,
        class predicate_arg_mode = entity_only,
        class Predicate
    >
    std::size_t __remove_flows_if (Predicate& predicate, const VertexDesc v) {
        using in_edge_iterator = typename NetworkTraits::in_edge_iterator;
        using out_edge_iterator = typename NetworkTraits::out_edge_iterator;

        static_assert(
            is_any<predicate_arg_mode, entity_only, with_descriptor>(),
            "Got invalid tag! Can only be `entity_only` or `with_descriptor`."
        );
        static_assert(
            is_any<edge_iterator, in_edge_iterator, out_edge_iterator>(),
            "Can only iterate over in- or out-edges!"
        );

        // Create iterators
        edge_iterator ei, ei_end, next;
        if constexpr (is_same<edge_iterator, in_edge_iterator>()) {
            boost::tie(ei, ei_end) = boost::in_edges(v, nw);
        }
        else {
            boost::tie(ei, ei_end) = boost::out_edges(v, nw);
        }

        std::size_t num_removed = 0;
        for (next = ei; ei != ei_end; ei = next) {
            ++next;

            // Check the predicate; if false, this edge is NOT removed
            if constexpr (is_same<predicate_arg_mode, entity_only>()) {
                if (not predicate(as<cast_to>(nw[*ei]))) continue;
            }
            else {
                if (not predicate(*ei, as<cast_to>(nw[*ei]))) continue;
            }

            remove_edge(*ei);
            num_removed++;
        }

        return num_removed;
    }


    /// Creates a transformed graph iterator range
    /** Depending on the `to_return` template parameter, either an iterator
      * over entities is returned or -- with the `with_descriptor` tag --
      * an iterator over pairs of (descriptor, entity) is returned.
      * The entity is the bundled property associated with the descriptor and
      * retrieved via `this->nw[d]`.
      * With `descriptor_only`, this falls back to using the Utopia::range
      * iterator that directly returns vertex descriptors.
      *
      * If the `cast_to` template template argument differs from the base
      * template types, BasePopulation and BaseFlow, the transformation
      * function will also perform an std::dynamic_pointer_cast to the
      * specified type.
      *
      */
    template<
        IterateOver iterate_over,
        template<class...> class cast_to = VoidT,
        class to_return = entity_only,
        class... Args
    >
    inline auto __make_trf_range (Args&&... args) const {
        static_assert(
            is_any<to_return, entity_only, descriptor_only, with_descriptor>(),
            "Got invalid tag! Can only be `entity_only`, `descriptor_only`, "
            "or `with_descriptor`."
        );

        if constexpr (is_same<to_return, with_descriptor>()) {
            return
                make_transformed_graph_iterator_range<iterate_over>(
                    [this](auto d){
                        return std::make_pair(d, as<cast_to>(this->nw[d]));
                    },
                    std::forward<Args>(args)...
                );
        }
        else if constexpr (is_same<to_return, entity_only>()) {
            return
                make_transformed_graph_iterator_range<iterate_over>(
                    [this](auto d){
                        return as<cast_to>(this->nw[d]);
                    },
                    std::forward<Args>(args)...
                );
        }
        else {
            return range<iterate_over>(std::forward<Args>(args)...);
        }
    }


    /// A helper that decides whether to return a descriptor, entity, or both
    template<
        class to_return,
        template<class...> class cast_to = VoidT,
        class Desc, class Entity
    >
    inline auto __build_rv ([[maybe_unused]] Desc&& desc,
                            [[maybe_unused]] Entity&& entity) const
    {
        static_assert(
            is_any<to_return, entity_only, descriptor_only, with_descriptor>(),
            "Got invalid tag! Can only be `entity_only`, `descriptor_only`, "
            "or `with_descriptor`."
        );

        if constexpr (is_same<to_return, descriptor_only>()) {
            return desc;
        }
        else if constexpr (is_same<to_return, entity_only>()) {
            return as<cast_to>(entity);
        }
        else {
            return std::make_pair(desc, as<cast_to>(entity));
        }
    }


    // -- Setup functions -----------------------------------------------------

    /// Set up the network given some configuration
    // FIXME Improve or remove
    void setup_nw (const Config& cfg) {
        const auto num_entities = get_as<Config>("num", cfg);
        const auto num_pops = get_as<Config>("populations", num_entities);
        const auto num_inflows = get_as<Config>("inflows", num_entities);

        const auto pop_defaults = get_as<Config>("populations", cfg);
        const auto flow_defaults = get_as<Config>("flows", cfg);

        // Create populations
        for (const auto& kv_pair : num_pops) {
            const auto kind = kv_pair.first.as<std::string>();
            const auto num = kv_pair.second.as<int>();
            const auto pop_cfg = get_as<Config>(kind, pop_defaults);

            log->info("Adding {} {} population{} ...", num, kind,
                      num != 1 ? "s":"");
            add_populations(num, kind, pop_cfg);
        }

        // For each population, add some incoming flows
        // FIXME Can lead to duplicate edges
        for (const auto& kv_pair : num_inflows) {
            const auto kind = kv_pair.first.as<std::string>();
            const auto num = kv_pair.second.as<int>();

            if (num < 1) {
                log->debug("Not adding {} flows.", kind);
                continue;
            }
            log->info("Adding {} {} flow{} to each population ...",
                      num, kind, num != 1 ? "s":"");

            const auto flow_cfg = get_as<Config>(kind, flow_defaults);

            // Specify the selection configuration
            // TODO Make configurable
            auto select_cfg = Config{};
            select_cfg["mode"] = "random";
            select_cfg["num"] = num;

            if (kind == "trophic") {
                for (const auto v : vertices()) {
                    add_flows<incoming, TrophicFlow>(v, select_cfg, flow_cfg);
                }
            }
            else if (kind == "base") {
                for (const auto v : vertices()) {
                    add_flows<incoming, BaseFlow>(v, select_cfg, flow_cfg);
                }
            }
            else {
                throw std::invalid_argument("Invalid flow kind: " + kind);
            }
        }
    }

};


} // namespace Utopia::Models::EEcosy

#endif // UTOPIA_MODELS_EECOSY_ECONET_HH
