#ifndef UTOPIA_MODELS_EECOSY_REACTIONS_HH
#define UTOPIA_MODELS_EECOSY_REACTIONS_HH

#include <armadillo>

#include "types.hh"


namespace Utopia::Models::EEcosy {

/// The set of reactions
template<class ResourceSpace>
struct ReactionSet {
    using ResSpace = ResourceSpace;
    static constexpr std::size_t rspace_dims = ResSpace::num_dims;
};


} // namespace Utopia::Models::EEcosy

#endif // UTOPIA_MODELS_EECOSY_REACTIONS_HH
