#ifndef UTOPIA_MODELS_EECOSY_FLOW_BASE_HH
#define UTOPIA_MODELS_EECOSY_FLOW_BASE_HH


#include "../types.hh"
#include "../utils.hh"


namespace Utopia::Models::EEcosy {

using Utopia::Utils::apply_noise;


/// Base class for flow objects
/** This object is intended to be associated with the bundled edge properties
  * of the ecological interaction network.
  * The derived classes implement the actual dynamics, while this class defines
  * the shared interface.
  */
template<class _ResSpace, class _RNG>
class BaseFlow {
public:
    /// Export the resource space
    using ResSpace = _ResSpace;

    /// Export the RNG type
    using RNG = _RNG;

    /// Shorthand for the number of dimensions in the resource space
    static constexpr std::size_t rspace_dims = ResSpace::num_dims;

private:
    /// A string describing the kind of flow type this class represents
    inline static const std::string __base_kind = "base";

    /// The ID of the next member of this type; incremented in constructor
    /** \note IDs start at 1 such that there is 0 to signify something else
      */
    inline static IDType next_id = 1;

    /// Whether the flow is actually active
    bool active;

public:
    /// ID of this object
    const IDType id;

    // .. Parameters ..........................................................
    /// A representation of the properties of this object at construction
    /** \detail This is set at construction. It is NOT automatically
      *         updated when parameters change. Refer to ::get_properties for
      *         more info.
      */
    const Config cfg;

public:
    // .. Constructors ........................................................
    /// Set up a BaseFlow
    BaseFlow (const std::shared_ptr<RNG>& rng,
              const ResSpace&,
              const Config& setup_cfg)
    :
        active(true),
        id(next_id++),
        cfg(apply_noise(setup_cfg, rng))

        // Initialize shared parameters
        // ...
    {}

    /// Base flow destructor
    virtual ~BaseFlow () {}

protected:
    // .. Initialization Methods ..............................................



public:
    // .. Getters .............................................................

    /// A description of this type, mainly for log messages
    virtual const std::string& kind () const {
        return __base_kind;
    }

    /// A numeric identifier of this type, mainly for writing data
    virtual char kind_id () const {
        return 0;
    }

    /// Whether the flow is active
    bool is_active () const {
        return active;
    }

    /// Retrieve the properties for recreating this flow
    virtual Config get_properties () const {
        return cfg;
    }


    // .. Setters .............................................................

    /// Deactivate the flow; note that it cannot be re-activated
    void deactivate () {
        active = false;
    }

};


} // namespace Utopia::Models::EEcosy

#endif // UTOPIA_MODELS_EECOSY_FLOW_BASE_HH
