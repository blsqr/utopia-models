#ifndef UTOPIA_MODELS_EECOSY_FLOW_TROPHIC_HH
#define UTOPIA_MODELS_EECOSY_FLOW_TROPHIC_HH

#include <cmath>
#include <limits>

#include "base.hh"


namespace Utopia::Models::EEcosy {


/// Base class for flow objects
template<class _ResSpace, class _RNG>
class TrophicFlow : public BaseFlow<_ResSpace, _RNG> {
public:
    using Base = BaseFlow<_ResSpace, _RNG>;

    /// Export the resource space
    using ResSpace = _ResSpace;

    /// Export the RNG type
    using RNG = _RNG;

    /// Type of resource vectors
    using ResVec = typename ResSpace::ResVec;

private:
    /// A string describing the kind of flow type this class represents
    inline static const std::string __trophic_kind = "trophic";

public:
    // .. Parameters ..........................................................

    /// Foraging strength
    /** The number of individuals of the source (prey) population being
      * consumed *per* individual in the target (predator) population.
      */
    const double strength;

protected:
    // .. State variables .....................................................

    /// Functional response
    double response;

    /// Foraging effort
    double effort;

    /// Tracking variable: the number of actually consumed individuals
    double consumption;

    /// Tracking variable for the resource flow during one time step
    ResVec resource_flow;
    // TODO Update and use this variable


public:
    // .. Constructors ........................................................
    /// Set up a TrophicFlow
    TrophicFlow (const std::shared_ptr<RNG>& rng,
                 const ResSpace& rspace,
                 const Config& setup_cfg)
    :
        Base(rng, rspace, setup_cfg),

        // Initialize parameters
        strength(get_as<double>("strength", this->cfg)),

        // Initialize state variables, using best guesses for the functional
        // response and foraging effort (might need balancing later on)
        response(0.001),
        effort(1.),
        consumption{std::numeric_limits<double>::quiet_NaN()},
        resource_flow{ResSpace::NaN()}
    {}


    // .. Getters .............................................................

    /// A description of this type, mainly for log messages
    const std::string& kind () const override {
        return __trophic_kind;
    }

    /// A numeric identifier of this type, mainly for writing data
    char kind_id () const override {
        return 1;
    }

    /// The current value of the functional response
    double get_response () const {
        return response;
    }

    /// The current foraging effort value
    double get_effort () const {
        return effort;
    }

    /// The current consumption value
    double get_consumption () const {
        return consumption;
    }

    /// The current resource flow value
    const ResVec& get_resource_flow () const {
        return resource_flow;
    }


    // .. Setters .............................................................


    // .. Dynamics ............................................................

    /// Updates the foraging effort of this trophic connection
    /**
      */
    void update_foraging_effort (const double prey_mass,
                                 const double total_pressure,
                                 const double min_effort)
    {
        effort = (prey_mass * response) / total_pressure;

        if (effort < min_effort) {
            effort = min_effort;
        }
    }

    /// Updates the functional response for this trophic connection
    /** Returns the relative change compared to the old response.
      */
    template<class Pop>
    double update_response (const double min_strength, Pop&& prey,
                            const double total_pressure)
    {
        const auto old_response = response;

        response =
              (strength * effort * prey->get_size())
            / (min_strength * prey->get_size() + total_pressure);

        // fmt::print(
        //     "  prey ID:          {:d}\n"
        //     "  prey size:        {:.1f}\n"
        //     "  prey mass:        {:.3g}\n"
        //     "  strength:         {:.3g}\n"
        //     "  effort:           {:.3g}\n"
        //     "  min_strength:     {:.3g}\n"
        //     "  total_pressure:   {:.3g}\n"
        //     "  old response:     {:.5g}\n"
        //     "  new response:     {:.5g}\n\n",
        //     prey->id,
        //     prey->get_size(),
        //     prey->mass,
        //     strength,
        //     effort,
        //     min_strength,
        //     total_pressure,
        //     old_response,
        //     response
        // );

        return std::fabs(old_response - response) / old_response;
    }

    /// Updates the number of consumed individuals via this trophic connection
    /**
      * \param dt    The time constant
      * \param prey  The prey of this trophic connection
      * \param pred  The predator of this trophic connection
      */
    template<class Pop>
    double update_consumption (const double dt, Pop&&, Pop&& pred) {
        consumption = dt * pred->get_size() * response;

        // fmt::print(
        //     "  prey ID:          {:d}\n"
        //     "  prey size:        {:.1f}\n"
        //     "  pred ID:          {:d}\n"
        //     "  pred size:        {:.1f}\n"
        //     "  response:         {:.3g}\n"
        //     "  consumption:      {:.1f}\n\n",
        //     prey->id, prey->get_size(),
        //     pred->id, pred->get_size(),
        //     response,
        //     consumption
        // );

        return consumption;
    }

    /// Scales the consumption by the given factor, then returns scaled value
    double scale_consumption (const double scale_factor) {
        consumption *= scale_factor;
        return consumption;
    }

    /// Applies the effect of foraging on the connected predator and prey
    template<class Pop>
    void apply_foraging (Pop&& prey, Pop&& pred) {
        // Compute and store resource flow
        resource_flow = consumption * prey->composition_unmasked;

        // In the prey population, deduce the population size
        prey->adjust_size(-consumption);

        // In the predator population, add foraged resources to the reagents
        pred->adjust_reagents(resource_flow);
    }

};

} // namespace Utopia::Models::EEcosy

#endif // UTOPIA_MODELS_EECOSY_FLOW_TROPHIC_HH
