#ifndef UTOPIA_MODELS_EECOSY_NETWORK_HH
#define UTOPIA_MODELS_EECOSY_NETWORK_HH

#include <type_traits>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/random.hpp>

#include <UtopiaUtils/utils/nw_filter.hh>

#include "types.hh"
#include "populations.hh"
#include "flows.hh"

// TODO Check which ones should be incldued here
#include "../../utils/filtered_nw.hh"
#include "../../utils/filtered_nws.hh"


namespace Utopia::Models::EEcosy::NW {
/** NOTE The ::NW namespace is meant to contain code that makes sense in the
  *      context of the BGL. It should not include any higher-level code.
  */

using namespace Utopia::Utils::NW;

/// The vertex and edge property wrapper
/** This supplies value-based semantics for use as a graph property that
  * transparently relays to the actual property type.
  *
  * \note In the simplest case, this can be an std::shared_ptr<Property>
  */
template<class Property>
using PropertyWrapper = std::shared_ptr<Property>;


// .. Graph-related type definitions ..........................................

/// The container type for out edges
/** Used by boost for vertex-based access to edges. Typical choices are
  * boost::listS or boost::setS, where the latter ensures that no parallel
  * edges can be added.
  */
using OutEdgeList = boost::setS;

/// Tye container type for vertices
/** While boost::vecS *should* be faster, it does not allow to delete vertices
  * during iteration! In practice, given that only small data is stored in the
  * vertex selector, it should not be that much of a difference.
  */
using VertexList = boost::listS;

/// Directedness of the graph
/** \NOTE If efficient access to in-edges is not required, the boost::directedS
  *       type may suffice.
  */
using Directedness = boost::bidirectionalS;

/// Vertex property type
template<class ResSpace, class RNG>
using VertexProperties = PropertyWrapper<BasePopulation<ResSpace, RNG>>;

/// Edge property type
template<class ResSpace, class RNG>
using EdgeProperties = PropertyWrapper<BaseFlow<ResSpace, RNG>>;

/// Edge property type
using GraphProperties = boost::no_property;

/// Edge list
using EdgeList = boost::listS;


// .. Graph type definition ...................................................

/// The EEcosy network type
template<class ResSpace, class RNG>
using Network =
    boost::adjacency_list<OutEdgeList,
                          VertexList,
                          Directedness,
                          VertexProperties<ResSpace, RNG>,
                          EdgeProperties<ResSpace, RNG>,
                          GraphProperties,
                          EdgeList>;

// .. Shorthand notations .....................................................

/// The vertex descriptor type of a given graph type
template<class G>
using VertexDesc = typename boost::graph_traits<G>::vertex_descriptor;

/// The edge descriptor type of a given graph type
template<class G>
using EdgeDesc = typename boost::graph_traits<G>::edge_descriptor;


// .. Helpers .................................................................



} // namespace Utopia::Models::EEcosy::NW

#endif // UTOPIA_MODELS_EECOSY_NETWORK_HH
