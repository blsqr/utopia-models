#ifndef UTOPIA_MODELS_EECOSY_RESOURCE_SPACE_HH
#define UTOPIA_MODELS_EECOSY_RESOURCE_SPACE_HH

#include <string>
#include <limits>

#include <armadillo>
#include <yaml-cpp/yaml.h>

#include <utopia/data_io/cfg_utils.hh>

#include <UtopiaUtils/utils/cfg.hh>
#include <UtopiaUtils/utils/math.hh>
#include <UtopiaUtils/utils/meta.hh>

#include "types.hh"


namespace Utopia::Models::EEcosy {

/// This struct contains all properties of the resource space
template<std::size_t num_dimensions>
struct ResourceSpace {
    /// The number of dimensions of the resource space
    static constexpr std::size_t num_dims = num_dimensions;

    /// Type for resource vectors
    using ResVec = ResourceVectorType<num_dims>;

    /// Type for mask vectors
    using MaskVec = MaskVectorType<num_dims>;

    /// Type for square resource transformation matrices
    using TrfMat = TransformationMatrixType<num_dims>;

    /// General type for vectors
    template<class eT>
    using FixedVec = typename arma::Col<eT>::template fixed<num_dims>;

    /// General type for matrices
    template<class eT>
    using FixedMat = typename arma::Mat<eT>::template fixed<num_dims,num_dims>;


    // -- Non-static interface ------------------------------------------------

    /// Resource dimension weights
    const ResVec weights;

    /// Initialize the resource space
    /**
      *
      * \param cfg   The resource space configuration. If a `weights` key is
      *              given, will use those to initialize the `weights` member.
      */
    ResourceSpace (const Config& cfg = {})
    :
        weights(this->get_as<ResVec>("weights", cfg, ones()))
    {}



    // -- Static utility functions --------------------------------------------

    /// An armadillo object filled with NaNs; useful for initializations
    template<class RT=ResVec>
    static RT NaN () {
        auto m = RT{};
        m.fill(std::numeric_limits<typename RT::elem_type>::quiet_NaN());
        return m;
    }

    /// An armadillo object filled with zeros; useful for initializations
    template<class RT=ResVec>
    static RT zeros () {
        auto m = RT{};
        m.fill(0.);
        return m;
    }

    /// An armadillo object filled with ones; useful for initializations
    template<class RT=ResVec>
    static RT ones () {
        auto m = RT{};
        m.fill(1.);
        return m;
    }

    /// A static member that can be used to return references
    static inline const ResVec fixed_NaN = NaN();  // TODO Consider removing

    /// Read a config entry, handling mappings as generator expressions
    template<class RT, class... tags, class Config>
    static RT get_as (const std::string& key, const Config& node) {
        using Utopia::Utils::apply_operations;
        using Utopia::Utils::get_as_vec;
        using arma::uword;  // the index type
        using eT = typename RT::elem_type;  // the element type

        if (not node[key] or node[key].Type() != YAML::NodeType::Map) {
            return apply_operations<tags...>(get_as_vec<RT>(key, node));
        }
        // Have to work on the nested mapping, supplying generator params
        const auto& params = node[key];

        // Generate an armadillo object using the info from the mapping
        auto m = RT{};
        m.fill(0.);

        // Fill the object
        if (params["fill"]) {
            m.fill(params["fill"].template as<eT>());
        }
        else if (params["fill_nan"] and params["fill_nan"].template as<bool>())
        {
            m.fill(std::numeric_limits<eT>::quiet_NaN());
        }

        // Set specified elements to specified values
        if (params["set"] and params["set"].Type() != YAML::NodeType::Null) {
            if (params["set"].Type() != YAML::NodeType::Map) {
                throw std::invalid_argument("The `set` key needs to be a "
                                            "mapping of indices to values!");
            }

            for (const auto& kv_pair : params["set"]) {
                const auto val = kv_pair.second.template as<eT>();

                // Distinguish vector- and matrix-like return types
                if constexpr (RT::is_col or RT::is_row) {
                    // Vector-like ==> single index
                    const auto idx = kv_pair.first.template as<uword>();

                    try {
                        m(idx) = val;
                    }
                    catch (std::logic_error&) {
                        // be generous, don't throw (more robust & flexible)
                    }
                }
                else {
                    // Matrix-like ==> multi-index
                    using IdxT = std::array<uword, 2>;
                    const auto idx = kv_pair.first.template as<IdxT>();

                    try {
                        m(idx[0], idx[1]) = val;
                    }
                    catch (std::logic_error&) {
                        // be generous, don't throw (more robust & flexible)
                    }
                }
            }
        }

        // Finally, apply some tag-controlled functions
        m = apply_operations<tags...>(m);

        return m;
    }

    /// Read a config entry, handling mappings as generator expressions
    template<class RT, class... tags, class Config>
    static RT get_as (const std::string& key, const Config& node, RT fallback){
        try {
            return ResourceSpace::get_as<RT, tags...>(key, node);
        }
        catch (Utopia::KeyError&) {
            return fallback;
        }
        // All other errors will (rightly) be thrown
    }


    // .. Non-static utility functions ........................................

};


/// The default resource space
using DefaultResourceSpace = ResourceSpace<10>;

/// A small resource space
using SmallResourceSpace = ResourceSpace<3>;


} // namespace Utopia::Models::EEcosy

#endif // UTOPIA_MODELS_EECOSY_RESOURCE_SPACE_HH
