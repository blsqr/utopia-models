#ifndef UTOPIA_MODELS_EECOSY_TEST_GRAPH_TYPES_HH
#define UTOPIA_MODELS_EECOSY_TEST_GRAPH_TYPES_HH

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/graph/graph_traits.hpp>


namespace Utopia::Models::EEcosy {

// -- Type definitions --------------------------------------------------------

/// A custom type for bundled graph properties
struct GraphEntity {
    inline static int next_id = 0;
    int id;
    int kind = 0;
    int visits = 0;

    GraphEntity() : id(next_id++), kind(0), visits(0) {}
};

using Node = GraphEntity;
using Edge = GraphEntity;

/// The BGL default adjacency_list type. Supports most iterator.hh tools
using DefaultGraph = boost::adjacency_list<>;

/// A test graph type for general purpose tests
using FullFunctioningGraph =
    boost::adjacency_list<
        // edge and vertex container types
        boost::listS,
        boost::vecS,
        boost::bidirectionalS,
        // vertex and edge property types
        Node,
        Edge
    >;

/// A FullFunctioningGraph with undirected edges
using UndirectedFFG =
    boost::adjacency_list<
        // edge and vertex container types
        boost::listS,
        boost::vecS,
        boost::undirectedS,
        // vertex and edge property types
        Node,
        Edge
    >;

/// A test graph type that uses different container types
using ExoticContainerTypesGraph =
    boost::adjacency_list<
        // edge and vertex container types
        boost::setS,
        boost::listS,
        boost::undirectedS,
        // vertex and edge property types
        Node,
        Edge
    >;

/// The BGL default adjacency_matrix type.
using DefaultMatrix = boost::adjacency_matrix<>;

/// An adjacency matrix with custom node and edge properties
using MatrixWithProperties =
    boost::adjacency_matrix<boost::undirectedS, Node, Edge>;


} // namespace Utopia::Models::EEcosy

#endif // UTOPIA_MODELS_EECOSY_TEST_GRAPH_TYPES_HH
