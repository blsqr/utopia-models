#define BOOST_TEST_MODULE test het_graph

#include <memory>

#include <boost/test/unit_test.hpp>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/graph_traits.hpp>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include "testtools.hh"


// ++ Definitions +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

using Utopia::get_as;
using Config = Utopia::DataIO::Config;

/// The graph type
template<class VertexProp, class EdgeProp>
using Graph =
    boost::adjacency_list<boost::setS,            // out edge container
                          boost::listS,           // vertex container
                          boost::bidirectionalS,  // directedness
                          std::shared_ptr<VertexProp>,
                          std::shared_ptr<EdgeProp>
                          >;

// Some bundled property types ................................................

/// A base class for bundled vertex or edge property types
class BaseEntity {
private:
    /// A string describing the kind of type this class represents
    inline static const std::string __base_kind = "BaseEntity";

    /// The ID of the next member of this type; incremented in constructor
    /** \note IDs start at 1 such that there is 0 to signify something else
      */
    inline static std::size_t next_id = 1;

    /// Whether the flow is actually active
    bool active;

protected:
    /// The logger for this instance, passed down from the model
    const std::shared_ptr<spdlog::logger> log;

public:
    /// ID of this object
    const std::size_t id;

    /// The configuration this object was set up with
    const Config cfg;

    /// Construct a base entity
    BaseEntity (std::shared_ptr<spdlog::logger>& logger,
                const Config& cfg = {})
    :
        active(true),
        log(logger),
        id(next_id++),
        cfg(cfg)

        // Initialize shared parameters
        // ...
    {
        this->log->info("BaseEntity constructor finished.");
    }

    /// Base class virtual destructor
    virtual ~BaseEntity () {}

    /// A description of this type, mainly for log messages
    virtual const std::string& kind () const {
        return __base_kind;
    }

    /// A numeric identifier of this type, mainly for writing data
    virtual char kind_id () const {
        return 0;
    }

    /// Whether the flow is active
    bool is_active () const {
        return active;
    }

    /// Deactivate the flow; note that it cannot be re-activated
    void deactivate () {
        active = false;
    }
};

/// Some vertex property
class VertexA : public BaseEntity {
private:
    /// A string describing the kind of flow type this class represents
    inline static const std::string __VertexA_kind = "VertexA";

public:
    /// Some parameter
    const double some_parameter;

    /// Set up derived object
    VertexA (std::shared_ptr<spdlog::logger>& logger,
             const Config& cfg = {})
    :
        BaseEntity(logger, cfg),

        // Initialize parameters
        some_parameter(get_as<double>("some_parameter", this->cfg, 1.23))
    {
        this->log->info("VertexA constructor finished.");
    }

    /// A description of this type, mainly for log messages
    const std::string& kind () const override {
        return __VertexA_kind;
    }

    /// A numeric identifier of this type, mainly for writing data
    char kind_id () const override {
        return 1;
    }
};

/// Another vertex property type
class VertexB : public BaseEntity {
private:
    /// A string describing the kind of flow type this class represents
    inline static const std::string __VertexB_kind = "VertexB";

public:
    /// Some parameter
    const int another_parameter;

    /// Set up derived object
    VertexB (std::shared_ptr<spdlog::logger>& logger,
             const Config& cfg = {})
    :
        BaseEntity(logger, cfg),

        // Initialize parameters
        another_parameter(get_as<double>("another_parameter", this->cfg, 42))
    {
        this->log->info("VertexB constructor finished.");
    }

    /// A description of this type, mainly for log messages
    const std::string& kind () const override {
        return __VertexB_kind;
    }

    /// A numeric identifier of this type, mainly for writing data
    char kind_id () const override {
        return 2;
    }
};


/// Another vertex property type
class Edge : public BaseEntity {
private:
    /// A string describing the kind of flow type this class represents
    inline static const std::string __Edge_kind = "Edge";

public:
    double weight;

    /// Set up derived object
    Edge (std::shared_ptr<spdlog::logger>& logger,
          const Config& cfg = {})
    :
        BaseEntity(logger, cfg),

        // Initialize parameters
        weight(get_as<double>("weight", this->cfg, 1.))
    {
        this->log->info("Edge constructor finished.");
    }

    /// A description of this type, mainly for log messages
    const std::string& kind () const override {
        return __Edge_kind;
    }

    /// A numeric identifier of this type, mainly for writing data
    char kind_id () const override {
        return 1;  // can be identical to vertex kind_id, because it's an edge
    }
};


// ++ Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

using Utopia::Models::EEcosy::InfrastructureBase;

/// The specialized infrastructure fixture
struct Infrastructure : public InfrastructureBase {
    Infrastructure () : InfrastructureBase("het_graph.yml") {}
};



// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Tests using the Infrastructure fixture
BOOST_FIXTURE_TEST_SUITE (heterogeneous_graph, Infrastructure)

BOOST_AUTO_TEST_CASE (example)
{
    // Use a graph with BaseEntity for vertex properties
    using G = Graph<BaseEntity, Edge>;
    using VertexProp = typename G::vertex_property_type;
    //                 ... which is std::shared_ptr<BaseEntity>

    // Create a graph
    G g(0);

    {
    // Add a vertex; will try to default-construct a BaseEntity, but fail
    const auto v = boost::add_vertex(g);
    BOOST_TEST(not g[v]);

    // ... but we want VertexA there anyway! Let's construct one and assign it
    VertexProp vtxA = std::make_shared<VertexA>(log, Config{});
    BOOST_TEST(vtxA->kind() == "VertexA");

    g[v] = vtxA;
    BOOST_TEST(g[v]->kind() == "VertexA");

    // ... and VertexB elsewhere
    const auto w = boost::add_vertex(g);
    VertexProp vtxB = std::make_shared<VertexB>(log, Config{});
    g[w] = vtxB;
    BOOST_TEST(g[w]->kind() == "VertexB");
    }

    // Ok, so far so good.
    // However, calling the non-shared interface is NOT possible right now!
    // As the bundled vertex properties are std::shared_ptr<BaseEntity>, only
    // the interface of BaseEntity is available!

    // We can circumvent this via dynamic pointer casts
    {
    const auto vA = boost::vertex(0, g);
    const auto vB = boost::vertex(1, g);

    BOOST_TEST(   std::dynamic_pointer_cast<VertexA>(g[vA])->some_parameter
               == 1.23);
    BOOST_TEST(   std::dynamic_pointer_cast<VertexB>(g[vB])->another_parameter
               == 42);
    }

    // This is the end of this example.
    // Of course, the above is super tedious to do in practice, which is why a
    // bunch of wrappers and manager code is required:
    //      - filtered graphs that allow iteration over one kind of specialised
    //        properties, ensuring that the dynamic casts will work
    //      - transformed iterator ranges that take care of performing the
    //        dynamic pointer cast, using the information from the filters
}

BOOST_AUTO_TEST_SUITE_END()
