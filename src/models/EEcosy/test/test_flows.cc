#define BOOST_TEST_MODULE test flows

#include <tuple>

#include <boost/test/unit_test.hpp>

#include "testtools.hh"

#include "../flows.hh"
#include "../resource_space.hh"



// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Models::EEcosy;

/// The specialized infrastructure fixture
struct Infrastructure : public InfrastructureBase {
    Infrastructure () : InfrastructureBase("flows.yml") {}
};


/// The available flow types
using flow_types =
    std::tuple<BaseFlow<DefaultResourceSpace, Infrastructure::RNG>,
               TrophicFlow<DefaultResourceSpace, Infrastructure::RNG>
               >;


/// Helper to find out the kind from a flow type
template<class FlowType, class ResSpace=DefaultResourceSpace>
const std::string get_kind() {
    if constexpr (std::is_same<FlowType,
                               BaseFlow<ResSpace, Infrastructure::RNG>>())
    {
        return "base";
    }
    else {
        return "trophic";
    }
}

// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Tests using the Infrastructure fixture
BOOST_FIXTURE_TEST_SUITE (test_flows, Infrastructure)

/// Test basic construction of flow types and their common interface
BOOST_AUTO_TEST_CASE_TEMPLATE (test_construction, FlowType, flow_types)
{
    test_config_callable([&](const Config& params){
        // Create the flow object
        auto flow = FlowType(rng, rspace, params);

        // ID should be larger than 0
        BOOST_TEST(flow.id > 0);

        // The kind descriptor and ID
        BOOST_TEST(flow.kind() == get_kind<FlowType>());

        if (flow.kind() == "base") {
            BOOST_TEST(flow.kind_id() == 0);
        }
        else if (flow.kind() == "trophic") {
            BOOST_TEST(flow.kind_id() == 1);
        }
        else {
            BOOST_FAIL("Unknown flow kind " << flow.kind());
        }

        // Whether the flow is active or not
        BOOST_TEST(flow.is_active());
        flow.deactivate();
        BOOST_TEST(not flow.is_active());

        // Parameter access
        // BOOST_TEST(flow.some_parameter == 42.);
        // ...

    }, cfg["construction"][get_kind<FlowType>()],
    "Construction", {__LINE__, __FILE__});
}

BOOST_AUTO_TEST_SUITE_END() // "test_flows"
