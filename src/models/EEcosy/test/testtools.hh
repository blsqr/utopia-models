#ifndef UTOPIA_MODELS_EECOSY_TEST_TESTTOOLS_HH
#define UTOPIA_MODELS_EECOSY_TEST_TESTTOOLS_HH

#include <yaml-cpp/yaml.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include <utopia/core/testtools.hh>

#include <UtopiaUtils/utils/testtools.hh>

#include "../types.hh"
#include "../resource_space.hh"


namespace Utopia::Models::EEcosy {

using namespace Utopia::TestTools;
using namespace Utopia::Utils;


// -- Fixtures ----------------------------------------------------------------

/// A base class for an infrastructure fixture
/** It's meant to be specialized in order to select the correct test file
  *
  * \note Unlike the Utopia::TestTools::BaseInfrastructure, this fixture sets
  *       up the ResourceSpace as well, thus reducing boilerplate code in the
  *       test-specialised fixtures.
  */
struct InfrastructureBase {
    using RNG = ModelRNG;

    using ResSpace = DefaultResourceSpace;
    using ResVec = typename ResSpace::ResVec;

    const Config cfg;
    std::shared_ptr<spdlog::logger> log;
    std::shared_ptr<RNG> rng;
    ResSpace rspace;

    InfrastructureBase (const std::string& config_file_path)
    :
        cfg(YAML::LoadFile(config_file_path)),

        // Set up a test logger
        log([](){
            auto logger = spdlog::get("test");

            // Create it only if it does not already exist
            if (not logger) {
                logger = spdlog::stdout_color_mt("test");
            }

            // Set level and global logging pattern
            logger->set_level(spdlog::level::trace);
            spdlog::set_pattern("%T.%e %^%l%$ %n :  %v");
            // "<HH:MM:SS.mmm> <level(colored)> <logger> :  <message>"

            return logger;
        }()),

        // Set up random number generator (with random seed)
        rng(std::make_shared<RNG>(std::random_device()())),
        rspace()
    {
        log->info("-------------------------------------------------------");
        log->info("InfrastructureBase fixture set up.");
        log->info("  Configuration file:  {}", config_file_path);
    }

    ~InfrastructureBase () {
        log->info("Tearing down InfrastructureBase fixture ...");
        log->info("-------------------------------------------------------\n");
        spdlog::drop(log->name());
    }
};


// TODO Add function to summarize benchmark results

} // end namespace

#endif // UTOPIA_MODELS_EECOSY_TEST_TESTTOOLS_HH
