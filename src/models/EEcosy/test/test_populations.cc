#define BOOST_TEST_MODULE test populations

#include <tuple>

#include <boost/test/unit_test.hpp>

#include "testtools.hh"

#include "../populations.hh"



// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Models::EEcosy;

/// The specialized infrastructure fixture
struct Infrastructure : public InfrastructureBase {
    Infrastructure () : InfrastructureBase("populations.yml") {}
};


/// All available population types
using pop_types =
    std::tuple<BasePopulation<DefaultResourceSpace, Infrastructure::RNG>,
               Population<DefaultResourceSpace, Infrastructure::RNG>>;

/// Helper to find out the kind from a population type
template<class PopType, class ResSpace=DefaultResourceSpace>
const std::string get_kind() {
    using BasePop = BasePopulation<ResSpace, Infrastructure::RNG>;
    if constexpr (std::is_same<PopType, BasePop>()) {
        return "base";
    }
    else {
        return "regular";
    }
}


// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Tests using the Infrastructure fixture
BOOST_FIXTURE_TEST_SUITE (test_populations, Infrastructure)

/// Test basic construction of population types
BOOST_AUTO_TEST_CASE_TEMPLATE (test_construction, PopType, pop_types)
{
    const auto kind = get_kind<PopType>();

    test_config_callable([&](const Config& params){
        // Create the population object
        auto pop = PopType(log, rng, rspace, params);

        // ID should be larger than 0
        BOOST_TEST(pop.id > 0);


        // The kind descriptor and ID
        BOOST_TEST(pop.kind() == kind);

        if (pop.kind() == "base") {
            BOOST_TEST(pop.kind_id() == 0);
        }
        else if (pop.kind() == "regular") {
            BOOST_TEST(pop.kind_id() == 1);
        }
        else {
            BOOST_FAIL("Unknown flow kind " << pop.kind());
        }

        // Also check the derived logger name
        BOOST_TEST(pop.get_logger()->name()
                   == "test.pop." + kind + "." + std::to_string(pop.id));

        // Whether the population is active or not
        BOOST_TEST(pop.is_active());
        pop.deactivate();
        BOOST_TEST(not pop.is_active());

        // Parameter access
        // BOOST_TEST(pop.some_parameter == 42.);
        // ...

    }, cfg["construction"][get_kind<PopType>()],
    "Construction", {__LINE__, __FILE__});
}

/// Test population destruction
BOOST_AUTO_TEST_CASE_TEMPLATE (test_destruction, PopType, pop_types)
{
    std::string logger_name;
    // Create a population object
    {
        auto pop = PopType(log, rng, rspace,
                           cfg["destruction"][get_kind<PopType>()]);

        // Store the logger name
        logger_name = log->name() + ".pop." + pop.kind() + "." + std::to_string(pop.id);

        // Check that a logger with that name was added to the logger registry
        BOOST_TEST(spdlog::get(logger_name));
    }
    // ... and let the population go out of scope
    // The associated logger should now be dropped
    BOOST_TEST(not spdlog::get(logger_name));
}


/// Test basic construction of population types
BOOST_AUTO_TEST_CASE (test_regular_pd)
{
    test_config_callable([&](const Config& params){
        // Get some config values
        const auto init_params = params["init"];
        const auto expected = params["expected"];

        // Create the regular population object
        auto pop = Population(log, rng, rspace, init_params);

        // Size should be as specified in config
        BOOST_TEST(pop.get_size() == get_as<double>("size", init_params));

        // Now, perform population dynamics and check that the size and
        // reservoir has changed as expected
        pop.perform_population_dynamics(.1, rng);

        BOOST_TEST(pop.get_size() == get_as<double>("size", expected));

        auto expected_reservoir = rspace.get_as<ResVec>("reservoir", expected);
        BOOST_TEST(pop.get_reservoir() == expected_reservoir,
                   tt::per_element());

    }, cfg["population_dynamics"]["regular"],
    "Population Dynamics", {__LINE__, __FILE__});
}


BOOST_AUTO_TEST_SUITE_END() // "test_populations"
