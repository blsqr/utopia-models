#define BOOST_TEST_MODULE test filtered_network

// NOTE This tests part of the Utopia::Utils namespace, because it's easier to
//      test it with a model than without ...

#include <algorithm>
#include <tuple>
#include <random>

#include <boost/test/unit_test.hpp>

#include <boost/graph/filtered_graph.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/range/adaptor/filtered.hpp>

#include <utopia/core/graph/iterator.hh>

#include <UtopiaUtils/utils/rng.hh>
#include <UtopiaUtils/utils/nw_filter.hh>
#include "../../../utils/filtered_nw.hh"

#include "../network.hh"
#include "../resource_space.hh"

#include "testtools.hh"
#include "graph_types.hh"



// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Models::EEcosy;

using boost::adaptors::filtered;

using Utopia::IterateOver;
using Utopia::range;

using Utopia::Models::EEcosy::NW::NetworkFilter;
using Utopia::Models::EEcosy::NW::FilteredNetwork;
using Utopia::Models::EEcosy::NW::FilterMode;

/// The specialized infrastructure fixture
struct Infrastructure : public InfrastructureBase {
    Infrastructure () : InfrastructureBase("filtered_nw.yml") {}

    /// Creates a new network of the given type
    template<class NW>
    NW setup_network (
        int num_vertices = 10,
        double p_link = 1.
    ){
        auto dist = std::uniform_real_distribution<double>(0., 1.);

        log->info("Creating graph with {} vertices (p_link: {}) ...",
                  num_vertices, p_link);
        auto nw = NW(num_vertices);

        for (auto v1 : range<IterateOver::vertices>(nw)) {
            for (auto v2 : range<IterateOver::vertices>(nw)) {
                if (dist(*rng) < p_link) {
                    boost::add_edge(v1, v2, nw);
                }
            }
        }
        log->info("Created graph with {} vertices and {} edges.",
                  boost::num_vertices(nw), boost::num_edges(nw));

        return nw;
    }

    template<class NW>
    NW setup_mixed_network (
        int num_vertices = 10,
        double p_link = 1.,
        const int vertex_kind_mod = 0,
        const int edge_kind_mod = 0
    ){
        auto nw = setup_network<NW>(num_vertices, p_link);

        // To some vertices and edges, assign a different `kind` value
        if (vertex_kind_mod > 0) {
            for (auto v : range<IterateOver::vertices>(nw)) {
                if (nw[v].id % vertex_kind_mod == 0) nw[v].kind = 1;
            }
        }
        if (edge_kind_mod > 0) {
            for (auto e : range<IterateOver::edges>(nw)) {
                if (nw[e].id % edge_kind_mod == 0) nw[e].kind = 1;
            }
        }

        log->info("Associated 1/{} vertices and 1/{} edges with kind 1.",
                  vertex_kind_mod, edge_kind_mod);

        return nw;
    }
};


/// The graph types to test, mostly defined in graph_types.hh
using nw_types =
    std::tuple<
        DefaultGraph
        // FullFunctioningGraph,
        // UndirectedFFG,
        // ExoticContainerTypesGraph,
        // DefaultMatrix,
        // MatrixWithProperties,
        // NW::Network<DefaultResourceSpace, DefaultRNG>
    >;

/// Advanced network types (with bundled properties)
using adv_nw_types =
    std::tuple<
        FullFunctioningGraph,
        UndirectedFFG,
        ExoticContainerTypesGraph
    >;



// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Tests of the FilteredRange on graph iterations ----------------------------
BOOST_FIXTURE_TEST_SUITE (test_FilteredRange, Infrastructure)


/// Tests correctness of the FilteredRange (on vertices only)
BOOST_AUTO_TEST_CASE (test_FilteredRange) {
    auto g = setup_network<FullFunctioningGraph>(10);

    // Change the kind property of vertices and edges in order to have
    // something to filter against ...
    auto cnt = 1u;  // offset intended
    for (auto v : range<IterateOver::vertices>(g)) {
        g[v].kind = (cnt % 3 == 0);
        cnt++;
    }
    cnt = 1;
    for (auto e : range<IterateOver::edges>(g)) {
        g[e].kind = (cnt % 3 == 0);
        cnt++;
    }

    // Check explicitly for vertices, because they will be checked in detail
    BOOST_REQUIRE(boost::num_vertices(g) == 10);
    BOOST_REQUIRE(g[boost::vertex(0, g)].kind == 0);
    BOOST_REQUIRE(g[boost::vertex(1, g)].kind == 0);
    BOOST_REQUIRE(g[boost::vertex(2, g)].kind == 1);
    BOOST_REQUIRE(g[boost::vertex(3, g)].kind == 0);
    BOOST_REQUIRE(g[boost::vertex(4, g)].kind == 0);
    BOOST_REQUIRE(g[boost::vertex(5, g)].kind == 1);
    BOOST_REQUIRE(g[boost::vertex(6, g)].kind == 0);
    BOOST_REQUIRE(g[boost::vertex(7, g)].kind == 0);
    BOOST_REQUIRE(g[boost::vertex(8, g)].kind == 1);
    BOOST_REQUIRE(g[boost::vertex(9, g)].kind == 0);

    // Define predicates
    auto keep_all = [](auto){ return true; };
    auto kind0 = [&g](auto d){ return g[d].kind == 0; };
    auto kind1 = [&g](auto d){ return g[d].kind == 1; };

    // Some manual tests
    log->info("Step-by-step iteration using custom FilteredRange ...");
    {
    auto [v, v_end] = boost::vertices(g);
    auto fit = make_filtered_range(kind0, v, v_end);

    BOOST_TEST(*fit == *fit.begin());
    BOOST_TEST(*fit == 0);
    ++fit;
    BOOST_TEST(*fit == 1);
    ++fit;
    BOOST_TEST(*fit == 3);
    ++fit;
    BOOST_TEST(*fit == 4);
    ++fit;
    BOOST_TEST(*fit == 6);
    ++fit;
    BOOST_TEST(*fit == 7);
    ++fit;
    BOOST_TEST(*fit == 9);
    ++fit;
    BOOST_TEST(*fit == *fit.end());
    }

    // Check distance
    {
    auto [v, v_end] = boost::vertices(g);
    auto fit = make_filtered_range(kind0, v, v_end);

    BOOST_TEST(std::distance(fit.begin(), fit.end()) == 7);
    }


    // Test range-based for loop iteration, both over vertices/edges
    log->info("Full FilteredRange tests commencing ...");
    // -- keep all
    {
    auto it = boost::make_iterator_range(boost::vertices(g));
    auto filtered_it = make_filtered_range(keep_all, it.begin(), it.end());
    BOOST_TEST(get_iter_size(filtered_it) == 10);
    }
    {
    auto it = boost::make_iterator_range(boost::edges(g));
    auto filtered_it = make_filtered_range(keep_all, it.begin(), it.end());
    BOOST_TEST(get_iter_size(filtered_it) == 100);
    }

    // -- kind 0
    {
    auto it = boost::make_iterator_range(boost::vertices(g));
    auto filtered_it = make_filtered_range(kind0, it.begin(), it.end());
    auto cnt = 0u;
    for (auto d : filtered_it) {
        BOOST_TEST(g[d].kind == 0);
        cnt++;
    }
    BOOST_TEST(cnt == 7);
    BOOST_TEST(get_iter_size(filtered_it) == 7);
    }
    {
    auto it = boost::make_iterator_range(boost::edges(g));
    auto filtered_it = make_filtered_range(kind0, it.begin(), it.end());
    auto cnt = 0u;
    for (auto d : filtered_it) {
        BOOST_TEST(g[d].kind == 0);
        cnt++;
    }
    BOOST_TEST(cnt == 67);
    BOOST_TEST(get_iter_size(filtered_it) == 67);
    }

    // -- kind 1
    {
    auto it = boost::make_iterator_range(boost::vertices(g));
    auto filtered_it = make_filtered_range(kind1, it.begin(), it.end());
    auto cnt = 0u;
    for (auto d : filtered_it) {
        BOOST_TEST(g[d].kind == 1);
        cnt++;
    }
    BOOST_TEST(cnt == 3);
    BOOST_TEST(get_iter_size(filtered_it) == 3);
    }
    {
    auto it = boost::make_iterator_range(boost::edges(g));
    auto filtered_it = make_filtered_range(kind1, it.begin(), it.end());
    auto cnt = 0u;
    for (auto d : filtered_it) {
        BOOST_TEST(g[d].kind == 1);
        cnt++;
    }
    BOOST_TEST(cnt == 33);
    BOOST_TEST(get_iter_size(filtered_it) == 33);
    }

    log->info("FilteredRange tests finished.");
}


/// For comparison, also test filtered iteration using boost::adaptors
BOOST_AUTO_TEST_CASE (boost_filtered_iteration) {
    auto g = setup_mixed_network<FullFunctioningGraph>(100, 1., 3, 3);

    log->info("Testing iteration using boost::adaptors::filtered ...");

    // Define predicates
    auto kind0 = [&g](auto d){ return g[d].kind == 0; };
    auto kind1 = [&g](auto d){ return g[d].kind == 1; };

    // Use range filtering ...
    {
    auto vertex_it = boost::make_iterator_range(boost::vertices(g));
    for (auto v : vertex_it | filtered(kind0)) { BOOST_TEST(g[v].kind == 0); }
    }
    {
    auto vertex_it = boost::make_iterator_range(boost::vertices(g));
    for (auto v : vertex_it | filtered(kind1)) { BOOST_TEST(g[v].kind == 1); }
    }
    {
    auto edge_it = boost::make_iterator_range(boost::edges(g));
    for (auto e : edge_it | filtered(kind0)) { BOOST_TEST(g[e].kind == 0); }
    }
    {
    auto edge_it = boost::make_iterator_range(boost::edges(g));
    for (auto e : edge_it | filtered(kind1)) { BOOST_TEST(g[e].kind == 1); }
    }
}



BOOST_AUTO_TEST_SUITE_END()


/// Tests that directly check behaviour of FilteredNetwork --------------------
BOOST_FIXTURE_TEST_SUITE (test_FilteredNetwork, Infrastructure)

/// Tests construction of FilteredNetwork with different underlying graphs
BOOST_AUTO_TEST_CASE_TEMPLATE (test_construction, NW, nw_types) {
    auto base = setup_network<NW>(10);

    // Create some filter objects
    auto keep_all = [](auto){ return true; };
    auto unfiltered = NetworkFilter<NW>::unfiltered(base);
    auto edge_filter = NetworkFilter<NW>::edges(base, keep_all);
    auto vertex_filter = NetworkFilter<NW>::vertices(base, keep_all);
    auto nw_filter = NetworkFilter(base, keep_all, keep_all);

    // Create FilteredNetwork using the already-created filter
    auto fnw0 = FilteredNetwork(base, vertex_filter);

    BOOST_TEST((&fnw0.base() == &base));
    BOOST_TEST((fnw0.filter().mode == FilterMode::vertices_only));
    BOOST_TEST((fnw0.mode() == FilterMode::vertices_only));

    // Test the FilteredNetwork ctor that creates NetworkFilter on the fly
    FilteredNetwork(base, keep_all);
    FilteredNetwork(base, keep_all, keep_all);
    auto fnw1 = FilteredNetwork(base, keep_all, keep_all,
                                FilterMode::unfiltered);

    BOOST_TEST((&fnw1.base() == &base));
    BOOST_TEST((fnw1.filter().mode == FilterMode::unfiltered));
    BOOST_TEST((fnw1.mode() == FilterMode::unfiltered));
}


/// Tests access to bundled properties via operator[]
BOOST_AUTO_TEST_CASE (test_bundled_property_access) {
    using NW = FullFunctioningGraph;
    auto base = setup_network<NW>(100, .2);

    // Create FilteredNetwork
    auto keep_all = [](auto){ return true; };
    auto fnw = FilteredNetwork(base, keep_all, keep_all);

    // NOTE In the following, always ensure that iteration actually took place

    // Visits should be zero
    auto num_vertices = 0u;
    auto num_edges = 0u;
    for (auto v : range<IterateOver::vertices>(base)) {
        BOOST_TEST(base[v].visits == 0);
        num_vertices++;
    }
    for (auto e : range<IterateOver::edges>(base)) {
        BOOST_TEST(base[e].visits == 0);
        num_edges++;
    }
    BOOST_TEST(num_vertices == boost::num_vertices(base));
    BOOST_TEST(num_edges == boost::num_edges(base));

    // Now visit every entity and -- via fnw -- increment the value
    num_vertices = 0u;
    num_edges = 0u;
    for (auto v : range<IterateOver::vertices>(fnw)) {
        BOOST_TEST(fnw[v].visits == 0);
        fnw[v].visits++;
        BOOST_TEST(fnw[v].visits == 1);
        num_vertices++;
    }
    for (auto e : range<IterateOver::edges>(fnw)) {
        BOOST_TEST(fnw[e].visits == 0);
        fnw[e].visits++;
        BOOST_TEST(fnw[e].visits == 1);
        num_edges++;
    }
    BOOST_TEST(num_vertices == boost::num_vertices(base));
    BOOST_TEST(num_edges == boost::num_edges(base));

    // Checking again in the base network ...
    num_vertices = 0u;
    num_edges = 0u;
    for (auto v : range<IterateOver::vertices>(base)) {
        BOOST_TEST(base[v].visits == 1);
        num_vertices++;
    }
    for (auto e : range<IterateOver::edges>(base)) {
        BOOST_TEST(base[e].visits == 1);
        num_edges++;
    }
    BOOST_TEST(num_vertices == boost::num_vertices(base));
    BOOST_TEST(num_edges == boost::num_edges(base));
}


/// Tests the overloads for scalar-returning getters like num_vertices
BOOST_AUTO_TEST_CASE (test_scalar_getter_overloads) {
    using NW = FullFunctioningGraph;
    auto base = setup_network<NW>(100, .2);

    // Some predicates
    auto mod4 = [&base](auto d){ return base[d].id % 4 == 0; };

    // Count vertices and edges with that certain property
    auto [v, v_end] = boost::vertices(base);
    const uint num_mod4_vertices = std::count_if(v, v_end, mod4);
    auto [e, e_end] = boost::edges(base);
    const uint num_mod4_edges = std::count_if(e, e_end, mod4);

    BOOST_REQUIRE(num_mod4_vertices > (100/4-1));
    BOOST_REQUIRE(num_mod4_edges > (boost::num_edges(base) / 4 - 1));
    log->info("With modulo-4 filter on the entity ID, {:d} vertices and "
              "{:d} edges should be part of the FilteredNetwork ...",
              num_mod4_vertices, num_mod4_edges);

    // Create FilteredNetwork
    auto fnw = FilteredNetwork(base, mod4, mod4);

    // ... and test!
    BOOST_TEST(boost::num_vertices(fnw) == num_mod4_vertices);
    BOOST_TEST(boost::num_edges(fnw) == num_mod4_edges);
    // NOTE Unlike for boost::filtered_graph, these overloads actually return
    //      the number of vertices and edges _after_ filtering

    // When the filter is disabled, information is read from the base network
    auto ufnw = FilteredNetwork(base, mod4, mod4, FilterMode::unfiltered);

    BOOST_TEST(boost::num_vertices(ufnw) == boost::num_vertices(base));
    BOOST_TEST(boost::num_edges(ufnw) == boost::num_edges(base));
}


/// Tests all iteration overloads
BOOST_AUTO_TEST_CASE_TEMPLATE (test_iteration_overloads, NW, adv_nw_types) {
    auto base = setup_network<NW>(1000, .1);

    // Count vertices and edges with that certain property
    auto mod4 = [&base](auto d){ return base[d].id % 4 == 0; };

    auto [v, v_end] = boost::vertices(base);
    const uint num_mod4_vertices = std::count_if(v, v_end, mod4);
    BOOST_REQUIRE(num_mod4_vertices > 100);  // to have enough iterations below

    auto [e, e_end] = boost::edges(base);
    const uint num_mod4_edges = std::count_if(e, e_end, mod4);
    BOOST_REQUIRE(num_mod4_edges > 1000);    // to have enough iterations below

    log->info("With modulo-4 filter on the entity ID, {:d} vertices and "
              "{:d} edges should be part of the FilteredNetwork ...",
              num_mod4_vertices, num_mod4_edges);

    // Create FilteredNetwork
    auto fnw = FilteredNetwork(base, mod4, mod4);

    /*
    log->info("Base network edges:");
    for (auto e : range<IterateOver::edges>(base)) {
        auto s = boost::source(e, base);
        auto t = boost::target(e, base);
        std::cout << "    " << s << " -> " << t;
        std::cout << "  (" << base[s].id << " -> " << base[t].id << ") ";
        std::cout << "  --  edge ID: " << base[e].id << std::endl;
    }

    log->info("FilteredNetwork edges:");
    for (auto e : range<IterateOver::edges>(fnw)) {
        auto s = boost::source(e, fnw);
        auto t = boost::target(e, fnw);
        std::cout << "    " << s << " -> " << t;
        std::cout << "  (" << fnw[s].id << " -> " << fnw[t].id << ") ";
        std::cout << "  --  edge ID: " << fnw[e].id << std::endl;
    }
    */

    // .. Testing .............................................................
    log->info("Iteration tests commencing ...");

    // -- vertices
    for (auto v : range<IterateOver::vertices>(fnw)) {
        BOOST_TEST(mod4(v));
    }
    BOOST_TEST(   get_iter_size(range<IterateOver::vertices>(fnw))
               == num_mod4_vertices);

    // -- edges
    for (auto e : range<IterateOver::edges>(fnw)) {
        BOOST_TEST(mod4(e));
    }
    BOOST_TEST(   get_iter_size(range<IterateOver::edges>(fnw))
               == num_mod4_edges);

    // -- vertex-specific ...
    for (auto u : range<IterateOver::vertices>(fnw)) {
        BOOST_TEST_CONTEXT("Index vertex: " << u) {
        // -- in-edges
        {
        auto [e, e_end] = boost::in_edges(u, base);
        const uint num_mod4_in_edges = std::count_if(e, e_end, mod4);

        auto cnt = 0u;
        for (auto e : range<IterateOver::in_edges>(u, fnw)) {
            BOOST_TEST(mod4(e));
            cnt++;
        }
        BOOST_TEST(cnt == num_mod4_in_edges);
        }

        // -- out-edges
        {
        auto [e, e_end] = boost::out_edges(u, base);
        const uint num_mod4_out_edges = std::count_if(e, e_end, mod4);
        auto cnt = 0u;
        for (auto e : range<IterateOver::out_edges>(u, fnw)) {
            BOOST_TEST(mod4(e));
            cnt++;
        }
        BOOST_TEST(cnt == num_mod4_out_edges);
        }

        // -- adjacent vertices
        if constexpr (std::is_same<
            typename boost::graph_traits<NW>::traversal_category,
            boost::bidirectionalS
        >())
        {
        uint num_mod4_adj_vertices;
        uint num_mod4_out_edges;
        uint max_num_mod4_adj_vertices;
        {
            auto [v, v_end] = boost::adjacent_vertices(u, base);
            num_mod4_adj_vertices = std::count_if(v, v_end, mod4);

            auto [e, e_end] = boost::out_edges(u, base);
            num_mod4_out_edges = std::count_if(e, e_end, mod4);

            // NOTE Unlike in BGL's filtered_graph, the adjacent_vertices
            //      overload actually filters for _both_ the involved
            //      out_edges and the resulting adjacent vertices. To get an
            //      upper limit, we can look at the minimum of the number of
            //      matching out edges and the number of matching adjacent
            //      vertices.
            max_num_mod4_adj_vertices = std::min(num_mod4_adj_vertices,
                                                 num_mod4_out_edges);
        }

        auto cnt = 0u;
        for (auto v : range<IterateOver::neighbors>(u, fnw)) {
            auto [uv_edge, edge_exists] = boost::edge(u, v, base);

            BOOST_TEST_CONTEXT("Lookup vertex: " << v << " with ID "
                               << fnw[v].id << " (mod4? " << mod4(v) << ") "
                               << "and edge " << u << "->" << v
                               << " with ID " << fnw[uv_edge].id
                               << " (mod4? " << mod4(uv_edge) << ")")
            {
                // BOOST_TEST(mod4(v));  // for BGL behaviour: need not be true
                BOOST_TEST(edge_exists);
                BOOST_TEST(mod4(uv_edge));
                cnt++;
            }
        }
        BOOST_TEST(cnt <= max_num_mod4_adj_vertices);
        }

        // -- inverse adjacent vertices
        if constexpr (std::is_same<
            typename boost::graph_traits<NW>::traversal_category,
            boost::bidirectionalS
        >())
        {
        uint num_mod4_iadj_vertices;
        uint num_mod4_in_edges;
        uint max_num_mod4_inv_adj_vertices;
        {
            auto [v, v_end] = boost::adjacent_vertices(u, base);
            num_mod4_iadj_vertices = std::count_if(v, v_end, mod4);

            auto [e, e_end] = boost::in_edges(u, base);
            num_mod4_in_edges = std::count_if(e, e_end, mod4);

            // NOTE See note for adjacent vertices, also holds here
            max_num_mod4_inv_adj_vertices = std::min(num_mod4_iadj_vertices,
                                                     num_mod4_in_edges);
        }

        auto cnt = 0u;
        for (auto v : range<IterateOver::inv_neighbors>(u, fnw)) {
            auto [vu_edge, edge_exists] = boost::edge(v, u, base);

            BOOST_TEST_CONTEXT("Lookup vertex: " << v << " with ID "
                               << fnw[v].id << " (mod4? " << mod4(v) << ") "
                               << "and edge " << v << "->" << u
                               << " with ID " << fnw[vu_edge].id
                               << " (mod4? " << mod4(vu_edge) << ")")
            {
                // BOOST_TEST(mod4(v));  // for BGL behaviour: need not be true
                BOOST_TEST(edge_exists);
                BOOST_TEST(mod4(vu_edge));
                cnt++;
            }
        }
        BOOST_TEST(cnt <= max_num_mod4_inv_adj_vertices);
        }

        } // test context
    }

    log->info("Iteration tests finished.");
}


BOOST_AUTO_TEST_SUITE_END() // "test_FilteredNetwork"


/// Benchmarking --------------------------------------------------------------
BOOST_FIXTURE_TEST_SUITE(filtering_benchmark, Infrastructure)

BOOST_AUTO_TEST_CASE(benchmark) {
    if (not get_as<bool>("enabled", cfg["benchmark"], false)) {
        return;
    }

    const auto num_samples = get_as<int>("num_samples", cfg["benchmark"]);
    const auto num_vertices = get_as<int>("num_vertices",
                                          cfg["benchmark"]["network"]);
    const auto p_link = get_as<double>("p_link", cfg["benchmark"]["network"]);
    const auto asymmetry = get_as<int>("asymmetry",
                                       cfg["benchmark"]["network"]);

    // Setup the network that is to be tested
    using NW = FullFunctioningGraph;
    auto g = setup_mixed_network<NW>(num_vertices, p_link,
                                     asymmetry, asymmetry);

    // Define predicates
    auto keep_all = [](auto){ return true; };
    auto kind0 = [&g](auto d){ return g[d].kind == 0; };
    auto kind1 = [&g](auto d){ return g[d].kind == 1; };

    // The filtered graph instances
    auto fg_0 = make_filtered_graph(g, kind0, kind0);
    auto fg_1 = make_filtered_graph(g, kind1, kind1);
    auto fg_full = make_filtered_graph(g, keep_all, keep_all);

    // The FilteredNetwork instances
    auto fnw_0 = make_filtered_network(g, kind0, kind0);
    auto fnw_1 = make_filtered_network(g, kind1, kind1);
    auto fnw_full = make_filtered_network(g, keep_all, keep_all,
                                          FilterMode::unfiltered);

    // Define benchmark functions
    auto native_BGL = [](auto& g){
        auto start = Clock::now();

        for (auto v : range<IterateOver::vertices>(g)) {
            g[v].visits++;
        }

        return time_since(start);
    };
    auto boost_adaptors_filtered = [](auto& g, auto pred){
        auto start = Clock::now();

        auto vertex_it = boost::make_iterator_range(boost::vertices(g));
        for (auto v : vertex_it | filtered(pred)) {
            g[v].visits++;
        }

        return time_since(start);
    };
    // Requires ranges-v3
    // auto ranges_views_filter = [](auto& g, auto pred){
    //     auto start = Clock::now();

    //     auto vertex_it = boost::make_iterator_range(boost::vertices(g));
    //     for (auto v : vertex_it | ranges::views::filter(pred)) {
    //         g[v].visits++;
    //     }

    //     return time_since(start);
    // };
    auto with_FilteredRange = [](auto& g, auto pred){
        auto start = Clock::now();

        // auto [v, v_end] = boost::vertices(g);
        // auto filtered_it = make_filtered_range(pred, v, v_end);

        auto it = boost::make_iterator_range(boost::vertices(g));
        auto filtered_it = make_filtered_range(pred, it.begin(), it.end());

        for (auto v : filtered_it) {
            g[v].visits++;
        }

        return time_since(start);
    };
    auto late_pred_eval = [](auto& g, auto pred){
        auto start = Clock::now();

        for (auto v : range<IterateOver::vertices>(g)) {
            if (not pred(v)) continue;
            g[v].visits++;
        }

        return time_since(start);
    };

    // -- Perform the benchmarks ..............................................
    log->info("Benchmarks commencing (with {} samples each) ...\n",
              num_samples);
    {
        auto bf = native_BGL;
        std::string bf_name = "native BGL";

        perform_benchmark(bf_name + " -- adjacency_list",
                          num_samples, log, bf, g);
        perform_benchmark(bf_name + " -- filtered_graph (keep all)",
                          num_samples, log, bf, fg_full);
        perform_benchmark(bf_name + " -- filtered_graph (kind 0)",
                          num_samples, log, bf, fg_0);
        perform_benchmark(bf_name + " -- filtered_graph (kind 1)",
                          num_samples, log, bf, fg_1);
        perform_benchmark(bf_name + " -- FilteredNetwork (keep all)",
                          num_samples, log, bf, fnw_full);
        perform_benchmark(bf_name + " -- FilteredNetwork (kind 0)",
                          num_samples, log, bf, fnw_0);
        perform_benchmark(bf_name + " -- FilteredNetwork (kind 1)",
                          num_samples, log, bf, fnw_1);
    }
    {
        auto bf = boost_adaptors_filtered;
        std::string bf_name = "boost::adaptors::filtered";

        perform_benchmark(bf_name + " -- keep all",
                          num_samples, log, bf, g, [](auto){return true;});
        perform_benchmark(bf_name + " -- kind 0",
                          num_samples, log, bf, g, kind0);
        perform_benchmark(bf_name + " -- kind 1",
                          num_samples, log, bf, g, kind1);
    }
    {
        auto bf = with_FilteredRange;
        std::string bf_name = "FilteredRange";

        perform_benchmark(bf_name + " -- keep all",
                          num_samples, log, bf, g, [](auto){return true;});
        perform_benchmark(bf_name + " -- kind 0",
                          num_samples, log, bf, g, kind0);
        perform_benchmark(bf_name + " -- kind 1",
                          num_samples, log, bf, g, kind1);
    }
    {
        auto bf = late_pred_eval;
        std::string bf_name = "late predicate evaluation";

        perform_benchmark(bf_name + " -- keep all",
                          num_samples, log, bf, g, [](auto){return true;});
        perform_benchmark(bf_name + " -- kind 0",
                          num_samples, log, bf, g, kind0);
        perform_benchmark(bf_name + " -- kind 1",
                          num_samples, log, bf, g, kind1);
    }
}

BOOST_AUTO_TEST_SUITE_END() // "filtering_benchmark"
