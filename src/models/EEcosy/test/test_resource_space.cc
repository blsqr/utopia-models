#define BOOST_TEST_MODULE test resource_space

#include <tuple>
#include <type_traits>

#include <boost/test/unit_test.hpp>

#include <armadillo>

#include "testtools.hh"
#include "../resource_space.hh"


// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Models::EEcosy;

/// The specialized infrastructure fixture
struct Infrastructure : public InfrastructureBase {
    Infrastructure () : InfrastructureBase("resource_space.yml") {}
};


/// The resource space types to test
using resource_spaces = std::tuple<ResourceSpace<3>,
                                   DefaultResourceSpace
                                   >;


/// Loads a vector or matrix from the config and checks its properties
template<class RT, class... tags, class ResSpace>
void load_and_check(const ResSpace& rspace, const Config& params) {
    // The element type
    using elem_type = typename RT::elem_type;

    // Declare the object, a matrix or a vector
    RT obj;

    // Create the vector or matrix
    if constexpr (RT::is_col or RT::is_row) {
        // Vector
        BOOST_TEST_CHECKPOINT("Constructing vector ...");
        obj = rspace.template get_as<RT, tags...>("vec", params);
    }
    else {
        // Matrix
        BOOST_TEST_CHECKPOINT("Constructing matrix ...");
        obj = rspace.template get_as<RT, tags...>("mat", params);
    }
    BOOST_TEST_INFO("Constructed object:\n" << obj);

    // Make elementwise comparisons
    if (params["compare"]) {
        if constexpr (RT::is_col or RT::is_row) {
            // Carry over into armadillo vector, for better comparison
            auto expected_v = params["compare"].as<std::vector<elem_type>>();
            const auto expected = arma::Col<elem_type>(expected_v);

            // Show them, for easier debugging
            obj.print("\nGenerated object:");
            expected.print("Expected object:");

            // First, compare length
            BOOST_TEST(expected.size() == obj.n_elem);

            // Now, compare elementwise with expected object
            if constexpr (    std::is_floating_point<elem_type>()
                          and not contains_type<clean_elements, tags...>()
                          and not contains_type<clean_and_mask, tags...>())
            {
                std::cout << "Comparing with precision: " << PREC << std::endl;
                auto prec_vec = arma::Col<elem_type>(expected).fill(PREC);
                BOOST_TEST(arma::abs(obj - expected).eval() < prec_vec,
                           tt::per_element());
            }
            else {
                BOOST_TEST(obj == expected, tt::per_element());
            }
        }
        else {
            BOOST_ERROR("Matrix comparison not implemented!");
        }
    }

    // Perform some additional checks
    if (params["perform_checks"]) {
        for (auto& e : params["perform_checks"]) {
            const auto to_check = e.as<std::string>();

            if (to_check == "has_nan") {
                BOOST_TEST(obj.has_nan());
            }
            else if (to_check == "is_finite") {
                BOOST_TEST(obj.is_finite());
            }
            else if (to_check == "is_zero") {
                // NOTE is_zero method is available only from 9.850
                if constexpr (std::is_floating_point<elem_type>()) {
                    BOOST_TEST(arma::all(arma::abs(obj) < PREC));
                }
                else {
                    BOOST_TEST(arma::all(obj == 0));
                }
            }
            else if (to_check == "non_zero") {
                BOOST_TEST(arma::all(obj));
            }
            else {
                BOOST_ERROR("No such check: " + to_check);
            }
        }
    }
}


// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Tests using the Infrastructure fixture
BOOST_FIXTURE_TEST_SUITE (test_resource_space, Infrastructure)

/// Test basic construction of ResourceSpace types
BOOST_AUTO_TEST_CASE_TEMPLATE (test_construction, ResSpace, resource_spaces)
{
    ResSpace();
}

/// Test some armadillo properties are as expected
BOOST_AUTO_TEST_CASE_TEMPLATE (test_arma_properties, ResSpace, resource_spaces)
{
    using ResVec = typename ResSpace::ResVec;

    // Fixed-size resource vectors can be initialized with `{}.fill(0.)`
    auto rvec = ResVec{}.fill(0.);
    rvec.print("rvec");
    BOOST_TEST(arma::all(rvec == 0.));
    BOOST_TEST(arma::all(rvec == ResSpace::zeros()));
}


/// Test configuration features
BOOST_AUTO_TEST_CASE_TEMPLATE (test_get_as, ResSpace, resource_spaces)
{
    test_config_callable([&](const Config& params){
        // Create a new resource space object
        auto rspace = ResSpace();

        // Read some test-related parameters
        const auto mode = get_as<std::string>("mode", params);
        const auto elem_type = get_as<std::string>("type", params, "double");

        // Distinguish by mode and element type
        if (mode == "vector") {
            if (elem_type == "int") {
                using VT = typename ResSpace::template FixedVec<int>;
                load_and_check<VT>(rspace, params);
            }
            else if (elem_type == "double") {
                using VT = typename ResSpace::template FixedVec<double>;

                const auto tag = get_as<std::string>("tag", params, "");
                if (tag == "clean_elements") {
                    BOOST_TEST_CONTEXT("Invoking with tags: clean_elements") {
                        load_and_check<VT, clean_elements>(rspace, params);
                    }
                }
                else if (tag == "mask_zeros") {
                    BOOST_TEST_CONTEXT("Invoking with tags: mask_zeros") {
                        load_and_check<VT, mask_zeros>(rspace, params);
                    }
                }
                else if (tag == "clean_and_mask") {
                    BOOST_TEST_CONTEXT("Invoking with tags: clean_and_mask") {
                        load_and_check<VT, clean_and_mask>(rspace, params);
                    }

                    // ... which is the same as:
                    BOOST_TEST_CONTEXT("Invoking with tags: clean_elements, "
                                       "mask_zeros") {
                        load_and_check<VT, clean_elements, mask_zeros>(rspace,
                                                                       params);
                    }
                }
                else { // Without tags
                    load_and_check<VT>(rspace, params);
                }
            }
            else {
                BOOST_ERROR("Bad element type: " + elem_type);
            }
        }
        else if (mode == "matrix") {
            BOOST_ERROR("Matrix checks not implemented");
        }
        else {
            BOOST_ERROR("Bad mode: " + mode);
        }

    }, cfg["test_get_as"][ResSpace::num_dims],
    "get_as (" + std::to_string(ResSpace::num_dims) + "-dimensional)");
}

BOOST_AUTO_TEST_SUITE_END() // "test_resource_space"
