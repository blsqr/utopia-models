#define BOOST_TEST_MODULE test experimental

#include <tuple>
#include <memory>
#include <algorithm>
#include <random>
#include <functional>
#include <iterator>

#include <boost/test/unit_test.hpp>

#include <boost/config.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/subgraph.hpp>

#include "testtools.hh"

#include "../network.hh"
#include "../econet.hh"


// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Models::EEcosy;

/// The specialized infrastructure fixture
struct Infrastructure : public InfrastructureBase {
    Infrastructure () : InfrastructureBase("experimental.yml") {}
};



// ++ Dynamic cast in NW ++++++++++++++++++++++++++++++++++++++++++++++++++++++


template<class ResSpace, class RNG>
class IdenticalPopulation : public Population<ResSpace, RNG> {};

template<class ResSpace, class RNG>
class SpecialPopulation : public Population<ResSpace, RNG> {
    void some_special_method () {}

public:
    template<class... Args>
    SpecialPopulation (Args&&... args)
    :   Population<ResSpace, RNG>(std::forward<Args>(args)...)
    {}
};

template<class ResSpace, class RNG>
class SpecialPopulationWithMember : public Population<ResSpace, RNG> {
    double some_special_member;
};

template<class ResSpace, class RNG>
class ParallelPopulation : public BasePopulation<ResSpace, RNG> {
    void some_parallel_method () {}
};

template<class ResSpace, class RNG>
class ParallelPopulationWithMember : public BasePopulation<ResSpace, RNG> {
    double some_parallel_member;
};



BOOST_FIXTURE_TEST_SUITE (dynamically_cast_properties, Infrastructure)

/// Test in which cases dynamic casting works or fails, using references
/** For EcoNet, it's mostly relevant to downcast, i.e. from a shared base to
  * some derived class.
  * Hierarchy:
  *                             BasePopulation
  *                              ^          ^
  *                             /            \
  *                      Population         ParallelPopulation…
  *                        ^
  *                       /
  *               SpecialPopulation…
  */
BOOST_AUTO_TEST_CASE (dyn_cast_population_references) {
    using BasePop = BasePopulation<ResSpace, RNG>;
    using Pop = Population<ResSpace, RNG>;
    using AliasPop = Population<ResSpace, RNG>;
    using IdenticalPop = IdenticalPopulation<ResSpace, RNG>;
    using SpecialPop = SpecialPopulation<ResSpace, RNG>;
    using SpecialPopWM = SpecialPopulationWithMember<ResSpace, RNG>;
    using ParaPop = ParallelPopulation<ResSpace, RNG>;
    using ParaPopWM = ParallelPopulationWithMember<ResSpace, RNG>;

    const auto pop_defaults = cfg["defaults"]["population"];

    // Down- or sidecasting should fail
    {
    auto base = BasePop(log, rng, rspace, {});
    BOOST_CHECK_THROW(auto pop = dynamic_cast<Pop&>(base),
                      std::bad_cast);
    BOOST_CHECK_THROW(auto pop = dynamic_cast<SpecialPop&>(base),
                      std::bad_cast);
    BOOST_CHECK_THROW(auto pop = dynamic_cast<ParaPop&>(base),
                      std::bad_cast);

    auto pop = Pop(log, rng, rspace, pop_defaults["regular"]);
    BOOST_CHECK_THROW(auto sp = dynamic_cast<SpecialPop&>(pop),
                      std::bad_cast);
    BOOST_CHECK_THROW(auto sp = dynamic_cast<SpecialPopWM&>(pop),
                      std::bad_cast);
    BOOST_CHECK_THROW(auto pp = dynamic_cast<ParaPop&>(pop),
                      std::bad_cast);
    BOOST_CHECK_THROW(auto pp = dynamic_cast<ParaPopWM&>(pop),
                      std::bad_cast);

    // ... even if the type is basically identical
    BOOST_CHECK_THROW(auto ip = dynamic_cast<IdenticalPop&>(pop),
                      std::bad_cast);
    }

    // Casting to an alias works, though!
    {
    auto pop = Pop(log, rng, rspace, pop_defaults["regular"]);
    auto ap = dynamic_cast<AliasPop&>(pop);
    BOOST_TEST(ap.is_active());
    BOOST_TEST(ap.mortality > 0.);
    }

    // Upcasting a derived population should succeed
    {
    auto pop = Pop(log, rng, rspace, pop_defaults["regular"]);
    auto& base = dynamic_cast<BasePop&>(pop);
    BOOST_TEST(base.is_active());
    }
}

/// Test in which cases dynamic casting works or fails, using pointers
BOOST_AUTO_TEST_CASE (dyn_ptr_cast_populations) {
    using BasePop = BasePopulation<ResSpace, RNG>;
    using Pop = Population<ResSpace, RNG>;
    using AliasPop = Population<ResSpace, RNG>;
    using IdenticalPop = IdenticalPopulation<ResSpace, RNG>;
    using SpecialPop = SpecialPopulation<ResSpace, RNG>;
    using SpecialPopWM = SpecialPopulationWithMember<ResSpace, RNG>;
    using ParaPop = ParallelPopulation<ResSpace, RNG>;
    using ParaPopWM = ParallelPopulationWithMember<ResSpace, RNG>;

    const auto pop_defaults = cfg["defaults"]["population"];

    // Down- or sidecasting should fail, but return nullpointers instead of the
    // std::bad_cast exception that the dynamic_cast would throw. This is
    // because std::dynamic_pointer_cast (of course) works with pointers ...
    {
    auto base = std::make_shared<BasePop>(log, rng, rspace, Config{});
    BOOST_TEST(not std::dynamic_pointer_cast<Pop>(base));
    BOOST_TEST(not std::dynamic_pointer_cast<SpecialPop>(base));

    auto pop = std::make_shared<Pop>(log, rng, rspace,
                                     pop_defaults["regular"]);
    BOOST_TEST(not std::dynamic_pointer_cast<SpecialPop>(pop));
    BOOST_TEST(not std::dynamic_pointer_cast<SpecialPopWM>(pop));
    BOOST_TEST(not std::dynamic_pointer_cast<ParaPop>(pop));
    BOOST_TEST(not std::dynamic_pointer_cast<ParaPopWM>(pop));

    // Casting to a basically identical type also fails for pointers
    BOOST_TEST(not std::dynamic_pointer_cast<IdenticalPop>(pop));

    // ... unless it's an alias, i.e. without derivation
    BOOST_TEST(std::dynamic_pointer_cast<AliasPop>(pop));
    }

    // Up-casting should succeed, even if it goes only half-way
    {
    auto spop = std::make_shared<SpecialPop>(log, rng, rspace,
                                             pop_defaults["regular"]);
    BOOST_TEST(std::dynamic_pointer_cast<Pop>(spop));
    BOOST_TEST(std::dynamic_pointer_cast<BasePop>(spop));
    }

    // Cast back and forth
    {
    auto spop = std::make_shared<SpecialPop>(log, rng, rspace,
                                             pop_defaults["regular"]);

    auto pop = std::dynamic_pointer_cast<Pop>(spop);
    BOOST_TEST(pop);

    auto base = std::dynamic_pointer_cast<BasePop>(spop);
    BOOST_TEST(base);

    BOOST_TEST(std::dynamic_pointer_cast<SpecialPop>(base) == spop);
    BOOST_TEST(std::dynamic_pointer_cast<Pop>(base) == pop);

    BOOST_TEST(std::dynamic_pointer_cast<SpecialPop>(pop) == spop);
    BOOST_TEST(std::dynamic_pointer_cast<BasePop>(pop) == base);
    }
}

/// Test how dynamically casting the entity pointers could work
BOOST_AUTO_TEST_CASE (econet_iteration) {
    using EcoNet = Utopia::Models::EEcosy::EcoNet<Infrastructure::RNG>;
    using Pop = Population<EcoNet::ResSpace, EcoNet::RNG>;

    BOOST_TEST_CHECKPOINT("Initializing EcoNet ...\n" << cfg["econet_setup"]);
    EcoNet econet(log, rng, cfg["econet"], cfg["econet_setup"]);

    // Iterate over it and try to dynamically cast to the derived type
    // Then compare the counters
    auto cnt_base = 0u;
    auto cnt_pop = 0u;
    for (auto base_pop : econet.populations()) {
        if (auto pop = std::dynamic_pointer_cast<Pop>(base_pop)) {
            // Try to access public interface (that is not available in base)
            BOOST_TEST(pop->mortality > 0.);
            cnt_pop++;
        }
        else {
            cnt_base++;
        }
    }
    BOOST_TEST(cnt_base == 4);
    BOOST_TEST(cnt_pop == 6);
}

BOOST_AUTO_TEST_SUITE_END() // "dynamic_cast_properties"


// ++ Subgraph stuff ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Create tags for the custom types
enum vertex_ptr_t { vertex_ptr };
enum edge_ptr_t { edge_ptr };

// Install the properties
// Example: https://www.boost.org/doc/libs/1_72_0/libs/graph/example/interior_property_map.cpp
namespace boost {
    BOOST_INSTALL_PROPERTY(vertex, ptr);
    BOOST_INSTALL_PROPERTY(edge, ptr);
}

namespace Utopia::Models::EEcosy::NW {
    /// Some subnetwork
    template<class ResSpace, class RNG>
    using SubNetwork =
        boost::subgraph<
        boost::adjacency_list<
            OutEdgeList,
            VertexList,
            Directedness,
            // Vertex properties
            boost::property<boost::vertex_index_t, std::size_t,
            boost::property<vertex_ptr_t, VertexProperties<ResSpace, RNG>>
            >,
            // Edge properties
            boost::property<boost::edge_index_t, std::size_t,
            boost::property<edge_ptr_t, EdgeProperties<ResSpace, RNG>>
            >,
            GraphProperties,
            EdgeList
        >
        >;
}

BOOST_FIXTURE_TEST_SUITE (subnetwork, Infrastructure)

/// Test subnetwork iteration and property access
BOOST_AUTO_TEST_CASE (iteration_and_access)
{
    using namespace Utopia::Models::EEcosy::NW;
    using Utopia::IterateOver;
    using Utopia::range;

    using G = SubNetwork<ResSpace, RNG>;
    G g(5);

    // Get subgraphs. IMPORTANT: By reference!
    auto& sg1 = g.create_subgraph();
    // auto& sg2 = g.create_subgraph();

    // Get property maps (this works)
    [[maybe_unused]] auto ptrs_g = boost::get(vertex_ptr, g);
    [[maybe_unused]] auto ptrs_sg1 = boost::get(vertex_ptr, sg1);

    // Test iteration .........................................................
    // 1 - Manual
    auto [_v, _v_end] = boost::vertices(sg1);
    auto rg = boost::make_iterator_range(_v, _v_end);
    for ([[maybe_unused]] auto v : rg) {
        // Works
        // ???

        // FIXME Does not work
        // boost::put(ptrs_sg1, v, "foo");
        // ptrs_sg1[v] = "foo";
        // ptrs_g[v] = {};
        // ptrs_sg1[v] = {};
        // g[v];
        // sg1[v];
    }

    // 2 - Utopia GraphUtils
    // Does not work. Due to const G& perhaps?
    for ([[maybe_unused]] auto v : range<IterateOver::vertices>(sg1)) {

    }
}

BOOST_AUTO_TEST_SUITE_END() // "subnetwork"
