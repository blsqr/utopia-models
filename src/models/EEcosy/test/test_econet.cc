#define BOOST_TEST_MODULE test econet

#include <boost/test/unit_test.hpp>

#include "testtools.hh"

#include "../econet.hh"
#include "../utils/yaml.hh"



// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Models::EEcosy;

/// The specialized infrastructure fixture
struct Infrastructure : public InfrastructureBase {
    /// Some default configuration options for flows and populations
    const Config defaults;

    Infrastructure ()
    :
        InfrastructureBase("econet.yml"),
        defaults(cfg["defaults"])
    {}
};


/// A fixture that provides an already-set-up dummy network
struct TestEcoNet : public Infrastructure {
    using EcoNet = Utopia::Models::EEcosy::EcoNet<Infrastructure::RNG,
                                                  SmallResourceSpace>;

    using ResSpace = typename EcoNet::ResSpace;
    using ResVec = typename ResSpace::ResVec;

    /// The EcoNet instance to test with
    EcoNet econet;

    TestEcoNet ()
    :
        Infrastructure(),
        econet(log, rng, cfg["econet"])
    {
        log->info("Setting up fully connected network...");

        const auto& defaults = cfg["defaults"];

        // Add some regular populations
        econet.add_populations(10, defaults["population"]["regular"]);
        BOOST_TEST(econet.num_populations() == 10);

        // Fully connect it
        fully_connect(econet);

        log->info("Setup finished.");
    }

    /// Fully connect the given network, including self-connections
    /** Uses trophic flows
      */
    void fully_connect (EcoNet& net) const {
        for (auto v : net.vertices()) {
            for (auto u : net.vertices()) {
                net.add_flow(v, u, defaults["flow"]["trophic"]);
            }
        }
    }

    /// Returns a string representation of the vertex descriptor
    /** This is simply the ID of the associated population
      */
    std::string to_string (const EcoNet::VertexDesc v) const {
        return std::to_string(econet[v]->id);
    }

    /// Returns a string representation of the edge descriptor
    /** This uses the IDs of the associated source and target populations
      */
    std::string to_string (const EcoNet::EdgeDesc e) const {
        return (  std::to_string(econet[e.m_source]->id) + " -> "
                + std::to_string(econet[e.m_target]->id));
    }

    template<class T = double>
    ResVec to_ResVec (std::vector<T> v) const {
        if (v.size() != ResVec::n_elem) {
            throw std::invalid_argument("to_ResVec argument needs to exactly "
                                        "match fixed size of ResVec.");
        }

        EcoNet::ResVec rv{};
        for (auto i = 0u; i < v.size(); i++) {
            rv[i] = v[i];
        }
        return rv;
    }
};


// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

BOOST_FIXTURE_TEST_SUITE (construction, Infrastructure)

/// Test the Econet setup
BOOST_AUTO_TEST_CASE (test_construction)
{
    test_config_callable([&](const Config& setup_cfg){
        // Create the object
        auto net = EcoNet(log, rng, cfg["econet"], setup_cfg);

        // NOTE Can test some more things here, but they should relate mostly
        //      to the construction, not so much to the interface itself!

    }, cfg["construction"], "Construction", {__LINE__, __FILE__});
}

BOOST_AUTO_TEST_SUITE_END() // end: construction ------------------------------
BOOST_FIXTURE_TEST_SUITE (structure, Infrastructure)

/// Test the Econet methods to add entities to the network
BOOST_AUTO_TEST_CASE (add_entities)
{
    // Set up an empty EcoNet instance
    auto econet = EcoNet(log, rng, cfg["econet"]);

    // Get the arguments that
    const auto pop_cfg = defaults["population"]["regular"];
    const auto flow_cfg = defaults["flow"]["trophic"];

    // Add some populations and flows and assert their number is correct
    auto v0 = econet.add_population(pop_cfg);
    auto v1 = econet.add_population("regular", pop_cfg);
    auto v2 = econet.add_population<BasePopulation>(pop_cfg);
    auto v3 = econet.add_population("base", pop_cfg);
    BOOST_TEST(econet.num_populations() == 4);

    auto e0 = econet.add_flow(v0, v1, flow_cfg);
    auto e1 = econet.add_flow("trophic", v1, v2, flow_cfg);
    auto e2 = econet.add_flow<BaseFlow>(v2, v3, flow_cfg);
    auto e3 = econet.add_flow("base", v3, v0, flow_cfg);
    BOOST_TEST(econet.num_flows() == 4);

    // Test the kinds are as expected
    BOOST_TEST(econet[v0]->kind() == "regular");
    BOOST_TEST(econet[v1]->kind() == "regular");
    BOOST_TEST(econet[v2]->kind() == "base");
    BOOST_TEST(econet[v3]->kind() == "base");

    BOOST_TEST(econet[e0]->kind() == "trophic");
    BOOST_TEST(econet[e1]->kind() == "trophic");
    BOOST_TEST(econet[e2]->kind() == "base");
    BOOST_TEST(econet[e3]->kind() == "base");

    // Add multiple populations, using different interfaces
    {
        const auto vdescs = econet.add_populations(10, pop_cfg);
        for (auto v : vdescs) {
            BOOST_TEST(econet[v]->kind() == "regular");
        }
    }
    {
        const auto vdescs = econet.add_populations<BasePopulation>(5, pop_cfg);
        for (auto v : vdescs) {
            BOOST_TEST(econet[v]->kind() == "base");
        }
    }
    {
        const auto vdescs = econet.add_populations<Population>(7, pop_cfg);
        for (auto v : vdescs) {
            BOOST_TEST(econet[v]->kind() == "regular");
        }
    }
    {
        const auto vdescs = econet.add_populations(2, "base", pop_cfg);
        for (auto v : vdescs) {
            BOOST_TEST(econet[v]->kind() == "base");
        }
    }
    BOOST_TEST(econet.num_populations() == 4 + 10 + 5 + 7 + 2);


    // Add multiple flows, using different interfaces
    // Explicit
    auto ec0 = econet.add_flows<outgoing>(v0, {v2, v3}, flow_cfg);
    BOOST_TEST(ec0.size() == 2);
    BOOST_TEST(econet.num_flows() == 6);

    // With random selection; use a new population to not get parallel edges
    auto v4 = econet.add_population(pop_cfg);
    BOOST_TEST(econet.num_populations() == 4 + 10 + 5 + 7 + 2 + 1); // 29
    auto ec1 = econet.add_flows<outgoing>(v4,
                                          cfg["select"]["random_single"],
                                          flow_cfg);
    BOOST_TEST(ec1.size() == 1);
    BOOST_TEST(econet.num_flows() == 7);

    // Test that the filtering worked, i.e. that a new population may not gain
    // flows going to itself. Do this by fully connecting this new vertex, such
    // that it is very unlikely that it would not get a connection to itself if
    // it was not explicitly excluded
    auto v5 = econet.add_population(pop_cfg);
    BOOST_TEST(econet.num_populations() == 4 + 10 + 5 + 7 + 2 + 2); // 30

    auto ec2 = econet.add_flows<outgoing>(v5,
                                          cfg["select"]["random_29"],
                                          flow_cfg);
    BOOST_TEST(ec2.size() == 29);
    BOOST_TEST(econet.num_flows() == 7 + 29);

    for (auto e : ec2) {
        BOOST_TEST(econet.source(e) == v5);
        BOOST_TEST(econet.target(e) != v5);
    }
}

/// Test removal of edges and vertices
BOOST_FIXTURE_TEST_CASE (entity_removal, TestEcoNet)
{
    // Start with 10 populations, fully connected
    BOOST_TEST(econet.num_populations() == 10);
    BOOST_TEST(econet.num_flows() == 10*10);

    // Remove every second population
    log->info("Removing every second population ...");

    econet.remove_populations_if([](const auto& pop){
        std::cout << "Predicate check of pop " << pop->id << std::endl;
        return (pop->id % 2) == 0;
    });
    BOOST_TEST(econet.num_populations() == 5);
    BOOST_TEST(econet.num_flows() == 5*5);  // still fully connected among e/o

    // Remove all flows
    log->info("Removing all flows ...");
    auto num_removed = econet.remove_flows_if([](auto){ return false; });
    BOOST_TEST(num_removed == 0);
    BOOST_TEST(econet.num_flows() == 5*5);

    num_removed = econet.remove_flows_if([](auto){ return true; });
    BOOST_TEST(num_removed == 25);
    BOOST_TEST(econet.num_flows() == 0);


    // Check network-level conditional removal of edges
    // Fully connect again, including self-edges, then removing self-edges
    log->info("\n\nFully connecting again ...");
    fully_connect(econet);
    BOOST_TEST(econet.num_flows() == 5*5);

    log->info("Removing self-connections ...");
    num_removed =
        econet.remove_flows_if<VoidT, with_descriptor>([&](auto e, auto){
            return econet.source(e) == econet.target(e);
        });
    BOOST_TEST(econet.num_flows() == 5*4);
    BOOST_TEST(num_removed == 5);

    // Check degree
    for (auto [v, p] : econet.populations<VoidT, with_descriptor>()) {
        BOOST_TEST_CONTEXT("Population " << to_string(v)) {
            BOOST_TEST(econet.out_degree(v) == 4);
            BOOST_TEST(econet.in_degree(v) == 4);
            BOOST_TEST(econet.degree(v) == 8);
        }
    }


    // Check vertex-level conditional removal of edges
    // Fully connect again, including self-edges, then removing self-edges
    log->info("\n\nClearing and fully connecting again ...");
    econet.remove_flows_if([](auto){ return true; });
    fully_connect(econet);
    BOOST_TEST(econet.num_flows() == 5*5);

    log->info("Population-wise, removing self-connections ...");
    for (auto [v, p] : econet.populations<VoidT, with_descriptor>()) {
        BOOST_TEST_CONTEXT("Population " << to_string(v)) {
            BOOST_TEST(econet.out_degree(v) == 5);
            BOOST_TEST(econet.in_degree(v) == 5);

            // This one should remove an edge ...
            auto num_removed =
                econet.remove_in_flows_if<VoidT, with_descriptor>(
                    [&](auto e, auto){
                        return econet.source(e) == econet.target(e);
                    }, v
                );
            BOOST_TEST(num_removed == 1);

            // ... but now that it's removed, this one should not remove any
            num_removed =
                econet.remove_out_flows_if<VoidT, with_descriptor>(
                    [&](auto e, auto){
                        return econet.source(e) == econet.target(e);
                    }, v
                );
            BOOST_TEST(num_removed == 0);

            BOOST_TEST(econet.out_degree(v) == 4);
            BOOST_TEST(econet.in_degree(v) == 4);
        }
    }

    BOOST_TEST(econet.num_flows() == 5*4);
}


BOOST_FIXTURE_TEST_CASE (connect, TestEcoNet)
{
    const auto flow_cfg = defaults["flow"]["trophic"];

    // Start with an empty network
    econet.remove_flows_if([](auto){ return true; });
    BOOST_TEST(econet.num_populations() == 10);
    BOOST_TEST(econet.num_flows() == 0);

    // Add them again using the connect method; fully connecting
    econet.connect(
        econet.filtered.unfiltered,
        econet.filtered.unfiltered,
        cfg["connect"]["full"],
        flow_cfg
    );
    BOOST_TEST(econet.num_flows() == 10*10);

    // Adding more will lead to an error
    check_exception<std::runtime_error>(
        [&](){
            econet.connect(
                econet.filtered.unfiltered,
                econet.filtered.unfiltered,
                cfg["connect"]["full"],
                flow_cfg
            );
        },
        "could not be added! Does a parallel edge already exist?",
        {__LINE__, __FILE__}
    );

    // ... unless it is ok if edges already exist
    econet.connect(
        econet.filtered.unfiltered,
        econet.filtered.unfiltered,
        cfg["connect"]["exist_ok"],
        flow_cfg
    );
    BOOST_TEST(econet.num_flows() == 10*10);

    // Remove all again; then test if p_link zero adds to no new edges
    econet.remove_flows_if([](auto){ return true; });
    BOOST_TEST(econet.num_flows() == 0);

    econet.connect(
        econet.filtered.unfiltered,
        econet.filtered.unfiltered,
        cfg["connect"]["zero_p_link"],
        flow_cfg
    );
    BOOST_TEST(econet.num_flows() == 0);

    // Now, add edges again, but without self-links
    econet.connect(
        econet.filtered.unfiltered,
        econet.filtered.unfiltered,
        cfg["connect"]["no_self_links"],
        flow_cfg
    );
    BOOST_TEST(econet.num_flows() == 10*9);
}


BOOST_AUTO_TEST_SUITE_END() // end: structure ---------------------------------
BOOST_FIXTURE_TEST_SUITE (iteration, Infrastructure)

/// Test the Econet methods that work on the descriptor interface
BOOST_FIXTURE_TEST_CASE (descriptor_iterators, TestEcoNet)
{
    BOOST_TEST(econet.num_populations() == 10);
    BOOST_TEST(econet.num_flows() == 10*10);

    // Check methods
    for (auto v : econet.vertices()) {
        BOOST_TEST_CONTEXT("Vertex of population " << to_string(v) << ":") {
            BOOST_TEST(econet.in_degree(v) == 10);
            BOOST_TEST(econet.out_degree(v) == 10);
            BOOST_TEST(econet.degree(v) == 20);

            // Test access to iterator range returning methods by testing empty
            BOOST_TEST(get_iter_size(econet.in_edges(v)) == 10);
            BOOST_TEST(get_iter_size(econet.out_edges(v)) == 10);
            BOOST_TEST(get_iter_size(econet.adjacent_vertices(v)) == 10);
            BOOST_TEST(get_iter_size(econet.inv_adjacent_vertices(v)) == 10);
        }
    }

    for (auto e : econet.edges()) {
        BOOST_TEST_CONTEXT("Edge " << to_string(e) << ":") {
            // Test access; these should _not_ return null pointers
            BOOST_TEST(econet.source(e));
            BOOST_TEST(econet.target(e));
        }
    }
}

/// Test the entity-based iteration methods
BOOST_FIXTURE_TEST_CASE (entity_iterators, TestEcoNet)
{
    BOOST_TEST(econet.num_populations() == 10);
    BOOST_TEST(econet.num_flows() == 10*10);

    // Iterate over populations
    for (const auto& pop : econet.populations()) {
        BOOST_TEST_CONTEXT("Population: " << pop->id) {
            // Test that access works
            BOOST_TEST(pop->kind() == "regular");
            BOOST_TEST(pop->is_active());
        }
    }

    // Iterate over flows
    for (const auto& flow : econet.flows()) {
        BOOST_TEST_CONTEXT("Flow: " << flow->id) {
            BOOST_TEST(flow->kind() == "trophic");
        }
    }

    // Iterate over vertices and populations and (for each vertex) iterate
    // over their connected flows and populations
    // This asserts that the "zip" iteration is working correctly
    for (const auto& [v, pop] : econet.populations<VoidT, with_descriptor>()) {
        BOOST_TEST_CONTEXT("Population: " << pop->id << ", Vertex: " << v)
        {
            BOOST_TEST(econet[v] == pop);

            for (const auto& flow : econet.in_flows(v)) {
                BOOST_TEST(flow->kind() == "trophic");
            }

            for (const auto& flow : econet.out_flows(v)) {
                BOOST_TEST(flow->kind() == "trophic");
            }

            for (const auto& pop : econet.downstream_populations(v)) {
                BOOST_TEST(pop->kind() == "regular");
            }

            for (const auto& pop : econet.upstream_populations(v)) {
                BOOST_TEST(pop->kind() == "regular");
            }

            // Again, now with the iteration that includes the descriptor
            for (const auto& [e, flow]
                 : econet.in_flows<VoidT, with_descriptor>(v))
            {
                BOOST_TEST(econet[e] == flow);
            }

            for (const auto& [e, flow]
                 : econet.out_flows<VoidT, with_descriptor>(v))
            {
                BOOST_TEST(econet[e] == flow);
            }

            for (const auto& [v, pop]
                 : econet.downstream_populations<VoidT, with_descriptor>(v))
            {
                BOOST_TEST(econet[v] == pop);
            }

            for (const auto& [v, pop]
                 : econet.upstream_populations<VoidT, with_descriptor>(v))
            {
                BOOST_TEST(econet[v] == pop);
            }
        }
    }
}

/// Test filtered iteration in general
BOOST_FIXTURE_TEST_CASE (filtered_iteration_general, TestEcoNet)
{
    BOOST_TEST(econet.num_populations() == 10);
    BOOST_TEST(econet.num_flows() == 10*10);

    // Iterate over regular populations only
    auto cntr = 0u;
    for (const auto& pop : econet.populations(econet.filtered.only_regular)) {
        BOOST_TEST_CONTEXT("Population: " << pop->id) {
            // Test that access works
            BOOST_TEST(pop->kind() == "regular");
            BOOST_TEST(pop->is_active());
        }
        cntr++;
    }
    BOOST_TEST(cntr == 10);

    // Add a few non-regular population and test again
    econet.add_population("base", Config{});
    econet.add_population("base", Config{});
    econet.add_population("base", Config{});

    cntr = 0;
    for (const auto& pop : econet.populations(econet.filtered.only_regular)) {
        BOOST_TEST_CONTEXT("Population: " << pop->id) {
            // Test that access works
            BOOST_TEST(pop->kind() == "regular");
            BOOST_TEST(pop->is_active());
        }
        cntr++;
    }
    BOOST_TEST(cntr == 10);

    // Now, a custom filter for non-regular populations
    cntr = 0;
    auto base_only = [&](auto v){ return (econet[v]->kind_id() == 0);};
    for (const auto& pop :
         econet.populations(econet.filtered(boost::keep_all(), base_only)))
    {
        BOOST_TEST_CONTEXT("Population: " << pop->id) {
            // Test that access works
            BOOST_TEST(pop->kind() == "base");
            BOOST_TEST(pop->is_active());
        }
        cntr++;
    }
    BOOST_TEST(cntr == 3);
}

/// Test filtered iteration with dynamic cast
BOOST_FIXTURE_TEST_CASE (filtered_iteration_with_cast, TestEcoNet)
{
    econet.add_population("base", Config{});
    econet.add_population("base", Config{});
    econet.add_population("base", Config{});
    BOOST_TEST(econet.num_populations() == 10 + 3);

    // Iterate over base populations; no casting necessary
    auto cntr = 0u;
    auto base_only = [&](auto v){ return (econet[v]->kind_id() == 0);};
    for (const auto& pop :
         econet.populations(econet.filtered(boost::keep_all(), base_only)))
    {
        BOOST_TEST_CONTEXT("Population: " << pop->id) {
            // Test that access works
            BOOST_TEST(pop->kind() == "base");
            BOOST_TEST(pop->is_active());
        }
        cntr++;
    }
    BOOST_TEST(cntr == 3);

    // Iterate over regular populations only
    cntr = 0;
    for (const auto& pop
         : econet.populations<Population>(econet.filtered.only_regular))
    {
        BOOST_TEST_CONTEXT("Population: " << pop->id) {
            // Test that access to base interface works
            BOOST_TEST(pop->kind() == "regular");
            BOOST_TEST(pop->is_active());

            // ... but also to Population-specific interface
            BOOST_TEST(pop->mortality > 0.);
        }
        cntr++;
    }
    BOOST_TEST(cntr == 10);

}


BOOST_AUTO_TEST_SUITE_END() // end: iteration ---------------------------------
BOOST_FIXTURE_TEST_SUITE (dynamics, Infrastructure)

/// Test the apply_conservation_laws method
BOOST_FIXTURE_TEST_CASE (conservation_laws, TestEcoNet)
{
    using ResSpace = typename EcoNet::ResSpace;

    // Shared defaults
    const ResVec c_old = to_ResVec({1, 2, 3});

    // -- Without changes in composition
    // Pass-through, no losses
    {
    const auto c_new = to_ResVec({1, 2, 3});
    const auto res = to_ResVec({10, 20, 30});
    auto [s, r, l] = econet.apply_conservation_laws(c_old, c_new, 42., res);

    BOOST_TEST(s == 42.);

    r.print("reservoir");
    BOOST_TEST(arma::all(r == res));

    l.print("losses");
    BOOST_TEST(arma::all(l == ResSpace::zeros()));
    }

    // -- With new composition being smaller in some elements
    // Size stays the same but there are non-zero losses due to each
    // individual having a smaller specific composition ...
    {
    const auto c_new = to_ResVec({1, 1, 1});
    const auto res = to_ResVec({10, 20, 30});
    auto [s, r, l] = econet.apply_conservation_laws(c_old, c_new, 42., res);

    BOOST_TEST(s == 42.);

    r.print("reservoir");
    BOOST_TEST(arma::all(r == res));

    l.print("losses");
    BOOST_TEST(arma::all(l == to_ResVec({0, 42, 84})));
    }

    // -- With new composition being larger in some elements (and not smaller)
    // Size is reduced. Losses are those lost due to the size change.
    {
    const auto c_new = to_ResVec({1, 3, 4});
    const auto res = to_ResVec({10, 20, 30});
    auto [s, r, l] = econet.apply_conservation_laws(c_old, c_new, 100, res);

    BOOST_TEST(s == 20);  // compensated by resources from reservoir (@ idx 1)

    r.print("reservoir");
    BOOST_TEST(arma::all(r == to_ResVec({10, 0, 10})));

    l.print("losses");
    BOOST_TEST(arma::all(l == (100-20) * c_old));
    }

    // -- With new composition being larger or smaller
    // Size is reduced. Losses are those lost due to the size change plus those
    // lost in the remaining population due to the composition change
    {
    const auto c_new = to_ResVec({0, 4, 5});  // (-1, +2, +2) compared to old
    const auto res = to_ResVec({10, 20, 30});
    auto [s, r, l] = econet.apply_conservation_laws(c_old, c_new, 100, res);

    BOOST_TEST(s == 10);  // compensated by resources from reservoir

    r.print("reservoir");
    BOOST_TEST(arma::all(r == to_ResVec({10, 0, 10})));

    l.print("losses (actual)");
    const ResVec expd_l = (100-10) * c_old + 10 * to_ResVec({1, 0, 0});
    expd_l.print("losses (expected)");
    BOOST_TEST(arma::all(l == expd_l));
    }
}



BOOST_AUTO_TEST_SUITE_END() // end: dynamics- ---------------------------------
