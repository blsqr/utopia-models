# Base plot configurations for the EEcosy model
---
# Some default values to use in definitions below
_default_values:
  labels:
    time: &label_time Time [steps]

  colors:
    source:       &c_source       gold
    population:   &c_population   mediumseagreen

  cyclers:
    tab20: &cy_tab20 "cycler('color', ['1f77b4', 'aec7e8', 'ff7f0e', 'ffbb78', '2ca02c', '98df8a', 'd62728', 'ff9896', '9467bd', 'c5b0d5', '8c564b', 'c49c94', 'e377c2', 'f7b6d2', '7f7f7f', 'c7c7c7', 'bcbd22', 'dbdb8d', '17becf', '9edae5'])"
    tab20_split: &cy_tab20_split "cycler('color', ['1f77b4', 'ff7f0e', '2ca02c', 'd62728', '9467bd', '8c564b', 'e377c2', '7f7f7f', 'bcbd22', '17becf', 'aec7e8', 'ffbb78', '98df8a', 'ff9896', 'c5b0d5', 'c49c94', 'f7b6d2', 'c7c7c7', 'dbdb8d', '9edae5'])"
    # First two colours swapped such that first colour looks more like source
    tab20_split_swap12: &cy_tab20_split_swap12 "cycler('color', ['ff7f0e', '1f77b4', '2ca02c', 'd62728', '9467bd', '8c564b', 'e377c2', '7f7f7f', 'bcbd22', '17becf', 'aec7e8', 'ffbb78', '98df8a', 'ff9896', 'c5b0d5', 'c49c94', 'f7b6d2', 'c7c7c7', 'dbdb8d', '9edae5'])"



# Plot configuration defaults -------------------------------------------------
# ... For all plots
_defaults: &defaults
  based_on: .default_style_and_helpers

  style: &default_style
    # https://matplotlib.org/users/customizing.html
    lines.linewidth: 1.2
    axes.prop_cycle: *cy_tab20_split
    legend.fontsize: x-small
    # legend.loc: best

_animation: &animation
  animation:
    enabled: true  # has to be manually enabled

    writer: frames  # assuming ffmpeg is not installed

    # Configuration for each writer
    writer_kwargs:
      frames:
        saving:
          dpi: 92

      ffmpeg:
        init:
          fps: 10
        saving:
          dpi: 92


# -----------------------------------------------------------------------------
# Helpers  --------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Limits
hlpr.limits.x_min_max:
  helpers:
    set_limits:
      x: [min, max]

hlpr.limits.x_from_zero:
  helpers:
    set_limits:
      x: [0, ~]

hlpr.limits.y_min_max:
  helpers:
    set_limits:
      y: [min, max]

hlpr.limits.y_from_zero:
  helpers:
    set_limits:
      y: [0, ~]

# Legend
hlpr.legend.use:
  helpers:
    set_legend:
      use_legend: true

hlpr.legend.hide:
  helpers:
    set_legend:
      use_legend: false

hlpr.legend.hide_if_large:
  helpers:
    set_legend:
      use_legend: true
      hiding_threshold: 10


# Other aesthetics ............................................................

style.no_grid:
  style:
    axes.grid: false

# Property cyclers . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
style.prop_cycle.tab20:
  style:
    axes.prop_cycle: *cy_tab20

style.prop_cycle.tab20_split:
  style:
    axes.prop_cycle: *cy_tab20_split

style.prop_cycle.tab20_split_swap12:
  style:
    axes.prop_cycle: *cy_tab20_split_swap12

# -----------------------------------------------------------------------------
# Specializations  ------------------------------------------------------------
# -----------------------------------------------------------------------------
kind.time_series:
  hue: id

  helpers:
    set_limits:
      x: [min, max]

    axis_specific:
      bottom_row:
        axis: [~, -1]
        set_labels:
          x: *label_time


# -----------------------------------------------------------------------------
# DAG Plots -------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Facet Grid plots ............................................................
dag.facet_grid: &dag_facet_grid
  <<: *defaults
  based_on: .plot.facet_grid

dag.facet_grid.uni:
  <<: *dag_facet_grid
  creator: universe
  universes: all

  dag_options:
    select_path_prefix: data/EEcosy

dag.facet_grid.mv:
  <<: *dag_facet_grid

  creator: multiverse

  select_and_combine:
    base_path: data/EEcosy


# Network plots ...............................................................
dag.network: &dag_network
  <<: *defaults
  based_on: .plot.graph

  select:
    graph_group: network

  compute_only: ~
  register_property_maps: []

  graph_creation:
    at_time_idx: 0
    node_props: [vertex_kinds]
    edge_props: [edge_kinds]

  graph_drawing:
    positions:
      model: graphviz_dot   # For hierarchical plotting; needs pydot

    nodes:
      node_color:
        from_property: vertex_kinds

      cmap:
        from_values:
          1: *c_population
          2: *c_source
        under:
      colorbar:
        enabled: true
        shrink: .2
        aspect: 10
        orientation: horizontal
        labels:
          1: regular
          2: source

    node_labels:
      enabled: true
      font_color: k
      font_size: 9

    edges:
      # edge_weight:
      #   from_property: edge_kinds
      edge_color: grey
      width: .7

dag.network.uni:
  <<: *dag_network
  creator: universe
  universes: all

  dag_options:
    select_path_prefix: data/EEcosy


# -----------------------------------------------------------------------------
# DAG Options -----------------------------------------------------------------
# -----------------------------------------------------------------------------
# Verbose mode
dag.verbose:
  dag_options:
    verbosity: &dag_verbosity 2

# Caching
dag.use_cache:
  dag_options:
    file_cache_defaults: &dag_file_cache
      read: true
      write:
        enabled: true
        min_cumulative_compute_time: 1.

# Default DAG options
dag.default_options:
  dag_options:
    verbosity: *dag_verbosity
    file_cache_defaults: *dag_file_cache

# Base paths
dag.uni.select_from.populations:
  dag_options:
    select_path_prefix: data/EEcosy/populations/regular

dag.mv.select_from.populations:
  select_and_combine:
    base_path: data/EEcosy/populations/regular

dag.uni.select_from.source_populations:
  dag_options:
    select_path_prefix: data/EEcosy/populations/source

dag.mv.select_from.source_populations:
  select_and_combine:
    base_path: data/EEcosy/populations/source

dag.uni.select_from.flows:
  dag_options:
    select_path_prefix: data/EEcosy/flows/trophic

dag.mv.select_from.flows:
  select_and_combine:
    base_path: data/EEcosy/flows/trophic

dag.uni.select_from.econet:
  dag_options:
    select_path_prefix: data/EEcosy/econet

dag.mv.select_from.econet:
  select_and_combine:
    base_path: data/EEcosy/econet

# facet_grid specifier options
dag.facet_specs.time_series:
  kind: line
  x: time
