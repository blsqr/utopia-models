#ifndef UTOPIA_MODELS_EECOSY_UTILS_YAML_HH
#define UTOPIA_MODELS_EECOSY_UTILS_YAML_HH

#include <vector>

#include <armadillo>
#include <yaml-cpp/yaml.h>

#include <UtopiaUtils/utils/iterators.hh>

#include "../types.hh"


namespace YAML {

// -- Armadillo Overloads -----------------------------------------------------

using Utopia::Models::EEcosy::ResourceVectorType;

template<std::size_t N>
Emitter& operator << (Emitter& out, const ResourceVectorType<N>& vec) {
    out << YAML::Flow << YAML::BeginSeq;
    for (const auto& e : vec) {
        out << e;
    }
    out << YAML::EndSeq;
    return out;
}


/// YAML conversion for EEcosy ResourceVector type
/** \NOTE Not working for yaml-cpp < 0.6.3.
  *       Workaround: to_vec, see below.
  */
template<std::size_t N>
struct convert<ResourceVectorType<N>> {
    static Node encode(const ResourceVectorType<N>& rhs) {
        Node node;
        for (auto e : rhs) {
            node.push_back(e);
        }
        return node;
    }

    static bool decode (const Node& node, ResourceVectorType<N>& rhs) {
        using eT = typename ResourceVectorType<N>::elem_type;
        using Utopia::Utils::in_range;

        if (not node.IsSequence() or node.size() != N) {
            return false;
        }

        for (const auto i : in_range(N)) {
            rhs[i] = node[i].template as<eT>();
        }
        return true;
    }
};


} // namespace YAML

namespace Utopia::Models::EEcosy {

/// Converts an arma vector into an std::vector
/** This is a workaround for custom YAML::convert implementations not working
  * on yaml-cpp < 0.6.3
  */
template<class ArmaVec, class eT = typename ArmaVec::elem_type>
std::vector<eT> to_vec (const ArmaVec& v) {
    return arma::conv_to<std::vector<eT>>::from(v);
}


} // namespace Utopia::Models::EEcosy

#endif // UTOPIA_MODELS_EECOSY_UTILS_YAML_HH
