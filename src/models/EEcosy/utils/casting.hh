#ifndef UTOPIA_MODELS_EECOSY_UTILS_CASTING_HH
#define UTOPIA_MODELS_EECOSY_UTILS_CASTING_HH

#include <memory>
#include <type_traits>

#include <UtopiaUtils/utils/meta.hh>
#include <UtopiaUtils/utils/tags.hh>

#include "../types.hh"


namespace Utopia::Models::EEcosy {

using namespace Utopia::Utils::tags;
using Utopia::Utils::is_same;
using Utopia::Utils::is_same_template;

/// Dynamically cast an entity pointer, if necessary -- passthrough case
template<template<class...> class cast_to = VoidT,
         class ET,
         class TargetET = cast_to<typename ET::ResSpace, typename ET::RNG>,
         bool passthrough = (   is_same_template<cast_to, VoidT>()
                             or is_same<TargetET, ET>()),
         std::enable_if_t<passthrough, int> = 0
         >
const std::shared_ptr<ET>& as (const std::shared_ptr<ET>& entity) {
    return entity;
}

/// Dynamically cast an entity pointer, if necessary -- casting case
template<template<class...> class cast_to = VoidT,
         class ET,
         class TargetET = cast_to<typename ET::ResSpace, typename ET::RNG>,
         bool passthrough = (   is_same_template<cast_to, VoidT>()
                             or is_same<TargetET, ET>()),
         std::enable_if_t<not passthrough, int> = 0
         >
std::shared_ptr<TargetET> as (const std::shared_ptr<ET>& entity) {
    return std::dynamic_pointer_cast<TargetET>(entity);
}

} // namespace Utopia::Models::EEcosy

#endif // UTOPIA_MODELS_EECOSY_UTILS_CASTING_HH
