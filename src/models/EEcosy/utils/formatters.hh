#ifndef UTOPIA_MODELS_EECOSY_UTILS_FORMATTERS_HH
#define UTOPIA_MODELS_EECOSY_UTILS_FORMATTERS_HH

#include <sstream>
#include <string_view>

#include <yaml-cpp/yaml.h>

#include <spdlog/spdlog.h>
#include <fmt/core.h>

#include "../types.hh"


namespace fmt {

using Utopia::Models::EEcosy::ResourceVectorType;

/// Custom formatter for Armadillo vectors
/** See: https://fmt.dev/latest/api.html#formatting-user-defined-types
  *
  * \note Not working with fmtlib < 6.0
  *
  * \FIXME somehow not detected on gcc-10 :roll_eyes:
  */
template<std::size_t N>
struct formatter<ResourceVectorType<N>> : formatter<std::string_view> {
    template<class FormatContext>
    auto format (const ResourceVectorType<N>& node, FormatContext& ctx) {
        std::stringstream node_stream;
        node.print(node_stream);
        return formatter<std::string_view>::format(node_stream.str(), ctx);
    }
};


// TODO Add formatters here for:
//        * BasePopulation
//        * BaseFlow

} // namespace fmt

#endif // UTOPIA_MODELS_EECOSY_UTILS_FORMATTERS_HH
