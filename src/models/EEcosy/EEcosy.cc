#include <iostream>

#include "EEcosy.hh"

using namespace Utopia::Models::EEcosy;


int main (int, char** argv) {
    try {
        // Initialize the PseudoParent from config file path
        Utopia::PseudoParent<ModelRNG> pp(argv[1]);

        // TODO Distinguish different resource space dimensionalities.
        // TODO Add a factory to do this, and make it config-accessible

        // Initialize the main model instance and directly run it.
        EEcosy<DefaultResourceSpace>("EEcosy", pp).run();

        // Done
        return 0;
    }
    catch (Utopia::Exception& e) {
        return Utopia::handle_exception(e);
    }
    catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    catch (...) {
        std::cerr << "Exception occurred!" << std::endl;
        return 1;
    }
}
