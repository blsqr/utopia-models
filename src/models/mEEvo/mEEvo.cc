#include <iostream>

#include "mEEvo.hh"

using namespace Utopia::Models::mEEvo;
using Utopia::get_as;

int main (int, char** argv) {
    try {
        // Initialize the PseudoParent from config file path
        Utopia::PseudoParent<ModelRNG> pp(argv[1]);

        // Get the model configuration
        const auto model_cfg = get_as<Config>("mEEvo", pp.get_cfg());

        // Deactivate separate competition feature to reduce compilation times
        if (get_as<bool>("with_separate_competition", model_cfg, false)) {
            throw std::invalid_argument(
                "`with_separate_competition` not activated currently!"
            );
        }
        // Initialize the main model instance, distinguishing different
        // template parameter scenarios; then directly run the model.
        if (get_as<bool>("with_envmod", model_cfg)) {
            mEEvo<with_envmod, write_with_reduced_float_precision>(
                "mEEvo", pp
            ).run();
        }
        else {
            mEEvo<write_with_reduced_float_precision>("mEEvo", pp).run();
        }

        // Done
        return 0;
    }
    catch (Utopia::Exception& e) {
        return Utopia::handle_exception(e);
    }
    catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    catch (...) {
        std::cerr << "Exception occurred!" << std::endl;
        return 1;
    }
}
