#ifndef UTOPIA_MODELS_MEEVO_TRACKERS_HH
#define UTOPIA_MODELS_MEEVO_TRACKERS_HH

#include <vector>
#include <functional>

#include <armadillo>

#include <UtopiaUtils/utils/armadillo.hh>
#include <UtopiaUtils/utils/meta.hh>

#include "types.hh"
#include "utils.hh"


namespace Utopia::Models::mEEvo {

struct BaseTracker {
    IDVector ids;

    BaseTracker ()
    :
        ids{}
    {}

    auto size () const {
        return ids.size();
    }
};

/// Tracks trait values and aggregates them into containers
/** This data is periodically flushed out via the buffer property writers.
  *
  * \note   While focussed on *traits*, this tracker does additionally contain
  *         other information about species that is not really a trait but some
  *         property during its initialization, which is convenient to track
  *         using this tracker rather than with a separate one ...
  */
template<class... tags>
struct TraitsTracker : public BaseTracker {
    /// Whether environment modifications are enabled; CURRENTLY IGNORED!
    static constexpr bool envmod_enabled =
        Utopia::Utils::contains_type<with_envmod, tags...>();

    Vector m;
    Vector f;
    Vector s;
    IDVector parent_ids;
    Vector frac_diverted;

    Vector initial_TL;

    // TODO Track distr_to_env

    TraitsTracker ()
    :
        BaseTracker()
    ,   m{}
    ,   f{}
    ,   s{}
    ,   parent_ids{}
    ,   frac_diverted{}
    ,   initial_TL{}
    {}

    /// Add a single entry, also allowing to supply a parent ID
    template<class ODESys, class T>
    void track_single (const ODESys& sys, const T idx,
                       const arma::uword parent_id = 0)
    {
        using Utopia::Utils::append_element;

        append_element(ids, sys.ids(idx));
        append_element(m, sys.m(idx));
        append_element(f, sys.f(idx));
        append_element(s, sys.s(idx));
        append_element(parent_ids, parent_id);

        // Non-trait observables
        append_element(initial_TL, sys.trophic_level(idx));

        // if constexpr (not envmod_enabled) return;
        // Environment-related traits
        append_element(frac_diverted, sys.frac_diverted(idx));
    }

    /// Add multiple entries either via a span or a vector of indices
    /** \note parent ID is set to zero
      */
    template<class ODESys, class Selector = arma::span>
    void track_multiple (const ODESys& sys, const Selector& sel) {
        const auto insert_at = size();

        ids.insert_rows(insert_at, sys.ids(sel));
        m.insert_rows(insert_at, sys.m(sel));
        f.insert_rows(insert_at, sys.f(sel));
        s.insert_rows(insert_at, sys.s(sel));

        parent_ids.insert_rows(
            insert_at, IDVector(sys.ids(sel).n_elem, arma::fill::zeros)
        );

        // Non-trait observables
        initial_TL.insert_rows(insert_at, sys.trophic_level(sel));

        // if constexpr (not envmod_enabled) return;
        // Environment-related traits
        frac_diverted.insert_rows(insert_at, sys.frac_diverted(sel));
    }
};


/// Tracks properties aggregated over the lifetime of a species
/**
  *
  * \note Assumes that the tracking is done upon *extinction* of a species!!
  */
struct PropertyTracker : public BaseTracker {
    UVector age;
    Vector lifetime;
    BoolVector was_viable;
    Vector mean_trophic_level;
    Vector extinction_delay;
    Vector TL_diff;

private:
    bool _tracked_final = false;


public:
    PropertyTracker ()
    :
        BaseTracker()
    ,   age{}
    ,   lifetime{}
    ,   was_viable{}
    ,   mean_trophic_level{}
    ,   extinction_delay{}
    ,   TL_diff{}
    {}

    void add_id (const arma::uword id) {
        using Utopia::Utils::append_element;

        append_element(ids, id);
        append_element(age, 0);
        append_element(lifetime, NaN);
        append_element(was_viable, false);
        append_element(mean_trophic_level, NaN);
        append_element(extinction_delay, NaN);
        append_element(TL_diff, NaN);
    }

    template<class ODESys, class T>
    void track_single (const ODESys& sys, const T idx) {
        using Utopia::Utils::append_element;

        if (_tracked_final) {
            throw std::runtime_error(
                "After PropertyTracker::track_final was called, calling of "
                "any of the other tracking methods is no longer possible!"
            );
        }

        append_element(ids, sys.ids(idx));
        append_element(age, sys.age(idx));
        append_element(lifetime, sys.age(idx));
        append_element(was_viable, sys.age(idx) >= sys.viability_age);
        append_element(mean_trophic_level, sys.mean_trophic_level(idx));
        append_element(
            extinction_delay,
            sys.get_model_time() - sys.last_species_addition_time
        );
        append_element(
            TL_diff,
            sys.last_species_addition_mean_TL - sys.mean_trophic_level(idx)
        );
    }

    template<class ODESys, class Selector = arma::span>
    void track_multiple (const ODESys& sys, const Selector& sel) {
        if (_tracked_final) {
            throw std::runtime_error(
                "After PropertyTracker::track_final was called, calling of "
                "any of the other tracking methods is no longer possible!"
            );
        }

        const auto insert_at = size();

        ids.insert_rows(insert_at, sys.ids(sel));
        age.insert_rows(insert_at, sys.age(sel));
        lifetime.insert_rows(
            insert_at, arma::conv_to<Vector>::from(sys.age(sel))
        );
        was_viable.insert_rows(insert_at, sys.age(sel) >= sys.viability_age);
        mean_trophic_level.insert_rows(insert_at, sys.mean_trophic_level(sel));

        Vector _delays{};
        _delays.copy_size(sys.ids(sel));
        _delays.fill(sys.get_model_time() - sys.last_species_addition_time);
        extinction_delay.insert_rows(insert_at, _delays);

        TL_diff.insert_rows(
            insert_at,
            sys.last_species_addition_mean_TL - sys.mean_trophic_level(sel)
        );
    }

    /// For tracking *all* species at the end of a simulation
    /** \note After invoking this, no other method can be called.
      */
    template<class ODESys>
    void track_final (const ODESys& sys) {
        if (_tracked_final) {
            throw std::runtime_error(
                "PropertyTracker::track_final may only be called once!"
            );
        }

        const auto insert_at = size();

        ids.insert_rows(insert_at, sys.ids);
        age.insert_rows(insert_at, sys.age);
        was_viable.insert_rows(insert_at, sys.age >= sys.viability_age);
        mean_trophic_level.insert_rows(insert_at, sys.mean_trophic_level);

        // The following only make sense if the species were extinct.
        // However, if this method is called, this is to flush out remaining
        // information (above). For some quantities (below), this does not
        // make sense, because they are only meaningful if the species became
        // extinct. Thus, they are set to NaN.
        const Vector all_NaN = NaN * Vector(sys.ids.size());

        lifetime.insert_rows(insert_at, all_NaN);
        extinction_delay.insert_rows(insert_at, all_NaN);
        TL_diff.insert_rows(insert_at, all_NaN);

        _tracked_final = true;
    }

};


/// Specifically tracks the effect of adding a new species
/** This is evaluated in a delayed manner and decoupled from the removal of
  * the species, because the effect may only appear at a later point ...
  *
  * In detail, this tracks the number of extinctions a species caused and the
  * change in functional diversity it caused.
  */
struct SpeciesEffectTracker : public BaseTracker {
    UVector n_extinctions;
    Vector delta_FD;
    Vector delta_trophic_incoherence;

    SpeciesEffectTracker ()
    :
        BaseTracker()
    ,   n_extinctions{}
    ,   delta_FD{}
    ,   delta_trophic_incoherence{}
    {}

    void add_id (const arma::uword id) {
        using Utopia::Utils::append_element;

        append_element(ids, id);
        append_element(n_extinctions, 0);
        append_element(delta_FD, NaN);
        append_element(delta_trophic_incoherence, NaN);
    }

    template<class ODESys>
    void track_latest (ODESys& sys) {
        using Utopia::Utils::append_element;

        append_element(ids, sys.get_last_added_species_id());
        append_element(n_extinctions, sys.n_extinctions);
        append_element(delta_FD, sys.delta_FD());
        append_element(delta_trophic_incoherence,
                       sys.delta_trophic_incoherence());
    }
};


/// Like the traits tracker, but for times
struct TimeTracker  : public BaseTracker {
    using TimeVector = arma::Col<Time>;
    using TimeFunc = std::function<Time()>;

    TimeVector times;

    TimeFunc get_time;

    TimeTracker (TimeFunc get_time)
    :
        BaseTracker()
    ,   times{}
    ,   get_time(get_time)
    {}

    void add_id (const arma::uword id) {
        using Utopia::Utils::append_element;

        append_element(ids, id);
        append_element(times, get_time());
    }

    template<class ODESys, class T>
    void track_single (const ODESys& sys, const T idx) {
        using Utopia::Utils::append_element;

        append_element(ids, sys.ids(idx));
        append_element(times, get_time());
    }

    template<class ODESys, class Selector = arma::span>
    void track_multiple (const ODESys& sys, const Selector& sel) {
        const auto insert_at = size();
        ids.insert_rows(insert_at, sys.ids(sel));

        TimeVector new_times{};
        new_times.copy_size(sys.ids(sel));
        new_times.fill(get_time());
        times.insert_rows(insert_at, new_times);
    }
};


} // namespace Utopia::Models::mEEvo

#endif // UTOPIA_MODELS_MEEVO_TRACKERS_HH
