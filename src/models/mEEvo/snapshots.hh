#ifndef UTOPIA_MODELS_MEEVO_SNAPSHOTS_HH
#define UTOPIA_MODELS_MEEVO_SNAPSHOTS_HH

#include <string>
#include <vector>

#include <boost/range/adaptor/reversed.hpp>

#include <UtopiaUtils/utils/snapshots.hh>
#include "types.hh"


namespace Utopia::Models::mEEvo {


/// The ODESystem snapshot managers, conditioned on different observables
template<class Snapshot>
struct Snapshots {
    /// The snapshot manager, specializing only the Snapshot type
    template<class... Args>
    using SnapshotManager =
        Utopia::Utils::MultiSnapshotManager<Snapshot, Args...>;

    // .. General parameters ..................................................
    /// Whether snapshotting is enabled at all
    bool enabled;

    /// The minimum time after which to make a snapshot; requires `enabled` set
    Time enabled_after;

    /// Whether to store the snapshots
    bool store;


    // .. Snapshot-specific parameters ........................................

    /// Whether to only include snapshots of viable species
    /** This is only a parameter, it is evaluated by some outside entity.
      */
    bool only_viable;


    // .. Snapshots ...........................................................
    /// Largest number of extinctions caused by addition of new species
    SnapshotManager<unsigned> n_extinctions;

    /// Largest *negative* delta FD
    SnapshotManager<double, std::less<double>> delta_FD_min;

    /// Largest *positive* delta FD
    SnapshotManager<double> delta_FD_max;

    /// Time-based snapshot
    SnapshotManager<Time> time;

    /// Smallest trophic incoherence
    SnapshotManager<double, std::less<double>> min_trophic_incoherence;

    /// Largest trophic incoherence
    SnapshotManager<double> max_trophic_incoherence;


    // ........................................................................

    Snapshots (const Config& cfg)
    :
        enabled(get_as<bool>("enabled", cfg))
    ,   enabled_after(get_as<Time>("enabled_after", cfg, 0))
    ,   store(get_as<bool>("store", cfg))

    // Snapshot-specific parameters
    ,   only_viable(get_as<bool>("only_viable", cfg))

    // Snapshot managers
    ,   n_extinctions(get_as<Config>("n_extinctions", cfg))
    ,   delta_FD_min(get_as<Config>("delta_FD_min", cfg))
    ,   delta_FD_max(get_as<Config>("delta_FD_max", cfg))
    ,   time(
            [](const auto& sys){ return sys.get_model_time(); },
            get_as<Config>("time", cfg)
        )
    ,   min_trophic_incoherence(get_as<Config>("min_trophic_incoherence", cfg))
    ,   max_trophic_incoherence(get_as<Config>("max_trophic_incoherence", cfg))
    {}

    /// Evaluates whether a snapshot should be made at this time
    bool is_enabled (const Time time) const {
        return enabled and (time >= enabled_after);
    }

    /// Store all snapshots in their own group
    void store_all (const std::shared_ptr<DataIO::HDFGroup>& base_grp) const {
        const auto adptr = [](const auto& grp, const auto, const auto& snap){
            snap.store(grp);
        };

        store_snapshot(n_extinctions, base_grp, "n_extinctions", adptr);

        store_snapshot(delta_FD_min, base_grp, "delta_FD_min", adptr);
        store_snapshot(delta_FD_max, base_grp, "delta_FD_max", adptr);

        store_snapshot(min_trophic_incoherence, base_grp,
                       "min_trophic_incoherence", adptr);
        store_snapshot(max_trophic_incoherence, base_grp,
                       "max_trophic_incoherence", adptr);

        store_snapshot(
            time, base_grp, "time",
            [](const auto& grp, const auto snap_time, const auto& snap){
                snap.store(grp);
                grp->add_attribute("time", snap_time);
            }
        );
    }

    /// Stores a single snapshot manager's snapshots in a new group
    template<class SM, class Adaptor, class... Args>
    void store_snapshot (
        const SM& sm,
        const std::shared_ptr<DataIO::HDFGroup>& base_grp,
        const std::string grp_name,
        const Adaptor storage_adaptor,
        const std::string& grp_name_fstr = "{:d}",
        Args&&... args
    ) const
    {
        using boost::adaptors::reverse;

        // Open the group to store the individual snapshots in; always do this
        // such that the group is always available, even without snapshots
        const auto grp = base_grp->open_group(grp_name);

        // If there are no snapshots, can return here already
        if (not sm.size()) return;

        // Keep track of the index values and the respective snapshot time
        auto idx_vals = std::vector<typename SM::Index>();
        auto times = std::vector<Time>();
        auto i = 0u;

        for (const auto& [idx, snap] : reverse(sm.snapshots())) {
            storage_adaptor(
                grp->open_group(fmt::format(grp_name_fstr, i)),
                idx, snap,
                std::forward<Args>(args)...
            );
            idx_vals.push_back(idx);
            times.push_back(snap.get_last_property_update_time());
            // TODO ... is this general enough? Probably not, store separately?
            i++;
        }

        // Store the meta data as separate datasets, where the dataset index
        // refers to the corresponding group and the value is the snapshot's
        // index value, addition time, …
        const auto idx_val_dset = grp->open_dataset("_index_values");
        idx_val_dset->write(
            idx_vals.begin(), idx_vals.end(), [](auto v){ return v; }
        );
        idx_val_dset->add_attribute("dim_name__0", "snapshot_no");

        const auto times_dset = grp->open_dataset("_times");
        times_dset->write(
            times.begin(), times.end(), [](auto v){ return v; }
        );
        times_dset->add_attribute("dim_name__0", "snapshot_no");
    }
};

} // namespace Utopia::Models::mEEvo

#endif // UTOPIA_MODELS_MEEVO_SNAPSHOTS_HH
