#ifndef UTOPIA_MODELS_MEEVO_TYPES_HH
#define UTOPIA_MODELS_MEEVO_TYPES_HH

#include <memory>
#include <vector>
#include <tuple>
#include <type_traits>

#include <armadillo>

#include <utopia/core/types.hh>

#include <UtopiaUtils/utils/rng.hh>

#include "state.hh"


namespace Utopia::Models::mEEvo {

/// The RNG type to use: the default RNG from the Utopia::Utils module
using ModelRNG = Utopia::Utils::DefaultRNG;



// -- General type definitions ------------------------------------------------

/// The configuration type used throughout this model
using Config = Utopia::DataIO::Config;

/// Type of the ID used for identification of entities
using IDType = arma::uword;

/// Type of the time value (should match that from model traits)
using Time = std::size_t;

/// Container holding shared pointers to some object
template<class T>
using PtrContainer = std::vector<std::shared_ptr<T>>;

/// Type of the ODE system's state (biomass, depot and environment)
using State = BDEState;


// .. Armadillo-based types ...................................................
/// The general column vector type
using Vector = typename State::Vector;

/// The general row vector type
using RowVector = arma::Row<typename Vector::elem_type>;

/// The column vector type for unsigned values
using UVector = arma::Col<arma::uword>;

/// The column vector type for booleans (actually can't use bool underneath)
using BoolVector = arma::Col<arma::uword>;

/// The column vector type for storing IDs
using IDVector = arma::Col<IDType>;

/// The general matrix type
using Matrix = arma::Mat<double>;

/// A matrix with IDs
using IDMatrix = arma::umat;

/// A sparse matrix type
using SpMatrix = arma::SpMat<double>;


// .. Misc ....................................................................
/// A pair of a key and a reference
template<class RefT>
using KeyRefPair = std::pair<std::string, RefT&>;

/// A sequence of (key, reference) pairs
template<class RefT>
using RefPairs = std::vector<KeyRefPair<RefT>>;

/// A sequence of (key, const reference) pairs
template<class RefT>
using ConstRefPairs = std::vector<KeyRefPair<const RefT>>;



// -- Enums & Tags ------------------------------------------------------------

/// Whether to use a separated competition term
/** ... rather than including competition into the functional response
  */
struct with_separate_competition;

/// Whether environment modifications should be enabled
struct with_envmod;

/// Whether to use numerical integration to compute some quantity
/** For instance, when computing the feeding range overlap. If this tag is
  * not given, it denotes that a different (e.g. analytical) method is to be
  * used for computing the quantity.
  */
struct use_numerical_integration;

/// If given, will write data as float32 instead of float64
struct write_with_reduced_float_precision;

/// Whether to normalize input
struct normalize_input;


// -- Forward Declarations ----------------------------------------------------



// -- Misc --------------------------------------------------------------------


} // namespace Utopia::Models::mEEvo

#endif // UTOPIA_MODELS_MEEVO_TYPES_HH
