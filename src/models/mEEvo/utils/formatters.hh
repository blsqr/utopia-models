#ifndef UTOPIA_MODELS_MEEVO_UTILS_FORMATTERS_HH
#define UTOPIA_MODELS_MEEVO_UTILS_FORMATTERS_HH

#include <sstream>
#include <string_view>

#include <yaml-cpp/yaml.h>
#include <fmt/core.h>
#include <spdlog/spdlog.h>

#include "../types.hh"


namespace fmt {

/** Can put custom formatters for the fmt library here, e.g. for custom types.
  * See: https://fmt.dev/latest/api.html#formatting-user-defined-types
  * or other models for more information.
  */

} // namespace fmt

#endif // UTOPIA_MODELS_MEEVO_UTILS_FORMATTERS_HH
