#ifndef UTOPIA_MODELS_MEEVO_UTILS_STATS_HH
#define UTOPIA_MODELS_MEEVO_UTILS_STATS_HH

#include <armadillo>

#include <UtopiaUtils/utils/meta.hh>
#include "../types.hh"


namespace Utopia::Models::mEEvo {

/// Computes some basic statistics: mean, stddev, 9 quantiles
template<class Vec>
std::array<double, 11> compute_stats (const Vec& v) {
    std::array<double, 11> stats;

    stats[0] = arma::mean(v);
    stats[1] = arma::stddev(v);

    const Vector quantiles = arma::quantile(
        v, Vector({0, 0.1, 0.25, 0.4, 0.5, 0.6, 0.75, 0.9, 1.})
    );
    stats[2]  = quantiles(0);
    stats[3]  = quantiles(1);
    stats[4]  = quantiles(2);
    stats[5]  = quantiles(3);
    stats[6]  = quantiles(4);
    stats[7]  = quantiles(5);
    stats[8]  = quantiles(6);
    stats[9]  = quantiles(7);
    stats[10] = quantiles(8);

    return stats;
}

/// Computes the weighted mean of the given armadillo object
template<class T = Vector, class W = Vector>
double weighted_mean (const T& x, const W& w) {
    return arma::accu(x % w) / arma::accu(w);
}

/// Computes the weighted variance of the given armadillo object
/** This is equivalent to the functional diversity
  */
template<class T = Vector, class W = Vector>
double weighted_var (const T& x, const W& w) {
    const auto mu_w = weighted_mean(x, w);
    return arma::accu(w % arma::square(x - mu_w)) / arma::accu(w);
}


/// Computes the shannon index (in nats) of the given armadillo object
/** May pass the normalize_input tag to apply normalization to the input object
  * using the sum.
  *
  * The log_offset value is used to prevent zero-valued entries to bring the
  * whole shannon index to NaN. The justification behind this is that for
  * elements that go towards zero, the shannon index as a whole goes towards
  * zero as well due to the linear part of the term. Thus, the zero-valued
  * items will remain zero and there are no errors introduced.
  */
template<class... tags, class T = Vector>
double shannon_index (const T& x, const double log_offset = 1.e-32) {
    using Utopia::Utils::contains_type;
    using Utopia::Utils::size;
    using Utopia::Utils::is_empty;
    static_assert(
        is_empty<tags...>() or
        (size<tags...>() == 1 and contains_type<normalize_input, tags...>()),
        "Only allowed optional tag is normalize_input"
    );

    if constexpr (contains_type<normalize_input, tags...>()) {
        const double x_tot = arma::accu(x);
        return - arma::accu((x/x_tot) % arma::log(x/x_tot + log_offset));
    }
    else {
        return - arma::accu(x % arma::log(x + log_offset));
    }
}


}

#endif // UTOPIA_MODELS_MEEVO_UTILS_STATS_HH
