#ifndef UTOPIA_MODELS_MEEVO_UTILS_YAML_HH
#define UTOPIA_MODELS_MEEVO_UTILS_YAML_HH

#include <armadillo>
#include <yaml-cpp/yaml.h>

#include <UtopiaUtils/utils/iterators.hh>
#include "../types.hh"


namespace YAML {

// -- Armadillo Overloads -----------------------------------------------------

template<class T>
Emitter& operator << (Emitter& out, const arma::Col<T>& vec) {
    out << YAML::Flow << YAML::BeginSeq;
    for (const auto& e : vec) {
        out << e;
    }
    out << YAML::EndSeq;
    return out;
}


/// YAML conversion for column vector types
/** \NOTE Not working for yaml-cpp < 0.6.3.
  */
template<class T>
struct convert<arma::Col<T>> {
    static Node encode (const arma::Col<T>& rhs) {
        Node node;
        for (auto e : rhs) {
            node.push_back(e);
        }
        return node;
    }

    static bool decode (const Node& node, arma::Col<T>& rhs) {
        using eT = typename arma::Col<T>::elem_type;
        using Utopia::Utils::in_range;

        rhs.resize(node.size());
        // if (not node.IsSequence() or node.size() != rhs.size()) {
        //     return false;
        // }

        for (const auto i : in_range(node.size())) {
            rhs(i) = node[i].template as<eT>();
        }
        return true;
    }
};


} // namespace YAML


namespace Utopia::Models::mEEvo {

/// Set vector values from a scalar or sequence config node
template<class Vec, class Sel = arma::span>
void set_vector_values (Vec& v,
                        const Config& node,
                        const Sel&& sel = arma::span::all)
{
    if (node.IsScalar()) {
        v(sel).fill(node.as<typename Vec::elem_type>());
    }
    else {
        // Assume it's a sequence; will throw otherwise, which is fine.
        v(sel) = node.as<Vec>();
    }
}


} // namespace Utopia::Models::mEEvo

#endif // UTOPIA_MODELS_MEEVO_UTILS_YAML_HH
