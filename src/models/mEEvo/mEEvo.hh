#ifndef UTOPIA_MODELS_MEEVO_HH
#define UTOPIA_MODELS_MEEVO_HH

#include <random>
#include <functional>
#include <filesystem>
#include <type_traits>

#include <boost/hana/ext/std/tuple.hpp>
#include <boost/hana/for_each.hpp>

#include <utopia/core/model.hh>
#include <utopia/core/types.hh>

#include <UtopiaUtils/utils/action_manager.hh>
#include <UtopiaUtils/utils/odeint.hh>
#include <UtopiaUtils/data_io/data_writer.hh>

#include "utils.hh"
#include "types.hh"
#include "ode_system.hh"
#include "snapshots.hh"



namespace Utopia::Models::mEEvo {

// ++ Type definitions ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Type helper to define types used by the model
using ModelTypes = Utopia::ModelTypes<ModelRNG, Utopia::WriteMode::manual>;

/// The integrator class
using Utopia::Utils::ODEIntegrator;

/// The system generation toggle
using Utopia::Utils::SystemGeneration;


// ++ Model definition ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// The mEEvo Model
template<class... tags>
class mEEvo:
    public Model<mEEvo<tags...>, ModelTypes>
{
public:
    /// The type of the Model base class of this derived class
    using Base = Model<mEEvo, ModelTypes>;

    /// Export Base model's time type
    using Time = typename Base::Time;

    /// The action manager type
    using ActionManager = Utopia::Utils::ActionManager;

    /// The data writer type
    using DataWriter = Utopia::DataIO::DataWriter::DataWriter;

    /// The ODESystem to use
    using ODESys = ODESystem<typename Base::RNG, tags...>;

    /// An error stepper using vector space algebra
    using ErrorStepper =
        boost::numeric::odeint::runge_kutta_cash_karp54<
            State, double, State, double,
            boost::numeric::odeint::vector_space_algebra
        >;

    /// The integrator, using a custom error stepper and OTF system generation
    /** The SystemGeneration::On toggle is very important performance-wise,
      * as it reduces copying around of the ODESystem (which is quite a large
      * object.
      */
    using ODEInt = ODEIntegrator<ODESys, SystemGeneration::On, ErrorStepper>;

    /// Also need some snapshot candidates, which are added at a later point
    struct SnapshotCandidates {
        ODESys before_species_addition;
    };


    // .. Evaluated tags ......................................................

    /// Whether environment modifications are enabled
    static constexpr bool envmod_enabled =
        contains_type<with_envmod, tags...>();

    /// Whether to write floating point data with reduced precision
    /** This is *NOT* applied to storing of the final state!
      */
    static constexpr bool use_float32 =
        contains_type<write_with_reduced_float_precision, tags...>();

private:
    // -- Members -------------------------------------------------------------
    // .. Basic infrastructure ................................................

    /// A manager making dynamically performing config-configurable actions
    ActionManager _am;

    /// A manager for all output data
    DataWriter _dw;

    /// The ODE system integrator
    ODEInt _odeint;


    // .. Parameters ..........................................................

    /// Whether mutations should be enabled
    bool _enable_mutation;

    /// For deterministic mutation times
    /** If given, takes precedence over mutate_every and p_mutation.
      *
      * This is a list of reverse-ordered mutation times, where used-up values
      * are popped off the back of the container.
      */
    std::vector<Time> _mutate_at;

    /// If non-zero, will mutate non-stochastically in this interval
    /** If this is given, takes precedence over p_mutation
      */
    Time _mutate_every;

    /// The mutation probability per iteration time step
    double _p_mutation;

    /// The evaluation function for whether a mutation should occur
    std::function<bool()> _should_mutate;

    /// How fast the ODE-based dynamics should be run
    /** This is the argument given to `integrate_adaptive` and determines how
      * far to integrate within a single time step.
      *
      * \note The mutation probability is not adjusted by this speedup!
      */
    double _speedup;


    // .. Data-writing parameters and managers ................................

    /// How frequently to update the ODESystem property tracker
    unsigned _update_properties_every;

    /// Whether the final ODESystem state should be stored via the epilog
    bool _store_final_state;

    /// The ODESystem snapshot managers, conditioned on different observables
    Snapshots<ODESys> _snapshots;

    /// Candidates for snapshots
    SnapshotCandidates _snapshot_candidates;


    // .. Custom datasets and groups ..........................................

    /// Group for storing snapshots
    const std::shared_ptr<DataIO::HDFGroup> _snapshots_grp;

    /// Group for storing network data
    const std::shared_ptr<DataIO::HDFGroup> _network_grp;


    // .. Utilities ...........................................................

    /// Uniform real distribution for evaluating probabilities
    std::uniform_real_distribution<double> _prob_distr;


    // .. Flags, temporary variables, tracking variables ......................
    /// Whether a cleanup will be necessary at the beginning of the next step
    bool _need_cleanup;

    /// The number of (internal) integrator steps
    std::size_t _num_integrator_steps;

    /// Path to a stop file which will lead to monitor containing a stop flag
    std::string _stop_file_path;


    /// Mapping properties that are marked invalidated in certain situations
    /** These can be conveniently called at different times of the run.
      */
    struct mappings {
        using MappingNames = std::vector<std::string>;

        static inline const MappingNames on_addition = {
            "odesys/ids",
            "traits/ids",
            "times/ids_add",
            "properties/ids_se"
        };

        static inline const MappingNames on_removal = {
            "odesys/ids",
            "times/ids_rem",
            "properties/ids"
        };

        static inline const MappingNames all_buffered = {
            "traits/ids",
            "times/ids_add",
            "times/ids_rem",
            "properties/ids",
            "properties/ids_se"
        };

    };


public:
    // -- Model Setup ---------------------------------------------------------
    /// Construct the mEEvo model
    /** \param name     Name of this model instance
     *  \param parent   The parent model this model instance resides in
     */
    template<class ParentModel>
    mEEvo (const std::string& name, ParentModel& parent)
    :
        Base(name, parent)

    // Set up ActionManager and DataWriter
    ,   _am(this->_log, this->_cfg["action_manager"])
    ,   _dw(*this)

    // Set up integrator and (as part of it) the ODESystem
    ,   _odeint(
            // -- ODESystem
            [&]() -> ODESys {
                const auto& sys_cfg = get_as<Config>("system", this->_cfg);
                auto time_func = [this]() -> Time { return this->get_time(); };

                this->_log->info("Setting up ODESystem ...");
                if (not get_as<bool>("restore_state", this->_cfg, false)) {
                    return ODESys(sys_cfg, time_func, this->_log, this->_rng);
                }

                this->_log->debug("... from stored ODESystem state.");
                return ODESys::restore(
                    get_as<Config>("restore_options", this->_cfg),
                    sys_cfg,
                    time_func,
                    this->_log,
                    this->_rng
                );
            }(),
            // -- ODEIntegrator
            get_as<Config>("odeint", this->_cfg)
        )

    // Mutation-related parameters and members
    ,   _enable_mutation(get_as<bool>("enable_mutation", this->_cfg))
    ,   _mutate_at(get_mutate_at(this->_cfg))
    ,   _mutate_every(get_as<Time>("mutate_every", this->_cfg))
    ,   _p_mutation(get_as<double>("p_mutation", this->_cfg))
    ,   _should_mutate(build_mutation_trigger())

    // Other parameters
    ,   _speedup(get_as<double>("speedup", this->_cfg, 1.))

    // Storage- and tracking-related parameters
    ,   _update_properties_every(
            get_as<unsigned>("update_properties_every", this->_cfg)
        )
    ,   _store_final_state(
            get_as<bool>("store_final_state", this->_cfg)
        )
    ,   _snapshots(get_as<Config>("snapshots", this->_cfg))
    ,   _snapshot_candidates({system()})

    // Datasets and groups for custom saving operations
    ,   _snapshots_grp(this->_hdfgrp->open_group("snapshots"))
    ,   _network_grp(setup_network_group("network/snapshots"))

    // Misc: utilities, temporary variables, ...
    ,   _prob_distr(0., 1.)
    ,   _need_cleanup(false)
    ,   _num_integrator_steps(0)
    ,   _stop_file_path(get_as<std::string>("stop_file_path", this->_cfg, ""))

    {
        // Make sure that Utopia::Utils::SystemGeneration::On is used in the
        // wrapping Utopia::Utils::ODEIntegrator class.
        // Otherwise, the integration of ODESystem gets very slow, because it
        // has a number of members which scale badly with system size (all the
        // matrices) ...
        static_assert(ODEInt::using_system_generation());

        // .. Setup procedure .................................................
        // ActionManager
        setup_action_manager();
        _am.invoke_hook("setup");

        // DataWriter write properties
        register_write_properties();

        // Store some information about this run as attributes
        this->_hdfgrp->add_attribute("speedup", _speedup);
        this->_hdfgrp->add_attribute("with_envmod", envmod_enabled);

        {
        const auto& sys = system();

        // Counters
        this->_hdfgrp->add_attribute("n_resources", sys.n_resources());
        this->_hdfgrp->add_attribute("n_species_0", sys.n_species());

        // IDs
        const auto resource_ids =
            arma::conv_to<std::vector<IDType>>::from(sys.ids(sys.resources()));
        this->_hdfgrp->add_attribute("resource_ids", resource_ids);

        std::vector<IDType> non_species_ids = resource_ids;
        non_species_ids.insert(non_species_ids.begin(), 0);
        this->_hdfgrp->add_attribute("non_species_ids", non_species_ids);

        this->_hdfgrp->add_attribute("min_species_id",
                                     sys.ids(sys.species()).front());
        }

        // ... and inform about them
        this->_log->info("Selected configuration parameters:");
        this->_log->info(
            "  Ecological time / iteration:       {}",
            _speedup
        );
        if (not _enable_mutation) {
            this->_log->info(
                "  Mutations:                         disabled"
            );
        }
        else if (_mutate_at.size()) {
            using Utopia::Utils::format_vec;
            const auto ts_asc = std::vector<Time>(
                _mutate_at.rbegin(), _mutate_at.rend()
            );
            this->_log->info(
                "  Mutations at {:d} specific times:  {}",
                ts_asc.size(), format_vec(ts_asc)
            );
        }
        else if (_mutate_every) {
            this->_log->info(
                "  Deterministic mutations every:     {} steps",
                _mutate_every
            );
        }
        else {
            this->_log->info(
                "  Mutation proability:               {} / step",
                _p_mutation
            );
        }
        this->_log->info(
            "  Environment modification:          {}",
            envmod_enabled ? "yes" : "no"
        );
        this->_log->info(
            "  Update tracking properties every:  {} steps",
            _update_properties_every
        );
        this->_log->info(
            "  Snapshots:                         {}",
            _snapshots.enabled ? "enabled" : "disabled"
        );

        if (_snapshots.enabled) {
            // Ensure there is always at least one snapshot
            _snapshots.n_extinctions.add(0, this->system());
        }

        _am.invoke_hook("setup_done");
        this->_log->info("{} is all set up now :)\n", this->_name);
    }


private:
    // .. Setup functions .....................................................

    /// Build the mutation trigger functor depending on current parameters
    std::function<bool()> build_mutation_trigger () {
        return [this]() -> std::function<bool()> {
            if (not _enable_mutation) {
                this->_log->debug("Mutations disabled.");
                return [](){ return false; };
            }
            else if (_mutate_at.size()) {
                this->_log->debug("Using deterministic mutation mode ...");
                return [this](){
                    auto& ts = this->_mutate_at;
                    if (ts.empty()) {
                        return false;
                    }
                    else if (ts.back() == this->get_time()) {
                        ts.pop_back();
                        return true;
                    }
                    return false;
                };
            }
            else if (_mutate_every > 0) {
                this->_log->debug("Using regular mutation mode ...");
                return [this](){
                    return (this->get_time() > 0 and
                            this->get_time() % this->_mutate_every == 0);
                };
            }
            else {
                this->_log->debug("Using stochastic mutation mode ...");
                return [this](){
                    return _prob_distr(*this->_rng) < _p_mutation;
                };
            }
        }();
    }

    // .. Helper functions ....................................................

    /// Build the `mutate_at` container
    std::vector<Time> get_mutate_at (const Config& cfg,
                                     const std::string key = "mutate_at") const
    {
        if (cfg[key].IsNull()) {
            return {};
        }
        else if (cfg[key].IsScalar()) {
            return {get_as<Time>(key, cfg)};
        }
        // else: assumed sequence-like
        auto ts = get_as<std::vector<Time>>(key, cfg);
        std::sort(ts.begin(), ts.end(), std::greater<>());
        return ts;
    }

public:
    // -- Simulation Control --------------------------------------------------

    /// Called before model iteration starts
    /** This override merely adds the ``prolog`` hook, then invokes the parent
      * method.
      */
    void prolog () override {
        _am.invoke_hook("prolog");
        this->__prolog();
    }

    /// Iterate a single step
    void perform_step () {
        this->_am.invoke_hook("perform_step");

        // Might need some cleaning up, i.e.: removing inactive entities
        if (_need_cleanup) {
            this->_log->trace("  Performing cleanup ...");

            // Do cleanup operations on ODE system
            system().remove_extinct();
            _need_cleanup = false;

            // With cleanup done, the ID map is no longer up to date
            _dw.mark_mappings_invalidated(mappings::on_removal);
            this->_log->trace("  Cleanup finished.");
        }

        // .. Main algorithm . . . . . . . . . . . . . . . . . . . . . . . . .
        // -- 1: Potentially add a mutant species --
        if (_should_mutate()) {
            // May want to make snapshots.
            // This does the following:
            //  - Evaluates the previous candidate, potentially adding it as
            //    snapshots for different observables: n_extinctions, delta_FD…
            //  - Add a new candidate state (important to do this _before_
            //    adding the new species)
            make_snapshots_before_species_addition();

            // Now, actually add the new species
            this->_log->debug("  Adding a mutant species ...");
            system().add_mutant();
            _dw.mark_mappings_invalidated(mappings::on_addition);
            _am.invoke_hook("added_mutant");
        }

        // -- 2: Perform environmental dynamics (if enabled) --
        if constexpr (envmod_enabled) {
            this->_log->trace("  Performing environmental dynamics ...");
            system().perform_environmental_dynamics();
        }

        // -- 3: Integrate the population dynamics --
        this->_log->trace("  Integrating ODE system ...");
        _num_integrator_steps += _odeint.integrate_adaptive(_speedup);

        // -- 4: Check extinction --
        this->_log->trace("  Checking for extinctions ...");
        if (system().check_extinction()) {
            this->_log->trace("  There were extinctions; requiring cleanup.");
            _need_cleanup = true;
        }

        this->_am.invoke_hook("perform_step_finished");
        this->_log->debug("  Finished step.");
    }


    /// Supply monitoring values
    /** Keys provided by this monitor:
      *
      *    - `n_alive`: the number of currently alive species
      *    - `n_cuml`: how many species were _ever_ alive
      *    - `B_tot`: the sum of all biomass densities
      *    - `B_max`: the largest species' biomass
      *    - `TL_max`: the largest trophic level
      *    - `frac_viable`: fraction of viable species so far
      *    - `max_n_extinctions`: largest extinction cascade size (if tracked)
      *
      * If environment modifications are enabled, the following additional
      * values are monitored:
      *
      *    - `D_tot`: the energy diverted to the species depots
      *    - `E_tot`: the energy invested into the environment
      *    - `mean_mod_eff`: the mean modification efficiency
      *
      * Furthermore, this is hooked to the `monitor` ActionManager hook.
      */
    void monitor () {
        using std::filesystem::exists;

        _am.invoke_hook("monitor");

        system().update_property_tracking_variables();
        const auto& sys = system();

        auto& mon = this->_monitor;
        mon.set_entry("n_alive", sys.n_species());
        mon.set_entry("n_cuml", sys.get_species_count());
        mon.set_entry("B_tot", sys.total_biomass());
        mon.set_entry("B_max", sys.B(sys.species()).max());
        mon.set_entry("FD", system().functional_diversity());
        mon.set_entry("TL_max", sys.trophic_level.max());
        // mon.set_entry("frac_viable", sys.frac_viable());
        mon.set_entry("frac_herbivore",
            static_cast<float>(
                arma::accu(sys.trophic_level(sys.species()) < 2.5)
            ) / sys.n_species()
        );

        if (_snapshots.enabled and _snapshots.n_extinctions.size()) {
            mon.set_entry(
                "max_n_extinctions", _snapshots.n_extinctions.back_index()
            );
        }

        // Evaluate stop file path
        mon.set_entry(
            "stop", _stop_file_path.empty() ? false : exists(_stop_file_path)
        );

        // Only relevant with environment modifications enabled:
        if constexpr (not envmod_enabled) return;
        mon.set_entry("D_tot", arma::accu(sys.D()));
        mon.set_entry("E_tot", arma::accu(sys.E()));
        mon.set_entry("mean_mod_eff", arma::mean(sys.env.mod_efficiency));
    }


    /// Write data
    /** As the number of active populations and flows differs, the data writing
      * has to store the output in differently-sized datasets.
      * This work is carried out by the Utils::DataWriter, which handles the
      * tracking and creation of ID mappings and the associated properties.
      *
      * Furthermore, this invokes the ActionManager hooks `write_data` (every
      * iteration, due to WriteMode::manual) and `write_data_callback` (if
      * the `write_every` and `write_start` parameters determined that there
      * should be a write operation by the DataWriter).
      */
    void write_data () {
        _am.invoke_hook("write_data");

        // May want to update property tracking variables in ODEsystem
        if (_update_properties_every > 0 and
            this->_time % _update_properties_every == 0)
        {
            system().update_property_tracking_variables();
        }

        // Everything henceforth is written only if the conditions defined by
        // the write_start and write_every parameters are fulfilled.
        // TODO Shouldn't this conditional be evaluated by the TimeManager?!
        if (not (this->_time >= this->_write_start and
                 (this->_time - this->_write_start) % this->_write_every == 0))
        {
            return;
        }

        // Compute some properties that are only needed when writing data
        system().update_trophic_levels();

        // Invoke the DataWriter and let it do all the rest
        _dw.write(this->get_time());
        _am.invoke_hook("write_data_callback");
    }

    /// Called after model iteration ends
    /** This override does the following:
      *
      *     1) Invoke the epilog action hook
      *     2) Write some scalar observables to HDF5 base group attributes
      *     3) Update trackers such that their values are written as well
      *         - property tracker (including *all* species)
      *         - cascade size tracker
      *     4) Flush buffered properties (basically: the trackers)
      *     5) Store snapshots, if enabled
      *     6) Store the final system state, if enabled
      *     7) Invoke the base class epilog method
      *
      */
    void epilog () override {
        this->_am.invoke_hook("epilog");

        auto& sys = system();

        this->_log->info("Evaluating and storing scalar observables ...");
        this->_hdfgrp->add_attribute(
            "n_species_final", system().n_species()
        );
        this->_hdfgrp->add_attribute(
            "n_species_total", sys.get_species_count()
        );
        this->_hdfgrp->add_attribute(
            "frac_viable", sys.frac_viable()
        );
        this->_hdfgrp->add_attribute(
            "n_viable",
            sys.n_viable +
            arma::accu(sys.age(sys.species()) >= sys.viability_age)
        );

        this->_log->info("Updating system trackers ...");
        sys.update_property_tracking_variables();
        sys.property_tracker.track_final(sys);
        sys.species_effect_tracker.track_latest(sys);

        this->_log->info("Flushing all buffered properties ...");
        _dw.mark_mappings_invalidated(mappings::all_buffered);
        _dw.write_buffered_properties();

        if (_snapshots.enabled and _snapshots.store) {
            this->_log->info("Storing snapshots ...");
            _snapshots.store_all(this->_snapshots_grp);
        }

        if (_store_final_state) {
            this->_log->info("Storing final ODESystem snapshot ...");
            sys.store(this->_snapshots_grp->open_group("_final_state"));
        }

        this->_log->info("Model epilog finished.");
        this->__epilog();
    }


    // -- Getters and setters -------------------------------------------------
    // Add getters and setters here to interface with other model

    /// Shortcut to retrieve the ODESystem
    ODESys& system () {
        return this->_odeint.get_system();
    }

    /// Shortcut to retrieve the ODESystem
    const ODESys& system () const {
        return this->_odeint.get_system();
    }


private:
    // -- Misc. Helpers -------------------------------------------------------

    /// Make a snapshot for `n_extinction`, `delta_FD_min`, `delta_FD_max`
    /** This should be called before adding a new species to the system.
      */
    void make_snapshots_before_species_addition () {
        if (not _snapshots.is_enabled(this->_time)) {
            return;
        }

        auto& sys = system();

        // -- Snapshots of the *current* state, not using the candidate.
        // .. Trophic incoherence
        // Only call these if they are enabled (to avoid duplicate computation)
        if (_snapshots.min_trophic_incoherence.enabled() or
            _snapshots.max_trophic_incoherence.enabled())
        {
            const auto TL_incoh = sys.trophic_incoherence();

            {
            const auto was_added =
                _snapshots.min_trophic_incoherence.add(TL_incoh, sys);
            this->_log->debug(
                "  Made `min_trophic_incoherence` snapshot of current state. "
                "(value: {}, added? {})",
                TL_incoh, was_added ? "yes" : "no"
            );
            }
            {
            const auto was_added =
                _snapshots.max_trophic_incoherence.add(TL_incoh, sys);
            this->_log->debug(
                "  Made `max_trophic_incoherence` snapshot of current state. "
                "(value: {}, added? {})",
                TL_incoh, was_added ? "yes" : "no"
            );
            }
        }

        // -- Evaluate the snapshot for the previous candidate.
        // Use the currently set number of extinctions as an index, because
        // that's the relevant quantity here: number of extinctions *since* the
        // *last* species (the candidate) was added ...
        // However, additionally may want to determine whether the species
        // that was added at that point was actually viable and not make that
        // snapshot if it was *not* viable ...
        const auto& candidate = _snapshot_candidates.before_species_addition;
        const bool was_viable = sys.ids.back() != candidate.ids.back();

        if (not _snapshots.only_viable
            or (_snapshots.only_viable and was_viable))
        {
            // Make the snapshots
            {
            const auto was_added = _snapshots.n_extinctions.add(
                sys.n_extinctions, candidate
            );
            this->_log->debug(
                "  Made `n_extinctions` snapshot of candidate state. "
                "(value: {}, added? {})",
                sys.n_extinctions, was_added ? "yes" : "no"
            );
            }

            {
            const auto was_added = _snapshots.delta_FD_min.add(
                sys.delta_FD(), candidate
            );
            this->_log->debug(
                "  Made `delta_FD_min` snapshot of candidate state. "
                "(value: {:.3g}, added? {})",
                sys.delta_FD(), was_added ? "yes" : "no"
            );
            }

            {
            const auto was_added = _snapshots.delta_FD_max.add(
                sys.delta_FD(), candidate
            );
            this->_log->debug(
                "  Made `delta_FD_max` snapshot of candidate state. "
                "(value: {:.3g}, added? {})",
                sys.delta_FD(), was_added ? "yes" : "no"
            );
            }
        }
        else {
            this->_log->debug("  Not making a snapshot (was not viable).");
        }

        // Add this state as a snapshot candidate for the next cycle
        // Need a property update before that, such that the
        // `_last_property_update_time` is set, which is later used for storing
        // the time this candidate snapshot was made.
        sys.update_property_tracking_variables();
        _snapshot_candidates.before_species_addition = sys;
        this->_log->debug(
            "Added current state as new snapshot candidate."
        );
    }


    // -- Custom Data Writing -------------------------------------------------

    /// Creates a group for saving network data in; assumes readout by dantro
    auto setup_network_group (const std::string& name) const {
        auto grp = this->_hdfgrp->open_group(name);

        grp->add_attribute("content", "graph");
        grp->add_attribute("is_directed", true);
        grp->add_attribute("allows_parallel", false);
        grp->add_attribute("edge_container_is_transposed", true); // true: 2xN

        // Prevent the some dimensions from being squeezed when selecting
        // graph data via the GraphGroup.
        grp->add_attribute(
            "keep_dim",
            std::vector<std::string>{"species_id", "edge_idx"}
        );

        return grp;
    }

    /// Stores the given network and its properties in a graph group
    template<class... VertexProps, class... EdgeProps>
    void store_network (const std::shared_ptr<DataIO::HDFGroup>& grp,
                        const IDVector& vertices,
                        const IDMatrix& edges,
                        const std::tuple<VertexProps...>& vertex_props,
                        const std::tuple<EdgeProps...>& edge_props) const
    {
        const auto time = this->get_time();
        const auto dset_path = [time](const auto name){
            return fmt::format("{}/{:d}", name, time);
        };

        // Specify element writer
        const auto write_item = [](const auto v){
            using eT = decltype(v);
            if constexpr (use_float32 and std::is_floating_point<eT>()) {
                return static_cast<float>(v);
            }
            else {
                return v;
            }
        };

        this->_log->debug("Storing network ...");

        // .. Vertices and Edges . . . . . . . . . . . . . . . . . . . . . . .
        // Open datasets for vertices and edges and write that data
        const auto dset_v = grp->open_dataset(dset_path("_vertices"),
                                              {vertices.n_rows});
        dset_v->write(vertices.begin(), vertices.end(), write_item);

        const auto dset_e = grp->open_dataset(dset_path("_edges"),
                                              {2, edges.n_rows});
        dset_e->write(edges.col(0).begin(), edges.col(0).end(), write_item);
        dset_e->write(edges.col(1).begin(), edges.col(1).end(), write_item);

        // Add the dataset attributes
        dset_v->add_attribute("dim_name__0", "species_id");
        dset_v->add_attribute("coords_mode__species_id", "linked");
        dset_v->add_attribute("coords__species_id",
                              fmt::format("{}", time)); // links to itself

        dset_e->add_attribute("dim_name__0", "label");
        dset_e->add_attribute("dim_name__1", "edge_idx");
        dset_e->add_attribute("coords_mode__label", "values");
        dset_e->add_attribute("coords__label",
                              std::vector<std::string>{"source", "target"});
        dset_e->add_attribute("coords_mode__edge_idx", "trivial");

        // ... and the group attributes
        grp->open_group("_vertices")->add_attribute("contains", "time_series");
        grp->open_group("_edges")->add_attribute("contains", "time_series");

        this->_log->debug(
            "Successfully stored network with {} vertices and {} edges.",
            vertices.n_rows, edges.n_rows
        );


        // .. Vertex and edge properties . . . . . . . . . . . . . . . . . . .
        // Define an adaptor to write the vertex and edge properties
        const auto write_prop = [&](const auto& kv_pair,
                                    const std::string dim_name,
                                    const std::string coords_mode,
                                    const auto coords)
        {
            const auto name = kv_pair.first;
            const auto& data = kv_pair.second;

            // Don't write if there was no data
            if (not data.size()) {
                return;
            }

            // Open and write
            const auto dset = grp->open_dataset(dset_path(name),
                                                {data.size()});
            dset->write(data.begin(), data.end(), write_item);

            // Add attributes for dimension names and coordinate information
            dset->add_attribute("dim_name__0", dim_name);
            if (coords_mode.size()) {
                dset->add_attribute(fmt::format("coords_mode__{}", dim_name),
                                    coords_mode);
            }
            if (coords.size()) {
                dset->add_attribute(fmt::format("coords__{}", dim_name),
                                    coords);
            }

            // Add group attributes, denoting it as a time series
            grp->open_group(name)->add_attribute("contains", "time_series");
        };

        // Specialize the write adaptor function for vertices and edges ...
        using namespace std::placeholders;
        using namespace std::literals;

        const auto write_vertex_prop = std::bind(
            write_prop, _1,
            "species_id"s,
            "linked"s, fmt::format("../{}", dset_path("_vertices"))
        );
        const auto write_edge_prop = std::bind(
            write_prop, _1,
            "edge_idx"s,
            "trivial"s, ""s
        );

        // ... and apply them for each entry of the property tuples:
        boost::hana::for_each(vertex_props, write_vertex_prop);
        boost::hana::for_each(edge_props, write_edge_prop);

        this->_log->debug(
            "Successfully stored {} vertex properties and {} edge properties.",
            sizeof...(VertexProps), sizeof...(EdgeProps)
        );
    }



    // -- Manager setup -------------------------------------------------------

    /// Registers all conceivable output properties to the DataWriter
    void register_write_properties () {
        using namespace Utopia::DataIO::DataWriter;

        // .. Preparations ....................................................
        this->_log->info(
            "Preparing registration of data writing properties ..."
        );
        const auto& io_cfg = get_as<Config>("data_io", this->_cfg);

        // Inform about writing precision
        if constexpr (use_float32) {
            this->_log->info("  Using reduced float precision.");
        }
        else {
            this->_log->info("  Using full float precision.");
        }

        // Trophic Level distribution
        Vector TL_distr_bin_centers;

        {
        auto TL_cfg = get_as<Config>("TL_distr", io_cfg);
        auto _rg = get_as<std::array<double, 2>>("linspace", TL_cfg, {1., 6.});
        const auto _n_bins = get_as<uint>("n_bins", TL_cfg, 6);

        TL_distr_bin_centers = arma::linspace(_rg[0], _rg[1], _n_bins);
        TL_distr_bin_centers += get_as<double>("offset", TL_cfg, 0.);

        // auto _bcs = fmt::join(TL_distr_bin_centers, ", ");
        // this->_log->debug("Trophic Level distribution bin centers:\n  {}",
        //                   std::forward<decltype(_bcs)>(_bcs));
        // FIXME
        }

        // Biomass distribution
        // ... get bin centers and arguments already at this point ...
        Vector B_per_TL_bin_centers;
        unsigned B_per_TL_bins_per_level, B_per_TL_max_level;
        {
        auto B_per_TL_cfg = get_as<Config>("B_distr", io_cfg);
        B_per_TL_bins_per_level = get_as<unsigned>("bins_per_level",
                                                   B_per_TL_cfg, 4);
        B_per_TL_max_level = get_as<unsigned>("max_level", B_per_TL_cfg, 6);

        auto [c, b] = system().biomass_per_trophic_level(
            B_per_TL_bins_per_level, B_per_TL_max_level
        );
        B_per_TL_bin_centers = c;
        }


        // .. Re-usable functors ..............................................
        // .. Adaptor wrappers . . . . . . . . . . . . . . . . . . . . . . . .

        // A general-purpose element write adaptor that may perform type casts
        // on floating point types (if configured above)
        const auto write_item = [](const auto v){
            using eT = decltype(v);
            if constexpr (use_float32 and std::is_floating_point<eT>()) {
                return static_cast<float>(v);
            }
            else {
                return v;
            }
        };

        // Writing a scalar
        const auto from_scalar =
            [write_item](const auto& get_scalar) {
                return [get_scalar, write_item](auto& dset){
                    dset->write(write_item(get_scalar()));
                };
            };

        // Working on the copy of some arbitrary container
        const auto from_container =
            [write_item](const auto& get_cont) {
                return [get_cont, write_item](auto& dset){
                    const auto cont = get_cont();
                    dset->write(cont.begin(), cont.end(), write_item);
                };
            };

        // Working on a reference from an ODESystem container; no copy
        const auto from_odesys_container =
            [this, write_item](const auto& get_cont) {
                return [this, get_cont, write_item](auto& dset){
                    const auto& cont = get_cont(this->system());  // reference!
                    dset->write(cont.begin(), cont.end(), write_item);
                };
            };


        // .. Max extent calculation . . . . . . . . . . . . . . . . . . . . .
        // 1D
        const auto max_extent_1d_write_ops =
            [](const auto num_write_ops) -> std::vector<hsize_t> {
                return {num_write_ops};
            };
        const auto max_extent_1d_system_B =
            [this](const auto) -> std::vector<hsize_t> {
                return {this->system().size_B()};
            };

        // 2D
        const auto max_extent_2d_system_B =
            [this](const auto num_write_ops) -> std::vector<hsize_t> {
                return {num_write_ops, this->system().size_B()};
            };
        const auto max_extent_2d_system_E =
            [this](const auto num_write_ops) -> std::vector<hsize_t> {
                return {num_write_ops, this->system().size_E()};
            };
        const auto max_extent_2d_TL_distr =
            [N=TL_distr_bin_centers.size()](const auto num_write_ops)
                -> std::vector<hsize_t>
            {
                return {num_write_ops, N};
            };
        const auto max_extent_2d_B_per_TL_distr =
            [N=B_per_TL_bin_centers.size()](const auto num_write_ops)
                -> std::vector<hsize_t>
            {
                return {num_write_ops, N};
            };
        const auto max_extent_2d_stats =
            [](const auto num_write_ops) -> std::vector<hsize_t> {
                return {num_write_ops, 11};
                // 11 columns: mean, stddev, and 9 quantiles:
                // 0, 0.1, 0.25, 0.4, 0.5, 0.6, 0.75, 0.9, 1.
            };



        // .. Attribute writers . . . . . . . . . . . . . . . . . . . . . . . .

        const auto write_attrs_2D_idx_trivial_coords =
            [](const auto& dset){
                dset->add_attribute("dim_name__1", "idx");
                dset->add_attribute("coords_mode__idx", "trivial");
            };

        const auto write_attrs_stats =
            [](const auto& dset){
                dset->add_attribute("dim_name__1", "label");
                dset->add_attribute("coords_mode__label", "values");
                dset->add_attribute("coords__label",
                    std::array<std::string, 11>{
                        "mean", "stddev",
                        "min", "q10", "q25", "q40", "median",
                        "q60", "q75", "q90", "max"
                    }
                );
            };

        const auto write_attrs_TL_distr =
            [TL_distr_bin_centers](const auto& dset){
                dset->add_attribute("dim_name__1", "bin_centers");
                dset->add_attribute("coords_mode__bin_centers", "values");
                dset->add_attribute("coords__bin_centers",
                                    TL_distr_bin_centers);
            };

        const auto write_attrs_B_per_TL_distr =
            [B_per_TL_bin_centers](const auto& dset){
                dset->add_attribute("dim_name__1", "bin_centers");
                dset->add_attribute("coords_mode__bin_centers", "values");
                dset->add_attribute("coords__bin_centers",
                                    B_per_TL_bin_centers);
            };

        // ....................................................................
        // .. PROPERTY REGISTRATION ...........................................
        // ....................................................................

        this->_log->info("Registering properties with DataWriter ...");

        // .. ODE SYSTEM ......................................................
        // Register an ID mapping
        _dw.register_property("odesys/ids",
            // The mode of this property
            PropMode::mapping,
            // The write function, here generated by a helper function
            from_odesys_container([](const auto& sys){
                return sys.ids;
            }),
            // The (required) max extent function, 1D: {size of container}
            max_extent_1d_system_B
            // Optional: lambda to set additional attributes
            // Optional: lambda to calculate chunk sizes; not given -> auto
        );

        _dw.register_property("odesys/biomass_density",
            PropMode::mapped, "odesys/ids",
            from_odesys_container([](const auto& sys){
                return sys.B();
            }),
            max_extent_2d_system_B
        );

        _dw.register_property("odesys/trophic_level",
            PropMode::mapped, "odesys/ids",
            from_odesys_container([](const auto& sys){
                return sys.trophic_level;
            }),
            max_extent_2d_system_B
        );

        _dw.register_property("odesys/total_response",
            PropMode::mapped, "odesys/ids",
            from_odesys_container([](const auto& sys) -> Vector {
                return sys.total_response();
            }),
            max_extent_2d_system_B
        );

        _dw.register_property("odesys/total_attack_rate",
            PropMode::mapped, "odesys/ids",
            from_odesys_container([](const auto& sys) -> Vector {
                return sys.total_attack_rate();
            }),
            max_extent_2d_system_B
        );

        _dw.register_property("odesys/perceived_competition",
            PropMode::mapped, "odesys/ids",
            from_odesys_container([](const auto& sys) -> Vector {
                return sys.perceived_competition();
            }),
            max_extent_2d_system_B
        );

        _dw.register_property("odesys/respiration",
            PropMode::mapped, "odesys/ids",
            from_odesys_container([](const auto& sys) -> Vector {
                return sys.x % sys.B();
            }),
            max_extent_2d_system_B
        );


        if constexpr (envmod_enabled) {

        _dw.register_property("odesys/total_modified_response",
            PropMode::mapped, "odesys/ids",
            from_odesys_container([](const auto& sys) -> Vector {
                return sys.total_modified_response();
            }),
            max_extent_2d_system_B
        );

        _dw.register_property("odesys/depot",
            PropMode::mapped, "odesys/ids",
            from_odesys_container([](const auto& sys){
                return sys.D();
            }),
            max_extent_2d_system_B
        );

        // .. Standalone (because not species-dependent) . . . . . . . . . . .

        _dw.register_property("odesys/environment",
            PropMode::standalone,  // don't need any IDs ...
            from_odesys_container([](const auto& sys){
                return sys.E();
            }),
            max_extent_2d_system_E,
            write_attrs_2D_idx_trivial_coords
        );

        _dw.register_property("odesys/modification_efficiency",
            PropMode::standalone,
            from_odesys_container([](const auto& sys){
                return sys.env.mod_efficiency;
            }),
            max_extent_2d_system_E,
            write_attrs_2D_idx_trivial_coords
        );
        }


        // .. TRAITS ..........................................................
        // These are buffered properties that write from the traits tracker
        // by flushing the therein aggregated values.

        auto& traits_tracker = system().traits_tracker;

        _dw.register_buffer_mapping("traits/ids", traits_tracker.ids);

        _dw.register_buffered_property("traits/body_mass",
            traits_tracker.m, "traits/ids"
        );

        _dw.register_buffered_property("traits/feeding_center",
            traits_tracker.f, "traits/ids"
        );

        _dw.register_buffered_property("traits/niche_width",
            traits_tracker.s, "traits/ids"
        );

        _dw.register_buffered_property("traits/parent_ids",
            traits_tracker.parent_ids, "traits/ids"
        );

        _dw.register_buffered_property("traits/frac_diverted",
            traits_tracker.frac_diverted, "traits/ids"
        );


        // .. PROPERTY TRACKERS ...............................................

        // Written upon species addition (via traits tracker!!)
        _dw.register_buffered_property("properties/initial_trophic_level",
            traits_tracker.initial_TL, "traits/ids"
        );


        // Written upon species addition (keyed on a possibly extinct species!)
        auto& se_tracker = system().species_effect_tracker;
        _dw.register_buffer_mapping("properties/ids_se", se_tracker.ids);

        _dw.register_buffered_property("properties/n_extinctions",
            se_tracker.n_extinctions, "properties/ids_se"
        );
        _dw.register_buffered_property("properties/delta_FD",
            se_tracker.delta_FD, "properties/ids_se"
        );
        _dw.register_buffered_property("properties/delta_trophic_incoherence",
            se_tracker.delta_trophic_incoherence, "properties/ids_se"
        );


        // Written upon species removal (via property tracker)
        auto& prop_tracker = system().property_tracker;
        _dw.register_buffer_mapping("properties/ids", prop_tracker.ids);

        _dw.register_buffered_property("properties/age",
            prop_tracker.age, "properties/ids"
        );
        _dw.register_buffered_property("properties/lifetime",
            prop_tracker.lifetime, "properties/ids"
        );
        _dw.register_buffered_property("properties/was_viable",
            prop_tracker.was_viable, "properties/ids"
        );
        _dw.register_buffered_property("properties/mean_trophic_level",
            prop_tracker.mean_trophic_level, "properties/ids"
        );
        _dw.register_buffered_property("properties/extinction_delay",
            prop_tracker.extinction_delay, "properties/ids"
        );
        _dw.register_buffered_property("properties/TL_diff",
            prop_tracker.TL_diff, "properties/ids"
        );


        // .. LIFETIMES .......................................................

        // Addition times
        auto& add_times = system().addition_times_tracker;
        _dw.register_buffer_mapping("times/ids_add", add_times.ids);

        _dw.register_buffered_property("times/additions",
                                       add_times.times, "times/ids_add");

        // Removal times
        auto& rem_times = system().removal_times_tracker;
        _dw.register_buffer_mapping("times/ids_rem", rem_times.ids);

        _dw.register_buffered_property("times/removals",
                                       rem_times.times, "times/ids_rem");


        // .. GLOBAL OBSERVABLES ..............................................

        _dw.register_property("global/n_species",
            PropMode::standalone,
            from_scalar([this](){
                return this->system().n_species();
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("global/n_viable",
            PropMode::standalone,
            from_scalar([this](){
                return this->system().n_viable;
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("global/frac_viable",
            PropMode::standalone,
            from_scalar([this](){
                return this->system().frac_viable();
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("global/biomass",
            PropMode::standalone,
            from_scalar([this](){
                return this->system().total_biomass();
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("global/biomass_species_only",
            PropMode::standalone,
            from_scalar([this](){
                return this->system().total_biomass(this->system().species());
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("global/biomass_flow",
            PropMode::standalone,
            from_scalar([this](){
                return this->system().biomass_flow();
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("global/biomass_flow_from_resource",
            PropMode::standalone,
            from_scalar([this](){
                const auto& sys = this->system();
                if (sys.n_resources() == 1) {
                    return arma::accu(sys.latest_g().col(0) % sys.B());
                }
                else {
                    return arma::accu(sys.biomass_flow_matrix()(
                        arma::span::all, sys.resources()
                    ));
                }
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("global/biomass_flow_intra_guild",
            PropMode::standalone,
            from_scalar([this](){
                const auto& sys = this->system();
                return arma::accu(
                    sys.biomass_flow_matrix()(sys.species(), sys.species())
                );
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("global/respiration",
            PropMode::standalone,
            from_scalar([this](){
                const auto& sys = this->system();
                return arma::accu(sys.x % sys.B());
            }),
            max_extent_1d_write_ops
        );

        // .. Environment-related . . . . . . . . . . . . . . . . . . . . . . .
        // .. still under "global" because these are related to global ones
        if constexpr (envmod_enabled) {

        _dw.register_property("global/biomass_flow_unmodified",
            PropMode::standalone,
            from_scalar([this](){
                return this->system().biomass_flow_unmodified();
            }),
            max_extent_1d_write_ops
        );

        }


        // .. DIVERSITY MEASURES ..............................................
        // .. still under "global" because it does not make it easier to put
        //    them into a separate category

        _dw.register_property("global/functional_diversity",
            PropMode::standalone,
            from_scalar([this](){
                return this->system().functional_diversity();
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("global/biomass_flow_shannon_index",
            PropMode::standalone,
            from_scalar([this](){
                return this->system().biomass_flow_shannon_index();
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("global/FD_log_m",
            PropMode::standalone,
            from_scalar([this](){
                const auto& sys = this->system();
                return weighted_var(
                    arma::log10(sys.m(sys.species())), sys.B(sys.species())
                );
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("global/FD_log_f",
            PropMode::standalone,
            from_scalar([this](){
                const auto& sys = this->system();
                return weighted_var(
                    arma::log10(sys.f(sys.species())), sys.B(sys.species())
                );
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("global/FD_s",
            PropMode::standalone,
            from_scalar([this](){
                const auto& sys = this->system();
                return weighted_var(
                    sys.s(sys.species()), sys.B(sys.species())
                );
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("global/FD_mf_ratio",
            PropMode::standalone,
            from_scalar([this](){
                const auto& sys = this->system();
                return weighted_var(
                    sys.m(sys.species()) / sys.f(sys.species()),
                    sys.B(sys.species())
                );
            }),
            max_extent_1d_write_ops
        );


        // .. NETWORK and NETWORK MEASURES ....................................
        // NOTE Full networks (including structure) are stored to a separate
        //      subgroup, `network/snapshots`, see `_network_grp` member

        // TODO Consider storing all measures in a labelled 2D dataset ...

        _dw.register_property("network/num_edges",
            PropMode::standalone,
            from_scalar([this](){
                auto& sys = this->system();
                return sys.num_edges();
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("network/connectance",
            PropMode::standalone,
            from_scalar([this](){
                auto& sys = this->system();
                return sys.connectance();
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("network/trophic_incoherence",
            PropMode::standalone,
            from_scalar([this](){
                auto& sys = this->system();
                return sys.trophic_incoherence();
            }),
            max_extent_1d_write_ops
        );


        // .. ENVIRONMENT .....................................................

        if constexpr (envmod_enabled) {
        _dw.register_property("envmod/biomass_depot",
            PropMode::standalone,
            from_scalar([this](){
                return arma::accu(this->system().D());
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("envmod/environment_energy",
            PropMode::standalone,
            from_scalar([this](){
                return arma::accu(this->system().E());
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("envmod/mean_modification_efficiency",
            PropMode::standalone,
            from_scalar([this](){
                return arma::mean(this->system().env.mod_efficiency);
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("envmod/mean_signature_density",
            PropMode::standalone,
            from_scalar([this](){
                return this->system().env.mean_signature_density();
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("envmod/signature_density",
            PropMode::standalone,
            from_container([this](){
                return this->system().env.aggregate_from_signatures(
                    [](auto, const auto& sig){
                        return static_cast<double>(sig.n_nonzero) / sig.n_elem;
                    }
                );
            }),
            max_extent_2d_system_E,
            write_attrs_2D_idx_trivial_coords
        );

        }


        // .. DISTRIBUTIONS ...................................................

        _dw.register_property("distr/trophic_level",
            PropMode::standalone,
            from_container([TL_distr_bin_centers, this]() -> UVector {
                const auto& sys = this->system();
                return arma::hist(sys.trophic_level, TL_distr_bin_centers);
            }),
            // Custom max_extent and attributes writers
            max_extent_2d_TL_distr,
            write_attrs_TL_distr
        );

        _dw.register_property("distr/biomass_per_trophic_level",
            PropMode::standalone,
            from_container([
                bins_per_level=B_per_TL_bins_per_level,
                max_level=B_per_TL_max_level,
                this
            ]() -> Vector {
                auto [bin_centers, biomass_per_TL] =
                    this->system().biomass_per_trophic_level(
                        bins_per_level, max_level
                    );
                return biomass_per_TL;
            }),
            // Custom max_extent and attributes writers
            max_extent_2d_B_per_TL_distr,
            write_attrs_B_per_TL_distr
        );

        // TODO distribution of effective modification matrix values


        // .. STATISTICS ......................................................

        _dw.register_property("stats/trophic_level",
            PropMode::standalone,
            from_container([this](){
                // NOTE TL update is ensured by call within write_data
                const auto& TL = system().trophic_level(system().species());
                return compute_stats(TL);
            }),
            max_extent_2d_stats,
            write_attrs_stats
        );

        _dw.register_property("stats/biomass_density",
            PropMode::standalone,
            from_container([this](){
                const auto& B = system().B()(system().species());
                return compute_stats(B);
            }),
            max_extent_2d_stats,
            write_attrs_stats
        );


        // .. BENCHMARKING ....................................................

        _dw.register_property("benchmark/num_integrator_steps",
            PropMode::standalone,
            from_scalar([this](){
                return this->_num_integrator_steps;
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("benchmark/wall_time_elapsed",
            PropMode::standalone,
            from_scalar([this](){
                const auto& timer =
                    this->_monitor.get_monitor_manager()->get_timer();
                return timer->get_time_elapsed_seconds();
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("benchmark/TL_error",
            PropMode::standalone,
            from_scalar([this](){
                return this->system().TL_error;
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("benchmark/biomass_correction",
            PropMode::standalone,
            from_scalar([this](){
                return this->system().biomass_correction;
            }),
            max_extent_1d_write_ops
        );

        _dw.register_property("benchmark/g_density",
            PropMode::standalone,
            from_scalar([this](){
                return this->system().g_density();
            }),
            max_extent_1d_write_ops
        );


        // .. Finished. .......................................................
        this->_log->info("Property registration finished.");
    }

    /// Registers triggers and actions with the ActionManager
    // TODO Make "network snapshots" via snapshot framework!
    void setup_action_manager () {
        using Time = typename Base::Time;

        // .. Triggers ........................................................
        _am.register_trigger("time",
            [this](const auto& cfg){
                return get_as<Time>("time", cfg) == this->get_time();
            }
        );
        _am.register_trigger("times",
            // Very naive implementation, slow if many times are given
            [this](const auto& cfg){
                using Utopia::Utils::contains;
                using Times = std::vector<Time>;

                return contains(get_as<Times>("times", cfg), this->get_time());
            }
        );
        _am.register_trigger("time_multiples",
            [this](const auto& cfg){
                return (this->get_time() % get_as<Time>("n", cfg) == 0);
            }
        );
        _am.register_trigger("time_slice",
            [this](const auto& cfg){
                const auto start = get_as<Time>("start", cfg, 0);
                const auto step = get_as<Time>("step", cfg);
                const auto time = this->get_time();
                return (
                    time >= start and
                    (time - start) % step == 0 and
                    time < get_as<Time>("stop", cfg, this->get_time_max() + 1)
                );
            }
        );
        _am.register_trigger("write_every_multiples",
            [this](const auto& cfg){
                const auto time = this->get_time();
                return (
                    time >= get_as<Time>("start", cfg, 0) and
                    time % (this->_write_every * get_as<Time>("n", cfg)) == 0
                );
            }
        );


        // .. Actions .........................................................
        _am.register_action("set_mutation_params",
            [this](const auto& cfg){
                // Read new mutation parameters from configuration, only
                // setting them if they were given
                if (cfg["enable_mutation"]) {
                    this->_enable_mutation =
                        get_as<bool>("enable_mutation", cfg);
                }

                if (cfg["mutate_at"]) {
                    this->_mutate_at = get_mutate_at(cfg);
                }

                if (cfg["mutate_every"]) {
                    this->_mutate_every = get_as<Time>("mutate_every", cfg);
                }

                if (cfg["p_mutation"]) {
                    this->_p_mutation = get_as<Time>("p_mutation", cfg);
                }

                // With parameters updated, build the new trigger function now
                this->_should_mutate = this->build_mutation_trigger();
            }
        );

        // .. Parameter changes . . . . . . . . . . . . . . . . . . . . . . . .
        _am.register_action("set_value",
            [this](const auto& cfg){
                // Which attribute to change
                const auto attr = get_as<std::string>("of_attr", cfg);

                // Which indices to change
                const auto idcs = this->system().find(
                    get_as<Config>("find", cfg)
                );

                // What value to change these to
                const auto new_val = get_as<double>("set_to", cfg);

                // Apply it to all the given indices
                Vector& ref = this->system().get(attr);
                ref.elem(idcs).fill(new_val);
            }
        );

        _am.register_action("change_value",
            [this](const auto& cfg){
                // Which attribute to change
                const auto attr = get_as<std::string>("of_attr", cfg);

                // Which indices to change
                const auto idcs = this->system().find(
                    get_as<Config>("find", cfg)
                );

                // How to change them
                const auto factor = get_as<double>("factor", cfg, 1.);
                const auto offset = get_as<double>("offset", cfg, 0.);

                // Apply it to all the given indices
                Vector& ref = this->system().get(attr);
                ref.elem(idcs) *= factor;
                ref.elem(idcs) += offset;
            }
        );

        // .. Manipulations . . . . . . . . . . . . . . . . . . . . . . . . . .
        _am.register_action("add_species",
            [this](const auto& cfg){
                // NOTE Need to perform the same procedures here as during the
                //      addition of mutants within perform_step

                this->make_snapshots_before_species_addition();

                this->_log->info("  Adding a species from config ...");
                this->system().add_species(cfg);

                this->_dw.mark_mappings_invalidated(mappings::on_addition);
                this->_am.invoke_hook("added_mutant");
            }
        );

        // .. Tracking . . . . . . . . . . . . . . . . . . . . . . . . . . . .
        _am.register_action("make_network_snapshot",
            [this](const auto& cfg){
                // Get the network ...
                const auto [vertices, edges, vprops, eprops] =
                    this->system().extract_network(cfg);

                // ... and store it
                this->store_network(
                    this->_network_grp, vertices, edges, vprops, eprops
                );
            }
        );

        _am.register_action("make_odesys_snapshot",
            [this](const auto&){
                this->_snapshots.time.add(this->system());
            }
        );

        _am.register_action("update_trophic_levels",
            [this](const auto& cfg){
                if (not cfg.size()) {
                    this->system().update_trophic_levels();
                }
                else {
                    this->system().update_trophic_levels(
                        get_as<double>("max_abs_diff", cfg),
                        get_as<unsigned>("max_n", cfg)
                    );
                }
            }
        );

        // .. Information . . . . . . . . . . . . . . . . . . . . . . . . . . .
        _am.register_action("print_odesys_info",
            [this](const auto&){
                this->system().print_info();
            }
        );



        // .. Procedures and Hooks ............................................
        const auto& setup_cfg = this->_cfg["setup"];

        // Register procedures
        if (setup_cfg["procedures"]) {
            for (const auto& kv : setup_cfg["procedures"]) {
                _am.register_procedure(kv.first.template as<std::string>(),
                                       kv.second);
            }
        }

        // From a bunch of scenarios, use a specific one for the setup hook.
        // This allows to easily switch between them.
        // If `use_scenario` is not given or Null, still register the hook, but
        // specify an empty procedure sequence.
        auto procedure_seq = ActionManager::ProcedureSequence {};

        if (setup_cfg["use_scenario"] and
            not setup_cfg["use_scenario"].IsNull())
        {
            const auto scenario = get_as<std::string>("use_scenario",
                                                      setup_cfg);
            this->_log->info("Using scenario '{}' for setup.", scenario);

            const auto& scenarios = get_as<Config>("scenarios", setup_cfg);
            procedure_seq =
                get_as<ActionManager::ProcedureSequence>(scenario, scenarios);
        }
        else {
            this->_log->info("No setup scenario specified.");
        }

        _am.register_hook("setup", procedure_seq);
    }

};


} // namespace Utopia::Models::mEEvo

#endif // UTOPIA_MODELS_MEEVO_HH
