#ifndef UTOPIA_MODELS_MEEVO_UTILS_HH
#define UTOPIA_MODELS_MEEVO_UTILS_HH

#include <random>

#include <armadillo>

// Include selected base utilities
#include <UtopiaUtils/utils/cfg.hh>
#include <UtopiaUtils/utils/formatters.hh>
#include <UtopiaUtils/utils/iterators.hh>
#include <UtopiaUtils/utils/rng.hh>
#include <UtopiaUtils/utils/tags.hh>
#include <UtopiaUtils/utils/types.hh>
#include <UtopiaUtils/utils/math.hh>
#include <UtopiaUtils/utils/meta.hh>

// Include _all_ mEEvo model-specific utilities
#include "utils/formatters.hh"
#include "utils/network.hh"
#include "utils/stats.hh"
#include "utils/yaml.hh"


namespace Utopia::Models::mEEvo {

// -- Parameter Structs -------------------------------------------------------

enum class cost_mode {
    polynomial,
    exponential
};

cost_mode cost_mode_from_str (const std::string& s) {
    if      (s == "polynomial")     return cost_mode::polynomial;
    else if (s == "exponential")    return cost_mode::exponential;

    throw std::invalid_argument(fmt::format(
        "Invalid cost mode '{}'! Choose from: zero, polynomial, exponential",
        s
    ));
}


struct CostParams {
    struct NicheWidthParams {
        cost_mode mode = cost_mode::polynomial;
        double factor = 0.;
        double exponent = 0.;
        double center = 0.;
        double offset = 0.;
    };

    // .. Parameters ..........................................................

    bool enabled = false;

    NicheWidthParams niche_width;


    // .. Constructors ........................................................

    CostParams (const Config& cfg = {})
    :
        niche_width()
    {
        enabled = get_as<bool>("enabled", cfg, false);

        if (cfg["niche_width"]) {
            auto nw_cfg = cfg["niche_width"];

            niche_width.mode = cost_mode_from_str(
                get_as<std::string>("mode", nw_cfg)
            );
            niche_width.factor = get_as<double>("factor", nw_cfg, 1.);
            niche_width.exponent = get_as<double>("exponent", nw_cfg);
            niche_width.center = get_as<double>("center", nw_cfg);
            niche_width.offset = get_as<double>("offset", nw_cfg, 0.);
        }
    }
};


// -- Small functions ---------------------------------------------------------
// TODO Consider migrating to appropriate place

/// Calls a value-drawing function until a value within the bounds is found
/** \note This considers boundary values as *exclusive*!
  *
  * \param  draw_func       The functor to generate the drawing value; called
  *                         without any arguments.
  * \param  lower           Lower bound, value needs to be strictly greater
  * \param  upper           Upper bound, value needs to be strictly smaller
  * \param  max_redraws     After how man redraws to throw an exception
  */
template<class DrawFunc, class B1, class B2>
auto draw_within_bounds (DrawFunc&& draw_func,
                         const B1 lower, const B2 upper,
                         std::size_t max_redraws = 100)
{
    auto val = draw_func();
    auto n = 0u;
    while (val < lower or val > upper) {
        if (n > max_redraws) {
            throw std::runtime_error(fmt::format(
                "Failed to draw a value within the bounds ({}, {}), even "
                "after {:d} redraws! Last drawn value was:  {}",
                lower, upper, n, val
            ));
        }

        val = draw_func();
        n++;
    }
    return val;
}

/// Returns a random index from a weighted uniform distribution
/** \note Requires armadillo objects
  * \todo generalise to std containers?
  */
template<class W, class I, class RNG>
arma::uword draw_weighted (const W& weights,
                           const I& indices,
                           const RNG& rng,
                           const std::string& context = "not given")
{
    using Utopia::Utils::in_range;

    if (weights.min() < 0. or weights.has_nan()) {
        weights.print("weights");
        throw std::invalid_argument(fmt::format(
            "Got negative or NaN value for one or more weights (see above)! "
            "Context: {}", context
        ));
    }

    double thrs = std::uniform_real_distribution<double>(
        0., arma::accu(weights)
    )(*rng);

    for (const auto i : in_range(indices.size())) {
        thrs -= weights(i);
        if (thrs <= 0.) {
            return indices(i);
        }
    }

    // Should not have reached this point
    weights.print("weights");
    indices.print("indices");

    throw std::runtime_error(fmt::format(
        "Failed drawing a weighted index (context: {})! After iterating "
        "through all weights and indices (see above), still have a positive "
        "weight value left: {}", context, thrs
    ));
}

/// Mixes two values together with weights `weight_a` and `1-weight_a`
double mix_values (const double a, const double b, const double weight_a) {
    if (weight_a < 0. or weight_a > 1.) {
        throw std::invalid_argument(fmt::format(
            "Mixing weight needs to be in [0, 1], was {}!", weight_a
        ));
    }
    return (a * weight_a) + (b * (1.-weight_a));
}

/// Mixes values after log-transforming them; then transforms them back
double mix_values_log (const double a, const double b, const double weight_a) {
    return std::exp(mix_values(std::log(a), std::log(b), weight_a));
}




// -- Various aliases and namespace imports -----------------------------------
using Utopia::Utils::contains_type;

/// Floating point precision to use in this model
using Utopia::Utils::PREC;

/// NaN
const inline double NaN = std::numeric_limits<double>::quiet_NaN();


}

#endif // UTOPIA_MODELS_MEEVO_UTILS_HH
