#ifndef UTOPIA_MODELS_MEEVO_ODE_SYSTEM_HH
#define UTOPIA_MODELS_MEEVO_ODE_SYSTEM_HH

#include <algorithm>
#include <cmath>
#include <random>
#include <limits>
#include <functional>
#include <filesystem>

#include <armadillo>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <boost/math/quadrature/gauss.hpp>

#include <utopia/core/logging.hh>

#include <UtopiaUtils/utils/armadillo.hh>
#include <UtopiaUtils/utils/contains.hh>
#include <UtopiaUtils/utils/iterators.hh>
#include <UtopiaUtils/utils/generators.hh>
#include <UtopiaUtils/utils/odeint.hh>
#include <UtopiaUtils/utils/math.hh>
#include <UtopiaUtils/utils/meta.hh>

#include "types.hh"
#include "trackers.hh"
#include "utils.hh"
#include "environment.hh"


namespace Utopia::Models::mEEvo {

/// Holds the ODE system of the mEEvo model
template<class RNG = ModelRNG, class... tags>
struct ODESystem : public Utopia::Utils::BaseODESystem<State> {
    friend SpeciesEffectTracker;

    // -- Types ---------------------------------------------------------------
    using Base = BaseODESystem<State>;
    using Self = ODESystem;

    using TimeFunc = std::function<Time()>;

    using NormalDist = std::normal_distribution<double>;
    using RealDist = std::uniform_real_distribution<double>;
    using IntDist = std::uniform_int_distribution<IDType>;

    using Traits = std::tuple<double, double, double, double>;
    using EnvTraits = std::tuple<double, Vector>;

    using BiomassCorrectorFunc =
        std::function<void(Vector&, arma::uword, arma::uword)>;

    using ParentSelectorFunc =
        std::function<arma::uword(const Self&)>;

    using TraitMutationFunc =
        std::function<Traits(const Self&, const arma::uword)>;

    using EnvTraitMutationFunc =
        std::function<EnvTraits(const Self&, const arma::uword)>;


    // .. Evaluated tags ......................................................

    /// Whether to model competition outside of functional response
    static constexpr bool separate_competition =
        Utopia::Utils::contains_type<with_separate_competition, tags...>();

    /// Whether environment modifications are enabled
    static constexpr bool envmod_enabled =
        Utopia::Utils::contains_type<with_envmod, tags...>();

    /// Compute feeding range overlap analytically? (false: numerically)
    static constexpr bool analytical_feeding_range_overlap =
        not Utopia::Utils::contains_type<use_numerical_integration, tags...>();


    // -- Members -------------------------------------------------------------
    // .. Infrastructure ......................................................
private:
    /// A logger instance
    std::shared_ptr<spdlog::logger> log;

public:
    /// A random number generator, typically shared
    std::shared_ptr<RNG> rng;

    /// Element IDs
    IDVector ids;

private:
    /// The ID counter, starting at 1 to leave 0 as a special value
    std::size_t next_id = 1;

    /// The number of species *ever* created
    std::size_t species_count = 0;

    /// The number of entries that are considered to belong to resources only
    std::size_t _n_resources = 0;

    /// A function that returns the current model time
    TimeFunc _get_model_time;

public:
    /// .. Trackers ...........................................................

    /// Aggregates the traits that appear in this ODE system
    TraitsTracker<tags...> traits_tracker;

    /// Stores aggregated species properties (age, mean trophic level, ...)
    PropertyTracker property_tracker;

    /// Stores specifically cascade size
    /** This has to be done separately because it has to be associated with the
      * added species even if the species already became extinct.
      */
    SpeciesEffectTracker species_effect_tracker;

    /// Aggregates addition times of new species
    TimeTracker addition_times_tracker;

    /// Aggregates removal times of extinct species
    TimeTracker removal_times_tracker;


    // .. Matrices ............................................................
    /// The functional response matrix (non-negative)
    Matrix g;

    /// The feeding matrix, i.e. evaluated attack rates, (non-negative)
    Matrix a;

    /// Competition matrix (non-negative)
    Matrix c;


    // .. Shared properties ...................................................

    /// The specific body mass
    Vector m;


    // .. Species-specific properties .........................................

    /// The prey body mass, i.e. the centre of the feeding range
    Vector f;

    /// The prey body mass range, i.e. the std. dev. of the feeding range
    Vector s;

    /// Conversion efficiency when consuming _from_ this resource / species
    Vector lambda;

    /// Respiration rate
    Vector x;

    /// The individual attack rate
    Vector ai;

    /// The handling time
    Vector h;

    /// The minimum biomass density, ie.: species-specific extinction threshold
    Vector B_min;


    // .. Resource-specific properties ........................................
    /// The (evaluated) growth rate of resources
    Vector G;

    /// Growth rate of logistic growth model for resources
    Vector R;

    /// The carrying capacity for each resource
    Vector K;


    // .. Environment-specific properties .....................................
    /// The Environment object, holding most environment parameters
    Environment env;

    /// The part of the foraged energy that is diverted into the depot
    /** This also has a counterpart, `_frac_used`
      */
    Vector frac_diverted;

private:
    /// The fraction of foraged energy used to generate biomass
    /** This is `1. - frac_diverted`. It is pre-computed in order to reduce
      * repetitively evaluating this; it only changes when a species is added
      * or removed ...
      */
    Vector _frac_used;

public:
    /// How the diverted energy is distributed into the environment
    /** Rows correspond to the target entries in the environment state, columns
      * to the species depots the energy is distributed from.
      * Subsequently, the size of this matrix is (n_envmod, n_species).
      * Importantly, each column needs to sum up to one.
      */
    Matrix distr_to_env;


    // .. Global parameters ...................................................

    /// Factor in computation of respiration rate
    double respiration_rate_parameter;

    /// Factor in computation of individual attack rate
    double attack_rate_parameter;

    /// Factor in computation of handling time
    double handling_time_parameter;

    /// The scalar food competition parameter
    double c_food;

    /// The intra-specific competition parameter, interference competition
    double c_intra;

    /// The extinction threshold in units of the specific species body mass
    double eps;

    /// How much of the total respiration losses become available for resources
    double recycling_factor;

    /// Number of feeding range stddev.s to use for effective quadrature bounds
    double quad_nsigma;

    /// Whether self-interaction is allowed
    bool self_interaction;

    /// Interaction threshold; functional responses below this get set to zero
    /** To deactivate, set to < 0
      */
    double ia_threshold;

    /// Cost parameters
    CostParams cost_params;

protected:
    /// Mutation parameters
    Config mutation_params;

    /// Parameters for biomass correction
    Config biomass_correction_params;

    /// Parameters for parent selection
    Config parent_selection_params;

    /// Parameters for network extraction
    Config network_extraction_params;

public:
    /// Age for a species to be considered viable
    unsigned viability_age;


    // .. Tracking properties .................................................

    /// Species age
    /** \warning This needs to be manually re-computed each time step,
      *          otherwise it will be wrong.
      */
    UVector age;

    /// The flow-based trophic level
    /** \warning This needs to be manually re-computed upon changes.
      */
    Vector trophic_level;

    /// The mean of the flow-based trophic level over the species lifetime
    /** \warning This needs to be manually re-computed upon changes.
      */
    Vector mean_trophic_level;

    /// The number of extinctions caused by the last species addition
    unsigned n_extinctions;

    /// The number of viable species, evaluated upon extinction
    unsigned n_viable;

    /// The model time of the last species addition
    Time last_species_addition_time;

    /// The (mean) trophic level of the last-added species
    double last_species_addition_mean_TL;

    /// The error in trophic level computation
    /** Tracks the "error" from the latest trophic level computation:
      * the maximum absolute difference between the last two iterations of the
      * trophic level computation.
      */
    double TL_error;

    /// How much energy was deduced from the fallback species during mutation
    /** This should be a small value, otherwise there is a distortion in energy
      * flows (energy should come from the parent, not the fallback species,
      * which is typically an external resource).
      */
    double biomass_correction;

private:
    /// The model time of the last property update
    Time _last_property_update_time;

    /// Whether a functional diversity calculation is needed
    /** This is set to True upon species removal or addition.
      */
    bool _need_FD_update;

    /// The latest functional diversity calculation
    double _last_FD;

    /// The latest functional diversity difference value
    /** Updated upon update of functional diversity
      */
    double _delta_FD;

    /// Cache of adjacency matrix
    Matrix _adj;

    /// Adjancency matrix cache time
    Time _adj_cache_time;

    /// Whether to track species-level trophic incoherence
    bool _track_TL_incoherence;

    /// Trophic incoherence value at time before the last species was added
    double _TL_incoh_before_addition;


    // .. Helper objects and temporary variables ..............................
    // ... used e.g. to avoid re-computation or reduce memory allocations

    /// The biomass correction functor
    BiomassCorrectorFunc _biomass_corrector;

    /// The parent selecting functor
    ParentSelectorFunc _parent_selector;

    /// The trait mutation functor
    TraitMutationFunc _trait_mutation;

    /// The environment-related trait mutation functor
    EnvTraitMutationFunc _envtrait_mutation;

    /// The modified foraging response matrix
    Matrix _g_mod;

    /// A vector of foraged resources
    Vector _foraged;



    // ========================================================================
    // -- Constructors --------------------------------------------------------

    /// Remove default constructor; force explicit initialization
    ODESystem () = delete;

    /// Set up with default state and parameters
    /** This is the *main* constructor, all others call this one.
      */
    ODESystem (
       const std::size_t n_resources,
       const std::size_t n_species,
       const std::size_t n_envmod,
       TimeFunc get_time,
       const std::shared_ptr<spdlog::logger>& parent_logger = {},
       const std::shared_ptr<RNG>& parent_rng = {}
    )
    :
        // Setup the ODESystem state, i.e. the BDEState struct
        Base({
            n_resources + n_species,                        // B state
            envmod_enabled ? n_resources + n_species : 1,   // D state
            envmod_enabled ? n_envmod : 1,                  // E state
            arma::fill::zeros
        })

    ,   log([&parent_logger](){
            // Set up a new logger, if no parent_logger was given
            using Utopia::init_logger;
            if (not parent_logger) {
                return init_logger("ODESystem", spdlog::level::info, false);
            }

            return spdlog::stdout_color_mt(parent_logger->name() + ".sys");
        }())
    ,   rng([this, &parent_rng](){
            if (parent_rng) {
                return parent_rng;
            }
            this->log->warn("No parent RNG given, setting up a new one!");
            return std::make_shared<RNG>(RNG(std::rand()));
        }())

    ,   ids(arma::regspace<IDVector>(1, size_B()))  // ... starting at 1
    ,   next_id(size_B() + 1)
    ,   species_count(n_species)
    ,   _n_resources(n_resources)

    ,   _get_model_time(get_time)

    ,   traits_tracker()
    ,   property_tracker()
    ,   species_effect_tracker()
    ,   addition_times_tracker(_get_model_time)
    ,   removal_times_tracker(_get_model_time)

    ,   g(size_B(), size_B(), arma::fill::zeros)
    ,   a(size_B(), size_B(), arma::fill::zeros)
    ,   c(size_B(), size_B(), arma::fill::zeros)

    ,   m(size_B(), arma::fill::zeros)

    ,   f(size_B(), arma::fill::zeros)
    ,   s(size_B(), arma::fill::zeros)
    ,   lambda(size_B(), arma::fill::zeros)

    ,   x(size_B(), arma::fill::zeros)
    ,   ai(size_B(), arma::fill::zeros)
    ,   h(size_B(), arma::fill::zeros)
    ,   B_min(size_B(), arma::fill::zeros)

    ,   G(n_resources, arma::fill::zeros)
    ,   R(n_resources, arma::fill::zeros)
    ,   K(n_resources, arma::fill::zeros)

    ,   env(*this)
    ,   frac_diverted(size_B(), arma::fill::zeros)
    ,   _frac_used(size_B(), arma::fill::zeros)
    ,   distr_to_env(size_E(), size_B(), arma::fill::zeros)

    ,   respiration_rate_parameter(.314)
    ,   attack_rate_parameter(1.)
    ,   handling_time_parameter(.398)
    ,   c_food(.6)
    ,   c_intra(1.4)
    ,   eps(1e-5)
    ,   recycling_factor(0.)
    ,   quad_nsigma(3)
    ,   self_interaction(true)
    ,   ia_threshold(1e-12)

    ,   cost_params()
    ,   mutation_params{}
    ,   biomass_correction_params{}
    ,   network_extraction_params{}
    ,   viability_age(0)

    ,   age(size_B(), arma::fill::zeros)
    ,   trophic_level(size_B(), arma::fill::zeros)
    ,   mean_trophic_level(size_B(), arma::fill::zeros)

    ,   n_extinctions(0)
    ,   n_viable(0)
    ,   last_species_addition_time(0)
    ,   TL_error(0.)
    ,   biomass_correction(0.)
    ,   _last_property_update_time(0)

    ,   _need_FD_update(true)
    ,   _last_FD(NaN)
    ,   _delta_FD(0.)

    ,   _adj(0, 0)
    ,   _adj_cache_time(std::numeric_limits<Time>::max())

    ,   _track_TL_incoherence(false)
    ,   _TL_incoh_before_addition(NaN)

    ,   _biomass_corrector(
            [](auto&, auto, auto){}
        )
    ,   _parent_selector(
            [](const auto&) -> arma::uword { return {}; }
        )
    ,   _trait_mutation(
            [](const auto&, const auto) -> Traits { return {}; }
        )
    ,   _envtrait_mutation(
            [](const auto&, const auto) -> EnvTraits { return {}; }
        )

    ,   _g_mod(size_B(), size_B())   // set to NaN below
    ,   _foraged(size_B())           // set to NaN below

    {
        // For simplicity, disallow some template parameter combinations
        static_assert(
            not separate_competition or
            (separate_competition and not envmod_enabled),
            "`with_separate_competition` and `with_envmod` cannot be combined!"
        );

        // Some safety checks to avoid that the template parameter and the
        // number of environment modifications diverges.
        if constexpr (envmod_enabled) {
            if (n_envmod < 1) throw std::invalid_argument(
                "With environment modifications enabled, need at least one "
                "environment modification entry but got < 1."
            );
        }

        _set_up_logger(parent_logger);

        log->debug("Set sizes and default values.");
        log->trace("  n_resources:  {}", n_resources);
        log->trace("  n_species:    {}", n_species);
        log->trace("  n_envmod:     {}", n_envmod);
        log->trace("  size_B:       {}", size_B());
        log->trace("  size_D:       {}", size_D());
        log->trace("  size_E:       {}", size_E());

        // Set variables that make no sense at this point to NaN
        _g_mod.fill(NaN);
        _foraged.fill(NaN);
    }

    /// Set up with default state and parameters (using environment size 1)
    ODESystem (
       const std::size_t n_resources,
       const std::size_t n_species,
       TimeFunc get_time,
       const std::shared_ptr<spdlog::logger>& parent_logger = {},
       const std::shared_ptr<RNG>& parent_rng = {}
    )
    :
        ODESystem(
            n_resources, n_species, 1, get_time, parent_logger, parent_rng
        )
    {}


public:
    /// Default copy constructor
    ODESystem (const ODESystem&) = default;

    /// Copy assignment operator
    ODESystem& operator= (const ODESystem&) = default;

    /// Set up the system from a configuration node
    ODESystem (
       const Config& cfg,
       TimeFunc get_model_time,
       const std::shared_ptr<spdlog::logger>& parent_logger = {},
       const std::shared_ptr<RNG>& parent_rng = {}
    )
    :
        // First, determine the system size and set up everything to defaults
        ODESystem(
            get_as<uint>("n_resources", cfg),
            get_as<uint>("n_species", cfg),
            envmod_enabled ? get_as<uint>("n_envmod", cfg) : 1,
            get_model_time,
            parent_logger,
            parent_rng
        )
    {
        _set_up_logger(parent_logger, cfg);
        log->debug("Evaluating ODESystem configuration entries ...");

        // .. Resource and species parameters .................................
        // Get the configurations
        const auto resources_cfg = get_as<Config>("resources", cfg);
        const auto species_cfg = get_as<Config>("species", cfg);

        // Define maps from a config name to a member reference, then set their
        // values using a helper function that parses vector-like values.

        // .. for resources
        log->debug("  Setting up resources ...");
        log->trace("    Configuration:\n\n{}\n", resources_cfg);
        const auto resource_params_map = RefPairs<Vector>{
            {"biomass_density",     B()},
            {"mass",                m},
            {"refill_rate",         R},
            {"carrying_capacity",   K},
            {"efficiency",          lambda}
        };
        for (auto& [key, ref] : resource_params_map) {
            set_vector_values(ref, resources_cfg[key], resources());
        }

        // .. for species
        log->debug("  Setting up species ...");
        log->trace("    Configuration:\n\n{}\n", species_cfg);
        const auto species_params_map = RefPairs<Vector>{
            {"biomass_density",     B()},
            {"mass",                m},
            {"feeding_center",      f},
            {"niche_width",         s},
            {"efficiency",          lambda},
            {"frac_diverted",       frac_diverted}
        };
        for (auto& [key, ref] : species_params_map) {
            set_vector_values(ref, species_cfg[key], species());
        }

        // Set up the environment
        _set_up_environment(cfg);

        // Use the helper functions to set up all the rest ...
        // ... the global parameters that need to be extracted from the config:
        _set_up_global_params(cfg);

        // ... using those and the state, computing the derived quantities:
        _set_up_derived_quantities();

        // ... and setting up the trackers with this initial state:
        _set_up_trackers();

        log->info("ODESystem set up from configuration.");
    }

private:
    /// Set up the logger, optionally setting the log level from config
    void _set_up_logger (const std::shared_ptr<spdlog::logger>& parent_logger,
                         const Config& cfg = {})
    {
        if (parent_logger) {
            log->set_level(parent_logger->level());
        }
        else {
            log->set_level(spdlog::level::warn);
        }

        if (cfg["log_level"]) {
            log->set_level(spdlog::level::from_str(
                get_as<std::string>("log_level", cfg))
            );
        }
    }

    /// Set up the environment from the given configuration
    void _set_up_environment (const Config& cfg) {
        if constexpr (not envmod_enabled) return;

        const auto& env_cfg = get_as<Config>("environment", cfg);
        log->debug("  Setting up environment ...");
        log->trace("    Configuration:\n\n{}\n", env_cfg);

        env = Environment(*this, env_cfg);

        log->debug("  Setting environment state ...");
        set_vector_values(E(), get_as<Config>("energy", env_cfg));

        log->debug("  Set up environment from configuration.");
    }

    /// Sets up the global parameters from the given configuration
    void _set_up_global_params (const Config& cfg) {
        log->debug("  Setting up global parameters ...");

        const auto global_params_map = RefPairs<double>{
            {"respiration_rate_parameter",  respiration_rate_parameter},
            {"attack_rate_parameter",       attack_rate_parameter},
            {"handling_time_parameter",     handling_time_parameter},
            {"c_food",                      c_food},
            {"c_intra",                     c_intra},
            {"eps",                         eps},
            {"recycling_factor",            recycling_factor}
        };
        for (auto& [key, ref] : global_params_map) {
            ref = get_as<double>(key, cfg);
        }

        // Potentially optional parameters, depending on scenario
        if constexpr (not analytical_feeding_range_overlap) {
            quad_nsigma = get_as<double>("quad_nsigma", cfg);
        }

        // Remaining configuration parameters
        self_interaction = get_as<bool>("self_interaction", cfg);
        ia_threshold = get_as<double>("ia_threshold", cfg);
        _track_TL_incoherence = get_as<bool>("track_trophic_incoherence", cfg);

        cost_params = CostParams(get_as<Config>("costs", cfg, {}));
        mutation_params = get_as<Config>("mutation", cfg);
        biomass_correction_params = get_as<Config>("biomass_correction", cfg);
        parent_selection_params = get_as<Config>("parent_selection", cfg);
        network_extraction_params = get_as<Config>("network_extraction", cfg);
        viability_age = get_as<unsigned>("viability_age", cfg);

        log->debug("  Setting up functors ...");
        _biomass_corrector =
            _build_biomass_corrector(biomass_correction_params);
        _parent_selector = _build_parent_selector(parent_selection_params);
        _trait_mutation = _build_trait_mutation_func(mutation_params);
        _envtrait_mutation = _build_env_trait_mutation_func(mutation_params);

        log->debug("  Global parameters and functors all set up.");
    }

    /// Computes the derived quantities (matrices, etc.)
    void _set_up_derived_quantities () {
        log->debug("  Computing derived quantities ...");

        update_species_parameters();
        update_attack_matrix();
        update_competition_matrix();

        // For a meaningful initial state ...
        update_responses(B());
        if constexpr (envmod_enabled) {
            update_g_mod();
            env.update_total_modification(E());
        }

        // For tracking ...
        update_trophic_levels();
        update_property_tracking_variables(0);
    }

    /// Sets up the trackers for the initial state
    void _set_up_trackers () {
        log->debug("  Setting up trackers ...");

        // To always have both the addition and removal times written out,
        // add values for ID 0, which is the "special ID" and thus irrelevant
        addition_times_tracker.add_id(0);
        removal_times_tracker.add_id(0);

        // Same for properties and cascade size trackers, which are also
        // triggered only upon the removal of species, thus might be empty
        property_tracker.add_id(0);
        species_effect_tracker.add_id(0);

        // Now add the actual addition times for all "real" resources & species
        addition_times_tracker.track_multiple(*this, arma::span::all);

        // For the traits, add entries for the full span (species & resources)
        traits_tracker.track_multiple(*this, arma::span::all);
    }


public:
    // -- Getters -------------------------------------------------------------
    /// The current model time
    auto get_model_time () const {
        return _get_model_time();
    }

    /// The last property update time
    auto get_last_property_update_time () const {
        return _last_property_update_time;
    }

    /// The ID of the last species that was added (even if already extinct)
    auto get_last_added_species_id () const {
        return next_id - 1;
    }

    /// The current biomass state
    auto& B () {
        return get_state().B;
    }

    /// The current biomass state
    const auto& B () const {
        return get_state().B;
    }

    /// The current depot state
    auto& D () {
        return get_state().D;
    }

    /// The current depot state
    const auto& D () const {
        return get_state().D;
    }

    /// The current environment state
    auto& E () {
        return get_state().E;
    }

    /// The current environment state
    const auto& E () const {
        return get_state().E;
    }

    /// The last-calculated effectively used functional response matrix
    /** With environment modifications activated, this will be g_mod, otherwise
      * it will be simply g. This is not the current value of the response
      * matrix but the last-calculated one!
      */
    auto& latest_g () {
        if constexpr (envmod_enabled) {
            return _g_mod;
        }
        else {
            return g;
        }
    }

    /// The last-calculated effectively used functional response matrix
    /** With environment modifications activated, this will be g_mod, otherwise
      * it will be simply g. This is not the current value of the response
      * matrix but the last-calculated one!
      */
    const auto& latest_g () const {
        if constexpr (envmod_enabled) {
            return _g_mod;
        }
        else {
            return g;
        }
    }

    // .. Element and subview access ..........................................

    /// An element of the current biomass state
    auto& B (const arma::uword idx) {
        return get_state().B(idx);
    }

    /// Const-reference of an element of the current biomass state
    const auto& B (const arma::uword idx) const {
        return get_state().B(idx);
    }

    /// A span-based contiguous subview of the current biomass state
    auto B (const arma::span span) const {
        return get_state().B(span);
    }

    /// An element-based non-contiguous subview of the current biomass state
    auto B (const arma::uvec& idcs) const {
        return get_state().B(idcs);
    }

    /// An element of the current depot state
    auto& D (const arma::uword idx) {
        return get_state().D(idx);
    }

    /// Const-reference of an element of the current depot state
    const auto& D (const arma::uword idx) const {
        return get_state().D(idx);
    }

    /// A span-based contiguous subview of the current depot state
    auto D (const arma::span span) const {
        return get_state().D(span);
    }

    /// An element-based non-contiguous subview of the current depot state
    auto D (const arma::uvec& idcs) const {
        return get_state().D(idcs);
    }

    // .. Sizes and querying ..................................................

    /// Number of resources *and* species, without modification types
    auto size_B () const {
        return B().size();
    }

    /// Number of resources *and* species in the depot state
    auto size_D () const {
        return D().size();
    }

    /// Number of environment modification types
    auto size_E () const {
        return E().size();
    }

    /// The number of resources
    auto n_resources () const {
        return _n_resources;
    }

    /// The number of species
    /** Computed from B state size minus the number of resources
      */
    auto n_species () const {
        return size_B() - _n_resources;
    }

    /// Whether the given index *of the B state* belongs to a resource
    template<class T>
    bool is_resource (T idx) const {
        return (idx < _n_resources);
    }

    // .. Spans ...............................................................

    /// A span referring only to the resources in the B state
    arma::span resources () const {
        return arma::span(0, _n_resources - 1);
    }

    /// A span referring only to the species in the B state
    arma::span species () const {
        return arma::span(_n_resources, size_B() - 1);
    }

    // .. Iteration helpers ...................................................

    /// Container of current `B` state indices, allowing subselection
    arma::uvec B_indices (const arma::span sel = arma::span::all) const {
        return arma::regspace<arma::uvec>(0, size_B() - 1)(sel);
    }

    // .. Observables .........................................................
    /// The cumulative species count, i.e. the number of species ever created
    auto get_species_count () const {
        return species_count;
    }

    /// Computes the fraction of viable species ever added to this network
    /**
      * \warning This might not be fully accurate if the last property tracking
      *          variable update is too far away, as this method relies on the
      *          age of the species already having been computed correctly.
      */
    double frac_viable () const {
        return
            static_cast<double>(
                n_viable + arma::accu(age(species()) >= viability_age)
            )
            / get_species_count();
    }

    /// The sum of the biomass density for the *full* biomass state
    auto total_biomass (const arma::span sel = arma::span::all) const {
        return arma::accu(B(sel));
    }

    /// The total instantaneous biomass flow
    /** This always works on the currently used functional response matrix.
      *
      * This does not account for conversion losses or input to the resource!
      */
    auto biomass_flow () const {
        return arma::accu(biomass_flow_vector());
    }

    /// The total *unmodified* instantaneous biomass flow
    /** This will always use the unmodified functional response matrix.
      *
      * Uses the latest computation of g, does *not* recalculate it.
      *
      * This does not account for conversion losses or input to the resource!
      */
    auto biomass_flow_unmodified () const {
        return arma::accu(B().t() * g);
    }

    /// The shannon index computed from the biomass flow matrix
    // FIXME What about values that are zero??
    auto biomass_flow_shannon_index () const {
        const auto flow_matrix = biomass_flow_matrix();
        return shannon_index(flow_matrix / arma::accu(flow_matrix));
    }

    /// Computes the functional diversity
    /** This the same measure as used in Allhoff2015: the integral over the
      * envelope of *all* species feeding kernels.
      *
      * Results are cached and only re-computed if needed. This will also
      * compute and cache the change in functional diversity value, accessible
      * via `delta_FD`.
      *
      * \tparam n_points   How many evaluation points to use for Gauss
      *                    quadrature. With larger bounds, consider using a
      *                    higher number of points as well in order to not lose
      *                    too much precision in the large-valued part of the
      *                    feeding kernel envelope.
      *                    Note that for some values (7, 15, 20, 25, 30)
      *                    pre-computed parameters exist, reducing runtime
      *                    overhead.
      *
      * \param skip_nsigma Used for reducing the number of evalated feeding
      *                    kernels: if a feeding center is this far away (in
      *                    units of niche width), it will not be part of the
      *                    envelope computation for a specific body mass.
      * \param pad_nsigma  Used for deducing the integration bounds: this is
      *                    the relative padding (in log space) in multiples of
      *                    the maximum niche width, applied to the largest and
      *                    smallest log feeding center:
      *                         `b0 = log10(f_min) - pad * s_max`
      *                         `b1 = log10(f_max) + pad * s_max`
      */
    template<std::size_t n_points = 30>
    auto functional_diversity (
        const double skip_nsigma = 2.5,
        const double pad_nsigma = 2.5)
    {
        using std::log10;
        using std::exp;
        using std::pow;
        using std::sqrt;
        using std::abs;
        using Utopia::Utils::in_range;

        if (n_species() == 0) {
            return 0.;
        }

        if (not _need_FD_update) {
            log->trace(
                "Returning cached functional diversity value: {:.3g}", _last_FD
            );
            return _last_FD;
        }
        log->trace(
            "Computing functional diversity (n_species: {}) ...", n_species()
        );

        // Define the functional to integrate over. For each point, need to go
        // over all species' feeding kernels and evaluate the gaussian. Then
        // choose the largest of those values to arrive at the value of the
        // envelope for the given log10 body mass.
        const auto envelope_func = [this, skip_nsigma](const auto log10_m){
            double envelope = 0.;
            double _efk, _s, _s_sq, _log10_f;
            const double TAU = 2. * M_PI;

            for (const auto idx
                 : in_range(static_cast<arma::uword>(n_resources()), size_B()))
            {
                _s = this->s(idx);
                _log10_f = log10(this->f(idx));
                if (abs(log10_m - _log10_f) > _s * skip_nsigma) {
                    this->log->trace(
                        "  idx {:3d}: skipping due to negligible effect", idx
                    );
                    continue;
                }
                _s_sq = _s * _s;

                _efk = (1. / sqrt(TAU * _s_sq))
                     * exp(- pow(log10_m - _log10_f, 2) / (2. * _s_sq));
                this->log->trace(
                    "  idx {:3d}: feeding kernel @ log10_m = {:.3g}: {:.3g}  "
                    "(envelope: {:.3g})", idx, log10_m, _efk, envelope
                );

                if (_efk > envelope) {
                    envelope = _efk;
                    this->log->trace(
                        "  envelope value updated to {:.3g}", _efk
                    );
                }
            }
            return envelope;
        };

        // Compute bounds with padding applied to largest s value
        const auto s_max = s(species()).max();
        const auto b0 = log10(f(species()).min()) - pad_nsigma * s_max;
        const auto b1 = log10(f(species()).max()) + pad_nsigma * s_max;
        log->trace("  Bounds ({:.3g}, {:.3g}) ...", b0, b1);

        // Now perform the integral over the envelope
        log->trace("  Now computing integral using gauss quadrature ...");
        const auto new_FD = gauss_quadrature<n_points>(envelope_func, b0, b1);
        log->trace("  new FD: {:.3g}", new_FD);

        // ... and cache the results
        _delta_FD = new_FD - _last_FD;
        _last_FD = new_FD;
        _need_FD_update = false;
        log->trace("New functional diversity value cached.");
        return new_FD;
    }

    /// The functional diversity change from the last species addition
    auto delta_FD () {
        if (_need_FD_update) {
            functional_diversity();
        }
        return _delta_FD;
    }

    /// The density of the interaction matrix
    /** The fraction of zeros in the functional response matrix. Uses the
      * interaction threshold as the minimum interaction strength.
      */
    double g_density () const {
        const double num_zeros = arma::accu(g < ia_threshold);
        return num_zeros / g.size();
    }

    /// Prints out *all* information about the current ODE systems state
    void print_info () const {
        [[maybe_unused]] const auto join = [](const auto& v){
            return fmt::join(v, ", ");
        };

        fmt::print("===== ODESystem Information =====\n");

        fmt::print("size_B:              {}\n", size_B());
        fmt::print("n_resources:         {}\n", n_resources());
        fmt::print("n_species:           {}\n", n_species());
        fmt::print("cuml. species count: {}\n", species_count);
        fmt::print("model time:          {}\n", get_model_time());

        fmt::print("c_food:              {}\n", c_food);
        fmt::print("c_intra:             {}\n", c_intra);
        fmt::print("eps:                 {}\n", eps);
        fmt::print("recycling_factor:    {}\n", recycling_factor);
        fmt::print("quad_nsigma:         {}\n", quad_nsigma);
        fmt::print("self-interaction:    {}\n", self_interaction);
        fmt::print("interaction thresh.: {}\n", ia_threshold);

        fmt::print("respiration rate p.: {}\n", respiration_rate_parameter);
        fmt::print("attack rate param.:  {}\n", attack_rate_parameter);
        fmt::print("handling time p.:    {}\n", handling_time_parameter);

        ids.print("\nids");
        B().print("B");

        m.print("m");
        f.print("f");
        s.print("s");
        lambda.print("lambda");

        x.print("x");
        ai.print("ai");
        h.print("h");
        B_min.print("B_min");

        G.print("G");
        R.print("R");
        K.print("K");

        g.print("g");
        a.print("a");
        c.print("c");

        fmt::print("\n----- Costs -----\n");
        const auto& cp = cost_params;
        fmt::print("enabled?             {}\n", cp.enabled ? "yes" : "NO");
        fmt::print("niche width\n");
        fmt::print("  mode:              {}\n",
                   cp.niche_width.mode == cost_mode::polynomial ?
                   "polynomial" : "exponential");
        fmt::print("  factor:            {}\n", cp.niche_width.factor);
        fmt::print("  exponent:          {}\n", cp.niche_width.exponent);
        fmt::print("  center:            {}\n", cp.niche_width.center);
        fmt::print("  offset:            {}\n", cp.niche_width.offset);


        fmt::print("\n----- Species' Biomass Depots -----\n");
        D().print("D");
        frac_diverted.print("frac_diverted");
        _frac_used.print("_frac_used");
        distr_to_env.print("distr_to_env");

        fmt::print("\n----- Environment State -----\n");
        fmt::print("modification?        {}\n", envmod_enabled ? "yes" : "no");
        fmt::print("size_E:              {}\n", size_E());

        E().print("E");
        env.reservoir_size.print("reservoir_size");
        env.mod_weight.print("mod_weight");
        env.decay_rate.print("decay_rate");
        env.mod_efficiency.print("mod_efficiency");
        env.mod_total.print("env.mod_total (latest)");

        fmt::print("p_mod_gain:          {}\n", env.p_mod_gain);
        fmt::print("p_mod_lose:          {}\n", env.p_mod_lose);

        fmt::print("\n----- Mutation Parameters -----\n");
        fmt::print("{}", mutation_params);

        fmt::print("\n----- Tracking Variables -----\n");
        age.print("age");
        fmt::print("n_extinctions:       {}\n", n_extinctions);
        fmt::print("last species add.:   {}\n", last_species_addition_time);

        trophic_level.print("trophic_level");
        mean_trophic_level.print("mean_trophic_level");
        fmt::print("TL max. abs. error:  {}\n", TL_error);

        fmt::print("biomass correction ...\n");
        fmt::print("  mode:              {}\n",
                   get_as<std::string>("mode", biomass_correction_params));
        fmt::print("  total correction:  {}\n", biomass_correction);

        fmt::print("\n===== End of ODESystem Information =====\n\n");
    }


    // .. Vector-valued observables ...........................................

    /// Computes the currently valid biomass flow vector
    /** Uses the latest computation of g or _g_mod; does not recalculate!
      */
    Vector biomass_flow_vector () const {
        if constexpr (envmod_enabled) {
            return (B().t() * _g_mod).t();
        }
        else {
            return (B().t() * g).t();
        }
    }

    /// Per-species total interaction strength from current functional response
    /** This is computed as follows:
      *
      *     total_response(i) = arma::dot(g.row(i).t() % lambda, B())
      */
    Vector total_response () const {
        using Utopia::Utils::imbue_index;

        auto tot_response = Vector(size_B());
        imbue_index(tot_response, [&](const auto i) -> double {
            return arma::dot(g.row(i).t() % lambda, B());
        });
        return tot_response;
    }

    /// Per-species *modified* mean interaction strength
    /** This is computed as follows:
      *
      *     total_response(i) = arma::dot(_g_mod.row(i).t() % lambda, B())
      */
    Vector total_modified_response () const {
        using Utopia::Utils::imbue_index;
        static_assert(envmod_enabled, "Requires envmod_enabled");

        auto tot_response = Vector(size_B());
        imbue_index(tot_response, [&](const auto i) -> double {
            return arma::dot(_g_mod.row(i).t() % lambda, B());
        });
        return tot_response;
    }

    /// Per-species total attack rate from current functional response
    /** This is computed as follows:
      *
      *     total_attack_rate(i) = arma::dot(h(i) * a.row(i).t(), B())
      */
    Vector total_attack_rate () const {
        using Utopia::Utils::imbue_index;

        auto tot_attack = Vector(size_B());
        imbue_index(tot_attack, [&](const auto i) -> double {
            return arma::dot(h(i) * a.row(i).t(), B());
        });
        return tot_attack;
    }

    /// Per-species total competition value from current functional response
    /** This is computed as follows:
      *
      *     perceived_competition(i) = arma::dot(c.row(i).t(), B())
      */
    Vector perceived_competition () const {
        using Utopia::Utils::imbue_index;

        auto perceived_competition = Vector(size_B());
        imbue_index(perceived_competition, [&](const auto i) -> double {
            return arma::dot(c.row(i).t(), B());
        });
        return perceived_competition;
    }

    /// Overall biomass distribution, binned over trophic levels
    /** Returns bin centers and sum of the absolute biomass density that is in
      * the respective bin.
      *
      * \TODO Consider outsourcing and generalising to other variables
      */
    std::pair<Vector, Vector>
    biomass_per_trophic_level (const int bins_per_level,
                               const int max_level = 6) const
    {
        if (max_level < 2 or bins_per_level < 1) {
            throw std::invalid_argument(fmt::format(
                "Need max_level >= 2 and bins_per_level >= 1, got {} and {}!",
                max_level, bins_per_level
            ));
        }
        const int min_level = 1;
        const int n_bins = (max_level - min_level) * bins_per_level + 1;

        const double bin_width = 1. / static_cast<double>(bins_per_level);
        const Vector bin_centers = (
            arma::linspace(min_level, max_level, n_bins) + (bin_width / 2.)
        );

        auto biomass_per_TL = Vector(bin_centers.n_elem, arma::fill::zeros);
        int bin_no = 0;

        for (auto i=0u; i < B().n_rows; i++) {
            bin_no = static_cast<int>(std::floor(
                (trophic_level(i) - static_cast<double>(min_level)) / bin_width
            ));
            if (bin_no >= 0 and bin_no < n_bins) {
                biomass_per_TL[bin_no] += B(i);
            }
            else {
                // warn about it!
            }
        }

        return std::make_pair(bin_centers, biomass_per_TL);
    }

    // .. Matrix-valued observables ...........................................

    /// Computes the currently valid biomass flow matrix
    /** Returns a matrix where each element corresponds to the absolute biomass
      * flow from species j (columns) to species i (rows).
      *
      * \note Uses the latest computation of g or _g_mod; does not recalculate!
      */
    Matrix biomass_flow_matrix () const {
        Matrix B_flow = latest_g();

        for (auto i=0u; i < size_B(); i++) {
            B_flow.row(i) *= B(i);
        }
        return B_flow;
    }

    // .. Search-based ........................................................

    /// Retrieve the reference to a certain state vector by its name
    Vector& get (const std::string& name) {
        for (auto [_name, _ref] : _extd_state_refs()) {
            if (name != _name) continue;
            return _ref;
        }
        throw std::invalid_argument(fmt::format(
            "Unknown attribute name '{}'!", name
        ));
    }

    /// Returns the indices of those entries that match a certain condition
    /** Currently, this can only work on attributes that are accessible via the
      * get method, i.e. `Vector`-type attributes.
      *
      * Under the hood, this uses Utopia::Utils::find_indices and uses as input
      * the attributes of this class.
      *
      * \param   mode       Which mode to use for finding indices
      * \param   attr_name  Values of which attribute to use for search,
      *                     defaulting to `ids`.
      * \param   params     Further parameters, e.g. `only` for selecting
      *                     whether to work only on species (default), only on
      *                     resources or on all entries of the attribute.
      *                     For some modes, the additional `rhs` parameter is
      *                     expected, e.g. for binary comparisons.
      *
      * \note    For `attr_name == "ids"`, the integer-valued vector will be
      *          converted to a floating-point valued vector.
      *
      * \warning This is not a particularly efficient implementation and it
      *          should *not* be used for highly-repetitive calls!
      *
      * \warning When accessing the environment, take care to set `only: all`,
      *          otherwise there will be a selection on the differently-sized
      *          environment state.
      */
    // TODO Should be const? Support inverting selection?
    IDVector find (const std::string& mode,
                   const std::string& attr_name = "ids",
                   const Config& params = {})
    {
        using Utopia::Utils::find_indices;

        // The matching indices, i.e. the return value
        IDVector idcs{};

        // Get the variable that is to be searched through; this also makes
        // additional values available, e.g. the vector of species IDs
        Vector v{};
        if (attr_name == "ids") {
            v = arma::conv_to<Vector>::from(ids);
        }
        else {
            v = get(attr_name);
        }

        // Decide whether to operate only on species or on all entries, then
        // reduce the vector to the corresponding subset.
        // If working only on species, determine the required index offset to
        // compensate for the cutoff.
        const auto _only = get_as<std::string>("only", params, "species");

        unsigned _idx_offset;
        if (_only == "species") {
            _idx_offset = _n_resources;
            v = v(species());
        }
        else if (_only == "resources") {
            _idx_offset = 0;
            v = v(resources());
        }
        else if (_only == "all") {
            _idx_offset = 0;
        }
        else {
            throw std::invalid_argument(fmt::format(
                "Got invalid `only` argument '{}'! May only be one of: "
                "species, resources, all", _only
            ));
        }

        // Find the corresponding indices and correct with the offset
        return find_indices(v, mode, params) + _idx_offset;
    }

    /// Overload of find that extracts all parameters from a config node
    auto find (const Config& params) {
        return find(
            get_as<std::string>("mode", params),
            get_as<std::string>("attr_name", params, "ids"),
            params
        );
    }



    // ========================================================================
    // -- System Function -----------------------------------------------------

    /// The system propagator: updates the responses and computes state change
    /** \warning The given `t` is the total time of the ODE integrator and does
      *          **not** correspond to the model time! It is influenced by the
      *          `speedup` parameter, for example.
      *          Also note that this propagation function may be invoked for
      *          times `t` that do *not* grow monotonously, e.g. when a step
      *          was discarded. Subsequently, this method should NEVER EVER be
      *          used to keep track of time-dependent changes in variable's
      *          values. In other words: it is not possible to reliably deduce
      *          `dt` and use that to compute the actual changes.
      */
    void operator() (const State& S, State& dSdt, double /* t */) {
        // Get the references from the structured state
        auto& B = S.B;
        auto& dBdt = dSdt.B;

        // Need to update response parameters G and g
        update_responses(B);

        // Ensure dBdt is correctly sized and is zero-valued
        dBdt.copy_size(B);
        dBdt.zeros();

        // -- Refill the resources (separately from the species)
        const arma::span res = resources();
        dBdt(res) = G % B(res);

        if (recycling_factor > 0.) {
            // Additionally, make biomass lost through respiration available to
            // the primary resources, dividing equally among them
            dBdt(res) += recycling_factor * arma::accu(x % B) / _n_resources;
        }

        // -- Compute the species' total biomass density change
        // Distinguish how this is done depending on whether the environment
        // modification mechanism is enabled or not
        if constexpr (not envmod_enabled) {
            if constexpr (not separate_competition) {
                //       reprod.   respir.   being foraged
                dBdt += (g * lambda - x) % B - (B.t() * g).t();
                //       M m V        V  H V    V     m M
                // w/: (V)ector, (M)atrix, (m)atrix mul., (H)adamard prod.
            }
            else {
                //       reprod.    comp.   respir.  being foraged
                dBdt += (g * lambda - c * B - x) % B - (B.t() * g).t();
            }
        }
        else {
            update_g_mod();

            // Compute total foraged biomass, deducing conversion efficiency
            _foraged = _g_mod * lambda % B;

            // Biomass change, with part of the resources diverted to the depot
            dBdt += (_foraged % _frac_used) - (x % B) - (B.t() * _g_mod).t();

            // Depot change: absolute biomass
            auto& dDdt = dSdt.D;
            dDdt = _foraged % frac_diverted;
        }
    }

    /// *Generates* the system propagator for use with SystemGeneration::On
    /** This captures all relevant properties, thus avoiding copies of this
      * whole ODESystem object. Instead, capturing `this` leads to the new
      * system only being the size of a single reference.
      */
    auto operator() () {
        return [this](const State& B, State& dBdt, double t) -> void {
            this->operator()(B, dBdt, t);
        };
    }


    // -- Update functions ----------------------------------------------------

    /// Update resource inflow vector and functional response matrix
    void update_responses (const Vector& _B) {
        // -- Resource inflow: logistic growth w/ resource-specific R and K
        G = R % (1. - _B(resources()) / K);
        // NOTE G, R and K are only referring to resources; B includes species


        // -- Functional response
        // .. The naive way ..
        // using Utopia::Utils::imbue_row_col;
        // NOTE This is the elegant way, but its performance is inferior to the
        //      old-school nested for-loops, because element lookup is
        //      repeated multiple times for selection of the current row.
        //      In the for-loop approach chosen below, the elements can be
        //      selected and transposed once and then used for computations
        //      of all values of the columns, thus letting those operations
        //      not scale with n^2 but linearly ...
        //
        // imbue_row_col(g, [&](auto i, auto j){
        //     // If the predator is a resource, g_ij remains zero.
        //     // Also, self-interaction might have been disabled.
        //     if (is_resource(i) or (i == j and not self_interaction)) {
        //         return 0.;
        //     }
        //
        //     return
        //           (a(i,j) * B(j) / m(i))
        //         / (1. + arma::dot(h(i) * a.row(i).t() + c.row(i).t(), B));
        // });

        // .. The fast way ..
        // Iterate over rows (i) and columns (j) separately and extract all the
        // row-related information only once. Also, handle resource separately.
        // See above for why this is faster.
        //
        // Resources have no predatory response: can set to zero
        g(resources(), arma::span::all).fill(0.);

        // Update species response
        Vector a_i;     // Row of attack-rate matrix, transposed
        double denom;   // Evaluated denominator: m_i (1+dot(h_i*a_i + c, B))

        for (auto i=_n_resources; i < g.n_rows; i++) {
            a_i = a.row(i).t();
            if constexpr (not separate_competition) {
                denom = m(i) * (1. + arma::dot(h(i) * a_i + c.row(i).t(), _B));
            }
            else {
                denom = m(i) * (1. + arma::dot(h(i) * a_i, _B));
            }

            // .. Loop-based formulation
            for (auto j=0u; j < g.n_cols; j++) {
                g(i, j) = (a_i(j) * _B(j)) / denom;
            }

            // .. Vectorised alternative
            // g.row(i) = (a_i % B).t() / denom;
            // NOTE The transpose operation seems to be necessary and it leads
            //      to a performance hit of 20% ... whyever.
        }

        // Finishing up ... perform matrix-level operations
        // If self-interaction is disabled, set the diagonal to zero
        if (not self_interaction) {
            g.diag().fill(0.);
        }

        // Apply the interaction threshold to reduce entries close to zero
        if (ia_threshold > 0.) {
            g.clean(ia_threshold);
        }
    }

    /// Updates the modified functional response matrix
    /** Compute modified functional response matrix `_g_mod`, if there were any
      * modifications; otherwise simply use the functional response matrix
      * directly, as they can be considered equal
      */
    void update_g_mod () {
        static_assert(envmod_enabled, "need envmod_enabled");

        if (env.no_modifications) {
            _g_mod = g;
        }
        else {
            _g_mod = g % env.mod_factors;
        }
    }

    /// Updates vectors h, ai, x, B_min, and _frac_used
    void update_species_parameters (const arma::span sel = arma::span::all) {
        h(sel) = handling_time_parameter * arma::pow(m(sel), -.75);
        ai(sel) = attack_rate_parameter * arma::pow(m(sel), +.75);
        x(sel) = compute_respiration_rate(sel);
        B_min(sel) = eps * m(sel);

        if constexpr (envmod_enabled) {
            _frac_used(sel) = 1. - frac_diverted(sel);
        }

        // For resources, all these values are zero (or -inf):
        //    * never a predator -> no handling time
        //    * never attacking  -> no individual attack rate
        //    * no respiration   -> no loss rate
        //    * no species       -> no extinction
        // Make sure they keep that value
        // TODO Consider doing this only if the given span overlaps ...
        const auto res_span = resources();
        h(res_span).zeros();
        ai(res_span).zeros();
        x(res_span).zeros();
        B_min(res_span).fill(- arma::datum::inf);
    }

    /// Update the attack rate matrix for the given row/column indices
    /** For an empty index container, the full matrix gets recomputed.
      *
      * \note The indices refer *both* to a column and to a row of the matrix,
      *       as both these entries need to be re-computed.
      */
    template<class IndexContainer = std::vector<IDType>>
    void update_attack_matrix (IndexContainer idcs = {}) {
        using std::log10;
        using std::pow;
        using Utopia::Utils::imbue_row_col;
        using Utopia::Utils::imbue_row_col_if;
        using Utopia::Utils::contains;

        const auto func = [&](const auto i, const auto j){
            if (is_resource(i)) {
                // Resources don't attack
                return 0.;
            }
            return
                  ai(i) / (s(i) * sqrt(2.*M_PI))
                * exp(- pow(log10(f(i)/m(j)), 2)
                      / (2. * pow(s(i), 2)));
        };

        if (not idcs.size()) {
            // Update whole matrix
            imbue_row_col(a, func);
        }
        else {
            // Compute only for rows and columns with these indices
            imbue_row_col_if(
                a, func,
                [&idcs](const auto row, const auto col){
                    return contains(idcs, row) or contains(idcs, col);
                }
            ); // TODO Use spans instead!
        }
    }

    /// Update the competition matrix for the given row/column indices
    /** For an empty index container, the full matrix gets recomputed.
      *
      * \note The indices refer *both* to a column and to a row of the matrix,
      *       as both these entries need to be re-computed.
      */
    template<class IndexContainer = std::vector<IDType>>
    void update_competition_matrix (IndexContainer idcs = {}) {
        using Utopia::Utils::imbue_row_col_if;
        using Utopia::Utils::imbue_row_col;
        using Utopia::Utils::contains;

        const auto func = [&](const auto i, const auto l){
            // No competition to or between resources
            if (is_resource(i) or is_resource(l)) {
                return 0.;
            }

            // Diagonal elements have a special definition
            if (i == l) {
                return c_food + c_intra;
            }

            // Compute overlap, normalising by self-overlap
            return
                  c_food * compute_feeding_range_overlap(i, l)
                / compute_feeding_range_overlap(i);
        };

        if (not idcs.size()) {
            // Update the whole matrix
            imbue_row_col(c, func);
        }
        else {
            // Update only affected rows or cols
            imbue_row_col_if(
                c, func,
                [&idcs](const auto row, const auto col){
                    return contains(idcs, row) or contains(idcs, col);
                }
            ); // TODO Use spans instead!
        }
    }

    /// Computes the respiration rate (and costs) for a selection of species
    Vector
    compute_respiration_rate (const arma::span sel = arma::span::all) const {
        // Base respiration rate
        const Vector _x = respiration_rate_parameter * arma::pow(m(sel), -.25);

        if (not cost_params.enabled) {
            return _x;
        }

        // Cost mechanism was enabled
        // Prepare species-specific vector of costs that come on top
        Vector cost_factor(_x.n_elem, arma::fill::zeros);

        // ... from choice of niche width
        const auto& cp_nw = cost_params.niche_width;
        if (cp_nw.mode == cost_mode::polynomial) {
            cost_factor += (
                cp_nw.offset +
                cp_nw.factor * arma::pow(s(sel) - cp_nw.center, cp_nw.exponent)
            );
        }
        else if (cp_nw.mode == cost_mode::exponential) {
            cost_factor += (
                cp_nw.offset +
                arma::exp(
                    cp_nw.factor *
                    arma::pow(s(sel) - cp_nw.center, cp_nw.exponent)
                )
            );
        }
        else {
            throw std::invalid_argument("cost mode not implemented!");
        }

        if (arma::any(cost_factor <= -1.)) {
            throw std::invalid_argument(fmt::format(
                "Got respiration rate cost factors <= -1 for one or more "
                "species! Check the cost parameters and make sure that they "
                "always sum to values > -1 !"
            ));
        }
        return _x % (1. + cost_factor);
    }

    /// Computes I_il, the feeding range overlap
    /** Depending on the presence of the `use_numerical_integration` tag, this
      * method will either return the analytical solution or perform a
      * numerical integration using Gauss quadrature.
      */
    template<std::size_t n_points = 25>
    double compute_feeding_range_overlap (const IDType i,
                                          const IDType l) const
    {
        using std::pow;
        using std::min;
        using std::max;
        using std::log10;

        // Extract parameters and compute log values
        const auto s_i = s(i);
        const auto s_l = s(l);
        const auto log_f_i = log10(f(i));
        const auto log_f_l = log10(f(l));

        // Analytical solution
        if constexpr (analytical_feeding_range_overlap) {
            const auto sig = pow(s_i, 2) + pow(s_l, 2);
            return
                  exp(- pow(log_f_i - log_f_l, 2) / (2. * sig))
                / sqrt(2. * M_PI * sig);
        }
        // else: numerical solution

        // Compute bounds
        const double b_i_min = log_f_i - quad_nsigma * s_i;
        const double b_i_max = log_f_i + quad_nsigma * s_i;
        const double b_l_min = log_f_l - quad_nsigma * s_l;
        const double b_l_max = log_f_l + quad_nsigma * s_l;

        if (b_i_max < b_l_min or b_l_max < b_i_min) {
            // Very little overlap ==> no need to compute it
            return 0.;
        }

        const double b_min = max(b_i_min, b_l_min);
        const double b_max = min(b_i_max, b_l_max);

        // Build function and use it to integrate
        // NOTE std::pow for exponent 2 is as fast as direct mulitplication
        auto f = [s_i, s_l, log_f_i, log_f_l](const double& log_m_j){
            return
                  (1. / (2. * M_PI * s_i * s_l))
                * exp(-pow(log_f_i - log_m_j, 2)/(2. * pow(s_i, 2)))
                * exp(-pow(log_f_l - log_m_j, 2)/(2. * pow(s_l, 2)))
            ;
        };

        return gauss_quadrature<n_points>(f, b_min, b_max);
    }

    /// Computes I_ii, the feeding range self-overlap
    /** If `use_numerical_integration` is given, this uses Gauss quadrature.
      * Otherwise, it will simply call `compute_feeding_range_overlap`, which
      * will then compute and return the analytical solution.
      */
    template<std::size_t n_points = 25>
    double compute_feeding_range_overlap (const IDType i) const
    {
        // For analytical solution, forward directly to general implementation
        if constexpr (analytical_feeding_range_overlap) {
            return compute_feeding_range_overlap(i, i);
        }
        // else: numerical solution

        // Extract parameters and compute log values
        const auto s_i = s(i);
        const auto log_f_i = std::log10(f(i));

        // Compute bounds
        const double b_min = log_f_i - quad_nsigma * s_i;
        const double b_max = log_f_i + quad_nsigma * s_i;

        // Build function and use it to integrate
        auto f = [s_i, log_f_i](const double& log_m_j){
            return
                  (1. / (2. * M_PI * s_i * s_i))
                * exp(-2. * pow(log_f_i - log_m_j, 2)/(2. * std::pow(s_i, 2)))
            ;
        };

        return gauss_quadrature<n_points>(f, b_min, b_max);
    }

    /// Computes and updates the trophic level for a single species
    void update_trophic_level (const IDType i) {
        auto& TL_i = trophic_level(i);

        if (is_resource(i)) {
            TL_i = 1.;
            return;
        }
        const Vector diet = arma::normalise(g.row(i).t() % lambda, 1);
        TL_i = 1. + arma::dot(trophic_level, diet);
    }

    /// Computes and updates the flow-based trophic level for all species
    /** Does this repeatedly until the highest absolute difference of changes
      * is below `max_abs_diff`.
      * Uses at most `max_n` iterations.
      *
      * This updates the `trophic_level` and `TL_error` tracking variables.
      *
      * \return The highest absolute difference between the last two iterations
      */
    double update_trophic_levels (const double max_abs_diff = 1.e-3,
                                  const unsigned max_n = 10)
    {
        using Utopia::Utils::imbue_index;

        log->debug("Updating trophic levels (max_n: {}) ...", max_n);

        auto n = 0u;
        const auto& _g = latest_g();
        Vector diet;
        Vector last_TL = trophic_level;
        double last_max_abs_diff = std::numeric_limits<double>::infinity();

        while (n < max_n and last_max_abs_diff > max_abs_diff) {
            imbue_index(trophic_level, [&](const auto i) -> double {
                if (is_resource(i)) {
                    return 1.;
                }
                diet = arma::normalise(_g.row(i).t() % lambda, 1);
                return 1. + arma::dot(trophic_level, diet);
            });

            n++;
            last_max_abs_diff = arma::abs(last_TL - trophic_level).max();
            last_TL = trophic_level;
            // fmt::print("{}: last_max_abs_diff: {}\n", n, last_max_abs_diff);
        }
        log->trace("Trophic levels updated. (n: {}, error: {:.2g})",
                   n, last_max_abs_diff);

        // Keep track of the last error to benchmark quality if n > max_n
        TL_error = last_max_abs_diff;
        return last_max_abs_diff;
    }

    /// Updates the property-tracking variables
    /** The following variables are updated here:
      *     - age
      *     - trophic_level
      *     - mean_trophic_level
      *     - last_species_addition_mean_TL
      *
      * Furthermore, the last update time is stored in
      * `_last_property_update_time`.
      * Repeated calls for the same `time` are ignored.
      */
    void update_property_tracking_variables (Time time) {
        if (time < _last_property_update_time) {
            throw std::invalid_argument(fmt::format(
                "Time may not be smaller than last property update time! "
                "Was {} < {} -- this should not have happened!",
                time, _last_property_update_time
            ));
        }

        // Compute time since last update and check if there's something to do
        const Time dt = time - _last_property_update_time;
        if (dt == 0) {
            log->trace("Property tracking variables need no update (dt == 0)");
            return;
        }
        log->debug("Updating property tracking variables ... (dt: {})", dt);

        // Update trophic level (with reduced number of iterations)
        update_trophic_levels(1.e-3, 3);

        // Now update the mean trophic level
        mean_trophic_level =
            (mean_trophic_level % age + trophic_level*dt) / (age + dt);

        // Can update the age now
        age += dt;

        // Only update last species' mean TL if it is still alive
        if (get_last_added_species_id() == ids.back()) {
            last_species_addition_mean_TL = mean_trophic_level.back();
        }

        _last_property_update_time = time;
    }

    /// Overload for update_property_tracking_variables using the model time
    void update_property_tracking_variables () {
        return update_property_tracking_variables(get_model_time());
    }


    // -- Depot and Environment -----------------------------------------------

    /// Perform the environment-related
    void perform_environmental_dynamics () {
        static_assert(
            envmod_enabled, "Only possible if `with_envmod` is set!"
        );
        auto& _E = E();

        // Distribute all energy from depot to reservoirs; let existing decay
        _E += distr_to_env * D() - (env.decay_rate % _E);
        D().fill(0.);

        // Update modification matrix to get new modification factors
        env.update_total_modification(_E);
    }


    // -- Network -------------------------------------------------------------
    // .. Thresholding ........................................................

    /// Builds the adjacency matrix from the current functional response matrix
    /** Avoids re-computation and copying by caching the matrix and returning
      * a const-reference to it instead of a copy.
      *
      * Like `extract_network`, this uses default `network_extraction_params`
      * if no parameters are given.
      * It implements the same thresholding algorithms as that method.
      */
    const Matrix& adjacency_matrix (Config params = {}) {
        using Utopia::Utils::in_range;

        if (_adj_cache_time != std::numeric_limits<Time>::max()
            and _adj_cache_time == get_model_time())
        {
            this->log->trace("Returning cached adjacency matrix ...");
            return _adj;
        }
        this->log->trace("Extracting adjacency matrix ...");

        // Prepare parameters
        if (not params.size()) {
            params = network_extraction_params;
        }
        const auto mode = get_as<std::string>("threshold_mode", params);
        const auto threshold = get_as<double>("threshold", params);

        if (mode != "average") {
            throw std::invalid_argument(fmt::format(
                "Invalid threshold mode '{}'! Supported:  average", mode
            ));
        }

        // Prepare adjacency matrix, re-using cached one to avoid re-allocation
        const auto& ia_mat = latest_g();
        _adj.resize(ia_mat.n_rows, ia_mat.n_cols);
        _adj.zeros();

        // Re-used objects
        double mean_ia_strength;
        double thrs_ia_strength;
        RowVector ia_strength(_adj.n_cols, arma::fill::zeros);
        arma::uvec no_ia_idcs;

        // Populate the matrix row by row (index referring to predator species)
        for (auto i : in_range(ia_mat.n_rows)) {
            // Retrieve corresponding interaction strengths, then compute the
            // mean interaction strength for this predator, from which the
            // threshold interaction strength will be computed
            ia_strength = ia_mat.row(i) % lambda.t();
            mean_ia_strength = arma::mean(ia_strength);

            if (mean_ia_strength <= PREC) {
                // Zero interaction, adj. matrix has no entries in this row
                continue;
            }
            thrs_ia_strength = threshold * mean_ia_strength;

            // Find indices without interactions and set those values to zero
            // in the interaction strength row vector
            no_ia_idcs = arma::find(ia_strength < thrs_ia_strength);
            ia_strength(no_ia_idcs).fill(0.);

            // Now set the corresponding row in the adjacency matrix
            _adj.row(i) = ia_strength;
        }

        _adj_cache_time = get_model_time();
        this->log->trace("Adjacency matrix successfully computed and cached.");

        return _adj;
    }

    /// Extracts a directed network from the functional response matrix
    /** The edges are directed parallel to the energy flow, i.e. edge source
      * vertices are prey species and destinations are predator species.
      * There are guaranteed to be *no* parallel edges, but there may be
      * self-edges.
      *
      * The return format is such that mEEvo::store_network can directly
      * write it ...
      *
      * The following vertex properties are available:
      *     - `trophic_level`
      *     - `mean_ia_strength`
      *     - `biomass_density`
      *
      * The following edge properties are available:
      *     - `ia_strength`, the functional response matrix element, weighted
      *       by the conversion efficiency of that connection. If environment
      *       modifications were enabled, this will be the *modified*
      *       functional response matrix!
      *     - `biomass_flow`, the outflow from the respective prey, i.e. not
      *       weighted by the conversion efficiency
      *     - `diet_fraction`, the relative interaction strength within the
      *       diet of this predator, in [0, 1]. Due to the thresholding, the
      *       sum over all in-edges of a predator may not add up to 1!
      *     - `mod_factor`, the environment modification factors; will always
      *       write 1 if environment modifications were *not* enabled.
      *
      * They can be deactivated by excluding them from the `include_properties`
      * parameter. In such a case, they are still part of the return value, but
      * the data will be empty.
      *
      * \param   params     The extraction parameters. If not given, will use
      *                     those given at initialization.
      *                     Expected keys: `threshold_mode`, `threshold`,
      *                     `include_properties` (list of names of those
      *                     vertex or edge properties that should be collected
      *                     and returned alongside the network structure).
      *
      * \returns 4-tuple of: species IDs, edge list (Nx2), vertex properties,
      *          and edge properties. The vertex and edge properties are
      *          themselves tuples of `(name, data)` pairs.
      */
    auto extract_network (Config params = {}) {
        using Utopia::Utils::in_range;
        using Utopia::Utils::contains;
        using Utopia::Utils::append_element;

        // May have to use the default parameters
        if (not params.size()) {
            params = network_extraction_params;
        }

        // Extract them ...
        const auto mode = get_as<std::string>("threshold_mode", params);
        const auto threshold = get_as<double>("threshold", params);
        const auto incl_props = get_as<std::vector<std::string>>(
            "include_properties", params,
            {
                // Vertex properties
                "trophic_level", "mean_ia_strength", "biomass_density",

                // Edge properties
                "ia_strength", "biomass_flow", "diet_fraction", "mod_factor"
            }
        );

        if (mode != "average") {
            throw std::invalid_argument(fmt::format(
                "Invalid threshold mode '{}'! Supported:  average", mode
            ));
        }
        // NOTE Everything below assumes the "average" mode; if other modes are
        //      to be implemented, may need to modularize this differently ...

        // For some properties, evaluate whether they should be added or not;
        // otherwise the evaluation is unnecessarily repeated in the loop
        const auto should_include = [&](const auto& prop_name){
            return contains(incl_props, prop_name);
        };

        const bool incl_mean_ia_strength = should_include("mean_ia_strength");
        const bool incl_ia_strength = should_include("ia_strength");
        const bool incl_biomass_flow = should_include("biomass_flow");
        const bool incl_diet_fraction = should_include("diet_fraction");
        const bool incl_mod_factor = should_include("mod_factor");

        // Now ready to gather vertex and edge data
        // Vertices are simply the species IDs.
        const IDVector species_ids = this->ids;

        // The edges container needs to be populated sequentially, done below.
        IDMatrix edges(0, 2);

        // The to-be-populated containers for vertex and edge properties, each
        // with zero rows. Depending on the `include_properties` parameter,
        // they will not be populated below, but simply left empty...
        Vector vp_biomass_density(0, arma::fill::zeros);
        Vector vp_trophic_level(0, arma::fill::zeros);
        Vector vp_mean_ia_strength(0, arma::fill::zeros);
        Vector ep_ia_strength(0, arma::fill::zeros);
        Vector ep_flow(0, arma::fill::zeros);
        Vector ep_diet_fraction(0, arma::fill::zeros);
        Vector ep_mod_factor(0, arma::fill::zeros);

        // Helper variables for extraction of network structure and properties
        Vector ia_strength;
        Vector diet;
        Vector flow;
        Vector mod_factors;
        double mean_ia_strength;
        double thrs_ia_strength;
        IDVector prey_idcs;
        IDMatrix src_and_dest_ids;

        // Now go over all rows of the interaction matrix, i.e.: predators,
        // and add entries for edges that have as destination the selected
        // predator species (parallel to direction of energy flow).
        for (const auto i : in_range(species_ids.size())) {
            ia_strength = latest_g().row(i).t() % lambda;

            // Compute the mean interaction strength for this predator, from
            // which the threshold will be computed
            mean_ia_strength = arma::mean(ia_strength);
            if (incl_mean_ia_strength) {
                append_element(vp_mean_ia_strength, mean_ia_strength);
            }

            // Don't create an edge if it's zero: equivalent to no interaction
            if (mean_ia_strength <= PREC) {
                continue;
            }

            // Now compute the threshold
            thrs_ia_strength = threshold * mean_ia_strength;

            // The edge source vertices are those of the prey species where the
            // interaction strength surpasses the threshold.
            // Get the indices first ...
            prey_idcs = arma::find(ia_strength >= thrs_ia_strength);

            // Build a two-column matrix (edge source and destination, i.e.
            // prey and predator species IDs, respectively), which is then
            // added to the edge list.
            src_and_dest_ids.set_size(prey_idcs.size(), 2);
            src_and_dest_ids.col(0) = species_ids.elem(prey_idcs);
            src_and_dest_ids.col(1).fill(species_ids(i));

            edges.insert_rows(edges.n_rows, src_and_dest_ids);

            // Edge properties
            if (incl_ia_strength) {
                ep_ia_strength.insert_rows(ep_ia_strength.n_rows,
                                           ia_strength.elem(prey_idcs));
            }
            if (incl_biomass_flow) {
                flow = latest_g().row(i).t() * B(i);
                ep_flow.insert_rows(ep_flow.n_rows, flow.elem(prey_idcs));
            }
            if (incl_diet_fraction) {
                diet = arma::normalise(ia_strength, 1);
                ep_diet_fraction.insert_rows(ep_diet_fraction.n_rows,
                                             diet.elem(prey_idcs));
            }
            if (incl_mod_factor) {
                if constexpr (envmod_enabled) {
                    mod_factors = env.mod_factors.row(i).t();
                    ep_mod_factor.insert_rows(ep_mod_factor.n_rows,
                                              mod_factors.elem(prey_idcs));
                }
                else {
                    mod_factors.copy_size(prey_idcs);
                    mod_factors.ones();
                    ep_mod_factor.insert_rows(ep_mod_factor.n_rows,
                                              mod_factors);
                }
            }
        }

        // Add the vertex properties
        if (should_include("trophic_level")) {
            update_trophic_levels();
            vp_trophic_level = this->trophic_level;
        }
        if (should_include("biomass_density")) {
            vp_biomass_density = this->B();
        }

        // Aggregate all information into a 4-tuple
        return std::make_tuple(
            species_ids,
            edges,
            std::make_tuple(
                std::make_pair("trophic_level", vp_trophic_level),
                std::make_pair("mean_ia_strength", vp_mean_ia_strength),
                std::make_pair("biomass_density", vp_biomass_density)
            ),
            std::make_tuple(
                std::make_pair("ia_strength", ep_ia_strength),
                std::make_pair("biomass_flow", ep_flow),
                std::make_pair("diet_fraction", ep_diet_fraction),
                std::make_pair("mod_factor", ep_mod_factor)
            )
        );
    }

    // .. Measures ............................................................

    /// Computes the number of edges of an adjacency matrix
    unsigned int num_edges (const Matrix& adj) const {
        return arma::accu(adj != 0);
    }

    /// Overload that uses the current adjacency matrix
    unsigned int num_edges () {
        return num_edges(adjacency_matrix());
    }

    /// Computes connectance of the given adjacency matrix
    double connectance (const Matrix& adj) const {
        if (adj.n_elem > 0) {
            return static_cast<double>(num_edges(adj)) / adj.n_elem;
        }
        return NaN;
    }

    /// Overload that uses the current adjacency matrix
    double connectance () {
        return connectance(adjacency_matrix());
    }

    /// Computes the trophic incoherence
    /** As the adjacency matrix has weighted edges, the regular notion of the
      * trophic coherence needs to be adjusted to accomodate edge weights.
      *
      * This method implements the formulation proposed by MacKay, Johnson,
      * and Sansom in 2020 (see 10.1098/rsos.201138), denoted as $F_0$ there.
      * It does not compute the trophic levels in the same way that they do,
      * though, but the measure of trophic incoherence applies equally to any
      * computation of trophic levels.
      */
    double trophic_incoherence (const Matrix& adj, const Vector& TLs) const {
        using Utopia::Utils::in_range;

        double weighted_diffs = 0.;
        double sum_of_weights = 0.;
        double TL_pred = NaN;
        Vector dTL;

        log->trace("Computing trophic incoherence ...");

        // Go over the adjacency matrix row by row (index referring to predator
        // species) and then use vector-based calculations on each column
        for (auto i : in_range(adj.n_rows)) {
            TL_pred = TLs(i);
            dTL = TL_pred - TLs - 1;

            weighted_diffs += arma::accu(adj.row(i).t() % (dTL % dTL));
            sum_of_weights += arma::accu(adj.row(i));
        }
        if (sum_of_weights <= 0.) {
            return NaN;
        }
        return weighted_diffs / sum_of_weights;
    }

    /// Overload that uses the current adjacency matrix and trophic levels
    double trophic_incoherence () {
        return trophic_incoherence(adjacency_matrix(), trophic_level);
    }

    /// The functional diversity change since the last species addition
    auto delta_trophic_incoherence () {
        return trophic_incoherence() - _TL_incoh_before_addition;
    }


    // -- Extinction and Mutation ---------------------------------------------

    /// True if any species (not: resources) is below the critical density
    bool check_extinction () const {
        if (n_species() == 0) {
            return false;
        }
        return arma::any(B(species()) < B_min(species()));
    }

    /// Determines extinct species and removes all corresponding entries
    /** \returns The entries that were removed
      */
    arma::uvec remove_extinct () {
        // Determine the indices of to-be-removed species, then remove.
        // NOTE With resources' extinction threshold being -inf, they will
        //      never be part of this list of indices.
        const arma::uvec extinct = arma::find(B() < B_min);
        remove_species(extinct);

        return extinct;
    }

    /// Removes the species with the given indices
    void remove_species (const arma::uvec& idcs) {
        if (idcs.empty()) {
            log->trace("No species given to remove.");
            return;
        }
        if (log->should_log(spdlog::level::debug)) {
            using Utopia::Utils::format_vec;
            log->debug(
                "Removing {:d} species: {} ...", idcs.size(), format_vec(idcs)
            );
        }

        // Ensure that properties are updated, such that the age etc is correct
        update_property_tracking_variables();

        // Associate these new extinctions to the last-added species
        n_extinctions += idcs.size();

        // Invalidate caches
        _need_FD_update = true;
        _adj_cache_time = std::numeric_limits<Time>::max();

        // Update the total number of viable species by evaluating the age of
        // the species that are to be removed
        n_viable += arma::accu(age(idcs) >= viability_age);

        // Before shedding rows, let the trackers gather information
        removal_times_tracker.track_multiple(*this, idcs);
        property_tracker.track_multiple(*this, idcs);

        // Now, shed the corresponding rows from all column vectors and shed
        // the corresponding rows and (!) columns from all matrices

        // -- Vectors
        ids.shed_rows(idcs);
        B().shed_rows(idcs);
        m.shed_rows(idcs);

        f.shed_rows(idcs);
        s.shed_rows(idcs);
        lambda.shed_rows(idcs);
        x.shed_rows(idcs);
        ai.shed_rows(idcs);
        h.shed_rows(idcs);
        B_min.shed_rows(idcs);

        age.shed_rows(idcs);
        trophic_level.shed_rows(idcs);
        mean_trophic_level.shed_rows(idcs);

        // -- Depot and Environment-related objects
        frac_diverted.shed_rows(idcs);

        if constexpr (envmod_enabled) {
            log->trace("Removing depot-related entries ...");

            D().shed_rows(idcs);
            _frac_used.shed_rows(idcs);

            // The columns of the distribution matrix correspond to species
            distr_to_env.shed_cols(idcs);

            // Rows and columns need to be shed _individually_ from sparse
            // matrices; have to remove "from the back", thus needing the index
            // list in sorted __reverse__ order:
            const arma::uvec idcs_desc = arma::sort(idcs, "descend");

            // ... alternatively, if the sparse matrices are empty, they can
            // directly be resized to the expected size.
            const auto n_target = D().size();

            log->trace("Removing entries from signature matrices ...");
            env.apply_to_signatures([&](auto, auto& sig){
                // sig.print(fmt::format("signature matrix {} (before)", i));

                for (auto idx : idcs_desc) {
                    if (sig.n_nonzero < 1) {
                        // If there are no non-zero elements left, e.g. due to
                        // elements having been shed, directly jump to the
                        // target size.
                        // NOTE _Not_ doing this leads to stochastic segfaults!
                        sig.set_size(n_target, n_target);
                        break;
                    };

                    sig.shed_row(idx);
                    sig.shed_col(idx);

                    // sig.print(fmt::format(
                    //     "signature matrix {} (after removing col/row {})",
                    //     i, idx)
                    // );
                }
            });

            // Also update some temporary objects
            _g_mod.shed_rows(idcs);
            _g_mod.shed_cols(idcs);
        }

        // -- Resource-related objects
        // NOTE Don't need to shed from G, R & K; they refer only to resources,
        //      which are not to be changed by this method (or at all).

        // -- Matrices
        log->trace("Removing entries from matrices ...");

        g.shed_rows(idcs);
        g.shed_cols(idcs);

        a.shed_rows(idcs);
        a.shed_cols(idcs);

        c.shed_rows(idcs);
        c.shed_cols(idcs);

        // NOTE Can't yet recompute trophic level etc., because it needs an
        //      the updated functional response, which will be computed in
        //      the next iteration anyway...
    }

    /// Adds a species with the given traits
    /** To ensure energy conservation, the biomass density is always split-off
      * from some parent species. If this would lead to a negative biomass
      * density for the parent species, a so-called "fallback" species is used
      * instead, e.g. the environment which can be assumed large enough that
      * this does not constitute a big error.
      * This biomass correction is kept track of and can be used to assess
      * whether too much energy was taken from the environment.
      *
      * The parameters all refer to the new species' trait value. Parameters
      * `_frac_diverted` and `_distr_to_env` are only relevant if environment
      * modifications are to be used; they are ignored otherwise.
      *
      * \note       While species mutation parameters are supplied to this
      *             method, the change of the *environment* (i.e. the signature
      *             matrices) is taking place in the Environment object, not
      *             here. Subequently, corresponding parameters need to be
      *             specified there.
      *
      * \param _m         Specific body mass
      * \param _f         Feeding range center
      * \param _s         Feeding range width
      * \param _lambda    Conversion efficiency *from* this species
      * \param parent_idx The parent species' index; effectively, this is the
      *                   species the biomass is taken from to ensure mass
      *                   conservation.
      * \param _frac_diverted  Fraction of energy diverted to the depot.
      * \param _distr_to_env   Distribution vector from depot to environment.
      *                        If this is zero-sized, it will be not be set.
      * \param _eps       The biomass density factor (in units of specific body
      *                   mass) to use for this new species.
      *                   If negative, the default value will be used.
      *
      * \return  The *index* of the newly added species.
      */
    auto add_species (const double _m, const double _f, const double _s,
                      const double _lambda,
                      const arma::uword parent_idx = 0,
                      const double _frac_diverted = NaN,
                      const Vector& _distr_to_env = {},
                      double _eps = -1)
    {
        using Utopia::Utils::append_element;
        using Utopia::Utils::append_row_col;
        using Utopia::Utils::append_col;

        log->trace("Preparing to add new species ...");

        // Before adding the species, update the tracking variables in order to
        // align the age of this new species (0) with the latest update time.
        // Otherwise the age calculation may be incorrect, because the time
        // delta is added to all species equally, regardless of the time
        // between two updates they were added at.
        update_property_tracking_variables();

        // Also, let the species effect tracker know ... this will store the
        // effect of the *previously* added species.
        log->trace("Updating species effect tracker ...");
        species_effect_tracker.track_latest(*this);

        // Can now (after it was tracked) update the trophic incoherence value
        if (_track_TL_incoherence) {
            _TL_incoh_before_addition = trophic_incoherence();
        }

        // Need a new ID; also compute the corresponding current index
        log->trace("Determining index for offspring species ...");
        const IDType child_id = next_id++;
        append_element(ids, child_id);
        const auto child_idx = ids.size() - 1;
        species_count++;

        log->debug("Adding new species with ID {} ...", child_id);
        log->trace("  parent_idx: {},  eps: {}", parent_idx, _eps);
        log->trace("  m: {:.4g},  f: {:.4g},  s: {:.4g},  "
                   "frac_diverted: {:.4g}", _m, _f, _s, _frac_diverted);

        // Split-off biomass from the "parent", ensuring mass conservation.
        // Need to check that this does not lead to negative biomass for the
        // parent; if it does, there's nothing that can be done here, because
        // the choice of the parent needs to happen outside of this method.
        // In such a case, the biomass *may* be taken from the so-called
        // "fallback" species, e.g. the environment, which is assumed to have
        // a large enough biomass that this does not constitute a problem.
        // This is the so-called "biomass correction mechanism" ...
        // If the correction is disabled, both species will be set to zero
        // biomass density, thus becoming extinct at the next check.
        if (_eps <= 0.) {
            _eps = eps;
        }

        const auto _B = _eps * _m;
        append_element(B(), _B);
        B(parent_idx) -= _B;

        if (B(parent_idx) < 0.) {
            _biomass_corrector(B(), parent_idx, child_idx);
        }

        // -- Add trait vector elements
        log->trace("Expanding trait vectors ...");
        append_element(m, _m);
        append_element(f, _f);
        append_element(s, _s);
        append_element(lambda, _lambda);
        append_element(frac_diverted, _frac_diverted);

        // -- Adjust Depot and Environment-related vectors
        if constexpr (envmod_enabled) {
            log->trace("Expanding depot and environment ...");
            append_element(D());

            append_col(distr_to_env);
            if (_distr_to_env.size() > 0) {
                distr_to_env.tail_cols(1) = _distr_to_env;
            }

            env.apply_to_signatures([](auto, auto& sig){
                append_row_col(sig);
            });

            // Also extend temporary variables, for consistency. The NaNs will
            // not take part in any calculations and are merely placeholders
            // until they are computed ...
            append_row_col(_g_mod, NaN);
            append_element(_frac_used, NaN);
        }

        // -- Adjust remaining species parameter vectors
        // Derived species parameters
        log->trace("Expanding species parameter vectors ...");
        append_element(h);
        append_element(ai);
        append_element(x);
        append_element(B_min);

        // Variables for property tracking
        append_element(age);
        append_element(trophic_level, 2.);   // will be >= 2., start there ...
        append_element(mean_trophic_level);  // set to correct value below!
        n_extinctions = 0;
        _need_FD_update = true;

        // Resize matrices
        log->trace("Expanding matrices ...");
        append_row_col(g);
        append_row_col(a);
        append_row_col(c);

        // Update derived parameters and matrices, restricting (where possible)
        // to an update of the affected rows and columns only...
        log->trace("Updating parameters and matrices ...");
        update_species_parameters(arma::span(child_idx, child_idx));
        update_attack_matrix({child_idx});
        update_competition_matrix({child_idx});

        // Although the following commands within perform_step will update
        // responses soon after this call, do so here already to have all
        // values set up properly (e.g. to allow them being written out ...)
        update_responses(B());
        update_trophic_levels();

        // Can now set the initial value for the mean trophic level
        mean_trophic_level(child_idx) = trophic_level(child_idx);

        // ... and the tracker variables
        last_species_addition_mean_TL = mean_trophic_level(child_idx);
        last_species_addition_time = get_model_time();

        // Species successfully added now.
        log->trace(
            "Offspring species {} added at index {}.", child_id, child_idx
        );

        // Invalidate caches
        _adj_cache_time = std::numeric_limits<Time>::max();

        // Update trackers for this new species, communicating the parent's ID
        log->trace("Updating trackers ...");
        traits_tracker.track_single(*this, child_idx, ids(parent_idx));
        addition_times_tracker.track_single(*this, child_idx);

        return child_idx;
    }

    /// Adds a species with information from config
    auto add_species (const Config& species_cfg) {
        if constexpr (envmod_enabled) {
            throw std::invalid_argument(
                "Adding species from config not supported with envmod_enabled!"
            );
        }

        const auto _m = get_as<double>("m", species_cfg);
        double _f;
        if (species_cfg["mf_ratio"] and species_cfg["mf_ratio"].IsScalar()) {
            _f = _m / get_as<double>("mf_ratio", species_cfg);
        }
        else {
            _f = get_as<double>("f", species_cfg);
        }
        const auto _s = get_as<double>("s", species_cfg);
        const auto _lambda = get_as<double>("lambda", species_cfg,
                                            arma::mean(lambda(species())));
        const auto _parent_idx =
            get_as<arma::uword>("parent_idx", species_cfg, 0);
        const auto _eps = get_as<double>("eps", species_cfg, -1.);

        return add_species(
            _m,
            _f,
            _s,
            _lambda,
            _parent_idx,
            NaN,  // frac_diverted
            {},   // distr_to_env
            _eps
        );
    }

    /// Adds a new mutant species, mutating traits of a randomly chosen parent
    /** Calls `add_species` and returns the child's index (not ID!).
      */
    auto add_mutant () {
        // Determine the parent's index
        const auto parent_idx = select_parent();
        log->trace("Determined parent index for mutant: {}", parent_idx);

        // Determine the main mutated offspring traits
        const auto [_m, _f, _s, _lambda] = draw_mutant_traits(parent_idx);

        if constexpr (not envmod_enabled) {
            // That's all that's necessary. Add the species.
            return add_species(_m, _f, _s, _lambda, parent_idx);
        }
        // else: need some more traits: diversion ratio and distribution
        const auto [_frac_diverted, _distr_to_env] =
            draw_mutant_env_traits(parent_idx);

        const auto child_idx = add_species(_m, _f, _s, _lambda, parent_idx,
                                           _frac_diverted, _distr_to_env);

        // Finally, let the environment take care of updating the corresponding
        // signature matrices; this can be considered the mutation process.
        log->trace("Mutating signature matrices for offspring ...");
        env.mutate_signatures(child_idx, parent_idx, rng);

        log->trace("Successfully added mutant.");
        return child_idx;
    }

    auto select_parent () const {
        log->trace("Invoking parent selector ...");
        return _parent_selector(*this);
    }

    /// Returns the offspring's traits (m, f, s, lambda)
    Traits draw_mutant_traits (const arma::uword parent_idx) const {
        log->trace("Invoking trait mutation functor ...");
        return _trait_mutation(*this, parent_idx);
    }

    /// Draws the offspring's envrionment traits (frac_diverted, distr_to_env)
    EnvTraits draw_mutant_env_traits (const arma::uword parent_idx) const {
        log->trace("Invoking environment trait mutation functor ...");
        return _envtrait_mutation(*this, parent_idx);
    }


    // -- Helpers -------------------------------------------------------------

    /// Get a random species index
    arma::uword random_species_idx () const {
        return IntDist(n_resources(), size_B() - 1)(*rng);
    }

    /// Performs gauss quadrature on the given functional and bounds
    /** This uses boost::math::quadrature::gauss, but unlike that function, it
      * returns zero if the b1 <= b0, i.e. if the bounds are invalid.
      */
    template<std::size_t n_points, class Func>
    static double gauss_quadrature (Func f, const double b0, const double b1) {
        using boost::math::quadrature::gauss;

        if (b1 > b0) {
            return gauss<double, n_points>::integrate(f, b0, b1);
        }
        else {
            // TODO Warning?
            return 0.;
        }
    }


public:
    // -- Functor generation --------------------------------------------------

    /// Builds a biomass correction functor from the given parameters
    /** The functor attaches at the point *after* the biomass was transferred.
      * This means, the parent biomass will be negative already.
      *
      * The functor should adjust the referenced vector in a way that the
      * specified entries become positive.
      */
    BiomassCorrectorFunc
    _build_biomass_corrector (const Config& params)
    {
        log->debug("  Setting up functor for biomass correction ...");
        const auto mode = get_as<std::string>("mode", params);
        log->info("Using biomass correction mode '{}'.", mode);

        if (mode == "keep_none") {
            return [](Vector& _B, const auto parent_idx, const auto child_idx){
                _B(parent_idx) = 0.;
                _B(child_idx) = 0.;
            };
        }
        else if (mode == "keep_parent") {
            return [](Vector& _B, const auto parent_idx, const auto child_idx){
                // Transfer back to parent
                _B(parent_idx) += _B(child_idx);
                _B(child_idx) = 0.;
            };
        }
        else if (mode == "keep_offspring") {
            const auto fallback_idx = get_as<arma::uword>(
                "fallback_parent_idx", biomass_correction_params
            );

            return [this, fallback_idx](
                Vector& _B, const auto parent_idx, const auto /* child_idx */
            ){
                using std::fabs;
                _B(fallback_idx) -= fabs(_B(parent_idx));
                this->biomass_correction += fabs(_B(parent_idx));
                _B(parent_idx) = 0.;
            };
        }
        throw std::invalid_argument(fmt::format(
            "Invalid biomass_correction mode: {}! Choose from: keep_parent, "
            "keep_offspring, and keep_none.", mode
        ));
    }

    /// Builds a parent selector functor
    ParentSelectorFunc _build_parent_selector (const Config& params) const {
        log->debug("  Setting up functor for parent selection ...");
        const auto mode = get_as<std::string>("mode", params);
        log->info("Using parent selection mode '{}'.", mode);

        // NOTE For some reason, this-capture does not seem to work for these
        //      lambdas, always leading to segfaults ... that's why the system
        //      is given as an argument.

        if (mode == "uniform") {
            // Draw uniformly
            // Allow specifying a minimum size (in units of B_min)
            const auto min_parent_size =
                get_as<double>("min_parent_size", params, 0.);

            return [min_parent_size](const auto& sys){
                arma::uword parent_idx;

                for (auto i=0u; i < 100; i++) {
                    parent_idx = sys.random_species_idx();

                    if (sys.B()(parent_idx)
                        > min_parent_size * sys.B_min(parent_idx))
                    {
                        // Parent species is large enough
                        break;
                    }
                    // else: draw again
                }

                return parent_idx;
            };
        }
        else if (mode == "by_biomass_density") {
            return [](const auto& sys) -> arma::uword {
                const auto sel = sys.species();
                return draw_weighted<Vector>(
                    sys.B(sel),
                    sys.B_indices(sel),
                    sys.rng,
                    "parent selection by biomass density"
                );
            };
        }
        else if (mode == "by_number_density") {
            return [](const auto& sys) -> arma::uword {
                const auto sel = sys.species();
                return draw_weighted<Vector>(
                    sys.B(sel) / sys.m(sel),
                    sys.B_indices(sel),
                    sys.rng,
                    "parent selection by number density"
                );
            };
        }
        else if (mode == "by_generation_time") {
            const auto exponent =
                get_as<double>("generation_time_exponent", params, -0.25);

            return [exponent](const auto& sys) -> arma::uword {
                const auto sel = sys.species();
                return draw_weighted<Vector>(
                    arma::pow(sys.m(sel), exponent),
                    sys.B_indices(sel),
                    sys.rng,
                    "parent selection by number density"
                );
            };
        }
        else {
            throw std::invalid_argument(fmt::format(
                "Invalid parent selection mode: {}!\nAvailable modes:  "
                "uniform, by_biomass_density, by_number_density, "
                "by_generation_time",
                mode
            ));
        }
    }

    /// Builds the trait mutation functor
    /** This also updates the internally stored `mutation_params` to the given
      * value, such that a copy of the parameters is available.
      */
    TraitMutationFunc _build_trait_mutation_func (const Config& params) {
        using std::log10;
        using std::pow;
        using Range = std::array<double, 2>;

        log->debug("  Setting up functor for trait mutation ...");
        const auto mode = get_as<std::string>("mode", params);
        log->info("Using trait mutation mode '{}'.", mode);

        const auto& mp = params;
        mutation_params = YAML::Clone(mp);
        log->trace("Mutation parameters updated with those for new functor.");
        // NOTE To not have side-effects, this should be done after the functor
        //      was built, I know ...

        if (mode == "Allhoff2015") { // .......................................
            const auto m_range = get_as<Range>("m_range", mp);
            const auto f_log10_range = get_as<Range>("f_log10_range", mp);
            const auto s_range = get_as<Range>("s_range", mp);
            const auto inheritance_strength =
                get_as<double>("inheritance_strength", mp);

            return [
                m_range, f_log10_range, s_range, inheritance_strength
            ](const auto& sys, const auto parent_idx) -> Traits {
                // RNG drawing helpers
                const auto pow10 = [](auto v){ return pow(10., v); };
                const auto draw_base10 = [&](const auto low, const auto high){
                    return pow10(RealDist(low, high)(*sys.rng));
                };
                // Note: Arguments are meant to be on log scale.

                // Specific mass
                const auto m_parent = sys.m(parent_idx);
                auto _m = draw_base10(log10(m_range[0] * m_parent),
                                      log10(m_range[1] * m_parent));

                // Feeding range center
                auto _f = draw_base10(log10(_m) + f_log10_range[0],
                                      log10(_m) + f_log10_range[1]);

                // Niche width
                auto _s = RealDist(s_range[0], s_range[1])(*sys.rng);

                // Conversion efficiency: always same as parent
                const auto _lambda = sys.lambda(parent_idx);

                // Apply inheritance strength: mix parent's trait value back in
                const auto inh_strength = inheritance_strength;
                if (inh_strength > 0.) {
                    sys.log->trace(
                        "Applying inheritance strength {} ...", inh_strength
                    );
                    auto _mf_ratio = _m / _f;  // important to do this first!

                    _m = mix_values_log(m_parent,       _m, inh_strength);
                    _s = mix_values(sys.s(parent_idx),  _s, inh_strength);

                    _mf_ratio = mix_values_log(
                        m_parent / sys.f(parent_idx), _mf_ratio, inh_strength
                    );
                    _f = _m / _mf_ratio;
                }

                return {_m, _f, _s, _lambda};
            };
        }
        else if (mode == "normal") { // .......................................
            const auto sigma_m = get_as<double>("sigma_m", mp);
            const auto sigma_mf_ratio = get_as<double>("sigma_mf_ratio", mp);
            const auto sigma_s = get_as<double>("sigma_s", mp);

            const auto& bounds = get_as<Config>("bounds", mp);
            const auto bounds_m = get_as<Range>("m", bounds);
            const auto bounds_mf_ratio = get_as<Range>("mf_ratio", bounds);
            const auto bounds_s = get_as<Range>("s", bounds);

            return [
                sigma_m, sigma_mf_ratio, sigma_s,
                bounds_m, bounds_mf_ratio, bounds_s
            ](const auto& sys, const auto parent_idx) -> Traits {
                const auto pow10 = [](auto v){ return pow(10., v); };

                // Get parent information
                const auto m_parent = sys.m(parent_idx);
                const auto f_parent = sys.f(parent_idx);
                const auto s_parent = sys.s(parent_idx);
                const auto mf_parent = m_parent / f_parent;

                // Specific mass
                auto m_dist = NormalDist(log10(m_parent), sigma_m);
                const double _m = pow10(draw_within_bounds(
                    [&]() mutable {
                        return m_dist(*sys.rng);
                    },
                    log10(bounds_m[0]), log10(bounds_m[1])
                ));

                // Feeding center, with mutation on rel. feeding distance
                auto mf_dist = NormalDist(log10(mf_parent), sigma_mf_ratio);
                const double _mf_ratio = pow10(draw_within_bounds(
                    [&]() mutable {
                        return mf_dist(*sys.rng);
                    },
                    log10(bounds_mf_ratio[0]), log10(bounds_mf_ratio[1])
                ));
                const auto _f = _m / _mf_ratio;

                // Niche width
                auto s_dist = NormalDist(s_parent, sigma_s);
                const double _s = draw_within_bounds(
                    [&]() mutable {
                        return s_dist(*sys.rng);
                    },
                    bounds_s[0], bounds_s[1]
                );

                // Conversion efficiency: always same as parent
                const auto _lambda = sys.lambda(parent_idx);

                return {_m, _f, _s, _lambda};
            };
        }
        else { // .............................................................
            throw std::invalid_argument(fmt::format(
                "Invalid trait mutation mode: {}! "
                "Available modes:  Allhoff2015, normal"
            ));
        }
    }

    /// Builds the environment-related trait mutation functor
    /** If environment modifications are not enabled, will return a nullop.
      */
    EnvTraitMutationFunc _build_env_trait_mutation_func (const Config& params){
        using Range = std::array<double, 2>;

        if constexpr (not envmod_enabled) {
            log->debug(
                "  NOT setting up functor for envionment trait mutation."
            );
            return [](const auto&, const auto) -> EnvTraits { return {}; };
        }

        log->debug("  Setting up functor for environment trait mutation ...");
        const auto mode = get_as<std::string>("env_mode", params);
        log->info("Using environment mutation mode '{}' ...", mode);

        const auto& mp = params;
        mutation_params = YAML::Clone(mp);
        log->trace("Mutation parameters updated with those for new functor.");
        // NOTE To not have side-effects, this should be done after the functor
        //      was built, I know ...

        if (mode == "gain_lose") {
            const auto inheritance_strength =
                get_as<double>("inheritance_strength", mp);

            const auto& fr_div_cfg = get_as<Config>("frac_diverted", mp);
            const auto fr_div_p_gain = get_as<double>("p_gain", fr_div_cfg);
            const auto fr_div_p_lose = get_as<double>("p_lose", fr_div_cfg);
            const auto fr_div_sigma = get_as<double>("sigma", fr_div_cfg);

            const auto& d2env_cfg = get_as<Config>("distr_to_env", mp);
            const auto d2env_new_range = get_as<Range>("new_range", d2env_cfg);
            const auto d2env_mutate_range = get_as<Range>("mutate_range",
                                                          d2env_cfg);

            return [
                fr_div_p_gain, fr_div_p_lose, fr_div_sigma,
                d2env_new_range, d2env_mutate_range, inheritance_strength
            ](const auto& sys, const auto parent_idx) -> EnvTraits {
                auto rand = [&]() { return RealDist(0., 1.)(*sys.rng); };

                // -- Fraction of energy diverted to environment
                // Start from the parent's value, which may be zero
                double _frac_diverted = sys.frac_diverted(parent_idx);

                // Depending on the parent value, may gain or lose the
                // ability. If not gaining or losing, will mutate ...
                if (
                    _frac_diverted > 0. and
                    (   fr_div_p_lose == 1. or
                        (fr_div_p_lose > 0. and rand() < fr_div_p_lose)
                    )
                ){
                    // Completely lose the ability to divert energy
                    _frac_diverted = 0.;
                }
                else if (
                    _frac_diverted == 0. and
                    (   fr_div_p_gain == 1. or
                        (fr_div_p_gain > 0. and rand() < fr_div_p_gain)
                    )
                ){
                    // Gain the ability to divert energy or mutate parent value
                    _frac_diverted =
                        NormalDist(0., fr_div_sigma)(*sys.rng);
                }
                else {
                    // Mutate around parent's value
                    _frac_diverted =
                        NormalDist(_frac_diverted, fr_div_sigma)(*sys.rng);
                }

                // If no energy is to be diverted, there's nothing else to do
                if (_frac_diverted <= 0.) {
                    return {0., {}};
                }
                // else: need to take care of distribution vector

                // -- Distribution vector
                // Start from parent distribution vector again
                Vector _d2env = sys.distr_to_env.col(parent_idx);

                // It might be completely zero, in which case a new vector
                // needs to be created from the d2env.new_range parameters.
                // If not, the existing vector is slightly perturbed.
                auto rg = d2env_new_range;
                if (not _d2env.is_zero()) {
                    rg = d2env_mutate_range;
                }
                auto _v = Vector(_d2env.size(), arma::fill::zeros);
                _v.imbue([&](){return RealDist(rg[0], rg[1])(*sys.rng); });
                _d2env += _v;

                // Replace negative values by zero
                _d2env.elem(arma::find(_d2env < 0.)).fill(0.);

                // If all are negative, have lost the ability through mutation
                if (_d2env.is_zero()) {
                    return {0., {}};
                }
                // else: have NOT lost the ability. Only need to normalize now
                _d2env = arma::normalise(_d2env, 1);  //  ==> sums to 1 now

                // And apply the inheritance strength
                const auto weight_parent = inheritance_strength;
                if (weight_parent > 0.) {
                    sys.log->trace(
                        "Applying inheritance strength {} ...", weight_parent
                    );
                    _frac_diverted = mix_values(
                        sys.frac_diverted(parent_idx), _frac_diverted,
                        weight_parent
                    );
                }

                return {_frac_diverted, _d2env};
            };
        }
        else {
            throw std::invalid_argument(fmt::format(
                "Invalid environment trait mutation mode: {}! "
                "Available modes:  gain_lose"
            ));
        }
    }


    // -- Storing and Restoring -----------------------------------------------
private:
    /// Returns a sequence of reference pairs that represent this ODESystem
    /**
      * \warning    The size of the corresponding vectors may not the same
      *             between the vectors!
      */
    RefPairs<Vector> _state_refs () {
        return {
            {"B",               B()},
            {"D",               D()},
            {"E",               E()},
            {"m",               m},
            {"f",               f},
            {"s",               s},
            {"lambda",          lambda},
            {"frac_diverted",   frac_diverted},
            {"R",               R},
            {"K",               K}
        };
    }

    /// Returns a sequence of reference pairs that represent this ODESystem
    ConstRefPairs<Vector> _state_refs () const {
        return {
            {"B",               B()},
            {"D",               D()},
            {"E",               E()},
            {"m",               m},
            {"f",               f},
            {"s",               s},
            {"lambda",          lambda},
            {"frac_diverted",   frac_diverted},
            {"R",               R},
            {"K",               K}
        };
    }

    /// Returns an extended sequence of reference pairs
    RefPairs<Vector> _extd_state_refs () {
        auto refs = _state_refs();

        refs.push_back({"x",                     x});
        refs.push_back({"ai",                    ai});
        refs.push_back({"h",                     h});

        refs.push_back({"_frac_used",            _frac_used});
        refs.push_back({"trophic_level",         trophic_level});

        return refs;
    }

    /// Returns an extended sequence of reference pairs
    ConstRefPairs<Vector> _extd_state_refs () const {
        auto refs = _state_refs();

        refs.push_back({"x",                     x});
        refs.push_back({"ai",                    ai});
        refs.push_back({"h",                     h});

        refs.push_back({"_frac_used",            _frac_used});
        refs.push_back({"trophic_level",         trophic_level});

        return refs;
    }

    /// Reference names that refer to the environment-state
    static inline const std::vector<std::string> _env_states =
        {"frac_diverted", "D", "E"};


public:
    /// Store the ODESystem into the given base group
    /** This takes into account whether `envmod_enabled` is set or not.
      * If enabled, it additionally stores `D`, `E`, `frac_diverted` and the
      * distribution matrix `distr_to_env` as well as the signature matrices
      * from the Environment.
      */
    void store (const std::shared_ptr<DataIO::HDFGroup>& base_grp) const {
        using Utopia::Utils::contains;
        log->info("Storing state to group '{}' ...", base_grp->get_path());

        // The passthrough-adapter to use for writing
        const auto adptr = [](const auto v){ return v; };

        // Open a base group and add separate datasets for each entry.
        for (auto& [key, ref] : _state_refs()) {
            if constexpr (not envmod_enabled) {
                // Skip some entries that don't hold valuable information
                if (contains(_env_states, key)) {
                    log->debug("NOT storing '{}'.", key);
                    continue;
                }
            }

            log->debug("Storing '{}' (n_elem: {}) ...", key, ref.n_elem);
            base_grp->open_dataset(key)->write(ref.begin(), ref.end(), adptr);
        }

        // Write other (non-Vector) properties separately ...
        log->debug("Storing 'ids' ...");
        base_grp->open_dataset("ids")->write(ids.begin(), ids.end(), adptr);

        // Environment-related state information
        if constexpr (envmod_enabled) {
            log->debug("Storing 'distr_to_env' ...");

            const auto& d2env = distr_to_env;
            const auto d2env_dset = base_grp->open_dataset(
                "distr_to_env", {d2env.n_rows, d2env.n_cols}
            );

            for (auto i=0u; i < d2env.n_rows; i++) {
                d2env_dset->write(
                    d2env.row(i).begin(), d2env.row(i).end(), adptr
                );
            }

            // Let the environment take care of storing its state variables
            log->debug("Storing Environment ...");
            env.store(base_grp->open_group("env"));
        }
        log->info("Successfully stored state.");
    }

    /// Named constructor to restore an ODESystem from a stored state
    /** Reads configuration entries from `restore_cfg` to determine the path
      * to a HDF5 file from which to restore data.
      *
      * This constructor takes into account if `envmod_enabled` is set and if
      * the to-be-loaded state stored environment information or not.
      *
      * \todo       Can surely be improved.
      *             The reason for this being so complicated is mostly the fact
      *             that there are four (!) possible combinations of the
      *             stored/restored state, where each one can have
      *             `envmod_enabled` or not ...
      */
    static ODESystem<RNG, tags...>
    restore (const Config& restore_cfg,
             const Config& cfg,
             TimeFunc get_model_time,
             const std::shared_ptr<spdlog::logger>& parent_logger,
             const std::shared_ptr<RNG>& parent_rng = {})
    {
        using arma::hdf5_name;
        using arma::hdf5_opts::trans;
        using namespace fmt::literals;
        using Utopia::Utils::contains;

        // Get the relevant parameters
        // ... for the file
        const auto base_dir = get_as<std::string>("base_dir", restore_cfg);
        std::string fpath;
        if (restore_cfg["from_file"].IsScalar()) {
            fpath = base_dir + get_as<std::string>("from_file", restore_cfg);
        }
        else {
            const auto& ff_cfg = get_as<Config>("from_file", restore_cfg);
            const auto fstr = get_as<std::string>("fstr", ff_cfg);
            fpath = fmt::format(
                fstr,
                "base_dir"_a = base_dir,
                "timestamp"_a = get_as<std::string>("timestamp", ff_cfg),
                "uni"_a = get_as<std::string>("uni", ff_cfg)
            );
        }

        // ... for the path within the HDF5 file; ensure trailing slash
        std::string base_grp;
        if (restore_cfg["base_group"].IsScalar()) {
            base_grp = get_as<std::string>("base_group", restore_cfg);
        }
        else {
            const auto& bg_cfg = get_as<Config>("base_group", restore_cfg);
            const auto fstr = get_as<std::string>("fstr", bg_cfg);
            base_grp = fmt::format(
                fstr,
                "name"_a = get_as<std::string>("name", bg_cfg),
                "index"_a = get_as<int>("index", bg_cfg)
            );
        }

        if (base_grp.back() != '/') {
            base_grp += "/";
        }

        // Display information gathered so far
        parent_logger->info("Restoring ODESystem state ...");
        parent_logger->info("  Base directory:   {}", base_dir);
        parent_logger->info("  HDF5 file path:   {}", fpath);
        parent_logger->info("  Base HDF5 group:  {}", base_grp);

        // Make sure the file actually exists; otherwise, the functions below
        // will simply load zero-sized matrices ...
        if (not std::filesystem::exists(fpath)) {
            throw std::invalid_argument(fmt::format(
                "No HDF5 file found at {} !", fpath
            ));
        }

        // Determine, whether the environment data needs to be loaded:
        // Reasons NOT to load: this simulation and/or the one to load from
        // were not run with envmod_enabled.
        bool load_env_state = false;
        auto n_env = 0u;

        if constexpr (envmod_enabled) {
            // How many were configured?
            const auto n_env_cfg = get_as<uint>("n_envmod", cfg);

            // Is the environment state part of the stored state?
            Vector E;
            if (not E.load(hdf5_name(fpath, base_grp + "E", trans))) {
                // Loading failed.
                // Upside: Can directly use the configured size:
                n_env = n_env_cfg;
            }
            else {
                // Success. There was something to load, let's use that.
                load_env_state = true;
                n_env = E.size();

                // Make sure this matches the number of configured ones
                if (n_env != n_env_cfg or E.size() < 1) {
                    throw std::invalid_argument(fmt::format(
                        "Size of loaded environment state ({}) does not match "
                        "size of configured environment state ({})!",
                        n_env, n_env_cfg
                    ));
                }
            }
        }

        // Find out sizes of species and resources
        Vector R;
        R.load(hdf5_name(fpath, base_grp + "R", trans));
        const auto n_res = R.size();

        IDVector ids;
        ids.load(hdf5_name(fpath, base_grp + "ids", trans));
        const auto n_spc = ids.size() - n_res;


        // Inform about the state that is to be set up ...
        parent_logger->info(
            "Setting up ODESystem with the following sizes:"
        );
        parent_logger->info("  n_resources:      {}", n_res);
        parent_logger->info("  n_species:        {}", n_spc);
        parent_logger->info("  n_envmod:         {}", n_env);
        parent_logger->info("  load_env_state?   {}",
                            load_env_state ? "yes" : "no");

        // If no resources or species could be detected, something was wrong...
        if (n_res == 0 or n_spc == 0) {
            throw std::invalid_argument(fmt::format(
                "Failed restoring ODESystem state, probably because the path "
                "to the HDF5 file ({}) or the path within the HDF5 file ({}) "
                "were invalid. Check if the file exists at that path and make "
                "sure the HDF5 path has a trailing slash!", fpath, base_grp
            ));
        }

        // Now, set up the ODESystem with the correct sizes
        auto sys = ODESystem<RNG, tags...>(
            n_res, n_spc, n_env, get_model_time, parent_logger, parent_rng
        );

        // Environment
        sys._set_up_environment(cfg);
        if constexpr (sys.envmod_enabled) {
            sys.log->info("Restoring environment ...");

            // Load remaining environment data: distribution matrix and the
            // signature matrices (Environment takes care of that)
            if (load_env_state) {
                sys.log->info("  Restoring distribution matrix ...");
                sys.distr_to_env.load(
                    hdf5_name(fpath, base_grp + "distr_to_env", trans)
                );

                // Safety check
                if (sys.distr_to_env.n_cols != sys.size_B() or
                    sys.distr_to_env.n_rows != sys.size_E())
                {
                    throw std::invalid_argument(fmt::format(
                        "Loaded distr_to_env matrix has bad shape. Expected: "
                        "({}, {}) but got ({})!",
                        sys.distr_to_env.n_rows, sys.distr_to_env.n_cols,
                        fmt::join(sys.distr_to_env, ", ")
                    ));
                }

                sys.log->info("  Restoring signature matrices ...");
                sys.env.restore(fpath, base_grp + "env");
            }
            sys.log->info("  Environment successfully restored.");
        }

        // Now load the actual state, (overwriting any entries that might have
        // been loaded from the configuration, e.g. for environment)
        sys.log->info("Loading HDF5 data into state ...");
        sys.ids = ids;
        sys.next_id = ids.max() + 1;
        sys.log->debug("Next ID will be {}.", sys.next_id);

        // Restore state and traits
        for (auto& [key, ref] : sys._state_refs()) {
            if (not load_env_state and contains(_env_states, key)){
                sys.log->trace("  NOT loading '{}' ...", key);
                continue;
            }
            sys.log->trace("  Loading '{}' ...", key);
            ref.load(hdf5_name(fpath, base_grp + key, trans));
        }
        sys.log->debug("ODESystem state variables now loaded.");

        // Do all the rest
        sys._set_up_global_params(cfg);
        sys._set_up_derived_quantities();
        sys._set_up_trackers();

        if (get_as<bool>("print", restore_cfg, false)) {
            sys.print_info();
        }
        sys.log->info("ODESystem state successfully restored.");

        return sys;
    }

};



} // Utopia::Models::mEEvo

#endif // UTOPIA_MODELS_MEEVO_ODE_SYSTEM_HH
