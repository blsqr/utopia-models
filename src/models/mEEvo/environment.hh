#ifndef UTOPIA_MODELS_MEEVO_ENVIRONMENT_HH
#define UTOPIA_MODELS_MEEVO_ENVIRONMENT_HH

#include <vector>
#include <limits>
#include <sstream>
#include <random>

#include <armadillo>

#include <utopia/data_io/hdffile.hh>

#include <UtopiaUtils/utils/iterators.hh>

#include "types.hh"
#include "utils/yaml.hh"


namespace Utopia::Models::mEEvo {


/// A class representing the environment and its modifications
struct Environment {
    /// The type of the signature matrices
    /** While each signature matrix currently holds only binary values, it is
      * much easier to use a floating-point element type; otherwise, more
      * runtime conversions become necessary ...
      */
    using SigMatrix = SpMatrix;

    // -- Members -------------------------------------------------------------
    // .. Parameters ..........................................................
    /// Scale factors for the environment reservoir
    Vector reservoir_size;

    /// Modification weights of the corresponding signature matrices
    Vector mod_weight;

    /// Decay rate
    Vector decay_rate;

    /// Probability that a signature entry flips from 0 to 1
    double p_mod_gain;

    /// Probability that a signature entry flips from 1 to 0
    double p_mod_lose;

    /// Minimum efficiency value
    /** Below this efficiency value, a modification is regarded as non-existent
      * and does not contribute to the total modification matrix
      */
    double min_efficiency;

    // .. State variables .....................................................
    // ... these represent the state of the Environment
    //     (excluding E, because it is already represented in the ODESystem!)

private:
    /// Modification signature matrices; should not be accessed directly
    std::vector<SigMatrix> mod_signatures;


public:
    // .. Tracking and helper variables .......................................
    // ... these need to be updated in order to hold the correct values
    /// The evaluated modification efficiencies
    Vector mod_efficiency;

    /// The evaluated total modification matrix
    /** This is a non-sparse matrix with elements in [-1, +inf)
      */
    Matrix mod_total;

    /// The modification matrix + 1, to use directly in ODESystem
    /** This is a non-sparse matrix with elements in [0, +inf)
      */
    Matrix mod_factors;

    /// Whether there are currently any modifications at all
    /** If true, the `mod_total` matrix can be assumed to be filled only with
      * zeros and the `mod_factors` matrix only with ones.
      *
      * This is updated as part of the update_total_modification method.
      */
    bool no_modifications;


    // -- Constructors --------------------------------------------------------

    template<class Sys>
    Environment (Sys& sys, const Config& cfg = {})
    :
        reservoir_size(sys.size_E(), arma::fill::zeros)
    ,   mod_weight(sys.size_E(), arma::fill::zeros)
    ,   decay_rate(sys.size_E(), arma::fill::zeros)

    ,   p_mod_gain(std::numeric_limits<double>::quiet_NaN())
    ,   p_mod_lose(std::numeric_limits<double>::quiet_NaN())
    ,   min_efficiency(1e-6)

        // Set up *empty* sparse matrix objects of the correct size
    ,   mod_signatures([&](){
            using Utopia::Utils::in_range;

            auto sigs = std::vector<SpMatrix>();
            for ([[maybe_unused]] auto i : in_range(sys.size_E())) {
                sigs.push_back(SpMatrix(sys.size_B(), sys.size_B()));
            }
            return sigs;
        }())

    ,   mod_efficiency(sys.size_E(), arma::fill::zeros)
    ,   mod_total(sys.size_B(), sys.size_B(), arma::fill::zeros)
    ,   no_modifications(true)

    {
        if (not cfg.size()) return;
        // else: fill with values from configuration

        const auto params_map = RefPairs<Vector>{
            {"reservoir_size",      reservoir_size},
            {"weight",              mod_weight},
            {"decay_rate",          decay_rate}
        };
        for (auto& [key, ref] : params_map) {
            set_vector_values(ref, cfg[key]);
        }

        // Load other parameters
        p_mod_gain = get_as<double>("p_mod_gain", cfg);
        p_mod_lose = get_as<double>("p_mod_lose", cfg);
        min_efficiency = get_as<double>("min_efficiency", cfg, 1.e-6);

        // Make sure that negative modification factors don't sum to below -1
        if (arma::accu(mod_weight(arma::find(mod_weight < 0.))) < -1.) {
            std::stringstream _s; _s << mod_weight;
            throw std::invalid_argument(fmt::format(
                "Negative modification weights may not sum up to below -1.0! "
                "Got:\n{}\n", _s.str()
            ));
        }
    }


    // .. Store/Restore .......................................................

    /// Store the Environment-specific state variables into a HDF5 group
    void store (const std::shared_ptr<DataIO::HDFGroup>& base_grp) const {
        call_on_signatures([&](const auto i, const SpMatrix& sig){
            // These sparse matrices are best saved in coordinate ascii mode
            std::stringstream _s;
            sig.save(_s, arma::coord_ascii);

            const auto dset =
                base_grp->open_dataset(fmt::format("sig/{:02d}", i), {1});

            dset->write(std::vector<std::string>({_s.str()}));
            dset->add_attribute("data_format", "arma::coord_ascii");
            dset->add_attribute("dim_names", std::vector<std::string>{"_"});
        });
    }

    /// Restore the Environment state from a HDF5 group
    /** This pertains only to the value of the signature matrices ... all
      * other properties should already have been set up!
      */
    void restore (const std::string& fpath, const std::string& base_grp) {
        // Open the file and the group containg the signature matrices
        auto f = DataIO::HDFFile(fpath, "r");
        std::shared_ptr<DataIO::HDFGroup> grp;
        try {
            grp = f.open_group(base_grp + "/sig");
        }
        catch (...) {
            // No such group available -> no environment matrices to load
            throw std::invalid_argument(fmt::format(
                "Failed opening group '{}/sig' in HDF5 file {}! Cannot load "
                "signature matrices.", base_grp, fpath
            ));
        }

        // TODO Check that the number of signature matrices matches.
        //      DataIO::HDF5Group does not give access to number of datasets,
        //      unfortunately, so this would have to be some workaround ...

        // Read modification signature matrices
        try {
            apply_to_signatures([&](const auto i, auto& sig){
                using VecOfStr = std::vector<std::string>;

                // Use the HDF library to read the data from the 1-sized array
                const auto dset = grp->open_dataset(fmt::format("{:02d}", i));
                const auto [shape, voc_data] = dset->template read<VecOfStr>();

                // Extract the data from the item-sized container
                if (shape.size() != 1 or shape[0] != 1) {
                    throw std::invalid_argument(fmt::format(
                        "Expected 1-sized dataset to restore signature matrix "
                        "{}, but got dataset with {} elements!", i, shape[0]
                    ));
                }

                // Let armadillo read it (from stream) into the sparse matrix
                std::stringstream coord_ascii_data;
                coord_ascii_data << voc_data[0];
                sig.load(coord_ascii_data, arma::coord_ascii);
            });
        }
        catch (...) {
            // TODO Could implement a gracious fallback if data is missing
            throw std::invalid_argument(fmt::format(
                "Failed loading signature matrices, most possibly due to "
                "missing data. Expected {} signature matrices. Check the "
                "'{}/sig' group in HDF5 file {} for sufficient datasets with "
                "zero-padded two-digit names and single-item string entries.",
                mod_signatures.size(), base_grp, fpath
            ));
        }
    }


    // -- Getters -------------------------------------------------------------

    /// Read-only access to the signature matrices
    const auto& signature_matrices () const {
        return mod_signatures;
    }

    /// Like apply_to_signatures, but without allowing to modify the matrices
    template<class Func, class... Args>
    void call_on_signatures (const Func& func, Args&&... args) const {
        for (auto k=0u; k < mod_signatures.size(); k++) {
            func(k, mod_signatures[k], std::forward<Args>(args)...);
        }
    }

    /// Aggregates values from signature matrices
    template<class Cont = std::vector<double>, class Func, class... Args>
    Cont aggregate_from_signatures (const Func& func, Args&&... args) const {
        auto cont = Cont();
        for (auto k=0u; k < mod_signatures.size(); k++) {
            cont.push_back(
                func(k, mod_signatures[k], std::forward<Args>(args)...)
            );
        }
        return cont;
    }


    /// Computes and returns the mean density of the signature matrices
    double mean_signature_density () const {
        double agg_density = 0.;
        call_on_signatures(
            [](auto, const auto& sig, double& agg){
                agg += static_cast<double>(sig.n_nonzero) / sig.n_elem;
            },
            agg_density
        );
        return agg_density / mod_signatures.size();
    }


    // -- Manipulation --------------------------------------------------------

    /// Apply a function to all signature matrices
    /** The function should accept the index of the matrix as first argument
      * and a *reference* to the signature matrix as second argument.
      * Any further arguments to this method are passed on to the functor.
      */
    template<class Func, class... Args>
    void apply_to_signatures (const Func& func, Args&&... args) {
        for (auto k=0u; k < mod_signatures.size(); k++) {
            func(k, mod_signatures[k], std::forward<Args>(args)...);
        }
    }

    /// Extends the signature matrices with a new entry, based on a parent's
    template<class RNG>
    void mutate_signatures (const arma::uword child_idx,
                            const arma::uword parent_idx,
                            const std::shared_ptr<RNG>& rng)
    {
        // Make sure the rows and columns corresponding to the child indices
        // are not yet set; this is a safe guard.
        apply_to_signatures([&](auto i, const auto& sig){
            if (not sig.col(child_idx).is_zero() or
                not sig.row(child_idx).is_zero())
            {
                throw std::invalid_argument(fmt::format(
                    "mutate_signatures assumes that the colums and rows of "
                    "the signature matrices that correspond to the offspring "
                    "have no entries, but signature matrix {} had non-zero "
                    "entries in column and/or row {}! parent_idx: {}",
                    i, child_idx, parent_idx
                ));
            }
        });

        // Can now proceed to copy elements over, probabilistically flipping
        // values "up" (0 -> 1) or "down" (1 -> 0)
        auto prob_distr = std::uniform_real_distribution<double>(0., 1.);
        auto flip_up =   [&](){ return prob_distr(*rng) < p_mod_gain; };
        auto flip_down = [&](){ return prob_distr(*rng) < p_mod_lose; };

        auto trf_element = [&](auto parent_val){
            if      (parent_val == 1)   return not flip_down();
            else if (parent_val == 0)   return     flip_up();
            else
                throw std::invalid_argument(fmt::format(
                    "Got invalid parent value {}, need be 0 or 1!", parent_val
                ));
        };

        apply_to_signatures([&](auto, auto& sig){
            // Copy parent row and column into dense vectors
            arma::rowvec _row(sig.n_cols, arma::fill::zeros);
            arma::vec _col(sig.n_rows, arma::fill::zeros);
            _row = sig.row(parent_idx);
            _col = sig.col(parent_idx);

            // Transform those in-place
            _row.transform(trf_element);
            _col.transform(trf_element);

            // Now set them in the sparse matrix
            sig.row(child_idx) = _row;
            sig.col(child_idx) = _col;
        });
    }

    /// Aggregate modifications into the total modification matrix
    /** This updates `mod_efficiency`, `mod_total`, and `mod_factors`.
      */
    void update_total_modification (const Vector& E) {
        __check_sizes(E);

        // Update the efficiencies
        mod_efficiency = (1. - arma::exp(-1. * E / reservoir_size));

        // Aggregate the signature matrices and keep track of whether there
        // are any modifications at all ...
        mod_total.zeros(mod_signatures[0].n_rows, mod_signatures[0].n_cols);
        no_modifications = true;

        call_on_signatures([&](auto k, const auto& sig, auto& mod_total){
            // Only need to do this, if there are any modifications at all
            if (sig.n_nonzero > 0 and mod_efficiency(k) > min_efficiency) {
                mod_total += mod_efficiency(k) * mod_weight(k) * sig;
                no_modifications = false;
            }
        }, mod_total);

        mod_factors = 1. + mod_total;
    }


private:
    // -- Helper methods ------------------------------------------------------

    /// Ensure that sizes of environment state and signatures are consistent
    void __check_sizes (const Vector& E) const {
        if (mod_signatures.size() == E.size()) return;

        throw std::invalid_argument(fmt::format(
            "Size mismatch between environment state ({}) and number of "
            "modification signature matrices ({})! Make sure that the "
            "environment state size did not change after initialization.",
            E.size(), mod_signatures.size()
        ));
    }
};


} // namespace Utopia::Models::mEEvo

#endif // UTOPIA_MODELS_MEEVO_ENVIRONMENT_HH
