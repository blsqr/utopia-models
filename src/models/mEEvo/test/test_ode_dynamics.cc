#define BOOST_TEST_MODULE test_the_ode_system_dynamics

#include <boost/test/unit_test.hpp>

#include "testtools.hh"

#include "../ode_system.hh"



// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Models::mEEvo;

/// The specialized infrastructure fixture (same as test_ode_system)
struct Infrastructure : public InfrastructureBase {
    Infrastructure () : InfrastructureBase("ode_system.yml") {}

    /// Retrieve the expected diagonal element for the competition matrix
    static double get_c_ii (const Config& cfg) {
        return get_as<double>("c_food", cfg) + get_as<double>("c_intra", cfg);
    }
};



// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Test the transformation efficiency calculation
BOOST_FIXTURE_TEST_CASE (test_lambda, Infrastructure,
                         *utf::tolerance(1.e-4))
{
    const auto& p = cfg["defaults"]["system"];
    auto model_time = 0u;
    auto sys = ODESystem(p, [&model_time](){ return model_time; });
    sys.print_info();

    auto& l = sys.lambda;
    auto& g = sys.g;
    auto& B = sys.B();
    auto& S = sys.get_state();

    BOOST_TEST(l == Vector({0.45, 0.85}), tt::per_element());
    BOOST_TEST(g(1, 0) == 0.667517);

    // Check the response itself does _not_ depend on the lambda values
    l.fill(1.);
    sys.update_responses(sys.B());
    BOOST_TEST(g(1, 0) == 0.667517);

    // However, dBdt does.
    // Set respiration and resource growth to zero to better see this
    sys.x.fill(0.);
    sys.R.fill(0.);

    State dSdt;
    sys(S, dSdt, 0.);
    BOOST_TEST(dSdt.B(0) == -dSdt.B(1));

    // Now, make this a 3-chain with far-away species
    // The resource needs to be very large to allow creating large new species
    sys.B(0) = 1.e23;

    sys.remove_species({1});
    BOOST_TEST_PASSPOINT();

    sys.add_species(1.e3, 1.e0, .5, 1.);
    BOOST_TEST_PASSPOINT();

    sys.add_species(1.e6, 1.e3, .5, 1.);
    BOOST_TEST_PASSPOINT();

    sys.x.fill(0.);
    sys.R.fill(0.);
    B = Vector({100, 10, 1});

    sys(S, dSdt, 0.);
    auto& dBdt = dSdt.B;
    dBdt.print("dBdt");
    BOOST_TEST(dBdt(0) == - (dBdt(1) + dBdt(2)));

    // Reducing lambda for resources, impacts specifically the middle species
    l(0) = 0.5;
    State dSdt_le;
    sys(S, dSdt_le, 0.);
    auto& dBdt_le = dSdt_le.B;
    dBdt_le.print("dBdt_le");
    BOOST_TEST(-dBdt_le(0) > (dBdt_le(1) + dBdt_le(2)));  // b/c of loss in 1
    BOOST_TEST(dBdt_le(0) == dBdt(0));
    BOOST_TEST(dBdt_le(1) < 0.5 * dBdt(1));
    BOOST_TEST(dBdt_le(2) == dBdt(2));

    // Reducing lambda for the species impact only the top-predator here
    l(sys.species()).fill(0.4);
    State dSdt_le2;
    sys(S, dSdt_le2, 0.);
    auto& dBdt_le2 = dSdt_le2.B;
    dBdt_le2.print("dBdt_le2");
    BOOST_TEST(dBdt_le2(0) == dBdt(0));
    BOOST_TEST(dBdt_le2(1) == dBdt_le(1));
    BOOST_TEST(dBdt_le2(2) == 0.4 * dBdt_le(2));

    // With respiration enabled (for species), the values are still lower
    sys.update_species_parameters(sys.species());
    State dSdt_le3;
    sys(S, dSdt_le3, 0.);
    auto& dBdt_le3 = dSdt_le3.B;
    dBdt_le3.print("dBdt_le3");
    BOOST_TEST(dBdt_le3(0) == dBdt_le2(0));
    BOOST_TEST(dBdt_le3(1) < dBdt_le2(1));
    BOOST_TEST(dBdt_le3(2) < dBdt_le2(2));

    // But resource growth has no effect because resource is full
    sys.R.fill(1.);
    State dSdt_le4;
    sys(S, dSdt_le4, 0.);
    auto& dBdt_le4 = dSdt_le4.B;
    dBdt_le4.print("dBdt_le4");
    BOOST_TEST(dBdt_le4(0) == dBdt_le3(0));
    BOOST_TEST(dBdt_le4(1) == dBdt_le3(1));
    BOOST_TEST(dBdt_le4(2) == dBdt_le3(2));
}
