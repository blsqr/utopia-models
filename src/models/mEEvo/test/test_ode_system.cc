#define BOOST_TEST_MODULE test_the_ode_system

#include <tuple>

#include <boost/test/unit_test.hpp>

#include "testtools.hh"

#include "../ode_system.hh"



// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Models::mEEvo;

/// The specialized infrastructure fixture
struct Infrastructure : public InfrastructureBase {
    Infrastructure () : InfrastructureBase("ode_system.yml") {}

    /// Retrieve the expected diagonal element for the competition matrix
    static double get_c_ii (const Config& cfg) {
        return get_as<double>("c_food", cfg) + get_as<double>("c_intra", cfg);
    }
};

// ... set it as global fixture
BOOST_TEST_GLOBAL_FIXTURE(Infrastructure);

/// A bunch of different ODESystem types
using ode_systems = std::tuple<
    ODESystem<ModelRNG>,
    ODESystem<ModelRNG, with_envmod>,
    ODESystem<ModelRNG, use_numerical_integration>
>;



// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

BOOST_FIXTURE_TEST_SUITE(setup, Infrastructure)

/// Test construction of the ODE system with different configuration entries
BOOST_AUTO_TEST_CASE_TEMPLATE (test_construction, System, ode_systems)
{
    test_config_callable([&](const Config& p){
        auto sys = System(p, [](){ return 0; });
        sys.print_info();

        // Check some values
        BOOST_TEST(sys.n_resources() > 0);
        BOOST_TEST(sys.n_resources() == get_as<size_t>("n_resources", p));
        BOOST_TEST(sys.n_species() > 0);
        BOOST_TEST(sys.n_species() == get_as<size_t>("n_species", p));

        // Size properties
        BOOST_TEST(sys.n_resources() + sys.n_species() == sys.size_B());
        BOOST_TEST(sys.size_B() + sys.size_D() + sys.size_E() == sys.size());

        // After initialization, species count is equal to number of species
        BOOST_TEST(sys.n_species() == sys.get_species_count());

        // IDs start at 1
        BOOST_TEST(sys.ids.min() == 1);

        // Trackers were populated
        BOOST_TEST(sys.traits_tracker.size() == sys.size_B());
        BOOST_TEST(sys.addition_times_tracker.size() == 1 + sys.size_B());
        BOOST_TEST(sys.removal_times_tracker.size() == 1 + 0);
        BOOST_TEST(sys.property_tracker.size() == 1 + 0);

        // ... including the "special" ID zero in addition and removal times
        BOOST_TEST(sys.addition_times_tracker.ids(0) == 0);
        BOOST_TEST(sys.removal_times_tracker.ids(0) == 0);
        BOOST_TEST(sys.property_tracker.ids(0) == 0);

        // Parent IDs of initial species are all zero
        BOOST_TEST(sys.traits_tracker.parent_ids.min() == 0);
        BOOST_TEST(sys.traits_tracker.parent_ids.max() == 0);

        // Rest of this function: whether parameters were transferred correctly
        if (not get_as<bool>("_is_species_specific", p, false)) return;
        // else: further tests to see if parameters were moved over as expected

        // There should be species-specific values
        BOOST_TEST(
            sys.m == Vector({1., 2., 100., 200., 300.}),
            tt::per_element()
        );
        BOOST_TEST(
            sys.f == Vector({0., 0., 1., 2., 3.}),
            tt::per_element()
        );
        BOOST_TEST(
            sys.s == Vector({0., 0., 1., 2., 3.}),
            tt::per_element()
        );
        BOOST_TEST(
            sys.lambda == Vector({.45, .90, .85, .90, .95}),
            tt::per_element()
        );
        BOOST_TEST(
            sys.B() == Vector({100., 200., 1., 2., 3.}),
            tt::per_element()
        );
        BOOST_TEST(
            sys.R == Vector({1., 2.}),
            tt::per_element()
        );
        BOOST_TEST(
            sys.K == Vector({100., 200.}),
            tt::per_element()
        );

    },
    cfg["cases"], "Construction", {__LINE__, __FILE__});
}


/// Tests setting up of matrices and trait vectors
BOOST_TEST_DECORATOR(*utf::tolerance(1e-8))
BOOST_AUTO_TEST_CASE_TEMPLATE (test_initial_calculations, System, ode_systems)
{
    test_config_callable([&](const Config& p){
        auto sys = System(p, [](){ return 0; });
        sys.print_info();

        // .. Matrices and vectors all set up and finite ......................
        BOOST_TEST(not sys.g.is_zero());
        BOOST_TEST(not sys.a.is_zero());
        BOOST_TEST(not sys.c.is_zero());
        BOOST_TEST(sys.g.is_finite());
        BOOST_TEST(sys.a.is_finite());
        BOOST_TEST(sys.c.is_finite());

        BOOST_TEST(sys.x.is_finite());
        BOOST_TEST(sys.ai.is_finite());
        BOOST_TEST(sys.h.is_finite());
        BOOST_TEST(not sys.B_min.is_finite()); // includes -inf for resources

        BOOST_TEST(sys.G.is_finite());
        BOOST_TEST(sys.R.is_finite());
        BOOST_TEST(sys.K.is_finite());


        // .. Check matrix properties .........................................
        // Get the species and resource spans to check the remaining properties
        const auto sps = sys.species();
        const auto res = sys.resources();

        // .. Response matrix g
        // Should be zero for resource-rows
        BOOST_TEST(sys.g(res, arma::span::all).is_zero());

        // If there is no self-interaction, the diagonal needs to be zero
        if (not sys.self_interaction) {
            BOOST_TEST(sys.g.diag().is_zero());
        }
        else {
            BOOST_TEST(not sys.g.diag().is_zero());
        }


        // .. Attack matrix a
        // Should be zero for resource-rows
        BOOST_TEST(sys.a(res, arma::span::all).is_zero());

        // ... and be non-zero along the diagonal (self-attack)
        // Note that self-interaction is suppressed via matrix g, not a.
        BOOST_TEST(not sys.a.diag().is_zero());

        // Should be non-negative
        BOOST_TEST(sys.a.min() >= 0.);


        // .. Competition matrix c
        // Should be zero in the resource elements, non-zero elsewhere and
        // have diagonal elements equal to c_food + c_intra
        BOOST_TEST(sys.c(res, arma::span::all).is_zero());
        BOOST_TEST(sys.c(arma::span::all, res).is_zero());

        const auto expected_c_ii = get_c_ii(p);
        BOOST_TEST(arma::all(sys.c(sps, sps).diag() == expected_c_ii));

        // Should be non-negative
        BOOST_TEST(sys.c.min() >= 0.);


        // .. Check species' property vectors .................................

        // Should all be smaller than the associated mass (for m > 1.)
        BOOST_TEST(arma::all(sys.x(sps) < sys.m(sps)));
        BOOST_TEST(arma::all(sys.ai(sps) < sys.m(sps)));
        BOOST_TEST(arma::all(sys.h(sps) < sys.m(sps)));

        // The extinction threshold should be eps * m
        BOOST_TEST(arma::all(sys.B_min(sps) == sys.eps * sys.m(sps)));

        // The elements that belong to resources are zero / -inf
        BOOST_TEST(sys.x(res).is_zero());
        BOOST_TEST(sys.ai(res).is_zero());
        BOOST_TEST(sys.h(res).is_zero());
        BOOST_TEST(not sys.B_min(res).is_finite());

        // Traits are zero for resource rows
        BOOST_TEST(sys.f(res).is_zero());
        BOOST_TEST(sys.s(res).is_zero());


        // .. Check resources' property vectors ...............................
        // G, R and K are of a smaller size
        BOOST_TEST(sys.G.size() == sys.n_resources());
        BOOST_TEST(sys.R.size() == sys.n_resources());
        BOOST_TEST(sys.K.size() == sys.n_resources());


        // .. Check some observable's values ..................................
        // Total biomass flow matches that from biomass flow matrix
        BOOST_TEST(
            sys.biomass_flow() == arma::accu(sys.biomass_flow_matrix())
        );


        // ....................................................................
        // Rest of this function: whether parameters were transferred correctly
        if (not get_as<bool>("_is_species_specific", p, false)) return;
        // else: further tests to see if parameters were moved over as expected
        // ...
    },
    cfg["cases"], "Initial Calculations", {__LINE__, __FILE__});
}


/// Test cases with environment explicitly enabled
BOOST_AUTO_TEST_CASE(with_environment) {
    test_config_callable([&](const Config& p){
        auto sys = ODESystem<ModelRNG, with_envmod>(p, [](){ return 0; });
        sys.print_info();

        // Check some values
        BOOST_TEST(sys.n_resources() > 0);
        BOOST_TEST(sys.n_resources() == get_as<size_t>("n_resources", p));
        BOOST_TEST(sys.n_species() > 0);
        BOOST_TEST(sys.n_species() == get_as<size_t>("n_species", p));
        BOOST_TEST(sys.size_E() > 0);
        BOOST_TEST(sys.size_E() == get_as<size_t>("n_envmod", p));

        // Size properties
        BOOST_TEST(sys.n_resources() + sys.n_species() == sys.size_B());
        BOOST_TEST(sys.size_D() == sys.size_B());
        BOOST_TEST(sys.size_B() + sys.size_D() + sys.size_E() == sys.size());
    },
    cfg["cases_with_envmod"], "With Environment", {__LINE__, __FILE__});
}


BOOST_AUTO_TEST_SUITE_END()  // "setup"


BOOST_FIXTURE_TEST_SUITE(competition, Infrastructure)

/// Test computation of the competition matrix
BOOST_AUTO_TEST_CASE_TEMPLATE (test_competition_matrix, System, ode_systems)
{
    const auto& p = cfg["defaults"]["system"];
    auto model_time = 0u;
    auto sys = System(p, [&model_time](){ return model_time; });

    // By default, will only have one species and one resource
    sys.c.print("c");
    BOOST_TEST(sys.c == Matrix({{0, 0}, {0, get_c_ii(p)}}), tt::per_element());

    // The resource needs to be very large to allow creating large new species
    sys.B(0) = 1.e23;

    // Remove that single species and start fresh
    sys.remove_species({1});
    BOOST_TEST(sys.size_B() == 1);
    BOOST_TEST(sys.n_species() == 0);

    // Add three new species, each feeding on the previous one
    BOOST_TEST_PASSPOINT();
    sys.add_species(1.e3, 1.e0, 1., 0.5);
    sys.add_species(1.e6, 1.e3, 1., 0.5);
    sys.add_species(1.e9, 1.e6, 1., 0.5);

    BOOST_TEST(sys.size_B() == 4);
    BOOST_TEST(sys.n_species() == 3);
    BOOST_TEST(sys.get_species_count() == 4);
    sys.print_info();

    // Far-away species should have (near) zero competition
    if constexpr (sys.analytical_feeding_range_overlap) {
        // near zero
        BOOST_TEST(sys.c(1, 3) < 1.e-4);
        BOOST_TEST(sys.c(3, 1) < 1.e-4);
    }
    else {
        // exact zero (cut due to quad_nsigma)
        BOOST_TEST(sys.c(1, 3) == 0.);
        BOOST_TEST(sys.c(3, 1) == 0.);
    }
}

/// Test computation of the feeding range overlap
/** Need an explicit decorator here ...
  */
BOOST_TEST_DECORATOR(*utf::tolerance(1e-8))
BOOST_AUTO_TEST_CASE_TEMPLATE (test_feeding_range_overlap,
                               System, ode_systems)
{
    const auto& p = cfg["defaults"]["system"];
    auto model_time = 0u;
    auto sys = System(p, [&model_time](){ return model_time; });

    // The resource needs to be very large to allow creating large new species
    sys.B(0) = 1.e23;
    BOOST_TEST(sys.B()(0) == 1.e23);

    // Add species to have two with close-by feeding centers, one far away
    sys.remove_species({1});
    BOOST_TEST_PASSPOINT();

    sys.add_species(1.e3, 1.e0, 1.1, .85);
    sys.add_species(2.e3, 2.e0, 0.9, .85);
    sys.add_species(1.e9, 1.5e3, 1., .85);
    BOOST_TEST_PASSPOINT();

    sys.print_info();
    BOOST_TEST(sys.size_B() == 4);

    // Self-overlap matches two-index overlap
    BOOST_TEST(sys.compute_feeding_range_overlap(1) ==
               sys.compute_feeding_range_overlap(1, 1));
    BOOST_TEST(sys.compute_feeding_range_overlap(2) ==
               sys.compute_feeding_range_overlap(2, 2));
    BOOST_TEST(sys.compute_feeding_range_overlap(3) ==
               sys.compute_feeding_range_overlap(3, 3));

    // Self-overlap is not a constant
    BOOST_TEST(sys.compute_feeding_range_overlap(1) !=
               sys.compute_feeding_range_overlap(2));
    BOOST_TEST(sys.compute_feeding_range_overlap(1) !=
               sys.compute_feeding_range_overlap(3));
    BOOST_TEST(sys.compute_feeding_range_overlap(2) !=
               sys.compute_feeding_range_overlap(3));
    // NOTE For same niche_width traits, it actually _is_ constant!

    // Overlap is symmetric (unlike competition matrix)
    BOOST_TEST(sys.compute_feeding_range_overlap(1, 2) ==
               sys.compute_feeding_range_overlap(2, 1));
    BOOST_TEST(sys.compute_feeding_range_overlap(1, 3) ==
               sys.compute_feeding_range_overlap(3, 1));
    BOOST_TEST(sys.compute_feeding_range_overlap(2, 3) ==
               sys.compute_feeding_range_overlap(3, 2));

    // For numerical integration, some additional tests
    if constexpr (not sys.analytical_feeding_range_overlap)
    {
        // Results changes with number of quadrature points
        BOOST_TEST(sys.template compute_feeding_range_overlap<5>(2) !=
                   sys.template compute_feeding_range_overlap<25>(2));

        // Smaller integration bounds lead to smaller overlap value
        BOOST_TEST(sys.quad_nsigma == 3);
        {
        const double I_11 = sys.compute_feeding_range_overlap(1);
        BOOST_TEST(I_11 > 0.);

        sys.quad_nsigma = 1.;
        BOOST_TEST(sys.compute_feeding_range_overlap(1) < I_11);
        }
        sys.quad_nsigma = 3.;  // ... resetting to old value

        // Extremely small bounds lead to zero overlap
        {
        const double I_12 = sys.compute_feeding_range_overlap(1, 2);
        BOOST_TEST(I_12 > 0.);

        sys.quad_nsigma = .001;
        BOOST_TEST(sys.compute_feeding_range_overlap(1, 2) == 0.);
        }
    }
}

BOOST_AUTO_TEST_SUITE_END()  // competition


/// Test the check_extinction and remove_extinct functionality
BOOST_FIXTURE_TEST_CASE (test_extinction, Infrastructure)
{
    const auto& p = cfg["defaults"]["system"];
    auto model_time = 0u;
    auto sys = ODESystem(p, [&model_time](){ return model_time; });
    sys.print_info();

    BOOST_TEST(not sys.check_extinction());

    // Change it to a value below the threshold
    sys.B()(1) = sys.B_min(1) / 2.;
    BOOST_TEST(sys.check_extinction());

    // Remove the extinct species
    BOOST_TEST(sys.n_species() == 1);
    BOOST_TEST(sys.remove_extinct() == arma::uvec({1}), tt::per_element());
    BOOST_TEST(sys.n_species() == 0);

    // Without species, there can be no extinctions
    BOOST_TEST(not sys.check_extinction());
}


/// Test the `find` method
BOOST_FIXTURE_TEST_CASE (test_find, Infrastructure)
{
    const auto params = cfg["test_find"];
    auto sys = ODESystem(params["system"], [](){ return 0; });
    sys.print_info();

    test_config_callable([&](const Config& p){
        const auto actual = sys.find(
            get_as<std::string>("mode", p),
            get_as<std::string>("attr_name", p, ""),
            p
        );
        const auto expected = get_as<std::vector<unsigned>>("expected", p);
        BOOST_TEST(actual == expected, tt::per_element());
    },
    params["cases"], "find()", {__LINE__, __FILE__});

}


/// Test the updating of the tracking variables
BOOST_FIXTURE_TEST_CASE (test_update_property_tracking_variables,
                         Infrastructure)
{
    const auto& p = cfg["defaults"]["system"];
    auto model_time = 0u;
    auto sys = ODESystem(p, [&model_time](){ return model_time; });
    sys.print_info();

    BOOST_TEST(sys.n_resources() == 1);
    BOOST_TEST(sys.n_species() == 1);

    // Age should be zero
    BOOST_TEST(sys.age(0) == 0);
    BOOST_TEST(sys.age(1) == 0);

    // Trophic level should contain default values
    BOOST_TEST(sys.mean_trophic_level(0) == 0.);
    BOOST_TEST(sys.mean_trophic_level(1) == 0.);

    // Update with a custom time
    sys.update_property_tracking_variables(3);
    BOOST_TEST(sys.age(0) == 3);
    BOOST_TEST(sys.age(1) == 3);

    BOOST_TEST(sys.mean_trophic_level(0) == 1.);
    BOOST_TEST(sys.mean_trophic_level(1) >= 2.0);

    // Another update with the same time doesn't change anything
    sys.update_property_tracking_variables(3);
    BOOST_TEST(sys.age(0) == 3);
    BOOST_TEST(sys.age(1) == 3);

    // ... but with negative time, throws
    check_exception<std::invalid_argument>([&](){
        sys.update_property_tracking_variables(2);
    }, "Time may not be smaller than last property update time! Was 2 < 3");
}
