#define BOOST_TEST_MODULE test_experimental

#include <string>
#include <cmath>
#include <random>
#include <unordered_map>

#include <boost/test/unit_test.hpp>

#include <boost/numeric/odeint.hpp>
#include <boost/operators.hpp>
#include <armadillo>
#include <fmt/core.h>

#include "testtools.hh"



// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Models::mEEvo;

/// The specialized infrastructure fixture
struct Infrastructure : public InfrastructureBase {
    Benchmark benchmark;

    Infrastructure ()
    :
        InfrastructureBase("experimental.yml")
    ,   benchmark(cfg["benchmark"], log)
    {}
};

/// Checks if two values from a map are similar
/** Similarity: "within a certain tolerance factor of each other"
  *
  * \warning    Assumes strictly positive entries!
  */
template<class Map, class Key>
void check_similar (const Map& m, Key k1, Key k2, double factor=2.) {
    BOOST_TEST_CONTEXT(fmt::format(
        "Checking if entries '{}' ({:.4g}) and '{}' ({:.4g}) are within a "
        "factor {} of each other ...", k1, m.at(k1), k2, m.at(k2), factor
    ))
    {
        BOOST_TEST(m.at(k1) < factor * m.at(k2));
        BOOST_TEST(m.at(k2) < factor * m.at(k1));
    }
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Misc. Experiments ---------------------------------------------------------

// .. Type definitions for structured ODE integration state ...................
struct StrucState
    :   boost::additive1<StrucState,
            boost::additive2<StrucState, double,
                boost::multiplicative2<StrucState, double>
            >
        >
{
    double x;
    double y;

    StrucState () : x(0.), y(0.) {}
    StrucState (const double val) : x(val) , y(val) {}
    StrucState (const double _x, const double _y) : x(_x), y(_y) {}

    StrucState& operator+= (const StrucState& p) {
        x += p.x;
        y += p.y;
        return *this;
    }

    StrucState& operator*= (const double a) {
        x *= a;
        y *= a;
        return *this;
    }
};

// only required for steppers with error control
StrucState operator/ (const StrucState& p1, const StrucState& p2) {
    return StrucState(p1.x/p2.x, p1.y/p2.y);
}

StrucState abs (const StrucState& p) {
    return StrucState(std::abs(p.x), std::abs(p.y));
}

namespace boost::numeric::odeint {
    using ::StrucState;

    template<>
    struct vector_space_norm_inf<StrucState> {
        using result_type = double;

        double operator() (const StrucState& p) const {
            using std::max;
            using std::abs;
            return max(abs(p.x), abs(p.y));
        }
    };
}


// ............................................................................
BOOST_FIXTURE_TEST_SUITE(misc_experiments, Infrastructure)

// .. ODE integration with structured state ...................................

const double gam = 0.15;

/// The rhs of x' = f(x) for the harmonic oscillator with specific state type
void harm_osc (const StrucState& S, StrucState& dSdt, const double /* t */) {
    dSdt.x = S.y;
    dSdt.y = -S.x - gam*S.y;
}

/// The expected result at t1 = 10. and gam = 0.15
/** This was computed with Runge-Kutta Cash-Karp (5,4) and custom error checker
  * tolerances of e_rel=1e-20 and e_abs=1e-20.
  */
const auto harm_osc_result_t10 = std::array<double, 2>{
    {-0.42190945039184452, 0.24640789042036049}
};

BOOST_AUTO_TEST_CASE(odeint_structured_state) {
    using namespace boost::numeric::odeint;
    using StateType = StrucState;

    // using Stepper = runge_kutta_dopri5<StrucState, double, StrucState,
    //                                    double, vector_space_algebra>;
    using Stepper = runge_kutta_cash_karp54<StrucState, double, StrucState,
                                            double, vector_space_algebra>;


    const auto t0 = 0.;
    const auto t1 = 10.;
    const auto dt = .1;

    // state initialization: start at x=1.0, p=0.0
    StateType state(1., 0.);

    // Integrate with controlled stepper
    int steps = integrate_adaptive(make_controlled<Stepper>(1E-10, 1E-10),
                                   harm_osc, state, t0, t1, dt);

    BOOST_TEST(steps > 0);

    BOOST_TEST(state.x == harm_osc_result_t10[0], tt::tolerance(1e-4));
    BOOST_TEST(state.y == harm_osc_result_t10[1], tt::tolerance(1e-4));
}


// .. Armadillo interface experiments .........................................
BOOST_AUTO_TEST_CASE(subviews) {
    using namespace arma;

    vec v = arma::regspace(0, 9);
    BOOST_TEST(v == vec({0,1,2,3,4,5,6,7,8,9}), tt::per_element());

    // Modify a subview
    auto v_sub1 = v(span(2, 5));
    v_sub1 *= -1;
    BOOST_TEST(v == vec({0,1,-2,-3,-4,-5,6,7,8,9}), tt::per_element());

    // Trying to access an *element* of a subview ... works :yay:
    BOOST_TEST(v_sub1(0) == -2);
    BOOST_TEST(v_sub1(1) == -3);
    BOOST_TEST(v_sub1(2) == -4);
    v_sub1(0) *= 10;
    BOOST_TEST(v_sub1(0) == -20);

    // Try to access a span to the subview ...  does NOT work!
    // auto v_sub2 = v_sub1(span(1,3));

    // How about non-contiguous subviews? ... does NOT work either!
    auto v_nc = v.elem(uvec({0,2,5,8}));
    // BOOST_TEST(v_nc(0) == 0);
    // BOOST_TEST(v_nc(1) == -20);
    // BOOST_TEST(v_nc(2) == -5);
    // BOOST_TEST(v_nc(3) == 8);
}


BOOST_AUTO_TEST_SUITE_END() // "misc_experiments"



/// Benchmarking --------------------------------------------------------------
BOOST_FIXTURE_TEST_SUITE(benchmarks, Infrastructure)

/// An example test case for a simple benchmarking function ...................
BOOST_AUTO_TEST_CASE(example) {
    if (not benchmark.enabled) return;
    // --- Add benchmarking below this line ---

    // Usage example
    {
    auto [mean, stddev, total, times] = benchmark.perform("foo",
    [](){
        // Start the timer
        const auto start = Clock::now();

        // Do benchmarking here
        // ...

        // As benchmarking result, return the time difference (in seconds)
        return time_since(start);
    });
    BOOST_TEST(mean > 0.);
    BOOST_TEST(mean < 1.e-6);
    }

    // --- Add benchmarking above this line ---
    benchmark.print_summary();
    // Finally, if results should be shown, trigger test case failure
    if (not benchmark.show_results) return;
    BOOST_ERROR("\n--- triggered test failure to show results, see log above");
}

// .. Actual benchmarks .......................................................

/// Tests performance of armadillo subviews
BOOST_AUTO_TEST_CASE(arma_subview) {
    if (not benchmark.enabled) return;
    if (not get_as<bool>("enabled", benchmark.cfg["arma_subview"])) return;
    // -----

    const auto N = get_as<int>("N", benchmark.cfg["arma_subview"]);
    const auto TOL = get_as<double>("factor", benchmark.cfg["arma_subview"]);

    // Define basic test objects, copies of which will be used below
    using Vector = arma::Col<double>;
    using Matrix = arma::Mat<double>;
    Vector v(N, arma::fill::ones);
    Matrix m(N, N, arma::fill::ones);

    // Set entries to random values
    auto udist = std::uniform_real_distribution<double>(0., 1.);
    auto rand = [&](){ return udist(*rng); };
    v.imbue(rand);
    m.imbue(rand);

    // Set up benchmark function
    const auto bench = [&](auto v1, auto v2, auto m1, auto m2){
        // Include a random value to reduce caching effects
        double salt = rand();

        // The actual benchmark: the calculation
        const auto start = Clock::now();
        auto res = m1 * (salt + m2.t()) * (v1 + salt + v2) / (2*v1 - salt*v2);
        const auto time = time_since(start);

        BOOST_TEST(res.is_finite());
        return time;
    };

    // --- Benchmark now
    // Native objects
    {
    Vector v1(v);
    Vector v2(v);
    Matrix m1(m);
    Matrix m2(m);

    benchmark.perform("native", bench, v1, v2, m1, m2);
    }

    // Subviews ...
    {  // ... full span
    auto v1 = v(arma::span::all);
    auto v2 = v(arma::span::all);
    auto m1 = m(arma::span::all, arma::span::all);
    auto m2 = m(arma::span::all, arma::span::all);
    BOOST_TEST(v1.n_rows == N);
    BOOST_TEST(m1.n_rows == N);
    BOOST_TEST(m1.n_cols == N);

    benchmark.perform("span_all", bench, v1, v2, m1, m2);
    }

    {  // ... almost a full span
    auto sp = arma::span(1, N-1);
    auto v1 = v(sp);
    auto v2 = v(sp);
    auto m1 = m(sp, sp);
    auto m2 = m(sp, sp);
    BOOST_TEST(v1.n_rows == N-1);
    BOOST_TEST(m1.n_rows == N-1);
    BOOST_TEST(m1.n_cols == N-1);

    benchmark.perform("span_almost_all", bench, v1, v2, m1, m2);
    }

    {  // ... half span
    Vector v_large = Vector(3*N, arma::fill::ones);
    Matrix m_large = Matrix(2*N, 2*N, arma::fill::ones);
    v_large.imbue(rand);
    m_large.imbue(rand);

    const auto sp = arma::span(v.n_rows, 2*v.n_rows-1);
    auto v1 = v_large(sp);
    auto v2 = v_large(sp);
    auto m1 = m_large(sp, sp);
    auto m2 = m_large(sp, sp);

    BOOST_TEST(v1.n_rows == N);
    BOOST_TEST(m1.n_rows == N);
    BOOST_TEST(m1.n_cols == N);

    benchmark.perform("span_half", bench, v1, v2, m1, m2);
    }

    {  // ... non-contiguous
    Vector v_large = Vector(3*N, arma::fill::ones);
    Matrix m_large = Matrix(3*N, 3*N, arma::fill::ones);
    v_large.imbue(rand);
    m_large.imbue(rand);

    const auto elems = arma::regspace<arma::uvec>(0, 3, 3*N-1);  // every 3rd
    BOOST_TEST(elems.size() == N);

    auto v1 = v_large(elems);
    auto v2 = v_large(elems);
    auto m1 = m_large(elems, elems);
    auto m2 = m_large(elems, elems);

    benchmark.perform("non_contiguous", bench, v1, v2, m1, m2);
    }

    // --- Compare values
    // The above should all be roughly equally fast
    auto& mean = benchmark.results.mean;
    check_similar(mean, "native", "span_all", TOL);
    check_similar(mean, "native", "span_almost_all", TOL);
    check_similar(mean, "native", "span_half", TOL);

    // For non-contiguous views, the factor needs to be higher
    check_similar(mean, "native", "non_contiguous", 1.5 * TOL);
    check_similar(mean, "span_all", "non_contiguous", 1.5 * TOL);


    // -----
    benchmark.print_summary();
    if (not benchmark.show_results) return;
    BOOST_ERROR("\n--- triggered test failure to show results, see log above");
}


/// Tests performance of armadillo operations with transposed matrices
BOOST_AUTO_TEST_CASE(arma_transpose_mat) {
    if (not benchmark.enabled) return;
    if (not get_as<bool>("enabled", benchmark.cfg["arma_transpose"])) return;
    // -----

    const auto N = get_as<int>("N", benchmark.cfg["arma_transpose"]);
    const auto TOL = get_as<double>("factor", benchmark.cfg["arma_transpose"]);

    // Define basic test objects with random values
    using Matrix = arma::Mat<double>;
    Matrix m(N, N, arma::fill::ones);

    auto udist = std::uniform_real_distribution<double>(0.1, 1.);
    auto rand = [&](){ return udist(*rng); };

    Matrix m1(m); m1.imbue(rand);
    Matrix m2(m); m2.imbue(rand);
    Matrix m3(m); m3.imbue(rand);

    // Set up benchmark function
    const auto bench = [&](auto m1, auto m2, auto m3, const auto& op){
        const auto start = Clock::now();
        Matrix res = op(m1, m2, m3, rand());
        const auto time = time_since(start);

        BOOST_TEST(not res.min() >= 0.);
        return time;
    };

    // --- Benchmark now
    benchmark.perform("transpose_0", bench, m1, m2, m3,
        [](auto m1, auto m2, auto m3, auto salt){
            return m1 * (salt + m2) + (m2 * m3 + salt);
        }
    );

    // FIXME Something below leads to a weird memory allocation error ...
    // benchmark.perform("transpose_1", bench, m1, m2, m3,
    //     [](auto m1, auto m2, auto m3, auto salt){
    //         return m1.t() * (salt + m2) + (m2 * m3 + salt);
    //     }
    // );
    // benchmark.perform("transpose_2", bench, m1, m2, m3,
    //     [](auto m1, auto m2, auto m3, auto salt){
    //         return m1.t() * (salt + m2).t() + (m2 * m3 + salt);
    //     }
    // );
    // benchmark.perform("transpose_3", bench, m1, m2, m3,
    //     [](auto m1, auto m2, auto m3, auto salt){
    //         return m1.t() * (salt + m2).t() + (m2.t() * m3 + salt);
    //     }
    // );
    // benchmark.perform("transpose_4", bench, m1, m2, m3,
    //     [](auto m1, auto m2, auto m3, auto salt){
    //         return m1.t() * (salt + m2).t() + (m2.t() * m3.t() + salt);
    //     }
    // );
    // benchmark.perform("transpose_5", bench, m1, m2, m3,
    //     [](auto m1, auto m2, auto m3, auto salt){
    //         return m1.t() * (salt + m2).t() + (m2.t() * m3.t() + salt).t();
    //     }
    // );

    // --- Compare values
    [[maybe_unused]] auto [cm, keys] = benchmark.comparison_matrix();
    BOOST_TEST(cm.max() < TOL);

    // -----
    benchmark.print_summary();
    if (not benchmark.show_results) return;
    BOOST_ERROR("\n--- triggered test failure to show results, see log above");
}


BOOST_AUTO_TEST_SUITE_END() // "benchmarks"
