#define BOOST_TEST_MODULE test_utils

#include <armadillo>
#include <boost/test/unit_test.hpp>

#include "testtools.hh"

#include "../utils.hh"



// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Models::mEEvo;

/// The specialized infrastructure fixture
struct Infrastructure : public InfrastructureBase {
    Infrastructure () : InfrastructureBase() {}
};



// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

BOOST_FIXTURE_TEST_SUITE(stats, Infrastructure)

BOOST_AUTO_TEST_CASE (test_weighted_mean, *utf::tolerance(1.e-6)) {
    BOOST_TEST(weighted_mean({1}, {1}) == 1);
    BOOST_TEST(weighted_mean({0, 10}, {1, 9}) == 9);
}

BOOST_AUTO_TEST_CASE (test_weighted_var, *utf::tolerance(1.e-6)) {
    BOOST_TEST(weighted_var({1}, {1}) == 0);
    BOOST_TEST(weighted_var({1, 1, 1}, {1, 1, 1}) == 0);
    BOOST_TEST(weighted_var({1, 1, 1}, {1, 2, 3}) == 0);
    BOOST_TEST(weighted_var({-1, -1, -1}, {1, 2, 3}) == 0);

    BOOST_TEST(weighted_var({1, 4, 7}, {.5, .5, .5}) == 6.);
    BOOST_TEST(weighted_var({1, 4, 7}, {1, 1, 1}) == 6.);
    BOOST_TEST(weighted_var({1, 4, 7}, {1, 4, 1}) == 3.);
    BOOST_TEST(weighted_var({-1, -4, -7}, {1, 4, 1}) == 3.);
    BOOST_TEST(weighted_var({1, 4, 7}, {0, 4, 0}) == 0.);

    BOOST_TEST(weighted_var({1, 4, 7}, {1, 0, 1}) == (3.*3 + 3.*3)/2.);
}

BOOST_AUTO_TEST_CASE (test_shannon_index, *utf::tolerance(1.e-6)) {
    BOOST_TEST(shannon_index({1}) == 0);
    BOOST_TEST(shannon_index({1, 2, 3}) == -4.68213122);
    BOOST_TEST(shannon_index({1, 2, 3}) == shannon_index({3, 1, 2}));
    BOOST_TEST(shannon_index({.5, .5}) == 0.693147);
    BOOST_TEST(shannon_index({.1, .2, .3, .4, .5}) == 1.6264278);

    Vector v1 = Vector({1, 2, 3, 4, 5});
    BOOST_TEST(shannon_index(v1/arma::accu(v1)) == 1.4897503);
    BOOST_TEST(shannon_index<normalize_input>(v1) == 1.4897503);

    // Robust for zero-valued entries
    BOOST_TEST(shannon_index({0}) == 0.);
    BOOST_TEST(shannon_index({0, 0, 0}) == 0.);
    BOOST_TEST(shannon_index({0, 1, 2, 0, 3}) == -4.68213122);

    // This fails without the very small log_offset being applied
    BOOST_TEST(std::isnan(shannon_index({0.}, 0.)));

    // Regardless, it should be nan for negative entries
    BOOST_TEST(std::isnan(shannon_index({-1.})));
}


// TODO weighted variance

BOOST_AUTO_TEST_SUITE_END()  // "stats" ---------------------------------------
