#define BOOST_TEST_MODULE test_environment

#include <cmath>

#include <boost/test/unit_test.hpp>
#include <armadillo>
#include <fmt/core.h>

#include "testtools.hh"

#include "../environment.hh"


// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Models::mEEvo;

/// A mock class of the ODESystem
struct MockSystem {
    std::size_t _size_B = 0;
    std::size_t _size_E = 0;

    MockSystem (std::size_t size_B, std::size_t size_E)
    :
        _size_B(size_B)
    ,   _size_E(size_E)
    {}

    auto size_B () const { return _size_B; }
    auto size_E () const { return _size_E; }
};


/// The specialized infrastructure fixture
struct Infrastructure : public InfrastructureBase {
    MockSystem sys;

    Infrastructure ()
    :
        InfrastructureBase("environment.yml")
    ,   sys(4,3)
    {
        BOOST_TEST(sys.size_B() == 4);
        BOOST_TEST(sys.size_E() == 3);
    }
};



// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

BOOST_FIXTURE_TEST_SUITE(environment, Infrastructure)

/// Construction without config is possible; values are irrelevant.
BOOST_AUTO_TEST_CASE(construction_without_cfg) {
    auto env = Environment(sys);
}

BOOST_AUTO_TEST_CASE(construction_from_cfg, *utf::tolerance(1e-10)) {
    auto env = Environment(sys, get_as<Config>("default", cfg));

    BOOST_TEST(sys.size_E() == 3);
    BOOST_TEST(env.reservoir_size == Vector({1, 2, 3}), tt::per_element());
    BOOST_TEST(env.mod_weight == Vector({.1, .2, .3}), tt::per_element());
    BOOST_TEST(env.decay_rate == Vector({.05, .05, .05}), tt::per_element());
    BOOST_TEST(env.p_mod_gain == .1);
    BOOST_TEST(env.p_mod_lose == .2);

    BOOST_TEST(env.mod_efficiency.size() == sys.size_E());
    BOOST_TEST(env.mod_efficiency.is_zero());
    BOOST_TEST(env.signature_matrices().size() == sys.size_E());

    BOOST_TEST(sys.size_B() == 4);
    for (const auto& sig : env.signature_matrices()) {
        BOOST_TEST(sig.n_rows == sys.size_B());
        BOOST_TEST(sig.n_cols == sys.size_B());
        BOOST_TEST(sig.is_zero());
        BOOST_TEST(sig.n_nonzero == 0);
    }
}


/// Construction fails if negative modification factors sum to below -1
BOOST_AUTO_TEST_CASE(construction_fails_with_negative_total_mod_weight) {
    check_exception<std::invalid_argument>([&](){
        Environment(sys, get_as<Config>("negative_mod_weight", cfg));
    },
    "Negative modification weights may not sum up to below -1"
    );
}


/// Signature matrix modifications
BOOST_AUTO_TEST_CASE(modify_signature_matrices) {
    auto env = Environment(sys, get_as<Config>("default", cfg));
    BOOST_TEST(env.mean_signature_density() == 0.);

    env.apply_to_signatures(
        [&](auto i, auto& sig, auto offset){
            using Utopia::Utils::in_range;

            for (auto row : in_range(sig.n_rows)) {
                for (auto col : in_range(sig.n_cols)) {
                    sig(row, col) = offset + i;
                }
            }
        },
        1
    );

    for (const auto& sig : env.signature_matrices()) {
        BOOST_TEST(sig.n_nonzero == sys.size_B() * sys.size_B());
    }
    env.call_on_signatures([&](auto, const auto& sig){
        BOOST_TEST(sig.n_nonzero == sys.size_B() * sys.size_B());
    });

    BOOST_TEST(env.mean_signature_density() == 1.);
}


/// Total modification matrix
BOOST_AUTO_TEST_CASE(total_modification_matrix) {
    auto env = Environment(sys, get_as<Config>("default", cfg));

    // Total modification matrix should be zero
    BOOST_TEST(env.mean_signature_density() == 0.);
    BOOST_TEST(env.mod_total.is_zero());
    env.update_total_modification({0, 0, 0});
    BOOST_TEST(env.mod_total.is_zero());
    BOOST_TEST(env.mod_efficiency.is_zero());

    // Set the first N entries to some non-zero values
    env.apply_to_signatures([&](uint i, auto& sig){
        sig(i%4, i/4) = 1;
    });
    BOOST_TEST(env.mean_signature_density() == 1./16);

    // Still zero, if no reservoir is specified
    env.update_total_modification({0, 0, 0});
    BOOST_TEST(env.mod_total.is_zero());
    BOOST_TEST(env.mod_efficiency.is_zero());

    // Starts to become non-zero with non-zero environment reservoir
    env.update_total_modification({1, 1, 1});
    BOOST_TEST(env.mod_efficiency.min() > 0.);
    BOOST_TEST(env.mod_total.max() > 0.);
    BOOST_TEST(env.mod_total.min() == 0.);

    // Certain values may approach their theoretical maximum
    env.update_total_modification({1e10, 1e10, 1e10});
    for (auto i = 0u; i < sys.size_E(); i++) {
        BOOST_TEST(env.mod_total(i%4, i/4) == 1. * env.mod_weight(i) * 1.);
        //                       efficiency --^            signature --^
    }

    // Modification factors should also be updated
    BOOST_TEST(env.mod_factors.max() == 1. + 0.3);  // from mod_factor(2)
    BOOST_TEST(env.mod_factors.min() == 1.);
    BOOST_TEST(
        Matrix(1. + env.mod_total) == env.mod_factors, tt::per_element()
    );

    // Bad environment state size throws an error
    check_exception<std::invalid_argument>([&](){
        env.update_total_modification({1, 2, 3, 4});
    },
    "Size mismatch"
    );
}


/// Signature matrix mutations
BOOST_AUTO_TEST_CASE(signature_matrix_mutation, *utf::tolerance(1e-10)) {
    using Utopia::Utils::append_row_col;

    // Increase system size to have more values: 40 * 4 * 4 = 640
    sys._size_E = 40;
    auto env = Environment(sys, get_as<Config>("all_equal", cfg));
    BOOST_TEST(env.mean_signature_density() == 0.);
    BOOST_TEST(env.signature_matrices().size() == 40);

    // Without gain probability, mutations have no effect
    env.p_mod_gain = 0.;
    env.p_mod_lose = 1.;
    env.mutate_signatures(1, 0, rng);
    BOOST_TEST(env.mean_signature_density() == 0.);

    // Flip the offspring completely to 1
    env.p_mod_gain = 1.;
    env.p_mod_lose = 0.;
    env.mutate_signatures(3, 0, rng);
    BOOST_TEST(env.mean_signature_density() == 7./16);

    // Child row and column need to be at zero: can't repeat this
    check_exception<std::invalid_argument>([&](){
        env.mutate_signatures(3, 0, rng);
    },
    "offspring have no entries"
    );

    // ... actually add a zero-valued row and column
    env.apply_to_signatures([&](auto, auto& sig){ append_row_col(sig); });

    // should copy values over from the parent => 1 in last two rows and cols
    env.mutate_signatures(4, 3, rng);
    BOOST_TEST(env.mean_signature_density() == 16./25);

    // Now with flipping probabilities
    env.p_mod_gain = .5;
    env.p_mod_lose = .5;
    env.apply_to_signatures([&](auto, auto& sig){ append_row_col(sig); });

    env.mutate_signatures(5, 1, rng);
    // ... 6 + 5 = 11 of 36 values might have flipped
    BOOST_TEST(env.mean_signature_density() >= 16./36);
    BOOST_TEST(env.mean_signature_density() <= (16.+11.)/36);
}


BOOST_AUTO_TEST_SUITE_END() // "benchmarks"
