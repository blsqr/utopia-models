#ifndef UTOPIA_MODELS_MEEVO_TEST_TESTTOOLS_HH
#define UTOPIA_MODELS_MEEVO_TEST_TESTTOOLS_HH

#include <yaml-cpp/yaml.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include <utopia/core/testtools.hh>

#include <UtopiaUtils/utils/testtools.hh>

#include "../types.hh"


namespace Utopia::Models::mEEvo {

using namespace Utopia::TestTools;
using namespace Utopia::Utils;


// -- Fixtures ----------------------------------------------------------------

/// A base infrastructre for mEEvo, using the same default RNG
using InfrastructureBase = Utopia::TestTools::BaseInfrastructure<ModelRNG>;


// -- Model-specific test helpers ---------------------------------------------



} // end namespace

#endif // UTOPIA_MODELS_MEEVO_TEST_TESTTOOLS_HH
