---
defaults:
  system:
    log_level: trace
    n_resources: 1
    n_species: 1
    n_envmod: 1

    resources: &resources_defaults
      biomass_density: 100.
      mass: 1.
      refill_rate: 1.
      carrying_capacity: 100.
      efficiency: .45

    species: &species_defaults
      biomass_density: 1.
      mass: 100.
      feeding_center: 1.
      niche_width: 1.
      efficiency: .85
      frac_diverted: 0.

    environment: &environment_defaults
      energy: 0.
      reservoir_size: 1.
      decay_rate: 1.e-3
      weight: +1.
      p_mod_gain: .05
      p_mod_lose: .05

    respiration_rate_parameter: .314
    attack_rate_parameter: 1.
    handling_time_parameter: .398

    c_food: .6
    c_intra: 1.4
    eps: 2.e-4
    recycling_factor: 0.
    quad_nsigma: 3.
    track_trophic_incoherence: true

    network_extraction:
      threshold_mode: average
      threshold: .75                          # ref: .75

    self_interaction: false
    ia_threshold: 1.e-12
    mutation: &mutation_defaults
      mode: Allhoff2015
      m_range: [0.5, 2.0]
      f_log10_range: [-3.0, -0.5]
      s_range: [0.5, 1.5]
      inheritance_strength: 0.
      env_mode: gain_lose
      frac_diverted:
        p_gain: 1.
        p_lose: 0.
        sigma: 0.003
      distr_to_env:
        new_range: [-3.0, +1.0]
        mutate_range: [-0.5, +0.05]
    biomass_correction: { mode: keep_parent }
    parent_selection: {mode: uniform, min_parent_size: 0.}
    network_extraction: {}
    viability_age: 100

# -----------------------------------------------------------------------------
cases:
  # With default parameters
  simple: &simple
    params:
      log_level: trace
      n_resources: 3
      n_species: 7
      n_envmod: 2

      resources: *resources_defaults
      species: *species_defaults
      environment: *environment_defaults
      respiration_rate_parameter: .314
      attack_rate_parameter: 1.
      handling_time_parameter: .398

      c_food: .6
      c_intra: 1.4
      eps: 2.e-4
      recycling_factor: 0.
      quad_nsigma: 3.
      track_trophic_incoherence: true

      network_extraction:
      threshold_mode: average
      threshold: .75                          # ref: .75

      self_interaction: false
      ia_threshold: 1.e-12
      mutation: *mutation_defaults
      biomass_correction: { mode: keep_parent }
      parent_selection: {mode: uniform, min_parent_size: 0.}
      network_extraction: {}
      viability_age: 100

  # With some species specific parameters
  species_specific:
    params: &species_specific
      _is_species_specific: true  # used in test

      log_level: trace

      n_resources: 2
      n_species: 3
      n_envmod: 4

      resources:
        biomass_density: [100., 200.]
        mass: [1., 2.]
        refill_rate: [1., 2.]
        carrying_capacity: [100., 200.]
        efficiency: [.45, .90]

      species:
        biomass_density: [1., 2., 3.]
        mass: [100., 200., 300.]
        feeding_center: [1., 2., 3.]
        niche_width: [1., 2., 3.]
        efficiency: [.85, .90, .95]
        frac_diverted: [0, .01, .02]

      environment:
        energy: [0, 1, 2, 3]
        reservoir_size: [1, 2, 3, 4]
        decay_rate: [1.e-3, 2.e-3, 3.e-3, 4.e-3]
        weight: [1, 2, 3, 4]
        p_mod_gain: .05
        p_mod_lose: .05

      respiration_rate_parameter: .314
      attack_rate_parameter: 1.
      handling_time_parameter: .398

      # slightly different competition parameters
      c_food: .8
      c_intra: 1.1
      eps: 2.e-4
      recycling_factor: 0.
      quad_nsigma: 3.
      track_trophic_incoherence: true

      network_extraction:
      threshold_mode: average
      threshold: .75                          # ref: .75

      self_interaction: false
      ia_threshold: 1.e-12
      mutation: *mutation_defaults
      biomass_correction: { mode: keep_parent }
      parent_selection: {mode: uniform, min_parent_size: 0.}
      network_extraction: {}
      viability_age: 100

  # With self-interaction
  with_self_interaction:
    params:
      log_level: trace

      n_resources: 2
      n_species: 4
      n_envmod: 5

      resources: *resources_defaults
      species: *species_defaults
      environment: *environment_defaults
      respiration_rate_parameter: .314
      attack_rate_parameter: 1.
      handling_time_parameter: .398

      c_food: .6
      c_intra: 1.4
      eps: 2.e-4
      recycling_factor: 0.
      quad_nsigma: 3.
      track_trophic_incoherence: true

      network_extraction:
      threshold_mode: average
      threshold: .75                          # ref: .75

      self_interaction: true
      ia_threshold: 1.e-12
      mutation: *mutation_defaults
      biomass_correction: { mode: keep_parent }
      parent_selection: {mode: uniform, min_parent_size: 0.}
      network_extraction: {}
      viability_age: 100

  # These are not expected to work
  missing_params:
    params: {}
    throws: Utopia::KeyError
    match: The given node contains no entries!

  bad_params:
    params:
      n_resources: 2
      n_species: 5
      n_envmod: 3
      resources:
        biomass_density: [1,2,3,4,5,6]  # not matching the number of resources!
      species: {}
    throws: std::logic_error
    match: incompatible matrix dimensions


# Test cases where environment is enabled
cases_with_envmod:
  simple: *simple

  bad_environment_mod_factors:
    params:
      log_level: trace

      n_resources: 2
      n_species: 4
      n_envmod: 5

      resources: *resources_defaults
      species: *species_defaults
      environment:
        energy: 0.
        reservoir_size: 1.
        decay_rate: 1.e-3
        weight: [+1, -.1, -.2, -.3, -.4001]  # negative < -1
        p_mod_gain: .05
        p_mod_lose: .05

      respiration_rate_parameter: .314
      attack_rate_parameter: 1.
      handling_time_parameter: .398

      c_food: .6
      c_intra: 1.4
      eps: 2.e-4
      recycling_factor: 0.
      quad_nsigma: 3.
      track_trophic_incoherence: true

      network_extraction:
      threshold_mode: average
      threshold: .75                          # ref: .75

      self_interaction: true
      ia_threshold: 1.e-12
      mutation: *mutation_defaults
      biomass_correction: { mode: keep_parent }
      parent_selection: {mode: uniform, min_parent_size: 0.}
      network_extraction: {}
      viability_age: 100
    throws: std::invalid_argument
    match: Negative modification weights may not sum up to below -1.0!


# -----------------------------------------------------------------------------
test_find:
  system: *species_specific
  # ... leading to resource indices [0, 1] and species [2, 3, 4]

  cases:
    # ... comparing to the system state, i.e. `attr_name: B` (default) ........
    largest:
      params:
        mode: max
        attr_name: B
        expected: [4]

    smallest:
      params:
        mode: min
        attr_name: B
        expected: [2]

    gt:
      params:
        mode: ">"
        attr_name: B
        rhs: 2.
        expected: [4]
    ge:
      params:
        mode: ">="
        attr_name: B
        rhs: 2.
        expected: [3, 4]
    le:
      params:
        mode: "<="
        attr_name: B
        rhs: 2.
        expected: [2, 3]
    lt:
      params:
        mode: "<"
        attr_name: B
        rhs: 4.
        expected: [2, 3, 4]
    eq:
      params:
        mode: "=="
        attr_name: B
        rhs: 3.
        expected: [4]
    ne:
      params:
        mode: "!="
        attr_name: B
        rhs: 0.
        expected: [2, 3, 4]

    finite:
      params:
        mode: finite
        attr_name: B
        expected: [2, 3, 4]
    nonfinite:
      params:
        mode: nonfinite
        attr_name: B
        expected: []

    by_id:
      params:
        mode: is_any_of
        attr_name: ids
        rhs: [3, 4, 5]
        expected: [2, 3, 4]

    # Having checked all modes above, go through some other attributes ........
    efficiency:
      params:
        mode: "=="
        attr_name: lambda
        rhs: .9
        expected: [3]

    trophic_level:
      params:
        mode: ">"
        attr_name: lambda
        rhs: 0.
        expected: [2, 3, 4]

    x:
      params:
        mode: "<"
        attr_name: x
        rhs: 0.
        expected: []

    # Resource access .........................................................
    res_by_id:
      params:
        mode: is_any_of
        only: resources
        attr_name: ids
        rhs: [2]
        expected: [1]

    all_res:
      params:
        only: resources
        attr_name: ids
        mode: ">"
        rhs: 0
        expected: [0, 1]

    # Error messages ..........................................................
    err_bad_mode:
      params:
        mode: bad
        attr_name: B
      throws: std::invalid_argument
      match: Invalid mode 'bad' for find_indices!

    err_bad_only:
      params:
        mode: ==
        attr_name: B
        only: foo
      throws: std::invalid_argument
      match: Got invalid `only` argument 'foo'!
