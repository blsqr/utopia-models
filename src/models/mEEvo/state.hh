#ifndef UTOPIA_MODELS_MEEVO_STATE_HH
#define UTOPIA_MODELS_MEEVO_STATE_HH

#include <cmath>
#include <armadillo>
#include <boost/operators.hpp>

#include <UtopiaUtils/utils/odeint.hh> // For specializing vector space algebra


namespace Utopia::Models::mEEvo {

/// A structured state class, consisting of Biomass state and Environment state
/** This is meant to comply to boost.odeint vector_space_algebra while at the
  * same time allowing to have multiple vector-like objects as states.
  * With a structured state object, a number of operations are far easier and
  * faster to carry out, e.g. because they don't have to be crammed into a
  * single vector object (which is not meant to represent the structural
  * differences between the environment and biomass state).
  */
struct BDEState
:
    boost::additive1<BDEState,
        boost::additive2<BDEState, double,
            boost::multiplicative2<BDEState, double>
        >
    >
{
    using Vector = arma::Col<double>;

    /// Biomass density of resources and species
    Vector B;

    /// Energy diverted from the species to be distributed into the environment
    Vector D;

    /// Energy stored in environment reservoirs
    Vector E;

    BDEState () : B(), D(), E() {}

    BDEState (const Vector& _B,
              const Vector& _D = {},
              const Vector& _E = {})
    :
        B(_B), D(_D), E(_E)
    {}

    template<class Fill>
    BDEState (const std::size_t n_B,
              const std::size_t n_D = 1,
              const std::size_t n_E = 1,
              const Fill fill_val = arma::fill::zeros)
    :
        B(n_B, fill_val)
    ,   D(n_D, fill_val)
    ,   E(n_E, fill_val)
    {}

    /// The size of the *full* state
    auto size () const {
        return B.size() + D.size() + E.size();
    }

    /// Resize to the size of respective members of the other BDEState object
    void resize_to (const BDEState& other) {
        B.resize(other.B.size());
        D.resize(other.D.size());
        E.resize(other.E.size());
    }

    BDEState& operator+= (const BDEState& other) {
        B += other.B;
        D += other.D;
        E += other.E;
        return *this;
    }

    BDEState& operator+= (const double a) {
        B += a;
        D += a;
        E += a;
        return *this;
    }

    BDEState& operator*= (const double a) {
        B *= a;
        D *= a;
        E *= a;
        return *this;
    }
};

BDEState operator/ (const BDEState& S1, const BDEState& S2) {
    return BDEState(
        S1.B / S2.B,
        S1.D / S2.D,
        S1.E / S2.E
    );
}

BDEState abs (const BDEState& S) {
    return BDEState(arma::abs(S.B), arma::abs(S.B), arma::abs(S.E));
}

} // namespace Utopia::Models::mEEvo


// -- Further overloads to support vector_space_algebra -----------------------
namespace boost::numeric::odeint {

using Utopia::Models::mEEvo::BDEState;

template<>
struct vector_space_norm_inf<BDEState> {
    using result_type = double;

    double operator() (const BDEState& S) const {
        return std::max(S.B.max(), std::max(S.D.max(), S.E.max()));
    }
};

// Resizing adaptors . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
// See  https://stackoverflow.com/a/41820991/1827608  for more information.

template<>
struct is_resizeable<BDEState> {
    using type = boost::true_type;
    const static bool value = type::value;
};

template<>
struct same_size_impl<BDEState, BDEState> {
    static bool same_size (const BDEState& S1, const BDEState& S2) {
        return (
            S1.B.size() == S2.B.size() and
            S1.D.size() == S2.D.size() and
            S1.E.size() == S2.E.size()
        );
    }
};

template<>
struct resize_impl<BDEState, BDEState> {
    static void resize (BDEState& S1, const BDEState& S2) {
        S1.resize_to(S2);
    }
};


} // namespace boost::numeric::odeint

#endif // UTOPIA_MODELS_MEEVO_STATE_HH
