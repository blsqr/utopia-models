"""Submodule for universe plots"""

from .network import interaction_network, genealogy

# Make the xarray plotting functionality available here
from ..xrplotting import uni_via_xr as via_xr
