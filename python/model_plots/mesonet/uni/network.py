"""Implements plotting methods for the consumer resource interaction network"""

from itertools import chain
import logging

import numpy as np
import xarray as xr
import networkx as nx

from utopya.eval import is_plot_func, PlotHelper, UniversePlotCreator, DataManager, UniverseGroup

from ..analysis import genealogy as gen

log = logging.getLogger(__name__)

# -----------------------------------------------------------------------------
# Helper functions

def calc_layout(g: nx.Graph, *, algo: str, use_graphviz: bool, root,
                graphviz_args: str='', **nx_layout_kwargs) -> dict:
    """Helper to calculate node positions

    Args:
        g (nx.Graph): The graph to calculate positions for
        algo (str): Which algorithm to use
        use_graphviz (bool, optional): Whether to use graphviz for layouting
        root: The root node, needed for some graphviz algorithms
        graphviz_args (str, optional): Passed to nx.nx_agraph.graphviz_layout
        **nx_layout_kwargs: networkx layouting kwargs

    Returns:
        dict: Node positions
    """
    log.remark("Layouting using %s ... (algorithm: %s)",
               "graphviz" if use_graphviz else "networkx",
               algo if algo else "default")

    if use_graphviz:
        return nx.nx_agraph.graphviz_layout(g, prog=algo, root=root,
                                            args=graphviz_args)

    # Use networkx' layouting algorithm
    layout_func = getattr(nx, algo + '_layout')
    return layout_func(g, **nx_layout_kwargs)


# -----------------------------------------------------------------------------

@is_plot_func(creator_type=UniversePlotCreator)
def interaction_network(dm: DataManager, *, uni: UniverseGroup,
                        hlpr: PlotHelper, process: dict, time_idx: int=-1,
                        size_from: str=None, color_from: str=None,
                        restype2color: dict=None, restype2size: dict=None,
                        layout_kwargs: dict=None, draw_kwargs: dict=None):
    """Plots the interaction network."""
    # --- Get topology data
    # TODO Improve performance
    topology = uni['data/mesonet/consumer/topology'].isel().isel(time=time_idx)
    topology = topology.dropna('id')
    log.trace("Topology data: %s", topology)

    # --- Build the network
    g = nx.DiGraph()

    # Keep track of node names; this is their only identifier
    res_pri = set()
    res_sec = set()
    consumers = set()

    # Iterate over consumers and add both the consumers and the resources
    for props in topology:
        consumer_id = int(props.coords['id'])
        props = props.item()

        consumer_name = "c{}".format(consumer_id)
        consumers.add(consumer_name)

        for res_pri_id in props['innovations_primary_source_ids']:
            res_pri_name = "p{}".format(res_pri_id)
            res_pri.add(res_pri_name)
            g.add_edge(res_pri_name, consumer_name)

        for res_sec_id in props['innovations_secondary_source_ids']:
            res_sec_name = "s{}".format(res_sec_id)
            res_sec.add(res_sec_name)
            g.add_edge(res_sec_name, consumer_name)


    # --- Style the network
    draw_kwargs = draw_kwargs if draw_kwargs else {}

    # color
    if color_from is None:
        draw_kwargs['node_color'] = [restype2color[nn[0]] for nn in g.nodes]
    else:
        draw_kwargs['node_color'] = [node_attrs[color_from]
                                     for node_attrs in g.nodes.values()]

    if size_from is None:
        draw_kwargs['node_size'] = [restype2size[nn[0]] for nn in g.nodes]
    else:
        draw_kwargs['node_size'] = [node_attrs[size_from]
                                    for node_attrs in g.nodes.values()]

    # --- Plot it
    # ... without the helper
    hlpr.ax.axis('off')

    # Layout it
    layout_kwargs = layout_kwargs if layout_kwargs else {}
    if layout_kwargs.get('algo') == 'bipartite':
        layout_kwargs['nodes'] = [*res_pri, *res_sec]

    pos = calc_layout(g, root=None, **layout_kwargs)

    # Draw it
    nx.draw(g, pos=pos, ax=hlpr.ax, **draw_kwargs)


@is_plot_func(creator_type=UniversePlotCreator)
def genealogy(dm: DataManager, *, uni: UniverseGroup, hlpr: PlotHelper,
              process: dict, time_idx: int=-1,
              color_from: str='generation',
              layout: str='random', layout_kwargs: dict=None,
              draw_kwargs: dict=None):
    """Plots the genealogy"""
    # --- Get parents data and build network from it
    parents = uni['data/mesonet/consumer/parents'].isel(time=time_idx).data

    g = gen.build_genealogy(parents)
    generations = gen.find_generations(g)

    # --- Style it
    draw_kwargs = draw_kwargs if draw_kwargs else {}

    if color_from:
        draw_kwargs['node_color'] = [node_attrs[color_from]
                                     for node_attrs in g.nodes.values()]

    # --- Plot it
    # ... without the helper
    hlpr.ax.axis('off')

    # Layout it
    layout_kwargs = layout_kwargs if layout_kwargs else {}
    if layout_kwargs.get('algo') == 'shell':
        layout_kwargs['nlist'] = generations

    pos = calc_layout(g, root=generations[0][0], **layout_kwargs)

    # Draw it
    nx.draw(g, pos=pos, ax=hlpr.ax,
            **(draw_kwargs if draw_kwargs else {}))
