"""This submodule can be used to build and analyse genealogies"""

from typing import List
import logging

import xarray as xr
import networkx as nx

log = logging.getLogger(__name__)

# -----------------------------------------------------------------------------

def build_genealogy(parents: List[xr.DataArray]) -> nx.DiGraph:
    """Given a list of parents, build a genealogy, i.e. a directed graph of
    inheritance relationships.
    """
    g = nx.DiGraph()

    # Iterate over list of parents and add both the parent and the offspring
    for entry in parents:
        consumer_id = int(entry.coords['id'])
        parent_id = int(entry)

        # parent_id 0 denotes that the consumer was available from the start
        if parent_id == 0:
            # No need to add an edge
            continue

        g.add_edge(parent_id, consumer_id)

    log.remark("Built genealogy with %d node(s), %d edge(s).",
               g.number_of_nodes(), g.number_of_edges())
    return g


def find_generations(g: nx.DiGraph) -> list:
    """Given a genealogy, finds the generations and returns a list where the
    list index specifies the generation number.
    """
    generations = [[n for n in g.nodes if g.in_degree(n) == 0]]

    while generations[-1]:
        children = [list(g.successors(parent)) for parent in generations[-1]
                    if g.out_degree(parent)]
        generations.append(sum(children, []))
    generations.pop()

    log.remark("Identified %s generations:\n%s", len(generations),
               "\n".join(["   {:2d}:  {}".format(gen_no, nodes)
                          for gen_no, nodes in enumerate(generations)]))

    # Now have all generations, write to attributes
    for gen_no, nodes in enumerate(generations):
        for node in nodes:
            g.node[node]['generation'] = gen_no

    return generations
