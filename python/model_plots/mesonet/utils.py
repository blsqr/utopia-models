"""Plot utilities"""

import os
import logging
from typing import TypeVar, Union, Sequence, Dict

import xarray as xr

import utopya
from utopya.eval import DataManager, UniverseGroup

log = logging.getLogger(__name__)

# Convenience function to return (len(l), suffix)
N_and_suffix = lambda l,sfx=("s", ""): (len(l), sfx[0] if len(l)!=1 else sfx[1])

# Type definitions . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
# Expected data
TData = TypeVar('TData', DataManager, UniverseGroup, xr.Dataset)

# Transformation operations definition
TTransformOps = Sequence[Union[str, dict]]


# -----------------------------------------------------------------------------

def process_data(data: TData, *, select_fields: Dict[str, dict]=None,
                 base_path: str=None,
                 transformations: Sequence[dict]=None,
                 transformations_log_level: int=10,
                 log_result: Union[int, bool]=False
                 ) -> xr.Dataset:
    """Processes the data, allowing to select parts of it and apply almost
    arbitrary transformations to it

    This is done in two steps. In the first, certain fields are selected from
    the data using parameters specified in ``select_fields``.
    Based on these selected fields, further transformations can be carried
    out, as specified by the sequence of transform operations given by the
    ``transformations`` argument.

    Each entry of the ``transformations`` can specify the following entries:

        * ``field``: From which field to get the source data on which the
            transformations are to be applied.
        * ``operations``: The sequence of transform operation that are to be
            applied to the data from the selected field.
        * ``target`` (optional): In which field to store the target of this
            transformation. If not given, it is stored back into the source
            field.

    As auxilary data, all other loaded fields are available during the second
    transformations operations, thus allowing binary operations. By storing
    the result in another field and using multiple transformations, almost
    arbitrary operations can be carried out using this interface.


    Args:
        data (TData): The data from which to select fields
        select_fields (Dict[str, dict], optional): Which fields to select from
            the given data. If not given, will select all.
            Values of this dict should be dicts with the key ``path``
            specifying from where to load the data. For dantro-based tree data,
            it is also possible to specify paths here. Additionally, one can
            already apply in-place transformations at this point by adding the
            ``transform`` key.
        base_path (str, optional): If given, relative field paths are regarded
            as relative to this base path.
        transformations (Sequence[dict], optional): The transformations to
            carry out on selected fields. See above for more informations.
        transformations_log_level (int, optional): The logging level for the
            transform operations; useful to increase this for debugging.
        log_result (Union[int, bool], optional): Whether to log the resulting
            dataset. If boolean, will use the REMARK level; if integer, will
            use that value as logging level.

    Returns:
        xr.Dataset: The selected and transformed data with fields stored in
            the data variables of this dataset

    Raises:
        ValueError: When given data was not an xr.Dataset, but no
            ``select_fields`` argument was provided.
    """
    def retrieve_field(data, *, path: str, transform: TTransformOps=None,
                       **attrs) -> xr.DataArray:
        """Retrieve a field from the given data.

        If ``path`` is relative, will use the (outer scope) ``base_path``, if
        given.

        Additional arguments are stored as xr.DataArray attributes.
        """
        if base_path and not os.path.isabs(path):
            path = os.path.join(base_path, path)

        # Need to drop initial "/", if there is one
        path = path[1:] if path[0] == "/" else path

        # Now, select the data
        field = data[path]

        # Apply transformations, if given
        if transform is not None:
            raise NotImplementedError("`transform` is no longer supported!")
            field = udp.transform(field, *transform, aux_data=data,
                                  log_level=transformations_log_level)

        # Carry over attributes
        for k, v in attrs.items():
            field.attrs[k] = v

        # Done.
        return field

    def apply_transformations(data, *, field: str, operations: TTransformOps,
                              target: str=None) -> None:
        """Applies transformation operations on data of the given field and
        stores it back in the data; no return value!

        If a target is not given, stores it back in the specified ``field``.
        """
        raise NotImplementedError("`transform` is no longer supported!")
        target = target if target else field
        data[target] = udp.transform(data[field], *operations,
                                     aux_data=data,
                                     log_level=transformations_log_level)

    # The to-be-populated dataset
    dset = xr.Dataset()


    # Select the fields . . . . . . . . . . . . . . . . . . . . . . . . . . .
    if select_fields:
        log.info("Selecting %d field%s ...", *N_and_suffix(select_fields))

        for field_name, field_params in select_fields.items():
            dset[field_name] = retrieve_field(data, **field_params)

        log.info("Data selected.")

    elif isinstance(data, xr.Dataset):
        # Select all data
        dset = data

    else:
        raise ValueError("Need to specify either an xr.Dataset as data or use "
                         "the select_fields argument to perform a selection "
                         "of data.")


    # Perform additional transformations . . . . . . . . . . . . . . . . . . .
    if transformations:
        log.info("Applying %s transformation sequence%s ...",
                 *N_and_suffix(transformations))

        for transform_params in transformations:
            apply_transformations(dset, **transform_params)

        log.info("Transformation sequence%s applied.",
                 "s" if len(transformations) != 1 else "")

    # Done. Show some information . . . . . . . . . . . . . . . . . . . . . . .
    # Gather the operations that were performed
    ops = []
    if select_fields:
        ops += ["{} selection{}".format(*N_and_suffix(select_fields))]
    if transformations:
        ops += ["{} transformation{}".format(*N_and_suffix(transformations))]

    # Inform the user
    if log_result:
        if isinstance(log_result, bool):
            _log = log.remark
        else:
            _log = lambda *args: log.log(log_result, *args)

        if ops:
            _log("Result of %s:  %s", " and ".join(ops), dset)
        else:
            _log("Result:  %s\n", dset)

    elif ops:
        # ... at least show some basic information
        log.note("Available data variables:      %s",
                 ", ".join(dset.data_vars))
        log.note("Dataset dimensions and sizes:  %s",
                 ", ".join("{}: {}".format(*kv) for kv in dset.sizes.items()))

    return dset
