"""This module provides a bridge to the xarray plotting interface"""

import copy
from typing import Union
import logging

import xarray as xr

from utopya.tools import recursive_update
from utopya.eval import is_plot_func, PlotHelper, UniversePlotCreator, MultiversePlotCreator, DataManager, UniverseGroup

from .utils import process_data

log = logging.getLogger(__name__)

# -----------------------------------------------------------------------------
# Helper functions

def _parse_params(*, data, to_plot: Union[str, dict]=None,
                  **common_params) -> dict:
    """Parses the plotting parameters, ensuring it has a uniform format"""
    # Make sure it's a dict.
    if to_plot is None:
        # Add an empty entry for each data variable
        to_plot = {dv_name: dict() for dv_name in data.data_vars}

    elif isinstance(to_plot, str):
        to_plot = [to_plot]

    if isinstance(to_plot, (tuple, list)):
        to_plot = {dv_name: dict() for dv_name in to_plot}

    # For each value, perform an update with the common parameters. Work on
    # deep copies to be super sure to not have any side effects.
    to_plot = {dv_name: recursive_update(copy.deepcopy(params),
                                         copy.deepcopy(common_params))
               for dv_name, params in to_plot.items()}

    return to_plot


# -----------------------------------------------------------------------------
# Re-usable plot functions

def plot_via_xr(*, data: xr.Dataset, hlpr: PlotHelper,
                to_plot: Union[str, dict]=None, **common_params):
    """Parses the to_plot parameters and invokes the xarray plotting function
    on the specified data variables.
    """
    # Determine the parameters
    to_plot = _parse_params(data=data, to_plot=to_plot, **common_params)

    log.note("Data variables to be plotted:  %s", ", ".join(to_plot.keys()))

    # TODO Can expand this to check dimensionality and create subplots ...
    # Iterate over all fields that are to be plotted
    for dv_name, params in to_plot.items():
        # ... and let xarray do the rest
        data[dv_name].plot(ax=hlpr.ax, **params)



# -----------------------------------------------------------------------------
# Wrappers for PlotCreator interface

@is_plot_func(creator_type=UniversePlotCreator)
def uni_via_xr(dm: DataManager, *, uni: UniverseGroup, hlpr: PlotHelper,
               process: dict, to_plot: Union[str, dict]=None,
               **common_params):
    """A plotting function that bridges to the xarray plotting interface with
    data from each universe.
    """
    data = process_data(uni, **process)
    plot_via_xr(data=data, to_plot=to_plot, hlpr=hlpr, **common_params)


@is_plot_func(creator_type=MultiversePlotCreator)
def mv_via_xr(dm: DataManager, *, mv_data: xr.Dataset, hlpr: PlotHelper,
              process: dict, to_plot: Union[str, dict]=None,
              **common_params):
    """A plotting function that bridges to the xarray plotting interface with
    the pre-selected multiverse data.
    """
    data = process_data(mv_data, **process)
    plot_via_xr(data=data, to_plot=to_plot, hlpr=hlpr, **common_params)
