"""Additional dantro data transformation operations"""

from typing import Union, Sequence

import numpy as np
import xarray as xr
import dantro as dtr


from utopya.eval import is_operation

# -----------------------------------------------------------------------------

@is_operation
def expand_structured_array(d: xr.DataArray, *,
                            dims: Sequence[str]=None,
                            coords: Union[dict, str]='trivial'):
    """Given an xr.DataArray with an underlying array that has a structured
    data type, converts it into a higher-dimensional xr.DataArray
    """
    d = xr.DataArray(d)
    arr = d.data
    dt = arr.dtype

    if dt.ndim < 1 or not dt.subdtype or dt.fields:
        raise ValueError(f"Expected simple structured dtype, got: {dt}")

    # Take care of dimension names and coordinates
    if dims is None:
        dims = tuple([f"inner_dim_{n:d}" for n in range(dt.ndim)])

    if coords == "trivial":
        coords = {n: "trivial" for n in dims}

    elif not isinstance(coords, dict):
        raise TypeError(
            f"Argument `coords` needs to be a dict or str, but was {coords}!"
        )

    if set(coords.keys()) != set(dims):
        raise ValueError(
            "Mismatch between dimension names and coordinate keys! Make sure "
            "there are coordinates specified for each dimension of the inner "
            f"arrays, {dims}! Got:\n{coords}"
        )


    # Now, let's reinterpret the data
    sub_dt, sub_shape = dt.subdtype
    arr_view = arr.view((sub_dt, sub_shape))

    # Assemble coordinates and dimensions
    new_dims = d.dims + tuple(dims)

    coords = {n: (range(l) if isinstance(c, str) and c == "trivial" else c)
              for (n, c), l in zip(coords.items(), sub_shape)}
    new_coords = dict(d.coords, **coords)

    return xr.DataArray(arr_view, dims=new_dims, coords=new_coords,
                        name=d.name, attrs=d.attrs)
