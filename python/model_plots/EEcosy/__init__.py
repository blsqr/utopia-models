"""Plots that are specific to the EEcosy model"""

# Import the module to trigger registration of operations
from .operations import *
