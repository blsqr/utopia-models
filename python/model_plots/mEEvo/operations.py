"""Additional dantro data transformation operations"""

import logging
from typing import Tuple, Any, Union
import random
import math

import numpy as np
import pandas as pd
import xarray as xr

import scipy
import scipy.ndimage

from dantro.data_ops.arr_ops import merge as _merge

from utopya.eval import is_operation

log = logging.getLogger(__name__)


# -- Calculations -------------------------------------------------------------

@is_operation
def transform_coords(
    d: xr.DataArray, trfs: dict, *, rename: str = "{ufunc:} {name:}"
) -> xr.DataArray:
    """Transforms coordinates using xarray's unary functions and returns a new
    object with transformed coordinates.

    Args:
        d (xr.DataArray): The data the coordinates of which are to be
            transformed
        trfs (dict, optional): Transformations dictionary, mapping coordinate
            name to the name of a numpy unary function
        rename (str, optional): The format string to use for renaming
            the corresponding dimensions. By default, prepends the name of the
            unary function. To not rename, pass something that evaluates to
            False.
    """
    # TODO Implement same interface as in xarray for passing arguments
    # if bool(kwargs) == bool(trfs):
    #     raise ValueError("Transformation dict needs to be passed via position")

    log.remark(
        "Transforming %d coordinates ...\n%s", len(trfs),
        "\n".join(f"{coord:>20s} : {ufunc}" for coord, ufunc in trfs.items())
    )
    for coord, ufunc in trfs.items():
        d = d.assign_coords(
            {coord: getattr(np, ufunc)(d.coords[coord])}
        )

    if rename:
        d = d.rename({
            coord: rename.format(ufunc=ufunc, name=coord)
            for coord, ufunc in trfs.items()
        })

    return d


# WIP
def _center_of_mass(d: xr.DataArray, *,
                    dims: list,
                    coord_transformations: dict = None):
    """Computes the center of mass of the given DataArray along the given
    dimensions and returns the corresponding coordinates.

    The ``coord_transformations`` argument denotes coordinate names that are
    to be transformed prior to this operation using ``transform_coords``.
    """
    if coord_transformations:
        d = transform_coords(d, coord_transformations)

    # TODO Continue here
    raise NotImplementedError("Not implemented in a general way! "
                              "Use the specialised `center_of_mass_regular")


def _coords_from_idcs(
    d: xr.DataArray, idcs: tuple, *, outer_coords: dict, **interp_kwargs
) -> xr.Dataset:
    """Given some coordinate index tuple, retrieves the corresponding
    coordinate values from ``d`` and assembles them into an xr.Dataset.
    If index values are not integer, linearly interpolates.
    """
    if len(d.dims) != len(idcs):
        raise ValueError(
            f"Mismatch between data dimensions ({d.dims}) and given "
            f"coordinate indices {idcs}!"
        )

    outer_dims = tuple(outer_coords.keys())
    coms = []
    for dim, idx in zip(d.dims, idcs):
        if dim in outer_coords:
            continue

        # Need trivial coordinates, because interp works on coordinate _values_
        coord_vals = d.coords[dim]
        coord_vals = coord_vals.assign_coords({dim: range(len(coord_vals))})

        try:
            pos = coord_vals.interp({dim: float(idx)}, **interp_kwargs)
        except:
            try:
                pos = coord_vals.isel({dim: int(idx)})
            except:
                log.caution(
                    f".interp and .isel failed for dim '{dim}': {idx} "
                    f"({type(idx).__name__}, outer coords: {outer_coords})"
                )
                pos = np.array(np.nan)  # to allow .item() call

        pos = pos.item()
        coms.append(
            xr.DataArray(
                [pos],
                name=dim,
                dims=outer_dims,
                coords=outer_coords,
            )
        )
    return xr.merge(coms)


@is_operation
def center_of_mass_regular(d: xr.DataArray, *,
                           dims: list,
                           coord_transformations: dict = None,
                           trf_kwargs: dict = {},
                           interp_kwargs: dict = {},
                           ) -> xr.Dataset:
    """Computes the center of mass of a DataArray with regularly-spaced
    coordinate values.

    Returns a 1D xr.Dataset with data variables for the dimensions over which
    the 2D center of mass was calculated.

    .. warning::

        Assumes fully regularly spaced coordinates and dimension sizes > 1

    Args:
        d (xr.DataArray): The data
        keep_dim (str, optional): If given, will not reduce this dimension to iterate over (will not be reduced)
        coord_transformations (dict, optional): Optional coordinate
            transformations that will be carried out prior to calculating the
            center of mass.
    """
    if coord_transformations:
        d = transform_coords(d, coord_transformations, **trf_kwargs)

    keep_dim = [d for d in d.dims if d not in dims]
    if len(keep_dim) != 1:
        raise ValueError(f"Need single keep_dim for now, got: {keep_dim}")
    keep_dim = keep_dim[0]

    # Construct a COM dataset for each slice of the data
    com = []
    for c, m in d.groupby(keep_dim):
        com_idcs = scipy.ndimage.measurements.center_of_mass(m.data)
        com.append(
            _coords_from_idcs(
                m, com_idcs, outer_coords={keep_dim: [c]}, **interp_kwargs
            )
        )

    return xr.merge(com)




# -- Plotting tools -----------------------------------------------------------

@is_operation
def vmin_vmax_centered_around(
    data, vc: float, *,
    quantiles: Tuple[float, float] = None,
    max_dist: float = None,
) -> Tuple[float, float]:
    """Computes (vmin, vmax) tuple such that vc is in the middle of that
    range, useful e.g. for using diverging colormaps.
    """
    if quantiles is None:
        vmin, vmax = float(data.min()), float(data.max())
    else:
        qmin, qmax = quantiles
        vmin, vmax = float(data.quantile(qmin)), float(data.quantile(qmax))

    dist = max(abs(vc - vmin), abs(vmax - vc))
    if max_dist is not None:
        dist = min(max_dist, dist)

    return (vc - dist, vc + dist)


# -----------------------------------------------------------------------------
# -- Sampling -----------------------------------------------------------------

# FIXME Doesn't work ... and probably the wrong approach anyway
@is_operation
def sample_from_bins(
    d: Union[xr.DataArray, xr.Dataset, pd.DataFrame],
    *,
    samples_per_bin: int,
    bin_edges: list,
    var: str = None,
    enforce_N: bool = False,
):
    """Bins the data along the given dimension and then samples elements from
    each bin.
    Returns a re-combined xr.DataArray with the same structure (but potentially
    fewer elements).

    If there is more than one coordinate dimension, the data will internally be
    stacked along all dimensions given in ``dims``.

    The ``var`` parameter is required for dataset-like data, where only one of
    the data variables can be used for binning.
    """
    # Prepare data
    if isinstance(d, pd.DataFrame):
        log.remark("Converting pd.DataFrame to xr.Dataset ...")
        d = xr.Dataset.from_dataframe(d)

    is_dset = isinstance(d, xr.Dataset)
    if d[var].ndim > 1:
        d = d.stack(dict(stacked=d.dims))
        _dim = "stacked"
        is_stacked = True
    else:
        _dim = d[var].dims[0]
        is_stacked = False

    log.remark("Pre-processed data to:\n%s\n", str(d))

    # Prepare binning and sampling procedure
    selected = []
    bin_edges = sorted(bin_edges)
    bin_ranges = list(zip(bin_edges[:-1], bin_edges[1:]))
    N_bins = len(bin_ranges)

    log.remark("Sampling from %d '%s' values along '%s' dimension ...",
               d.sizes[_dim], var, _dim)
    log.remark("Number of bins:  %d  (edges:  %s)", N_bins, bin_edges)

    bins = [[] for _ in bin_ranges]
    masked = []
    below = []
    above = []

    # Bin the data
    # TODO Make faster -- but need to maintain coordinate information!
    # FIXME ... this is a horrible approach ... stack / unstack arghhh
    log.remark("Binning ...")
    for _, v in d.groupby(_dim):
        # For dataset-like data, need to get the data variable
        _v = v if not is_dset else v[var]

        # For being able to combine them later, need to unstack again
        print("normal: ", v)
        print("unstacked: ", v.unstack())
        v = v if not is_stacked else v.unstack(_dim)

        if np.isnan(_v):
            masked.append(v)
            continue

        if _v < bin_edges[0]:
            below.append(v)
            continue

        for i, (lower, upper) in enumerate(bin_ranges):
            if lower <= _v < upper:
                bins[i].append(v)
                break

        else:
            above.append(v)

    log.remark(
        "Found %d values outside of bins (below: %d, above: %d, masked: %d)",
        len(below) + len(above) + len(masked),
        len(below), len(above), len(masked),
    )

    # Sample from the bins
    log.remark("Sampling ...")
    for i, bin_vals in enumerate(bins):
        # May have to adjust sample size
        _N = samples_per_bin
        if len(bin_vals) < _N and not enforce_N:
            _N = len(bin_vals)
            log.caution(
                "Reduced sample size to %d for bin %s!", _N, bin_ranges[i]
            )

        selected += random.sample(bin_vals, _N)

    log.remark("Sampled %d values (average: %.3g per bin)",
               len(selected), len(selected)/N_bins)

    # Recombine (maintaining coordinate information)
    log.remark("Recombining into xr.DataArray ...")
    print(selected)
    d_sampled = _merge(selected, reduce_to_array=isinstance(d, xr.DataArray))

    return d_sampled
