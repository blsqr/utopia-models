"""A module that implements the computation of various network characteristics.

These follow the naming and definitions given in the PhD thesis (2015) of
Korinna Allhoff, chapter 4.4.2, page 53.
"""
import math
import logging
from functools import partial
from typing import Union, Sequence, Dict, Callable, List

import numpy as np
import xarray as xr

import networkx as nx
import networkx.algorithms
import networkx.algorithms.approximation

from dantro.tools import adjusted_log_levels

from utopya.eval.groups import GraphGroup
from utopya.eval import is_operation

log = logging.getLogger(__name__)


# -----------------------------------------------------------------------------
# General

@is_operation
def build_graph(*, graph_group: GraphGroup,
                register_property_maps: dict=None,
                **kwargs) -> nx.Graph:
    """Builds a graph from the selected graph group and registers property maps
    with it. The returned graph object can then be used to compute topological
    characteristics.
    """
    g = graph_group.create_graph(**kwargs)

    if register_property_maps:
        for k, v in register_property_maps.items():
            g.register_property_map(k, v)

    return g


@is_operation
def get_node_attributes(g: nx.Graph, name: str, default=None) -> xr.DataArray:
    """Returns a labelled array containing the specified attribute values"""
    return xr.DataArray(
        [v for _, v in g.nodes.data(name, default=default)],
        name=f"Node Property '{name}'",
        dims=("node",),
        coords=dict(node=list(g.nodes))
    )


@is_operation
def get_edge_attributes(g: nx.Graph, name: str, default=None) -> xr.DataArray:
    """Returns a labelled array containing the specified attribute values"""
    return xr.DataArray(
        [v for _, _, v in g.edges.data(name, default=None)],
        name=f"Edge Property '{name}'",
        dims=("edge",),
        coords=dict(edge=[i for i in range(g.number_of_edges())],
                    source=("edge", [s for s, _ in g.edges]),
                    target=("edge", [t for _, t in g.edges]))
    )


@is_operation
def set_node_attributes(g: nx.Graph, name: str,
                        values: Union[xr.DataArray, Callable, dict],
                        ) -> nx.Graph:
    """Sets the node attribute with the given name to the given values. This
    is essentially a wrapper around ``nx.set_node_attributes`` which provides
    some additional functionality depending on the type of ``values`` given.

    For xarray data, assumes that the appropriate data is selectable via the
    ``node`` coordinate dimension.

    If a callable was given, it is invoked to determine the value for each
    node. The callable receives the graph and the node name as its positional
    arguments.
    """
    if callable(values):
        for n in g.nodes:
            g.nodes[n][name] = values(g, n)
        return g

    if isinstance(values, xr.DataArray):
        for val in values:
            n = val.coords["node"].item()
            g.nodes[n][name] = val.item()
        return g

    # else: assume its trivially assignable, e.g. a dict
    nx.set_node_attributes(g, values=values, name=name)
    return g


@is_operation
def set_edge_attributes(g: nx.Graph, name: str,
                        values: Union[xr.DataArray, Callable, dict],
                        edge_dim: str = "edge",
                        source_coord: str = "source",
                        target_coord: str = "target",
                        ) -> nx.Graph:
    """Sets the edge attribute with the given name to the given values. This
    is essentially a wrapper around ``nx.set_edge_attributes`` which provides
    some additional functionality depending on the type of ``values`` given.

    For xarray data, assumes that the appropriate data is selectable via the
    ``source`` and ``target`` non-coordinate dimensions.

    If a callable was given, it is invoked to determine the value for each
    edge. The callable receives the graph and the edge name as its positional
    arguments.

    # TODO Add fallback value for missing stuff
    """
    if callable(values):
        for e in g.edges:
            g.edges[e][name] = values(g, e)
        return g

    if isinstance(values, xr.DataArray):
        try:
            vals_midx = values.set_index(
                {edge_dim: (source_coord, target_coord)}
            )
        except:
            # Assume it already has the appropriate multi index
            vals_midx = values

        for s, t in g.edges:
            val = vals_midx.sel({edge_dim: (s, t)}).item()
            g.edges[s, t][name] = val
        return g

    # else: assume its trivially assignable, e.g. a dict
    nx.set_edge_attributes(g, values=values, name=name)
    return g



# -----------------------------------------------------------------------------
# Model-specific
# TODO Use nx.set_node_attributes more frequently!

@is_operation("mEEvo.annotate_network")
def annotate_network(
    g: nx.DiGraph, *, TL_eps_abs: float=0.005, compute_loops: bool=False
) -> nx.DiGraph:
    """Adds node, edge, and graph attributes to the given graph, computing
    several node-, edge-, or graph-specific characteristic values.

    *Requires* the following attributes to already be available in the graph:

    - ``trophic_level`` node property
    - ...

    Node attributes:

    - ``in_degree`` and ``out_degree``
    - ``has_self_loop``: nodes with a self-loop, i.e. cannibalistic nodes
    - ``is_external_resource``: nodes without in-edges, ignoring cannibalism
    - ``without_predator``: nodes without out-edges, ignoring cannibalism
    - ``is_intermediate``: nodes with in- and out-edges, ignoring cannibalism
    - ``clustering_coefficient``: the nodes' clustering coefficient
    - ``max_similarity``: the highest value of each nodes' simrank similarity
    - ``characteristic_path_length``: the characteristic path length to all
        *reachable* nodes, excluding self-edges
    - ``TL_shortest``: shortest trophic level
    - ``TL_prey_avg``: prey-averaged trophic level
    - ``TL_short_weighted``: short-weighted trophic level
    - ``is_herbivore``: nodes that feed *only* on external resources
    - ``is_omnivore``: nodes that feed on more than one (flow-based) TL
    - ``num_cycles``: how many elementary cycles go over this species
    - ``num_<N>cycles``: how many elementary N-cycles go over this species
        (with ``<N>``: 2, 3, 4)

    Edge attributes:

    - ``is_self_edge``: edges that have the same node as source and target

    Trophic levels are computed as defined by Williams et al., 2014.

    Args:
        g (nx.DiGraph): The network to annotate
        TL_eps_abs (float, optional): The absolute tolerance to use when
            iteratively computing trophic levels
        compute_loops (bool, optional): whether to compute loops (may not work
            for large networks)
    """
    # Define some default values
    # NOTE Not all properties assigned below also need a default ...
    node_attr_defaults = dict(
        num_cycles=0, num_2cycles=0, num_3cycles=0, num_4cycles=0,
    )
    for n in g.nodes:
        for k, v in node_attr_defaults.items():
            g.nodes[n][k] = v

    # Need some filtered views ...
    # ... containing only self-edges
    g_only_self_edges = nx.subgraph_view(g,
        filter_edge=lambda s, t: g.edges[s, t]["is_self_edge"],
    )

    # ... containing no self-edges
    g_no_self_edges = nx.subgraph_view(g,
        filter_edge=lambda s, t: not g.edges[s, t]["is_self_edge"]
    )

    # ... containting only basal resource nodes (needed for some algorithms)
    g_only_ext_res = nx.subgraph_view(g,
        filter_node=lambda n: g.nodes[n]["is_external_resource"]
    )

    # .. Basic classifications ................................................
    log.debug("  Computing basic classifications ...")

    # Self-loops
    for s, t in g.edges:
        g.edges[s, t]["is_self_edge"] = bool(s == t)

    # Mark nodes with self-loops by filtering the view such that there are
    # *only* self-edges in the network...
    for n, k in g_only_self_edges.degree:
        g.nodes[n]["has_self_loop"] = bool(k > 0)

    # External resources: nodes with an in-degree of zero (w/o self-loops)
    for n, k_in in g_no_self_edges.in_degree:
        g.nodes[n]["in_degree"] = k_in
        g.nodes[n]["is_external_resource"] = bool(k_in == 0)

    # Top predators: nodes with an out-degree of zero (w/o self-loops)
    for n, k_out in g_no_self_edges.out_degree:
        g.nodes[n]["out_degree"] = k_out
        g.nodes[n]["without_predator"] = bool(k_out == 0)
    # TODO Top predator?

    # Intermediate: nodes with non-zero in- *and* out-degree (w/o self-loops)
    for (n, k_in), (_, k_out) in zip(g_no_self_edges.in_degree,
                                     g_no_self_edges.out_degree):
        g.nodes[n]["is_intermediate"] = bool(k_in > 0 and k_out > 0)

    # .. Network mesaures .....................................................
    log.debug("  Computing network measures ...")

    # Clustering coefficient
    from networkx.algorithms.cluster import clustering
    for n in g.nodes:
        g.nodes[n]["clustering_coefficient"] = clustering(g, n)
        # NOTE Ignores self-loops

    # Similarity, excluding self-similarity
    from networkx.algorithms.similarity import simrank_similarity
    similarity = simrank_similarity(g, tolerance=0.005, max_iterations=40)
    for n in g.nodes:
        g.nodes[n]["max_similarity"] = max([
            s for _n, s in similarity[n].items() if _n != n
        ])

    # Characteristic path length, excluding self-loops
    # Need shortest paths matrix for that (here: dict of dicts)
    from networkx.algorithms.shortest_paths.generic import shortest_path
    shortest_paths = shortest_path(g_no_self_edges)

    for n in g.nodes:
        g.nodes[n]["characteristic_path_length"] = np.array([
            len(path) - 1 for path in shortest_paths[n].values()
        ]).mean()


    # .. Trophic levels .......................................................
    log.debug("  Computing trophic levels ...")
    # ... as defined in Williams et al., 2014

    # Flow-based TL (real-valued)
    # ... is already computed during the C++ run (see `trophic_level`)

    # Shortest TL (discrete-valued)
    for n in g.nodes:
        g.nodes[n]["TL_shortest"] = min([
            len(shortest_paths[src][n])  # + 1 - 1
            for src in g_only_ext_res.nodes
        ])

    # TODO Trophic coherence?!

    # Prey-averaged TL (real-valued) . . . . . . . . . . . . . . . . . . . . .
    # Use flow-based TL for initial values
    TL_prey_avg = get_node_attributes(g, "trophic_level")
    TL_prey_avg.name = "TL_prey_avg"
    i = 0
    diff = np.inf

    while i <= 100 and diff > TL_eps_abs:
        _new_TL = xr.zeros_like(TL_prey_avg)
        for n in g.nodes:
            if not g.in_degree(n):
                _new_TL.loc[dict(node=n)] = 1.
                continue

            _new_TL.loc[dict(node=n)] = (
                1. +
                TL_prey_avg.sel(
                    node=list(g_no_self_edges.predecessors(n))
                    # TODO Is it correct to ignore self-edges?!
                ).mean()
            )

        i += 1
        diff = abs((TL_prey_avg - _new_TL).max())
        TL_prey_avg = _new_TL
    log.debug("Computed prey-averaged TL in %d iterations to an absolute "
              "accuracy of %f < %f.", i, diff, TL_eps_abs)

    for n in g.nodes:
        g.nodes[n]["TL_prey_avg"] = float(TL_prey_avg.sel(node=n))

    # Short-weighted TL (real-valued)
    TL_shortest = get_node_attributes(g, "TL_shortest")
    for n in g.nodes:
        g.nodes[n]["TL_short_weighted"] = np.mean([
            TL_shortest.sel(node=n), TL_prey_avg.sel(node=n),
        ])



    # .. TL-based measures ....................................................
    log.debug("  Computing measures based on trophic levels ...")
    # Herbivory
    for n in g_no_self_edges.nodes:
        g.nodes[n]["is_herbivore"] = all([
            g.nodes[res]["is_external_resource"]
            for res in g_no_self_edges.predecessors(n)
        ])

    # Omnivory, using the rounded-down flow-based trophic level
    for n in g_no_self_edges.nodes:
        g.nodes[n]["is_omnivore"] = len(set([
            int(g.nodes[prey]["trophic_level"])
            for prey in g_no_self_edges.predecessors(n)
        ])) > 1


    # .. Loops/Cycles .........................................................
    if compute_loops:
        log.debug("  Computing loops ...")
        from networkx.algorithms.cycles import simple_cycles

        # Simple cycles
        for cycle in simple_cycles(g_no_self_edges):
            for n in cycle:
                g.nodes[n]["num_cycles"] += 1
                g.nodes[n]["num_2cycles"] += int(len(cycle) == 2)
                g.nodes[n]["num_3cycles"] += int(len(cycle) == 3)
                g.nodes[n]["num_4cycles"] += int(len(cycle) == 4)

    # Food Chains
    # TODO

    # print(
    #     "node data\n" +
    #     "\n".join([f"-- {n:>3d}: {v}" for n, v in g.nodes(data=True)])
    # )
    # print(
    #     "edge data\n" +
    #     "\n".join([f"-- {s:} -> {t:}: {v}" for s, t, v in g.edges(data=True)])
    # )

    return g

@is_operation("mEEvo.build_and_annotate_network")
def build_and_annotate_network(*, graph_group: GraphGroup,
                               graph_creation: dict = {},
                               register_property_maps: dict = {},
                               **annotate_kwargs) -> nx.DiGraph:
    """Builds a graph from the selected graph group, registers property maps,
    and annotates it.
    """
    g = graph_group.create_graph(**graph_creation)

    if register_property_maps:
        for k, v in register_property_maps.items():
            g.register_property_map(k, v)

    return annotate_network(g, **annotate_kwargs)

@is_operation("mEEvo.compute_nw_characteristics")
def compute_nw_characteristics(
    *,
    graph_group: GraphGroup,
    graph_creation: dict = {},
    to_compute: List[str] = None,
) -> xr.Dataset:
    """Computes network characteristics for all networks defined in the given
    graph group.
    This is done for all available times.

    # TODO Allow restricting time and which measures are computed.
    """
    NETWORK_CHARACTERISTICS = {
        # Basics ..............................................................
        "num_species": lambda g: (
            g.number_of_nodes()
        ),
        "links_per_species": lambda g: (
            g.number_of_edges() / g.number_of_nodes()
        ),
        "connectance": lambda g: (
            g.number_of_edges() / g.number_of_nodes()**2
        ),
        "clustering_coefficient": lambda g: (
            get_node_attributes(g, "clustering_coefficient").mean()
        ),
        "mean_max_similarity": lambda g: (
            get_node_attributes(g, "max_similarity").mean()
        ),
        "max_max_similarity": lambda g: (
            get_node_attributes(g, "max_similarity").max()
        ),
        "mean_characteristic_path_length": lambda g: (
            get_node_attributes(g, "characteristic_path_length").max()
        ),


        # Absolute numbers ....................................................
        # Number of top predators, i.e. species without predators but not
        # depending on the external resource?
        # TODO Does this make sense?!
        "num_top": lambda g: (
            np.nan
        ),

        # Species that don't have an out-edge, i.e.: no predator
        "num_without_predator": lambda g: (
            int(get_node_attributes(g, "without_predator").sum())
        ),

        # Species with non-zero in- and out-degree (w/o self-loops)
        "num_intermediate": lambda g: (
            int(get_node_attributes(g, "is_intermediate").sum())
        ),


        # Fractions ...........................................................
        # Fraction of species with prey from more than one trophic level.
        # This uses the flow-based definition of the trophic level.
        "fraction_omnivores": lambda g: (
            get_node_attributes(g, "is_omnivore").mean()
        ),

        # Fraction of species that feed only on external resources
        "fraction_herbivores": lambda g: (
            get_node_attributes(g, "is_herbivore").mean()
        ),

        # Fraction of species with a cannibalistic link, i.e. a self-loop
        "fraction_cannibals": lambda g: (
            get_node_attributes(g, "has_self_loop").mean()
        ),

        # Fraction of species that is part of at least one feeding loop, e.g.,
        # link patterns of the type i feeds on j, j feeds on k, k feeds on i.
        # This is excluding cannibalism and is *not* limited to 3-loops.
        "fraction_loops": lambda g: (
            (get_node_attributes(g, "num_cycles") > 0).mean()
        ),


        # Trophic level .......................................................
        # short-weighted
        "mean_short_weighted_TL": lambda g: (
            get_node_attributes(g, "TL_short_weighted").mean()
        ),
        "stddev_short_weighted_TL": lambda g: (
            get_node_attributes(g, "TL_short_weighted").std()
        ),
        "max_short_weighted_TL": lambda g: (
            get_node_attributes(g, "TL_short_weighted").max()
        ),

        # flow-based
        "mean_flow_based_TL": lambda g: (
            get_node_attributes(g, "trophic_level").mean()
        ),
        "stddev_flow_based_TL": lambda g: (
            get_node_attributes(g, "trophic_level").std()
        ),
        "max_flow_based_TL": lambda g: (
            get_node_attributes(g, "trophic_level").max()
        ),


        # Distributions .......................................................
        # TODO stddevs of distributions: generality, etc.
    }

    # -------------------------------------------------------------------------
    gg = graph_group

    # Filter times
    # TODO
    times = [int(k) for k in gg["_vertices"].keys()]

    # Filter network characteristics
    to_compute = to_compute if to_compute else NETWORK_CHARACTERISTICS.keys()
    nw_characteristics = {
        k:v for k, v in NETWORK_CHARACTERISTICS.items() if k in to_compute
    }

    log.note("Computing %d network characteristics for %d time points ...",
             len(nw_characteristics), len(times))

    # Helper functions to prepare the dataset
    make_1Darr = lambda **kws: xr.DataArray(np.full((len(times),), np.nan),
                                            dims=("time",),
                                            coords=dict(time=times),
                                            **kws)

    # The dataset to collect the data for each time point and charateristics
    nwc = xr.Dataset()

    for cname in nw_characteristics:
        nwc[cname] = make_1Darr()

    # For each of these times, create the graph object, annotate it, and
    # compute all the network characteristics, adding them to the dataset
    with adjusted_log_levels(("dantro.groups.graph", 20)):
        for i, time in enumerate(times):
            log.remark("Time %d  (%d/%d)  ...", time, i+1, len(times))

            g = gg.create_graph(sel=dict(time=time), **graph_creation)
            annotate_network(g)

            for cname, cfunc in nw_characteristics.items():
                nwc[cname].loc[dict(time=time)] = cfunc(g)

    return nwc


def pos_from_TL(g: nx.Graph, *, prop_name: str = "trophic_level",
                x_from_prop: str = None, x_prop_rescale: bool = True,
                x_center: bool = True,
                x_wiggle: float = .01, seed: int = 42,
                dot_edge_threshold: int = 500) -> Dict[int, tuple]:
    """A positioning model using trophic levels"""
    log.note("Using node layouting model based on trophic levels ...")

    # Use a fixed seed, such that random positions are only random within this
    # invocation of the positioning model, not between invocations
    np.random.seed(seed)

    # If there are few edges, use dot to set everything up and minimize edge
    # overlap. For more edges, this becomes too costly; resort to a simple
    # spring layout there, which should be faster than determining hierarchy
    if g.number_of_edges() <= dot_edge_threshold:
        log.remark("Using dot model for initial layout ...")
        pos = nx.nx_pydot.graphviz_layout(g, prog="dot")
    else:
        log.remark("Using spring model for initial layout ...")
        pos = nx.spring_layout(
            g, seed=seed, threshold=0.005, k=g.number_of_nodes()**(-.3)
        )

    # Set y positions directly from property value
    TL = {n: g.nodes[n][prop_name] for n in g.nodes}
    pos = {n: (pos[n][0], TL[n]) for n in g.nodes}

    # For x positions, bin the values into groups and distribute them evenly.
    # To reduce crossing edges, maintain the order determined by dot.
    level = {n: int(_TL + .5) for n, _TL in TL.items()}
    levels = set(level.values())
    log.remark("Identified %d trophic levels from attribute '%s'.",
               len(levels), prop_name)

    # Might want to use some property for x positions as well, inform about it
    if x_from_prop:
        log.remark("Will use property '%s' for x positions ...", x_from_prop)

    xpos = dict()
    for lvl in levels:
        # Depending on mode, either use property values for the x positions or
        # use the positions determined by the layouting algorithm, but evenly
        # distributed to prevent overlap
        if x_from_prop:
            # Get the property values and transform than to a position
            get_val = lambda n: g.nodes[n][x_from_prop]
            xpos[lvl] = {n: get_val(n) for n in pos.keys() if level[n] == lvl}
            x = np.array(list(xpos[lvl].values()))

            # Normalise by the range and the size
            if x.size > 1 and x_prop_rescale:
                rg = x.max() - x.min()
                x = (x / rg) * math.sqrt(x.size)

        else:
            # Get the positions on this level
            xpos[lvl] = {n: xy[0] for n, xy in pos.items() if level[n] == lvl}
            x = np.array(list(xpos[lvl].values()))

            # If there is more than one element, distribute them anew,
            # maintaining their order ...
            if x.size > 1:
                # Add random numbers to prohibit equal values
                x += np.random.random(x.shape) * (x.size / 10000.)

                # Create an order-mapping and calculate x-positions anew, in
                # that order but with uniform distances between them.
                # Use a sqrt-scaling, such that wider levels actually take up
                # more space but less wide levels have higher spacing between
                # nodes.
                _sorted = sorted(x)
                idx = {i: _sorted.index(v) for i, v in enumerate(x)}
                x = np.array(
                    [idx[i]/(x.size-1) * math.sqrt(x.size)
                     for i, _ in enumerate(x)]
                )

                # Add a little wiggle to let them appear more natural
                width = x.max() - x.min()
                x += (2 * np.random.random(x.shape)-1) * (width * x_wiggle)

        # Center around zero
        if x_center:
            x = x - x.min() - (x.max() - x.min())/2.

        # Assign them back to the nodes on this level ...
        xpos[lvl] = {n: _x for n, _x in zip(xpos[lvl], x)}

    log.remark("... with sizes:  %s",
               ",  ".join(f"{k}: {len(v)}" for k, v in xpos.items()))

    # Now update the entire positions dict from the individual levels
    return {n: (xpos[level[n]][n], xy[1]) for n, xy in pos.items()}


def pos_from_props(
    g: nx.Graph, *,
    x_prop_name: str, y_prop_name: str,
    x_fillna: float = None, y_fillna: float = None,
) -> Dict[int, tuple]:
    """Returns positions based on node properties"""
    def get_val(n, prop, *, fillna: float = None):
        v = g.nodes[n][prop]
        if np.isnan(v):
            v = fillna
        return v

    log.note("Using node layouting model based on properties ...")
    log.remark("  x:  %s", x_prop_name)
    log.remark("  y:  %s", y_prop_name)

    x_prop = {n: get_val(n, x_prop_name, fillna=x_fillna) for n in g.nodes}
    y_prop = {n: get_val(n, y_prop_name, fillna=y_fillna) for n in g.nodes}

    pos = {n: (x_prop[n], y_prop[n]) for n in g.nodes}
    return pos


def get_positioning_model(kind: str = "pos_from_TL", **kwargs) -> Callable:
    """Returns the positioning model callable, with all kwargs bound to it."""
    if kind == "pos_from_TL":
        return partial(pos_from_TL, **kwargs)

    elif kind == "pos_from_props":
        return partial(pos_from_props, **kwargs)

    raise ValueError(
        f"Invalid kind '{kind}', choose from: pos_from_TL, pos_from_props"
    )
