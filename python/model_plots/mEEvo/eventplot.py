"""Implements an event plot function"""

import copy
import logging
from typing import Union, Sequence

import xarray as xr

from dantro.plot.funcs.generic import make_facet_grid_plot

from utopya.eval import is_plot_func, PlotHelper


log = logging.getLogger(__name__)


# -----------------------------------------------------------------------------

@make_facet_grid_plot(
    map_as="dataarray_line",
    encodings=("x",),
    supported_hue_styles=("discrete",),
    #
    # defaults
    hue_style="discrete",  # needs to be set, doesn't work otherwise
    add_guide=False,
)
def eventplot(
    pos: xr.DataArray,
    *,
    hlpr: PlotHelper,
    _is_facetgrid: bool,
    x: str = None,
    hue: str = None,
    **eventplot_kwargs
) -> None:
    """Wraps pyplot.eventplot

    TODO hue, colours from cycler, ?
    """
    if not hue:
        hlpr.ax.eventplot(pos, **eventplot_kwargs)

    else:
        raise NotImplementedError("hue")
        for i, (_, _pos) in enumerate(pos.groupby(hue)):
            hlpr.ax.eventplot(pos, **eventplot_kwargs)
