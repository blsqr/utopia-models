"""Implements a custom plot"""

import copy
import logging
from typing import Union, Sequence

import xarray as xr

from utopya.eval import is_plot_func, PlotHelper


log = logging.getLogger(__name__)


# -----------------------------------------------------------------------------

@is_plot_func(
    use_dag=True, required_dag_tags=('custom_data',)
)
def my_custom_plot(*, data: dict, hlpr: PlotHelper) -> None:
    """..."""
    pass
