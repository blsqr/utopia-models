"""Additional dantro data transformation operations"""

import logging
from typing import Tuple, Any

import numpy as np
import xarray as xr
import networkx as nx

import dantro as dtr

from .network import set_node_attributes, set_edge_attributes
from .network import get_node_attributes, get_edge_attributes

from utopya.eval import is_operation

log = logging.getLogger(__name__)


# -----------------------------------------------------------------------------

@is_operation("mEEvo.build_phylogeny")
def build_phylogeny(
    *, parent_ids, initial_id: int, drop_ids: list = None
) -> nx.DiGraph:
    """Builds a GraphGroup to represent the phylogeny, ready to plot.

    Available annotations:

        - ``generation``: Distance from ``initial_id``
        - ``n_offspring``: Out degree (before any filtering)
        - ...

    """
    # Child IDs are the coordinates
    nodes = parent_ids.coords["id"]
    nodes.name = "child_ids"

    log.remark("Building phylogeny graph with %d nodes ...", len(nodes))

    # Parent IDs are the data itself
    if not isinstance(parent_ids, xr.DataArray):
        parent_ids = parent_ids.data
    parent_ids.name = "parent_ids"

    # For the edges, need parent-child connections.
    # NOTE This may not include all nodes, because there are nodes that have
    #      no parent.
    if not drop_ids:
        e_child = nodes
        e_parent = parent_ids

    else:
        log.remark("Dropping IDs: %s", drop_ids)

        nodes = nodes.drop_sel(id=drop_ids, errors="ignore")
        e_parent = parent_ids.drop_sel(id=drop_ids, errors="ignore")
        e_child = nodes.drop_sel(id=drop_ids, errors="ignore")

        # Additionally, need to drop those entries that point to the drop IDs
        selected = ~e_parent.isin(drop_ids)
        e_parent = e_parent.where(selected, drop=True).astype(int)
        e_child = e_child.sel(id=e_parent.coords["id"])

    # Edges are simply the given parent IDs, associated with their child
    edges = xr.merge([e_parent, e_child]).to_array().astype(int)

    # Create network
    log.remark("Building graph now ...")
    g = nx.DiGraph()
    g.add_nodes_from(nodes.values)
    g.add_edges_from([tuple(pc.values) for _, pc in edges.groupby("id")])

    # Annotate
    log.remark("Annotating ...")
    set_node_attributes(g, "labels",
        lambda g, n: n
    )
    set_node_attributes(g, "generation",
        lambda g, n: len(nx.shortest_path(g, source=initial_id, target=n))
    )
    set_node_attributes(g, "n_offspring",
        lambda g, n: g.out_degree(n)
    )

    log.remark("Built and annotated phylogeny graph.")
    return g


@is_operation("mEEvo.filter_phylogeny")
def filter_phylogeny(
    g: nx.DiGraph, *,
    min_age: int = None,
    min_degree: int = None,
    remove_zero_frac_diverted: bool = None,
) -> nx.DiGraph:
    """Filters the phylogeny graph according to certain criteria"""
    log.remark("Filtering graph with %d nodes and %d edges ...",
               g.number_of_nodes(), g.number_of_edges())

    # TODO Actually use filters ...

    if min_age:
        log.remark("  ... removing nodes with age < %s", min_age)
        g.remove_nodes_from(
            [n for n in g.nodes if g.nodes[n]["age"] < min_age]
        )

    if min_degree is not None:
        log.remark("  ... removing nodes with degree < %s", min_degree)
        g.remove_nodes_from(
            [n for n in g.nodes if g.degree(n) < min_degree]
        )

    if remove_zero_frac_diverted:
        log.remark("  ... removing nodes with zero frac_diverted")
        g.remove_nodes_from(
            [n for n in g.nodes if g.nodes[n]["frac_diverted"] <= 0.]
        )
        # TODO Make this more clever!

    log.remark("Filtered graph; have %d nodes and %d edges left.",
               g.number_of_nodes(), g.number_of_edges())
    return g
