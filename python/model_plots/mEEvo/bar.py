"""Implements an animated stacked bar plot function"""

import copy
import logging
from typing import Union, Sequence

import xarray as xr

from dantro.plot.funcs.generic import make_facet_grid_plot

from utopya.eval import is_plot_func, PlotHelper


log = logging.getLogger(__name__)


# -----------------------------------------------------------------------------

@make_facet_grid_plot(
    map_as="dataarray_line",
    encodings=("x", "hue"),
    supported_hue_styles=("discrete",),
    #
    # defaults
    hue_style="discrete",  # needs to be set, doesn't work otherwise
    add_guide=False,
)
def bar(
    da: xr.DataArray,
    *,
    hlpr: PlotHelper,
    _is_facetgrid: bool,
    x: str = None,
    hue: str = None,
    use_horizontal_bars: bool = False,
    **bar_kwargs
) -> None:
    """Plots a simple bar plot for categorical data.

    TODO Stacking, categorical data, ...
    """
    dim = x
    if hue:
        raise NotImplementedError("hue")

    # Vertical or horizontal bars?
    if use_horizontal_bars:
        bar_kwargs["height"] = bar_kwargs.pop("width", None)

    # Get data
    counts = da.data
    pos = da.coords[dim]

    # Plot
    if not use_horizontal_bars:
        hlpr.ax.bar(pos, counts, **bar_kwargs)
    else:
        hlpr.ax.barh(pos, counts, **bar_kwargs)
