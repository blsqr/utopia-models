"""Implements mutation-related operations and plots"""

import math
import logging
from typing import Union, Sequence

import scipy
import scipy.stats
import numpy as np
import xarray as xr
from xarray.plot.utils import _infer_interval_breaks

from utopya.eval import is_operation

log = logging.getLogger(__name__)


# -----------------------------------------------------------------------------

@is_operation("mEEvo.mutant_probability")
def mutant_probability(
    *,
    species: xr.Dataset,
    trait_mapping: dict,
    mutation_params: dict,
    evaluate_at: xr.DataArray,
) -> xr.DataArray:
    """Evaluates the probability of a species being added to the system having
    specific traits.

    The trait values of interest are given by coordinates of ``evaluate_at``.
    To be exact, the returned array contains the probability of the new
    species being in the trait *ranges* given by the "bins" defined by
    ``evaluate_at`; the bin edges are inferred from the coordinates.

    Args:
        species (xr.Dataset): The species that can be mutated from
        trait_mapping (dict): The names of the data variables in ``species`` to
            use for extracting the traits.
            Required keys are ``m``, ``mf_ratio``, ``s``
        mutation_params (dict): The mutation parameters; this can be the
            ``system.mutations`` entry of the ``mEEvo`` model configuration.
        evaluate_at (xr.DataArray): A 3D grid (dimensions ``m``, ``mf_ratio``,
            and ``s``, each with at least size 2)
    """
    # -- Get mutation parameters
    mutation_mode = mutation_params.get("mode", "Allhoff2015")  # for legacy

    if mutation_mode == "Allhoff2015":
        m_range = mutation_params["m_range"]
        f_log10_range = mutation_params["f_log10_range"]  # e.g. (-3, -.5)
        s_range = mutation_params["s_range"]
        if mutation_params["inheritance_strength"] > 0.:
            raise NotImplementedError(
                "inheritance_strength > 0 not supported yet"
            )

    elif mutation_mode == "normal":
        sigma_m = mutation_params["sigma_m"]
        sigma_mf_ratio = mutation_params["sigma_mf_ratio"]
        sigma_s = mutation_params["sigma_s"]
        bounds = mutation_params["bounds"]  # m, mf_ratio, s
        log.warning(
            "In 'normal' mode, bounds are not included into probability "
            "distributions; absolute values may be off!"
        )
        # NOTE Could do this by applying bounds to each CDF computation, the
        #      re-normalising to 1. This would be the better approach than via
        #      scipy.stats.truncnorm (which is a pain to handle)

    else:
        raise ValueError(f"Unsupported mutation mode: {mutation_mode}")


    # -- Setup the distribution creation function
    # These use the parameters defined above by name

    def get_dists_Allhoff2015(m: float, mf: float, s: float) -> tuple:
        """Returns appropriate distribution objects for the given traits"""
        mf_log10_range = (-f_log10_range[1], -f_log10_range[0])  # (.5, 3)
        dm = scipy.stats.uniform(
            math.log10(m * m_range[0]),
            math.log10(m * m_range[1]) - math.log10(m * m_range[0])
        )
        dmf = scipy.stats.uniform(
            mf_log10_range[0],
            mf_log10_range[1] - mf_log10_range[0]
        )
        ds = scipy.stats.uniform(s_range[0], s_range[1] - s_range[0])

        return dm, dmf, ds

    def get_dists_normal(m: float, mf: float, s: float) -> tuple:
        """Returns appropriate distribution objects for the given traits"""
        dm = scipy.stats.norm(math.log10(m), sigma_m)
        dmf = scipy.stats.norm(math.log10(mf), sigma_mf_ratio)
        ds = scipy.stats.norm(s, sigma_s)

        return dm, dmf, ds

    get_dists = dict(
        Allhoff2015=get_dists_Allhoff2015,
        normal=get_dists_normal,
    )[mutation_mode]


    # -- Get the information about the species, using a trait mapping
    m = species[trait_mapping["m"]]
    mf_ratio = species[trait_mapping["mf_ratio"]]
    s = species[trait_mapping["s"]]

    if (
        evaluate_at.ndim != 3
        or any(d not in ("m", "mf_ratio", "s") for d in evaluate_at.dims)
        or any(s < 2 for s in evaluate_at.shape)
    ):
        raise ValueError(
            "The evaluation array needs to be a 3D array with dimension names "
            "'m', 'mf_ratio', and 's' and sizes ≥ 2!"
        )


    # -- Prepare the evaluation points of the CDF
    # The final probability for each evaluation volume
    prob = xr.zeros_like(evaluate_at)
    prob.name = "Mutant Probability"

    # Will need to compute the CDF for a volume around each of the points.
    # So need to infer "edges" from the coordinates. To arrive at the
    # probability to be in that volume, will later compute the difference
    # between the CDF of these points.
    # NOTE Need to use log scale here!
    m_edges = _infer_interval_breaks(np.log10(prob.coords["m"]))
    mf_edges = _infer_interval_breaks(np.log10(prob.coords["mf_ratio"]))
    s_edges = _infer_interval_breaks(prob.coords["s"])

    m_edges = xr.DataArray(
        m_edges, name="m_edges", dims=("m",), coords=dict(m=m_edges)
    )
    mf_edges = xr.DataArray(
        mf_edges, name="mf_edges", dims=("mf_ratio",),
        coords=dict(mf_ratio=mf_edges)
    )
    s_edges = xr.DataArray(
        s_edges, name="s_edges", dims=("s",), coords=dict(s=s_edges)
    )

    # For s, correct edge values to remain in bounds
    if mutation_mode == "Allhoff2015":
        # TODO Make configurable whether this is done
        # TODO Allow scalar s value
        if s_edges[0] < s_range[0]:
            s_edges[0] = s_range[0]
        if s_edges[-1] > s_range[-1]:
            s_edges[-1] = s_range[-1]

    # Create 3D array for combined cumulative probability of the edges
    ccp_edges = m_edges * mf_edges * s_edges
    ccp_edges[:] = 0.
    ccp_edges.name = "ccp_edges"

    # Temporary objects for evaluated edges
    cpm = m_edges.copy()
    cpm[:] = 0.

    cpmf = mf_edges.copy()
    cpmf[:] = 0.

    cps = s_edges.copy()
    cps[:] = 0.

    # -- Now go over all species and evaluate the CDFs
    log.info("Evaluating mutant probabilities ...")
    log.note("  # species:            %d", len(m))
    log.note("  # evaluation points:  %s", dict(evaluate_at.sizes))
    log.note("  m interval:           [%.3g, %.3g]",
             10**m_edges.min(),     10**m_edges.max())
    log.note("  m/f interval:         [%.3g, %.3g]",
             10**mf_edges.min(),    10**mf_edges.max())
    log.note("  s interval:           [%.3g, %.3g]",
             s_edges.min(),         s_edges.max())

    log.note("Mutation rules:")
    log.note("  mutation mode:        %s", mutation_mode)
    if mutation_mode == "Allhoff2015":
        log.note("  m_range:              %s", m_range)
        log.note("  f_log10_range:        %s", f_log10_range)
        log.note("  s_range:              %s", s_range)

    elif mutation_mode == "normal":
        log.note("  sigma_m:              %s", sigma_m)
        log.note("  sigma_mf_ratio:       %s", sigma_mf_ratio)
        log.note("  sigma_s:              %s", sigma_s)
        log.note("  bounds:               %s", bounds)

    species_iterator = zip(
        m.groupby("id"), mf_ratio.groupby("id"), s.groupby("id")
    )
    n_species = 0
    for (_id, _m), (_, _mf), (_, _s) in species_iterator:
        _m = _m.item()
        _mf = _mf.item()
        _s = _s.item()
        log.remark(
            "species %d   \tm: %.3g, \tm/f: %.3g, \ts: %.3g", _id, _m, _mf, _s
        )
        if _id < 2:  # TODO generalise
            continue

        # Build the distribution objects and evaluate their CDF at the edges
        dm, dmf, ds = get_dists(_m, _mf, _s)
        cpm.data = dm.cdf(m_edges)
        cpmf.data = dmf.cdf(mf_edges)
        cps.data = ds.cdf(s_edges)

        # Multiply them to arrive at the combined probability for all three
        # distributions. Those can now be added to the combined cumulative
        # probabilities for the edges:
        ccp_edges += (cpm * cpmf * cps)

        n_species += 1

    # From the combined cumulative probabilities for the edges, compute the
    # combined cumulative probabilities of the enclosed volume.
    ccp = ccp_edges.diff("m")
    ccp = ccp.diff("mf_ratio")
    ccp = ccp.diff("s")

    # Depending on mode, evaluate the
    if mutation_mode in ("Allhoff2015", "normal"):
        prob.data = ccp / n_species

    log.note("Mutant probabilities computed from %d species.", n_species)
    log.remark("  max:   %.3g", prob.max())
    log.remark("  min:   %.3g", prob.min())
    log.remark("  mean:  %.3g", prob.mean())
    log.remark("  sum:   %.3g", prob.sum())

    return prob
