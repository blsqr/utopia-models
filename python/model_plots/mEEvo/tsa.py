"""Time series analysis"""

import logging
from collections import deque
from itertools import chain
from time import time_ns

import numpy as np
import xarray as xr

import dantro as dtr
from dantro.data_ops.arr_ops import populate_ndarray

from utopya.eval import is_operation

log = logging.getLogger(__name__)

# -----------------------------------------------------------------------------

@is_operation("mEEvo.compute_cascade_stats")
def compute_cascade_stats(
    *,
    additions: xr.DataArray,
    removals: xr.DataArray,
    t_max: int,
    initial_TL: xr.DataArray,
    mean_TL: xr.DataArray,
    removal_time_correction: int = 0,
    show_progress_interval: int = 50000,
    show_profile_threshold: float = 10.
) -> xr.Dataset:
    """Computes extinction cascade statistics from addition and removal times
    of species. All data is aggregated into an xr.Dataset with dimensions
    ``added_id`` and ``removed_id``.

    Args:
        additions (xr.DataArray): Addition times, labelled by species ID
        removals (xr.DataArray): Removal times, labelled by species ID
        t_max (int): The maximum simulation time, used in comparison of the
            very last interval.
        initial_TL (xr.DataArray): Initial trophic level (only for newly
            added species)
        mean_TL (xr.DataArray): Mean trophic level (only for removed species)
        removal_time_correction (int, optional): Value to substract from the
            removal times, e.g. because removal times are offset by a constant
            factor due to implementation details.
        show_progress_interval (int, optional): Number of iterations between
            update of the progress indicator. Progress will be shown only if
            ``len(removals) > 2 * show_progress_interval`` and if the argument
            evaluates to True.
        show_profile_threshold (float, optional): Time in seconds above which
            the profiling information should be displayed. This may help to
            find out which part of the statistics take how long to calculate.
    """
    # Set up profiling
    t0 = time_ns()
    times = dict(search=0., track=0., postprocess=0.)

    # Check arguments
    if (additions.id.values != initial_TL.id.values).any():
        raise ValueError(
            "Mismatch between addition times data and initial trophic level!\n"
            f"{additions}\n{initial_TL}\n"
        )

    if (removals.id.values != mean_TL.id.values).any():
        raise ValueError(
            "Mismatch between removal times data and mean trophic level!\n"
            f"{removals}\n{mean_TL}\n"
        )

    # Clean up arguments
    t_max = int(t_max)
    rtc = int(removal_time_correction)

    # Work on deques to be able to pop left (much faster than list.pop(0)).
    # Furthermore, use integer types, easier and faster to handle.
    # Also apply removal time correction here, rather than element-wise in loop
    rm_t = deque(removals.astype(int).values - rtc)
    rm_id = deque(removals.coords["id"].values)
    rm_TL = deque(mean_TL.values)

    additions = additions.astype(int)

    # Target objects
    # ... alongside added species
    cascade_size = []
    cascade_size_viable = []
    extinctions = []
    delays = []
    mean_TL_diff = []
    std_TL_diff = []

    # ... alongside removed species
    all_delays = []
    TL_diff = []

    # ... other
    viable_ids = []
    non_viable_ids = []


    # Emit some info
    log.remark("Computing extinction cascade statistics ... "
               "(add: %d, rm: %d, RTC: %d, t_max: %d)",
               len(additions), len(removals), rtc, t_max)

    log.trace("Removal times:\n%s", rm_t)
    log.trace("Removal IDs:\n%s\n", rm_id)
    log.trace("Addition times:\n%s", additions.values)
    log.trace("Addition IDs:\n%s\n", additions.id.values)

    i = 0
    show_progress = (
        show_progress_interval and len(removals) > 2 * show_progress_interval
    )
    progress_fstr = "%3.0f%%  (%d / %d)"

    # Here we go ...
    for add_id, add_t, next_add_t, add_TL in zip(
        additions.id.values,
        additions.values,
        chain(additions.values[1:], [t_max + 1]),
        initial_TL.values,
    ):
        t1_loop = time_ns()

        log.trace("Checking extinctions in [%d, %d) ...", add_t, next_add_t)

        new_extinctions = []
        new_delays = []
        new_TL_diff = []
        non_viable = False

        while len(rm_t) and rm_t[0] < next_add_t:
            _rm_t = rm_t.popleft()
            _delay = _rm_t - add_t

            _rm_id = rm_id.popleft()
            if non_viable is not True:  # ... to not overwrite previous
                non_viable = bool(_rm_id == add_id)

            _rm_TL = rm_TL.popleft()
            _TL_diff = add_TL - _rm_TL

            # Progress indicator
            i += 1
            if show_progress and i % show_progress_interval == 0:
                log.remark(
                    progress_fstr, (i / len(removals)) * 100, i, len(removals)
                )
            log.trace(
                "  Species %d extinct at time %s (delay: %s, remaining: %d)",
                _rm_id, _rm_t, _delay, len(rm_t)
            )

            # Trackers specific to the *removed* species
            # ... tracked alongside added species
            new_extinctions.append(_rm_id)
            new_delays.append(_delay)
            new_TL_diff.append(_TL_diff)

            # ... tracked individually
            all_delays.append(_delay)
            TL_diff.append(_TL_diff)

        # Profile search time
        t2_loop = time_ns()
        times["search"] += t2_loop - t1_loop

        # Trackers specific to the *added* species (that caused extinction)
        cascade_size.append(len(new_extinctions))
        cascade_size_viable.append(
            max(0, len(new_extinctions) - int(non_viable))
        )
        extinctions.append(tuple(new_extinctions))
        delays.append(tuple(new_delays))

        mean_TL_diff.append(np.mean(new_TL_diff) if new_TL_diff else np.nan)
        std_TL_diff.append(np.std(new_TL_diff) if new_TL_diff else np.nan)

        # Track viability
        if non_viable:
            non_viable_ids.append(add_id)  # because added id == removed id
        else:
            viable_ids.append(add_id)

        log.trace(
            "Adding species %d led to %d extinction(s).\n",
             add_id, len(new_extinctions),
        )

        # Profile track time
        times["track"] += time_ns() - t2_loop

    if show_progress:
        log.remark(
            progress_fstr, (i / len(removals)) * 100, i, len(removals)
        )

    # Begin profiling postprocessing time
    t3 = time_ns()
    log.trace("cascade size:\n%s", cascade_size)
    log.trace("cascade size viable:\n%s", cascade_size_viable)
    log.trace("extinctions:\n%s", extinctions)
    log.trace("delays:\n%s", delays)
    log.trace("all_delays:\n%s", all_delays)
    log.trace("TL_diff:\n%s", TL_diff)
    log.trace("viable_ids:\n%s", viable_ids)
    log.trace("non_viable_ids:\n%s", non_viable_ids)

    # .. Bring values into labelled data arrays ...............................
    # ... for ID-based indexing of added species
    create_DataArray_add_id = lambda data, **kws: xr.DataArray(
        data,
        dims=("added_id",),
        coords=dict(added_id=additions.id.values),
        **kws
    )

    cascade_size = create_DataArray_add_id(
        cascade_size, name="cascade size"
    )

    cascade_size_viable = create_DataArray_add_id(
        cascade_size_viable, name="cascade size (viable only)"
    )

    extinctions = create_DataArray_add_id(
        populate_ndarray(
            extinctions,
            shape=(len(extinctions),),
            dtype=object,
        ),
        name="extinctions",
    )

    delays = create_DataArray_add_id(
        populate_ndarray(
            delays,
            shape=(len(delays),),
            dtype=object,
        ),
        name="delays",
    )

    initial_TL = create_DataArray_add_id(
        initial_TL, name="initial trophic level"
    )

    mean_TL_diff = create_DataArray_add_id(
        mean_TL_diff, name="trophic level difference (mean)"
    )

    std_TL_diff = create_DataArray_add_id(
        std_TL_diff, name="trophic level difference (std)"
    )

    # ... for ID-based indexing of removed species
    create_DataArray_rm_id = lambda data, **kws: xr.DataArray(
        data,
        dims=("removed_id",),
        coords=dict(removed_id=removals.id.values),
        **kws
    )

    delay = create_DataArray_rm_id(
        all_delays, name="delay"
    )

    mean_TL = create_DataArray_rm_id(
        mean_TL, name="mean trophic level"
    )

    TL_diff = create_DataArray_rm_id(
        TL_diff, name="trophic level difference"
    )

    was_non_viable = create_DataArray_rm_id(
        np.zeros((removals.id.size,), dtype=bool),
        name="was non-viable",
    )
    was_non_viable.loc[dict(removed_id=non_viable_ids)] = True

    # Combine all into a dataset
    ds = xr.merge([
        # added_id-based
        cascade_size,
        cascade_size_viable,
        extinctions,
        delays,
        initial_TL,
        mean_TL_diff,
        std_TL_diff,
        #
        # removed_id-based
        delay,
        mean_TL,
        TL_diff,
        was_non_viable,
    ])

    # ... and add some attributes
    ds.attrs["t_max"] = t_max
    ds.attrs["removal_time_correction"] = rtc
    ds.attrs["viable_ids"] = viable_ids
    ds.attrs["non_viable_ids"] = non_viable_ids

    t4 = time_ns()
    times["postprocess"] += t4 - t3
    t_total = (t4 - t0)/10**9

    if t_total > show_profile_threshold:
        log.remark(
            "Finished after %.2gs (%s).",
            t_total,
            ", ".join([f"{k:s}: {v/10**9:.2g}s" for k, v in times.items()])
        )

    return ds
