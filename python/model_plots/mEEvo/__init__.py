"""Plots that are specific to the mEEvo model"""

# Make plots available
from .value_spans import value_spans
from .snsplot import snsplot
from .bar import bar
from .eventplot import eventplot


# Import the remaining modules to trigger registration of operations
from .operations import *
from .mutation import *
from .network import *
from .phylogeny import *
from .tsa import *
