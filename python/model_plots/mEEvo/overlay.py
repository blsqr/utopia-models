"""Implements a heatmap facetgrid plot with scatter overlay"""

import copy
import logging
from typing import Union, Sequence

import xarray as xr

from utopya.eval import is_plot_func, PlotHelper


# Local variables
log = logging.getLogger(__name__)


# -----------------------------------------------------------------------------

@is_plot_func(
    use_dag=True, required_dag_tags=("heatmap", "scatter")
)
def overlay(*, data: dict, hlpr: PlotHelper) -> None:
    """..."""
    raise NotImplementedError("overlay")
