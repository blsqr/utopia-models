"""Implements the value spans plot function"""

import copy
import logging
from typing import Union, Sequence, Tuple, Callable

import xarray as xr

from matplotlib.collections import LineCollection
from matplotlib.cm import ScalarMappable
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from dantro.plot.funcs.generic import make_facet_grid_plot
from dantro.tools import adjusted_log_levels

from utopya.eval import is_plot_func, PlotHelper
from utopya.eval.plots._mpl import ColorManager

log = logging.getLogger(__name__)


# -----------------------------------------------------------------------------

@make_facet_grid_plot(
    map_as="dataset",
    encodings=("y", "hue"),
    supported_hue_styles=("discrete",),
    #
    # defaults
    hue_style="discrete",  # needs to be set, doesn't work otherwise
    add_guide=False,
)
def value_spans(
    ds: xr.Dataset,
    *,
    hlpr: PlotHelper,
    _is_facetgrid: bool,
    x: str, x_end: str,
    y: str = None,
    y_fstr: str = "{y_coord:}",
    hue: str = None,
    color: str = None,
    color_kws: dict = None,
    add_cbar: bool = None,
    cbar_kws: dict = None,
    min_length: float = None,
    skip_if_below_min_length: bool = False,
    use_line_collection: Union[bool, int] = 20,
    transform_x: Callable = None,
    **kwargs
) -> None:
    """Plots value spans, i.e.: lines that go from ``x0`` to ``x1``, located
    at a constant value ``y`` and (optionally) with a ``hue``.

    This plot function expects the ``data`` tag being available, being an
    xr.Dataset, and containing data variables that match the names given in the
    ``x``, ``x_end``, ``y`` and ``hue`` arguments.

    Args:
        ds (xr.Dataset): The dataset containing the variables specified in the
            arguments ``x``, ``x_end``, ``y`` and ``hue``.
        hlpr (PlotHelper): The PlotHelper
        _is_facetgrid (bool): Whether this is part of a facet grid iteration
        x (str): Data variable containing beginnings of lines
        x_end (str): Data variable containing ends of lines
        y (str, optional): data varibale to use for y positions. If this is not
            available upon the data variables, uses dimension coordinates.
        y_fstr (str, optional): Format string to use for labels; available
            keys are: ``y``, ``y_coord``
        hue (str, optional): Dimension name to create line segments from.
            NOTE That the naming here is not very intuitive, mainly due to
            xarray facet grid plotting restrictions. Have a look at ``color``.
        color (str, optional): Data variable to use for line colors
        color_kws (dict, optional):
        min_length (float, optional): If given, will extend the end coordinate
            of each line segments to ``(start + min_length)`` in case the
            segment would otherwise be shorter than the specified length.
            NOTE That this distorts the actual value of the elements and should
            only be used for making very short segments visible.
        skip_if_below_min_length (bool, optional): If True, will *not* draw
            line segments that are shorter than ``min_length``
        use_line_collection (Union[bool, int], optional): Whether to use a line
            collection or (if an integer) *above* which number of lines to use
            a line collection instead of a labelled line. If this is set, there
            will be no labels and ``y_fstr`` is ignored.
        transform_x (Callable, optional): if given, is applied to the ``x``
            and ``x_end`` data variables.
        **kwargs: Passed on to ``hlpr.ax.plot`` or ``LineCollection``.

    Raises:
        ValueError: On bad data dimensionality
    """
    class SkipSegment(Exception):
        pass

    def prepare_line_segment(x0, x1, y) -> Tuple[float, float, float]:
        """Prepares line segment information"""
        # Assume xarray items
        x0, x1, y = x0.item(), x1.item(), y.item()

        # Evaluate minimum segment length
        if min_length is not None and x1 - x0 < min_length:
            if skip_if_below_min_length:
                raise SkipSegment()
            x1 = x0 + min_length
        return x0, x1, y

    x0, x1 = x, x_end

    # Get the x-data
    x0_data = ds.data_vars[x0]
    x1_data = ds.data_vars[x1]

    if x0_data.ndim != 1 or x1_data.ndim != 1:
        raise ValueError(f"Expected 1D x-data, got:\n\n{x0_data}\n\n{x1_data}")

    # Potentially transform it
    if transform_x is not None:
        x0_data = transform_x(x0_data)
        x1_data = transform_x(x1_data)

    # Determine the y dimension automatically, if needed
    y = y if y else x0_data.dims[0]

    # Set hue to y, if not given
    hue = hue if hue else y

    # Get y data from data variables or (alternatively: coordinates)
    if y in ds.data_vars:
        y_data = ds.data_vars[y]
        log.debug("Using data variable '%s' for lines ...", y)
    else:
        y_data = ds.coords[y]
        log.debug("Using coordinates of dimension '%s' for y-data ...", y)

    if y_data.ndim != 1:
        raise ValueError(f"Expected 1D y-data, got:\n\n{y_data}")

    # Get color data and prepare color manager
    c_data = None
    if color:
        c_data = ds.data_vars[color]

        color_kws = color_kws if color_kws else {}
        color_kws["vmin"] = color_kws.get("vmin", c_data.min().item())
        color_kws["vmax"] = color_kws.get("vmax", c_data.max().item())

        with adjusted_log_levels(("utopya.plot_funcs._mpl_helpers", 20)):
            c_mngr = ColorManager(**color_kws)

    # Will we use a line collection?
    use_lc = (use_line_collection
              and x0_data.sizes[hue] > int(use_line_collection))

    log.remark(
        "Plotting %d value spans using %s ...%s",
        x0_data.sizes[hue],
        "line collection" if use_lc else "individual lines",
        (
            f" (skipping segments shorter than {min_length})"
            if skip_if_below_min_length
            else ""
        )
    )

    # Set some plot aesthetics
    if not _is_facetgrid:
        hlpr.provide_defaults("set_labels", x=f"{x0} & {x1}", y=y)

        if not use_lc:
            hlpr.provide_defaults("set_legend", title=y)

    # .. Draw lines now .......................................................
    line_iter = zip(
        x0_data.groupby(hue),
        x1_data.groupby(hue),
        y_data.groupby(hue),
        (
            c_data.groupby(hue) if c_data is not None
            else enumerate(range(x0_data.sizes[hue]))
        ),
    )
    # TODO Make this a generator that includes prepare_line_segment and the
    #      handling of segment skipping etc.

    if use_lc:
        # .. Draw as line collection . . . . . . . . . . . . . . . . . . . . .
        lines = []
        colors = []
        for (_, _x0), (_, _x1), (_y_coord, _y), (_, c) in line_iter:
            try: _x0, _x1, _y = prepare_line_segment(_x0, _x1, _y)
            except SkipSegment: continue

            lines.append(((_x0, _y), (_x1, _y)))
            if color:
                colors.append(c.item())

        if color:
            kwargs["colors"] = c_mngr.map_to_color(colors)

        # Add collection
        lc = LineCollection(lines, **kwargs)
        hlpr.ax.add_collection(lc)

        # Need to autoscale, because `add_collection` doesn't do it.
        hlpr.ax.autoscale_view()

        # Add colorbar
        # TODO Improve. This is a big big mess… repeated on every axis
        if color and add_cbar:  # and not getattr(hlpr, "_cbar", None):
            cbar_kws = cbar_kws if cbar_kws else {}
            hlpr._cbar = hlpr.fig.colorbar(
                ScalarMappable(norm=c_mngr.norm, cmap=c_mngr.cmap),
                # ax=hlpr.axes.ravel().tolist(),  # FIXME fails (tight layout)
                **cbar_kws
            )
            if cbar_kws.get("label"):
                hlpr._cbar.set_label(cbar_kws["label"], size='x-small')
            hlpr._cbar.ax.tick_params(labelsize='small')

            if c_mngr.labels is not None:
                cb.set_ticks(list(c_mngr.labels.keys()))
                cb.set_ticklabels(list(c_mngr.labels.values()))

    else:
        # .. Draw as individual lines . . . . . . . . . . . . . . . . . . . . .
        # Keep track of handles and labels
        handles, labels = [], []

        for (_, _x0), (_, _x1), (_y_coord, _y), (_, c) in line_iter:
            try: _x0, _x1, _y = prepare_line_segment(_x0, _x1, _y)
            except SkipSegment: continue

            _y_coord = _y_coord.item()

            if color:
                kwargs["color"] = c_mngr.map_to_color([c])[0]

            l = y_fstr.format(y_coord=_y_coord, y=y)
            h = hlpr.ax.plot([_x0, _x1], [_y, _y], label=l, **kwargs)

            handles += h
            labels += [l]

        hlpr.track_handles_labels(handles, labels)
        # NOTE If supporting color, will need to make the labels contain both
        #      the y and color coordinate!?
