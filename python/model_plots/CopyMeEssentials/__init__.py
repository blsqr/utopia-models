"""Plots that are specific to the CopyMeEssentials model"""

# Import the module to trigger registration of operations
from .operations import *
