"""Tests population behaviour of the EEcosy model"""

import pytest
import numpy as np
from dantro.tools import recursive_getitem

from utopya.testtools import ModelTest

# Configure the ModelTest class
mtc = ModelTest("EEcosy", test_file=__file__, use_tmpdir=False)


# Fixtures --------------------------------------------------------------------
# Define fixtures


# Tests -----------------------------------------------------------------------
# Source Populations ..........................................................

@pytest.mark.skip("Needs adaption")
def test_source():
    """In a (source -> population) scenario, test that the shape of the time
    series is as expected
    """
    _, dm = mtc.create_run_load(from_cfg="cfgs/populations/source.yml")

    for uni in dm['multiverse'].values():
        print("\n----- Universe: ", uni.name)

        data = uni['data/EEcosy']
        cfg = uni['cfg']['EEcosy']

        # Find out the refill function of the source populations
        refill_func = recursive_getitem(cfg, "setup/procedures/"
                                             "source_and_primary/defaults/"
                                             "add_source_populations/init/"
                                             "refill_function".split("/"))
        print("Refill function: ", refill_func)

        # There should only be two populations, one source and one regular
        vertex_kinds = data['network/vertex_kinds'].sel()
        assert (vertex_kinds == [2, 1]).all()

        # They should be connected and that should be the only connection
        edges = data['network/_edges'].sel()
        assert edges.coords['idx'].size == 1
        assert (edges.values.flat == [1, 2]).all()

        # Retrieve population size time series
        size_s = data['populations/source/size'].sel()
        size_r = data['populations/regular/size'].sel()
        print("size_s:\n", size_s, end="\n"*3)
        print("size_r:\n", size_r, end="\n"*3)

        # As there is no outflow (zero mortality, no leak), the population
        # size should grow monotonically
        assert (size_r.diff(dim='time') >= 0.).all()

        # What about the source size? For both refill functions, it should
        # grow monotonically and reach a stable state
        assert (size_s.sel(time=slice(10, None)).diff(dim='time') >= 0.).all()

        # Compute growth and change of growth
        d_size_r = size_r.diff(dim='time')
        print("d_size_r:\n", d_size_r, end="\n"*3)

        dd_size_r = d_size_r.diff(dim='time')
        print("dd_size_r:\n", dd_size_r, end="\n"*3)

        # Growth is always positive
        assert (d_size_r > 0).all()

        # What about the change in growth?
        # It is negative (after some equilibriation)
        assert (dd_size_r.sel(time=slice(50, None)) <= 0.).all()

        # ... and goes towards a small value (negative, as asserted above)
        assert abs(dd_size_r.isel(time=-1)) < 1.

@pytest.mark.skip("Memory use too large")
def test_cep():
    """Tests that the CEP holds"""
    _, dm = mtc.create_run_load(from_cfg="cfgs/populations/cep.yml")

    for uni in dm['multiverse'].values():
        print("\n----- Universe: ", uni.name)

        data = uni['data/EEcosy']
        cfg = uni['cfg']['EEcosy']

        # Find out the refill function of the source populations
        asymmetry = recursive_getitem(cfg,
                                      "setup/procedures/source_and_primary/"
                                      "defaults/connect/init/asymmetry"
                                      "".split("/"))
        random_strengths = recursive_getitem(cfg,
                                             "setup/procedures/"
                                             "source_and_primary/defaults/"
                                             "connect/init/_apply_noise/"
                                             "_enabled".split("/"))
        # NOTE Once possible in dantro, select from uni coordinates
        print("Competitiveness asymmetry: ", asymmetry)
        print("Randomized strengths?      ", random_strengths)

        # There should be one source population and four primary producers
        vertex_kinds = data['network/vertex_kinds'].sel(time=0)
        assert (vertex_kinds == [2, 1, 1, 1, 1]).all()

        # Retrieve population size data of regular populations
        sizes = data['populations/regular/size'].sel()
        final_size = sizes.isel(time=-1)
        print("Sizes:\n", sizes, end="\n"*3)

        # Not becoming extinct
        assert np.nanmin(sizes) > 0

        # With unequal strengths, coexistence should only be possible for an
        # competitiveness asymmetry strictly smaller than 1.
        # For equal strengths, coexistence is possible at asymmetry <= 1.
        if random_strengths:
            if asymmetry < 1.:
                # All populations should survive; there is coexistence
                assert np.nanmin(final_size) > 0

            else:
                # Only one population should surive
                assert np.count_nonzero(~np.isnan(final_size)) == 1

        else:   # non-randomized strengths
            if asymmetry <= 1.:
                # All populations should survive; there is coexistence
                assert np.nanmin(final_size) > 0

            else:
                # Only one population should surive
                assert np.count_nonzero(~np.isnan(final_size)) == 1

            # Additionally, for asymmetry < 1, population sizes converge, as
            # the competitiveness asymmetry (an exponent) smoothes out all size
            # advantages and disadvantages
            if asymmetry < 1.:
                final_size_std = final_size.std()
                print("Final size std. dev.: ", final_size_std)
                assert final_size_std < 1.
