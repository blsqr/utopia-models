"""mesonet model test tools"""

import pytest

from utopya.tools import center_in_line


# Fixtures --------------------------------------------------------------------
# .. Passing configs ..........................................................
# NOTE Careful: These cannot be combined, i.e. unpacked side by side
def model_cfg(**kwargs) -> dict:
    """Creates a dict that can update the model config of mesonet directly"""
    return dict(parameter_space=dict(mesonet=dict(**kwargs)))

def mesonet_setup(**kwargs) -> dict:
    """Creates a dict that updates mesonet.setup"""
    return model_cfg(setup=dict(**kwargs))


# Tools -----------------------------------------------------------------------
def part_begin(test_part_name: str):
    """Prints a """
    print("\n\n",
          center_in_line("Testing part: " + test_part_name + " ...",
                         fill_char="-"),
          end="\n"*1)
