"""Tests dynamics of mesonet"""

import numpy as np
import pytest

from utopya.testtools import ModelTest
from utopya.tools import center_in_line

from ._testtools import model_cfg, mesonet_setup, part_begin

# Configure the ModelTest class
mtc = ModelTest("mesonet", test_file=__file__)


# Fixtures --------------------------------------------------------------------
# NOTE Also imported some from testtols


# Tests -----------------------------------------------------------------------

def test_res_secondary_refill():
    """Tests the refill dynamics of secondary resources"""
    # .. Test that without initial refill, initial state energy is 0 ..........
    part_begin("Without initial refill")

    _, dm = mtc.create_run_load(from_cfg="dyn_res_secondary_refill.yml",
                                **mesonet_setup(initial_refill=False),
                                perform_sweep=False)

    for uni in dm['multiverse'].values():
        # Get the group containing energy data
        energy = uni['data/mesonet/res_secondary/energy']

        for d in energy.values():
            assert (d[0] == 0.).all()

    # .. Now, test different parameter combinations ...........................
    part_begin("Parameter combinations")

    _, dm = mtc.create_run_load(from_cfg="dyn_res_secondary_refill.yml")

    for uni in dm['multiverse'].values():
        print("\nUniverse: ", uni.name)

        # Get the group containing all secondary resource data and the config
        data = uni['data/mesonet/res_secondary']
        cfg = uni['cfg']['mesonet']['setup']['secondary']
        print("Configuration: {}".format(cfg))

        # Check energy values
        for d in data['energy'].values():
            # Always non-negative
            assert (d >= 0.).all()

            # Should be the same between all resources
            assert (d.diff('id') == 0.).all()

            # For stepwise logistic growth, the growth is strictly monotonic
            if cfg['refill_mode'] == "StepwiseLogistic":
                assert (d.diff('time') > 0.).all()

            # For carrying capacity mode, monotony depends on initial condition
            elif cfg['refill_mode'] == "CarryingCapacity":
                if cfg['energy'] < cfg['refill_max_flow']:
                    # Should grow strictly monotonically
                    assert (d.diff('time') > 0.).all()

                else:
                    # Should be equal to initial value at all times
                    assert (np.isclose(d, cfg['energy'])).all()

        # Check energy input
        for d in data['energy_in'].values():
            # Is the same for all resources
            assert (d.diff('id') == 0.).all()

            if cfg['refill_mode'] == "StepwiseLogistic":
                # Is strictly monotonically falling
                assert (d.diff('time') < 0.).all()

            elif cfg['refill_mode'] == "CarryingCapacity":
                if cfg['energy'] < cfg['refill_max_flow']:
                    print(d.data)

                    # Input should be positive at all times
                    assert (d > 0.).all()

                    # Input should first grow and then decrease
                    t_peak = d.where(d == d.max(),
                                     drop=True).coords['time'].item()
                    print(t_peak)
                    growing = d.sel(time=slice(None, t_peak))
                    shrinking = d.sel(time=slice(t_peak, None))

                    print(growing)
                    print(shrinking)

                    assert (growing.diff('time') > 0.).all()
                    assert (shrinking.diff('time') < 0.).all()

                else:
                    # Input should be zero at all times
                    assert (d == 0.).all()


        # Energy output is zero, because there are no consumers. In the first
        # iteration step, energy output is not carried out, thus it is NaN
        for d in data['energy_out'].values():
            # In the dataset containing time step 0, all values are NaN
            if d.attrs['coords__time'][0] == 0:
                assert (np.isnan(d[0])).all()
                assert (d[1:] == 0.).all()
            else:
                assert (d == 0.).all()

        # Scaling factor is constant 1.; but NaN for initial state
        for d in data['scaling_factor'].values():
            # In the dataset containing time step 0, all values are NaN
            if d.attrs['coords__time'][0] == 0:
                assert (np.isnan(d[0])).all()
                assert (d[1:] == 1.).all()
            else:
                assert (d == 1.).all()


    # .. Make really long runs to test saturation .............................
    part_begin("Asymptotic behaviour: StepwiseLogistic refill mode")

    _, dm = mtc.create_run_load(from_cfg="dyn_res_secondary_refill.yml",
                                perform_sweep=False,
                                parameter_space=dict(num_steps=10000))

    for uni in dm['multiverse'].values():
        # Get the group containing all secondary resource data
        data = uni['data/mesonet/res_secondary']

        # Get the data
        energy_in = data['energy_in'][0]

        # Energy input should be non-zero, but negligible after a long time
        assert (energy_in[-1] > 0.).all()
        assert (energy_in[-1] < 1.).all()

    # Again for carrying capacity mode
    part_begin("Asymptotic behaviour: CarryingCapacity refill mode")

    _, dm = mtc.create_run_load(from_cfg="dyn_res_secondary_refill.yml",
                                perform_sweep=False,
                                parameter_space=dict(
                                    num_steps=500,  # 500 suffice here
                                    mesonet=dict(setup=dict(secondary=dict(
                                        energy=1.,
                                        refill_mode="CarryingCapacity")))))

    for uni in dm['multiverse'].values():
        # Get the configuration
        cfg = uni['cfg']['mesonet']['setup']['secondary']

        # Get the group containing all secondary resource data
        data = uni['data/mesonet/res_secondary']

        # Get the data
        energy = data['energy'][0]
        energy_in = data['energy_in'][0]

        cc = cfg['energy_scale_factor'] * cfg['refill_max_flow']

        # Energy should be close to energy_scale
        assert (np.isclose(energy[-1], cc, atol=1.e-6)).all()

        # Energy input should be non-zero, but negligible after a long time
        assert (energy_in[-1] > 0.).all()
        assert (energy_in[-1] < 1.).all()




def test_positive_energy():
    """Make sure energy is always zero or positive for both resources and
    consumers"""
    # .. Simple scenario ......................................................
    part_begin("Simple")

    _, dm = mtc.create_run_load(from_cfg="dyn_positive_energy.yml",
                                **mesonet_setup(num=dict(primary=3,
                                                         secondary=3,
                                                         consumer=3)))

    for uni in dm['multiverse'].values():
        data = uni['data/mesonet']

        for rt in ('res_primary', 'res_secondary', 'consumer'):
            print("Asserting non-negative energy for '{}' ...".format(rt))

            for energy in data[rt]['energy'].values():
                assert (energy.data >= 0.).all()

    # .. Depleting primary resource ...........................................
    part_begin("Depleting primary resource")

    setup = dict(num=dict(primary=1, secondary=0, consumer=1),
                 primary=dict(energy=1e6),
                 innovations=dict(num=dict(primary=1, secondary=0)))
    _, dm = mtc.create_run_load(from_cfg="dyn_positive_energy.yml",
                                **mesonet_setup(**setup))

    for uni in dm['multiverse'].values():
        data = uni['data/mesonet']

        for rt in ('res_primary', 'consumer'):
            print("Asserting non-negative energy for '{}' ...".format(rt))

            for energy in data[rt]['energy'].values():
                assert (energy.data >= 0.).all()

    # .. Single secondary resource ............................................
    part_begin("Single secondary resource")

    setup = dict(num=dict(primary=0, secondary=1, consumer=1),
                 innovations=dict(num=dict(secondary=1)))
    _, dm = mtc.create_run_load(from_cfg="dyn_positive_energy.yml",
                                **mesonet_setup(**setup))

    for uni in dm['multiverse'].values():
        data = uni['data/mesonet']

        for rt in ('res_secondary', 'consumer'):
            print("Asserting non-negative energy for '{}' ...".format(rt))

            for energy in data[rt]['energy'].values():
                assert (energy.data >= 0.).all()

    # .. Competition scenario .................................................
    part_begin("Competition scenario")

    # Increase max_flow to be in competition more often...
    setup = dict(num=dict(primary=1, secondary=1, consumer=3),
                 innovations=dict(num=dict(primary=1, secondary=1),
                                  primary=dict(max_flow=10.),
                                  secondary=dict(max_flow=10.)))
    _, dm = mtc.create_run_load(from_cfg="dyn_positive_energy.yml",
                                **mesonet_setup(**setup))

    for uni in dm['multiverse'].values():
        data = uni['data/mesonet']

        for rt in ('res_primary', 'res_secondary', 'consumer'):
            print("Asserting non-negative energy for '{}' ...".format(rt))

            for energy in data[rt]['energy'].values():
                assert (energy.data >= 0.).all()


# TODO add energy conservation test


def test_cep():
    """Makes sure that one species dominates in a competition scenario"""
    # .. Basic scenario .......................................................
    part_begin("Multiple consumers, single secondary resource")
    _, dm = mtc.create_run_load(from_cfg="dyn_cep.yml")

    for uni_name, uni in dm['multiverse'].items():
        data = uni['data/mesonet']
        setup = uni['cfg']['mesonet']['setup']

        print("Universe {} consumer count time series:\n  {}\n"
              "".format(uni_name, data['consumer/count'].data))

        # Check that the initial consumer count is same as initial value
        assert data['consumer/count'][0] == setup['num']['consumer']

        # Check that the final consumer count is 1
        assert data['consumer/count'][-1] == 1

        # TODO Check for scaling factor needed here?


    # .. Low refill flow ......................................................
    part_begin("Low refill_max_flow")

    sec = dict(secondary=dict(refill_max_flow=100.))
    _, dm = mtc.create_run_load(from_cfg="dyn_cep.yml",
                                **mesonet_setup(**sec))

    for uni_name, uni in dm['multiverse'].items():
        data = uni['data/mesonet']
        setup = uni['cfg']['mesonet']['setup']

        print("Universe {} consumer count time series:\n  {}\n"
              "".format(uni_name, data['consumer/count'].data))

        # Check that the initial consumer count is same as initial value
        assert data['consumer/count'][0] == setup['num']['consumer']

        # Check that the final consumer count is 1
        assert data['consumer/count'][-1] == 1

        # Here, the scaling factor dips below 1.0 due to strong competition
        for scaling_factor in data['res_secondary/scaling_factor'].values():
            assert np.any(scaling_factor[1:] < 1.)

    # .. Low flow .............................................................
    part_begin("Identical conditions lead to coexistence")

    no_noise = dict(innovations=dict(
                        secondary=dict(
                            new=dict(_apply_noise=dict(_enabled=False)))),
                    consumer=dict(_apply_noise=dict(_enabled=False)))
    _, dm = mtc.create_run_load(from_cfg="dyn_cep.yml",
                                **mesonet_setup(**no_noise))

    for uni_name, uni in dm['multiverse'].items():
        data = uni['data/mesonet']
        setup = uni['cfg']['mesonet']['setup']

        print("Universe {} consumer count time series:\n  {}\n"
              "".format(uni_name, data['consumer/count'].data))

        # Check that the initial consumer count is same as initial value
        assert data['consumer/count'][0] == setup['num']['consumer']

        # The final consumer count should be the same, because they coexist
        assert data['consumer/count'][-1] == setup['num']['consumer']
