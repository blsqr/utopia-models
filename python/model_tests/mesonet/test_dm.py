"""Tests data writing of mesonet"""

from itertools import chain

import pytest
import numpy as np
import xarray as xr

from utopya.eval.containers import XarrayDC
from utopya.testtools import ModelTest

from ._testtools import model_cfg, mesonet_setup, part_begin

# Configure the ModelTest class
mtc = ModelTest("mesonet", test_file=__file__)


# Fixtures --------------------------------------------------------------------
# NOTE Also imported some from testtols


# Tests -----------------------------------------------------------------------

def test_write_times():
    """Makes sure the write times are correct"""
    _, dm = mtc.create_run_load(from_cfg="dm_write_times.yml")

    for uni in dm['multiverse'].values():
        # Get the model data
        data = uni['data/mesonet']

        # Get write_start and write_every
        start = uni['cfg']['write_start']
        stop = uni['cfg']['num_steps'] + 1  # including last
        step = uni['cfg']['write_every']

        # Correct start for very negative values
        if start < -stop:
            start = 0

        if start > stop:
            time_coords = np.array([])
        else:
            time_coords = np.arange(start, stop, step)
            # TODO Will have to adjust this once write_start may be negative

        print("Expected time coordinates: ", time_coords)

        # If there are no coordinates, should not have written
        if not time_coords.size:
            # Groups of mapped and mapping properties are not created
            assert 'res_secondary/ids' not in data
            assert 'res_secondary/energy' not in data
            assert 'consumer/ids' not in data
            assert 'consumer/energy' not in data

            # standalone datasets are not created
            assert 'res_secondary/count' not in data
            assert 'consumer/count' not in data

        else:
            # datasets should have been created with a certain name
            # dset_name = "{:02d}".format(int(time_coords[0])) # with padding
            dset_name = "{:d}".format(int(time_coords[0]))   # without

            for path in ('res_secondary/ids', 'res_secondary/energy',
                         'consumer/ids', 'consumer/energy', 'consumer/sizes'):
                assert dset_name in data[path]

            # coordinates are set
            assert (   data['res_secondary/count'].coords['time']
                    == time_coords).all()
            assert (data['consumer/count'].coords['time'] == time_coords).all()


def test_dset_labelling():
    """Make sure datasets are labelled correctly"""
    _, dm = mtc.create_run_load(from_cfg="dm_dset_labelling.yml")

    for uni in dm['multiverse'].values():
        # Get the model data
        data = uni['data/mesonet']

        # Find out the time coordinates
        time_coords = list(range(uni['cfg']['num_steps'] + 1))

        # Go over resource types
        for res_type in ('res_primary', 'res_secondary', 'consumer'):
            print("Checking for resource type '{}' ...".format(res_type))
            res_data = data[res_type]

            # Check that id-mappings are labelled as such
            for cont in res_data['ids'].values():
                print("  Container: ", cont.path)
                assert isinstance(cont, XarrayDC)
                assert cont.ndim == 1
                assert cont.dims == ('idx',)

            # Check that datasets only encoding time are labelled with time
            for cont in (res_data['count'],):
                print("  Container: ", cont.path)
                assert isinstance(cont, XarrayDC)
                assert cont.ndim == 1
                assert cont.dims == ('time',)
                assert (cont.coords['time'] == time_coords).all()

            if res_type in ('res_primary', 'res_secondary'):
                it = chain(res_data['energy'].values(),
                           res_data['energy_out'].values(),
                           res_data['scaling_factor'].values())

            else:
                it = chain(res_data['energy'].values(),
                           res_data['energy_in'].values(),
                           res_data['sizes'].values(),
                           res_data['cc_frac'].values(),
                           res_data['alive'].values())

            # Check that datasets are labelled as (time, id)
            for cont in it:
                print("  Container: ", cont.path)
                assert isinstance(cont, XarrayDC)
                assert cont.ndim == 2
                assert cont.dims == ('time', 'id')
                assert (cont.coords['time'] == time_coords).all()
