"""Tests splitting and mutation"""

import math
from collections import defaultdict

import numpy as np
import pytest

from utopya.testtools import ModelTest
from utopya.tools import center_in_line

from ._testtools import model_cfg, mesonet_setup, part_begin

# Configure the ModelTest class
mtc = ModelTest("mesonet", test_file=__file__)


# Fixtures --------------------------------------------------------------------
# NOTE Also imported some from testtols

def mutations(**kwargs) -> dict:
    """Can be used to update mesonet.setup.consumer.split_params.mutatuibs"""
    return mesonet_setup(consumer=dict(
                            split_params=dict(
                                mutations=dict(**kwargs))))

# Helpers ---------------------------------------------------------------------

def calc_expected(initial, *, split_factor: float, split_size: int) -> list:
    """Given initial sizes and a homogeneous split factor, calculates the
    expected size distribution
    """
    print("Simulating expected consumer sizes for initial sizes {} "
          "and split factor {} ...".format(initial, split_factor))

    expected = [s for s in initial]
    while any([s > split_size for s in expected]):
        print(" "*16, expected)
        offspring_sizes = []

        # Iterate over expected sizes and split by split factor
        for i, s in enumerate(expected):
            if s > split_size:
                offspring_sizes.append(int(s * split_factor))
                expected[i] -= offspring_sizes[-1]

        expected += offspring_sizes
    return expected

# Tests -----------------------------------------------------------------------

def test_split_size_and_factor():
    """Tests that the split size option works and leads to populations not
    having sizes larger than the split size."""

    _, dm = mtc.create_run_load(from_cfg="split_size_and_factor.yml")

    for uni in dm['multiverse'].values():
        # Get data group and configuration
        cfg = uni['cfg']['mesonet']['setup']
        data = uni['data/mesonet']

        print("\n\n=====  Universe {:>4s}  =====".format(uni.name))

        # Make sure mutations were disabled
        assert not cfg['consumer']['split_params']['mutations']['enabled']

        # For zero split size, there should be no splitting, regardless of the
        # refill_max_flow parameter. Also, if the refill maximum flow, i.e. the
        # energy input into the system, is low, there should be no splitting
        # either, because there is not enough energy to reach a large size
        if (   cfg['consumer']['split_size'] == 0
            or cfg['secondary']['refill_max_flow'] < 1000.):
            assert (data['consumer/count'] == 1).all()

            # No need to test anything else
            print("No splitting occurred (as expected).")
            continue

        # If this point is reached, there should have been splitting. Check it.
        assert (data['consumer/count'] > 1).any()

        # Check there were no mutations, i.e. the number of innovations is
        # exactly the same for all consumers
        for d in data['consumer/num_innovations_secondary'].values():
            assert (d == cfg['innovations']['num']['secondary']).all()

        # However, the number of outflows should have increased for one of the
        # the resources, while the others should still have zero outflows
        for d in data['res_secondary/num_outflows'].values():
            # There should only be one active resource, i.e. with nonzero
            # number of outflows
            active_res = d.where(d > 0, drop=True)
            assert active_res.shape[1] == 1

            # Make sure it matches the consumer count
            assert (active_res == data['consumer/count']).all()

        # Check that splitting occurred according to the split_factor .........
        # Prepare iterator
        dsets = zip(data['consumer/ids'].values(),
                    data['consumer/sizes'].values(),
                    data['consumer/properties_full'].values(),
                    data['consumer/energy'].values(),
                    data['consumer/energy_in'].values())

        # Prepare tracking variables
        id_data, size_data, props_data, E_data, Ein_data = [], [], [], [], []
        offspring_ids = defaultdict(list)

        # Iterate over the change in ID maps, representing a splitting event
        for i, (ids, size, props, energy, energy_in) in enumerate(dsets):
            # Use only a list for ids; easier to handle, need no coordinates
            ids = list(ids.values)

            # Keep track of the data
            id_data.append(ids)
            size_data.append(size)
            props_data.append(props)
            E_data.append(energy)
            Ein_data.append(energy_in)

            # If this is the first iteration, can't do anything else here
            if i == 0:
                continue

            print("\n--- Splitting Event #{:>2d} ---".format(i))
            print("Time:    ", int(size.name))

            # Find out the new IDs
            old_ids =  [_id for _id in ids if _id in id_data[i-1]]
            new_ids =  [_id for _id in ids if _id not in id_data[i-1]]
            lost_ids = [_id for _id in id_data[i-1] if _id not in ids]
            print("Old IDs: ", old_ids)
            print("New IDs: ", new_ids)
            print("Lost IDs:", lost_ids)

            # Check that the splitting occurred as expected
            for new_id in new_ids:
                print("... Checking offspring with ID {} ...".format(new_id))
                # Check that offspring has the correct parent association
                parent_id = props.sel(id=new_id).item()["parent_id"]
                assert parent_id in old_ids

                # ... and parent has the correct offspring association
                parent_props = props.sel(id=parent_id).item()
                assert new_id in parent_props["offspring_ids"]

                # Offspring has no energy input (part taken from parent does
                # not count as "input")
                assert np.isnan(energy_in.sel(id=new_id)[0])

                # Determine parent's size and energy prior to splitting
                pts_size = size_data[i-1].sel(id=parent_id)[-1]
                pts_energy = E_data[i-1].sel(id=parent_id)[-1]

                split_size = parent_props["split_size"]
                split_factor = parent_props["split_params"]["split_factor"]

                print("Parent properties prior to split:"
                      "\n  Size:          {:>6d}"
                      "\n  Energy:        {:>8.1f}"
                      "\n  Split size:    {:>6d}"
                      "\n  Split factor:       {:.3g}"
                      "".format(pts_size.item(),
                                pts_energy.item(),
                                split_size,
                                split_factor))

                # Determine parent and offspring properties after split
                as_p_size = size.sel(id=parent_id)[0]
                as_p_energy = energy.sel(id=parent_id)[0]
                as_o_size = size.sel(id=new_id)[0]
                as_o_energy = energy.sel(id=new_id)[0]

                print("Parent and offspring properties after split:"
                      "\n  Size:          {:>6d}         {:>6d}"
                      "\n  Energy:        {:>8.1f}       {:>8.1f}"
                      "".format(as_p_size.item(),   as_o_size.item(),
                                as_p_energy.item(), as_o_energy.item()))

                # Only their combined size should be above the split size
                assert pts_size <= split_size
                assert as_p_size <= split_size
                assert as_o_size <= split_size
                assert as_p_size + as_o_size > split_size

                # As some population dynamics already occurred, cannot test the
                # split factor directly. But indirectly with some tolerance ...
                rel_o_size = (as_o_size / (as_p_size+as_o_size)).item()
                rel_o_energy = (as_o_energy / (as_p_energy+as_o_energy)).item()

                print("Relative offspring size:             {:.3g}"
                      "".format( rel_o_size))
                print("Relative offspring energy:           {:.3g}"
                      "".format( rel_o_energy))

                # Relative offspring size and energy should be equal
                assert np.isclose(rel_o_size, rel_o_energy)

                # ... should be within 5% relative tolerance
                assert abs(rel_o_size - split_factor) < (.05 * split_factor)
                assert abs(rel_o_energy - split_factor) < (.05 * split_factor)


def test_multi_splitting():
    """Check that if a population is heavily exceeding the split size, it
    splits multiple times such that it cannot remain above the split size.

    For this test, already initialize it above way above its split size, and
    disable _all_ kinds of dynamics, i.e.: no resource uptake, no population
    dynamics, no stochastic update, ...
    """

    _, dm = mtc.create_run_load(from_cfg="split_multi.yml")

    for uni in dm['multiverse'].values():
        # Get data group and configuration
        cfg = uni['cfg']['mesonet']['setup']
        data = uni['data/mesonet']

        print("\n\n=====  Universe {:>4s}  =====".format(uni.name))

        # At time 0, there should be the initial number... afterwards: more
        num_initial = cfg['num']['consumer']
        assert data['consumer/count'].sel(time=0) == num_initial
        assert data['consumer/count'].sel(time=1) > num_initial

        # Can check this more specifically, given the split factor and the
        # initial size
        split_size = cfg['consumer']['split_size']
        split_factor = cfg['consumer']['split_params']['split_factor']
        sizes0 = data['consumer/sizes'][0][0]
        sizes1 = data['consumer/sizes'][1][0]

        # Calculate expected size
        initial_sizes = [s for s in sizes0.values]

        expected = calc_expected(initial_sizes,
                                 split_factor=split_factor,
                                 split_size=split_size)
        actual = [s for s in sizes1.values]
        print("Expected sizes: ", expected)
        print("Actual sizes:   ", actual)
        assert expected == actual

        # Total size should be preserved between step 0 and step 1
        assert sum(initial_sizes) == sum(actual)


def test_mutations():
    """Test that offspring properties have mutated"""
    _, dm = mtc.create_run_load(from_cfg="split_mutations.yml", use_tmpdir=False)

    for uni in dm['multiverse'].values():
        print("\n\n=====  Universe {:>4s}  =====".format(uni.name))

        # Get data group and configuration
        cfg = uni['cfg']['mesonet']['setup']
        data = uni['data/mesonet']

        # Get some further config information
        split_params = cfg['consumer']['split_params']
        mutations = split_params['mutations']
        innovs = cfg['consumer']['innov_params']
        p_loss = mutations['innovations']['secondary']['p_loss']

        if p_loss not in [0., 1.]:
            raise ValueError("Unexpected p_loss: {}".format(p_loss))

        # At time 0, there should be the initial number... afterwards: more
        num_initial = cfg['num']['consumer']
        assert data['consumer/count'].sel(time=0) == num_initial
        assert data['consumer/count'].sel(time=1) > num_initial

        # In this configuration, all consumers should have the exact same size
        # Given initial size 6000 and split factor .5, ...
        assert (data['consumer/sizes'][1] == 750).all()
        # TODO Here and below, use dantro 0.8 features once available!

        # Test the maximum number of outflows; this indirectly tests the
        # selector 'least_num_outflows' chosen here ...
        if p_loss == 1.:
            # No resource has more than 1 outflow
            assert data['res_secondary/num_outflows'][0].max() == 1
        else:
            # Due to inheritance, the maximum number of outflows is larger
            assert data['res_secondary/num_outflows'][0].max() > 1

        # Prepare the properties data
        props = data['consumer/properties_full'][1]

        # For each consumer that is available at time step 1, can now check the
        # properties and compare it to those of the parent...
        for cid in props.coords['id'].values:
            cprops = props.sel(id=cid).item()

            pid = cprops.get('parent_id', 0)
            print("\nExamining properties of consumer {} (with parent {}) ..."
                  "".format(cid, pid))

            if pid == 0:
                print("  ... is primordial consumer. Skipping further tests.")
                continue

            # Get properties
            pprops = props.sel(id=pid).item()

            # Check that proliferation_rate mutated (as configured)
            assert cprops['proliferation_rate'] != pprops['proliferation_rate']

            # ... other properties should have stayed the same
            for p in ('energy_need', 'split_size', 'p_innovation',
                      'birth_rate', 'pd_update_sigma'):
                assert cprops[p] == pprops[p]

            # Get info on the innovations
            c_invsrc_ids = cprops['innovations_secondary_source_ids']
            p_invsrc_ids = pprops['innovations_secondary_source_ids']
            print("  Innovation sources:    {}\t (parent: {})"
                  "".format(c_invsrc_ids, p_invsrc_ids))

            # For innovations tests, need to distinguish between the different
            # scenarios depending on p_loss
            if p_loss == 1.:
                print("  ... parent innovations should have been lost")
                # Innovations should have changed completely
                assert all([iid not in p_invsrc_ids for iid in c_invsrc_ids])

                # Check number of innovations equals num_draws
                num_draws = mutations['innovations']['secondary']['num_draws']
                assert len(c_invsrc_ids) == num_draws

                # Done here. Cannot test innovation inheritance because all
                # the parent's innovations were lost
                continue
            # else: Can test inherited innovation's property mutation
            print("  ... parent innovations should have mutated")

            # Find out which innovations were inherited
            inherited = [iid for iid in p_invsrc_ids if iid in c_invsrc_ids]
            print("  Inherited:             {}".format(inherited))

            # Get the properties of the flows of consumer and parent
            c_fprops_all = cprops['innovations_secondary_properties']
            p_fprops_all = pprops['innovations_secondary_properties']
            # Keys are flow IDs, not source IDs

            # Find out the maximum number of innovations; relevant for whether
            # there will be only a mutation or also an improvement ...
            max_num_innovs = innovs['max_num']['secondary']
            imprv_factor = mutations['innovations']['secondary']['improvement']
            got_imprv = (max_num_innovs == len(c_invsrc_ids))
            num_imprvs = 0

            # For all inherited innovations, check the properties
            for src_id in inherited:
                print("  Source ID:              {}".format(src_id))

                c_flow_props = [fp for fp in c_fprops_all.values()
                                if fp['source_id'] == src_id]
                p_flow_props = [fp for fp in p_fprops_all.values()
                                if fp['source_id'] == src_id]

                assert len(c_flow_props) == 1
                assert len(p_flow_props) == 1

                c_flow_props = c_flow_props[0]
                p_flow_props = p_flow_props[0]
                print("    Properties of parent flow:\n     ", p_flow_props)
                print("    Properties of inherited flow:\n     ", c_flow_props)

                # Now compare the values. max_flow should have mutated
                assert c_flow_props['max_flow'] != p_flow_props['max_flow']

                # The efficacy should have always stayed the same if the
                # limit for maximum number of innovations was not hit.
                # Otherwise, it should differ by the improvement factor which
                # was applied potentially multiple times
                if not got_imprv:
                    assert c_flow_props['efficacy'] == p_flow_props['efficacy']

                else:
                    # The value compared to the initial value should be integer
                    _n_imprvs = math.log(c_flow_props['efficacy'],
                                         imprv_factor)
                    assert abs(_n_imprvs - round(_n_imprvs)) < 1e-16

                    # Calculate increase wrt parent
                    p_num_imprvs = round(math.log(p_flow_props['efficacy'],
                                                  imprv_factor))
                    num_imprvs += (  round(math.log(c_flow_props['efficacy'],
                                                    imprv_factor))
                                   - p_num_imprvs)

            # Check the number of improvements is as expected
            num_draws = mutations['innovations']['secondary']['num_draws']
            assert (   num_imprvs + (len(c_invsrc_ids) - len(p_invsrc_ids))
                    == num_draws)
