"""Tests initialisation of mesonet"""

import pytest
import numpy as np
import xarray as xr

from utopya.eval.containers import XarrayDC
from utopya.testtools import ModelTest

# Configure the ModelTest class
mtc = ModelTest("mesonet", test_file=__file__)


# Fixtures --------------------------------------------------------------------
# Define fixtures



# Tests -----------------------------------------------------------------------

def test_that_it_runs():
    """Tests ... that it runs..."""
    # Create a Multiverse using the default model configuration
    mv = mtc.create_mv()

    # Run a single simulation
    mv.run_single()

    # Load data using the DataManager and the default load configuration
    mv.dm.load_from_cfg(print_tree=True)


def test_num_entities():
    """Make sure the configuration options are applied."""
    _, dm = mtc.create_run_load(from_cfg="init_num_entities.yml")

    # Check that the number of primary and secondary resources as well as
    # of consumers and their innovations is as desired
    for uni in dm['multiverse'].values():
        print("Universe: ", uni.name)

        # Get the configuration key where number of entities is specified
        num = uni['cfg']['mesonet']['setup']['num']
        print("  Expected number of entities: ", num)

        # Get the data
        data = uni['data/mesonet']

        # Check that, if entities were to be created, the count matches, or
        # if they were not to be created, there is no such data group
        if num['primary']:
            assert data['res_primary/count'][0] == num['primary']
        else:
            assert 'res_primary' not in data

        if num['secondary']:
            assert data['res_secondary/count'][0] == num['secondary']
        else:
            assert 'res_secondary' not in data

        if num['consumer']:
            assert data['consumer/count'][0] == num['consumer']
        else:
            assert 'consumer' not in data

        # Check number of innovations
        if num['consumer']:
            # Get the configuration key where number of innovations per
            # consumer is specified
            num_innos = uni['cfg']['mesonet']['setup']['innovations']['num']
            print("  Expected number of innovations per consumer: ", num_innos)

            # Get the actual group of datasets
            num_innos_primary = data['consumer/num_innovations_primary']
            num_innos_secondary = data['consumer/num_innovations_secondary']

            # Compare.
            for d in num_innos_primary.values():
                assert np.all(d == num_innos['primary'])
            for d in num_innos_secondary.values():
                assert np.all(d == num_innos['secondary'])

def test_max_num_innovations():
    """Test that the number of innovations is within the specified bounds."""
    _, dm = mtc.create_run_load(from_cfg="init_max_num_innovations.yml")

    for uni in dm['multiverse'].values():
        print("Universe: ", uni.name)

        # Get the configuration key where number of entities is specified
        setup_cfg = uni['cfg']['mesonet']['setup']
        num = setup_cfg['num']
        print("  Expected number of entities:      ", num)

        # Get the data and check that number of innovations is correct
        data = uni['data/mesonet']
        # Check number of innovations

        # Get the configuration key where number of innovations per
        # consumer is specified and where the maximum is specified
        num_innos = setup_cfg['innovations']['num']
        print("  Configured number of innovations: ", num_innos)

        max_num_innos = setup_cfg['consumer']['innov_params']['max_num']
        print("  Maximum number of innovations:    ", max_num_innos)

        # Get the actual data
        num_innos_primary = data['consumer/num_innovations_primary'].sel()
        num_innos_secondary = data['consumer/num_innovations_secondary'].sel()
        print("  Actual number of innovations:")
        print("    Primary:   ", num_innos_primary.item())
        print("    Secondary: ", num_innos_secondary.item())

        # Compare
        assert (   num_innos_primary.item()
                == min(num_innos['primary'], max_num_innos['primary']))

        assert (   num_innos_secondary.item()
                == min(num_innos['secondary'], max_num_innos['secondary']))
