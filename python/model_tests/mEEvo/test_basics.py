"""Tests of the output of the mEEvo model"""

import os

import pytest
import numpy as np

from utopya.testtools import ModelTest

# Configure the ModelTest class
mtc = ModelTest("mEEvo", test_file=__file__)
DEFAULT_CFG_PATH = mtc.info_bundle.paths['default_cfg']
CFGS_DIR = os.path.join(os.path.dirname(DEFAULT_CFG_PATH), "cfgs")


# Fixtures --------------------------------------------------------------------
# Define configuration helpers
def from_cfgs_dir(path: str) -> dict:
    """Builds the from_cfg argument for create_mv or create_run_load"""
    return dict(from_cfg=os.path.join(CFGS_DIR, path))

def pspace(**kwargs) -> dict:
    """Builds a parameter_space dict"""
    return dict(parameter_space=dict(**kwargs))


# Tests -----------------------------------------------------------------------

def test_that_it_runs():
    """Tests that the model runs through with the default settings"""
    mv = mtc.create_mv()
    mv.run_single()
    mv.dm.load_from_cfg(print_tree=True)


def test_actions():
    """Tests that all actions and triggers are usable; note that this does
    not test their functionality itself, just if they work at all.
    """
    mv0, dm0 = mtc.create_run_load(from_cfg="actions.yml")


def test_config_sets():
    """Carries out all additional configurations that were specified alongside
    the default model configuration: the config sets

    This is done automatically for all run and eval configuration pairs that
    are located in subdirectories of the ``cfgs`` directory (at the same level
    as the default model configuration).
    If no run or eval configurations are given in the subdirectories, the
    respective defaults are used.

    .. note::

        The number of steps is reduced and there are no sweeps performed to
        reduce load during testing ...

    See :py:meth:`~utopya.model.Model.run_and_eval_cfg_paths` for more info.
    """
    # Need to explicitly define which ones to run, in case there are
    # incompatible additional config sets registered ...
    TO_RUN = (
        "benchmark",
        "cascade_stats",
        "cep",
        "competition",
        "costs_nw",
        "envmod",
        "envmod_num",
        "envmod_scenarios",
        "f_range",
        "ia_threshold",
        "inh_strength",
        "long",
        "mutation",
        "mutation_normal",
        "recycling",
        "ref",
        "res_growth",
        "s_range",
        "self_ia",
    )

    for cfg_name, cfg_paths in mtc.default_config_sets.items():
        if cfg_name not in TO_RUN or not cfg_paths.get("run"):
            print(f"\n\nSkipping '{cfg_name}' example ...")
            continue

        print(f"\n\nRunning '{cfg_name}' example ...")
        mv, _ = mtc.create_run_load(from_cfg=cfg_paths["run"],
                                    perform_sweep=False,
                                    print_tree=False,
                                    **pspace(
                                        num_steps=100,
                                        mEEvo=dict(
                                            mutate_every=10
                                        )
                                    ))
        # mv.pm.plot_from_cfg(plots_cfg=cfg_paths.get('eval'))
        # NOTE The plots would fail anyway because of `perform_sweep: False`
        # FIXME ...

        print(f"Succeeded running '{cfg_name}'.\n")


def test_long_run():
    """Tests that the model runs with an increased number of time steps.

    This test is also used to check that the written data is consistent.
    """
    mv, dm = mtc.create_run_load(from_cfg="long_run.yml", print_tree=False)

    for uni in dm["multiverse"].values():
        print("Universe: ", uni.name)
        print(uni.tree_condensed)

        data = uni["data/mEEvo"]
        cfg = uni["cfg"]["mEEvo"]

        # There should be some species removals
        times = data["times"]
        assert "removals" in times
        assert "ids_rem" in times

        # But fewer removals than additions
        assert len(times["additions"]) > len(times["removals"])
        assert len(times["ids_add"]) > len(times["ids_rem"])
        assert len(times["additions"]) == len(times["ids_add"])
        assert len(times["removals"]) == len(times["ids_rem"])

        # Can calculate a species age from this
        print(times["additions"].data)
        print(times["removals"].data)
        age_from_tt = (times["removals"].data - times["additions"].data)
        age_from_tt = age_from_tt.rename("age_from_tt")
        print(age_from_tt)
        assert (age_from_tt >= 0).all()
        assert (age_from_tt.drop_sel(id=0) >= 1).all()

        # Traits should be stored
        traits = data["traits"]
        assert "ids" in traits
        assert "body_mass" in traits
        assert "feeding_center" in traits
        assert "niche_width" in traits

        # Properties should be stored
        props = data["properties"]
        assert "ids" in props
        assert "age" in props
        assert "lifetime" in props
        assert "was_viable" in props
        assert "mean_trophic_level" in props
        assert "TL_diff" in props
        assert "extinction_delay" in props
        assert "initial_trophic_level" in props
        assert "n_extinctions" in props

        # Check that the age and lifetime data matches those from the time
        # tracker. The lifetimes will include null values; apart from those,
        # all entries should be >= 1.
        print(props["age"].data)
        print(props["lifetime"].data)

        assert (props["age"] == age_from_tt).all()
        assert (props["age"] == props["lifetime"].dropna("id")).all()
        assert (props["age"].drop_sel(id=0) >= 1).all()

        # The total biomass should always be non-negative
        assert (data["global/biomass"] >= 0.).all()

        # All species' biomass densities should be non-negative
        densities = data["odesys/biomass_density"].sel()
        assert (densities.min() >= 0.).all()

        # Check attributes
        # ID information
        resource_ids = data.attrs["resource_ids"]
        assert list(resource_ids) == [1]

        min_species_id = data.attrs["min_species_id"]
        assert min_species_id == 2  # sentinel 0, resource 1, species >=2

        non_species_ids = data.attrs["non_species_ids"]

        # Initial and final counts
        assert data.attrs["n_species_0"] == 1
        assert (
            data.attrs["n_species_total"] ==
            len(times["additions"].drop_sel(id=non_species_ids))
        )

        # Viability counts
        n_viable = data.attrs["n_viable"]
        viable_age = cfg["system"]["viability_age"]
        assert viable_age > 0

        age_viable = props["age"].drop_sel(id=non_species_ids) >= viable_age
        age_viable = age_viable.rename("was_viable (from age data)")
        print(age_viable)
        assert n_viable == age_viable.sum().item()

        # Species-specific viability information
        was_viable = props["was_viable"].data
        print(was_viable)
        assert not was_viable.sel(id=0)
        assert was_viable.sel(id=1)
        assert was_viable.sel(id=2)
        assert was_viable.drop_sel(id=non_species_ids).sum().item() == n_viable



def test_store_restore():
    """Tests the feature of storing and restoring the ODESystem state

    This tests that store-restore works in all of the following combinations:
        - stored    with_envmod,    restore with_envmod
        - stored    !with_envmod,   restore with_envmod
        - stored    with_envmod,    restore !with_envmod
        - stored    !with_envmod,   restore !with_envmod
    """
    mv0, dm0 = mtc.create_run_load(from_cfg="store_restore.yml",
                                   print_tree=False)

    for uni0 in dm0["multiverse"].values():
        # Get the final state and output file
        data0 = uni0["data/mEEvo"]
        final0 = data0["snapshots/_final_state"]
        fpath0 = uni0["cfg"]["output_path"]

        n_envmod0 = uni0["cfg"]["mEEvo"]["system"]["n_envmod"]
        with_envmod0 = uni0["cfg"]["mEEvo"]["with_envmod"]

        print("\n" + 60*"=")
        print("Output path:", fpath0)
        print("with_envmod0:", with_envmod0)
        print("Now running simulations that restore this state ...\n\n")

        # Some basic checks
        B0 = final0["B"].data.values
        if not with_envmod0:
            assert "E" not in final0
            assert "D" not in final0
            assert "frac_diverted" not in final0
            assert "distr_to_env" not in final0
        else:
            D0 = final0["D"]
            E0 = final0["E"]
            assert D0.size == B0.size
            assert E0.size == n_envmod0

        # Now, start another simulation from that file
        mv1, dm1 = mtc.create_run_load(
            from_cfg="store_restore.yml",
            **pspace(
                num_steps=0,
                mEEvo=dict(
                    restore_state=True,
                    restore_options=dict(
                        base_dir="",
                        from_file=fpath0
                    ),
                    system=dict(
                        # ignored:
                        n_resources=-12.34  # ... would raise error otherwise
                    )
                )
            ),
            print_tree=False
        )

        # With num_steps == 0, the new final state of the restored system
        # should be the same as the stored state of the initial system (dm0)
        # --> can use the loaded data for comparing that everything was done as
        #     it was intended.
        for uni1 in dm1["multiverse"].values():
            print("\n" + 40*"-")

            n_envmod1 = uni1["cfg"]["mEEvo"]["system"]["n_envmod"]
            with_envmod1 = uni1["cfg"]["mEEvo"]["with_envmod"]
            print("with_envmod0:", with_envmod0)
            print("with_envmod1:", with_envmod1)

            print("\nstored state:" + final0.tree)

            data1 = uni1["data/mEEvo"]
            final1 = data1["snapshots/_final_state"]
            print("restored state:" + final1.tree)

            # Compare final values of datasets are equal
            for k1, v1 in final1.items():
                k = k1
                print(f"  Checking state '{k}' ...")

                # Need to check some special cases, depending on with_envmod
                # combinations in states 0 and 1 ...
                if k == "env":
                    # Is a group, skip it, checked later
                    continue

                elif k in ("frac_diverted", "distr_to_env", "D", "E"):
                    # Need to check depending on whether the environment state
                    # is to be loaded at all ...
                    if with_envmod1:
                        assert v1.data.ndim >= 1

                        if with_envmod0:
                            v0 = final0[k]
                            assert (v0.data == v1.data).all();

                else:
                    # Simply check equivalence
                    v0 = final0[k]
                    assert (v0.data == v1.data).all();

            # Compare some specific values
            # But we can also compare the (separately stored) odesys values
            print("Checking biomass density ...")
            B1 = data1["odesys/biomass_density"].sel({}).squeeze().values
            assert np.allclose(B0, B1)

            # Traits are also matching
            print("Checking traits ...")
            ids0 = final0["ids"].data.values
            m0 = final0["m"].data.values
            m1 = data1["traits/body_mass"].sel(id=ids0).values
            assert np.allclose(m0, m1)

            # If the run has NO environment, there are no further checks ...
            if not with_envmod1:
                print("Not expecting environment-related entries ...")
                assert "D" not in final1
                assert "E" not in final1
                assert "env" not in final1
                assert "frac_diverted" not in final1
                assert "distr_to_env" not in final1
                continue
            # else: check the environment-related values

            # What about the depot and environment?
            print("Checking depot and environment...")
            D1 = final1["D"]
            E1 = final1["E"]
            assert D1.size == B1.size
            assert E1.size == n_envmod1

            print("Checking distribution matrix ...")
            d2env1 = final1["distr_to_env"]
            print(d2env1.data)
            assert d2env1.shape[0] == final1["E"].size
            assert d2env1.shape[1] == len(B1)

            print("Checking signature matrices ...")
            env1 = final1["env"]
            assert list(env1.keys()) == ["sig"]

            for s1 in env1["sig"].values():
                _s1 = str(s1.item())
                print(_s1)
                assert len(_s1) > 1
                assert (f"{len(B1)-1} {len(B1)-1} 0" in _s1 or
                        f"{len(B1)-1} {len(B1)-1} 1" in _s1)

            # If the first run stored modification matrices, can compare
            if with_envmod0:
                env0 = final0["env"]
                assert len(env0["sig"]) == len(env1["sig"])

                for s0, s1 in zip(env0["sig"].values(), env1["sig"].values()):
                    assert s0 == s1
