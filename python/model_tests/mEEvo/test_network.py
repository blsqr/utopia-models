"""Tests network extraction from mEEvo model"""

import os
import sys

import pytest

import networkx as nx

from utopya.testtools import ModelTest

# Manually import model_plots module
# TODO Should be available via `ModelTest` class!
sys.path.append(os.path.join(os.path.dirname(__file__), "../../"))
import model_plots
import model_plots.mEEvo
print(dir(model_plots))
ampl = model_plots.mEEvo


# Configure the ModelTest class
mtc = ModelTest("mEEvo", test_file=__file__)

# Constants
NETWORK_BASE = "network/snapshots"
GRAPH_CREATION_KWARGS = dict(
    node_props=("trophic_level", "biomass_density", "mean_ia_strength"),
    edge_props=("ia_strength", "diet_fraction", "mod_factor"),
)

# Fixtures --------------------------------------------------------------------
# Import some
from .test_basics import from_cfgs_dir, pspace

# Define fixtures


# Tests -----------------------------------------------------------------------

def test_extraction_and_annotation():
    """Tests that network characterisation works"""
    mv, dm = mtc.create_run_load(from_cfg="network.yml",
                                 print_tree="condensed")
    # ... such that a network is extracted at every 1000th time step

    for uni in dm["multiverse"].values():
        print("Universe: ", uni.name)
        print(uni.tree_condensed)

        data = uni["data/mEEvo"]
        cfg = uni["cfg"]["mEEvo"]

        nw_data = data[NETWORK_BASE]

        # Was written out every 50th time step, as configured
        times = nw_data["_vertices"].sel().coords["time"]
        assert (times / 50 == list(range(times.size))).all()

        # Test graph creation
        g0 = ampl.build_graph(graph_group=nw_data, at_time_idx=0,
                              **GRAPH_CREATION_KWARGS)
        g1 = ampl.build_graph(graph_group=nw_data, at_time_idx=-1,
                              **GRAPH_CREATION_KWARGS)

        # It is a directed graph with at least one node, even at time zero
        assert isinstance(g0, nx.DiGraph)
        assert isinstance(g1, nx.DiGraph)

        assert g0.number_of_nodes() > 1
        assert g1.number_of_nodes() > g0.number_of_nodes()

        # Test annotation and some basic properties
        for g in (g0, g1):
            ampl.annotate_network(g)

            # Only node 1 is an external resource
            assert g.nodes[1]["is_external_resource"]
            assert all([not g.nodes[n]["is_external_resource"]
                        for n in g.nodes if n != 1])

            # Trophic level is only 1 for the external resource
            assert g.nodes[1]["TL_shortest"] == 1
            assert g.nodes[1]["trophic_level"] == 1.

            assert all([g.nodes[n]["TL_shortest"] > 1
                        for n in g.nodes if n != 1])
            assert all([g.nodes[n]["trophic_level"] > 1.
                        for n in g.nodes if n != 1])


        # Test network characteristics ........................................
        nwc = ampl.compute_nw_characteristics(
            graph_group=nw_data, graph_creation=GRAPH_CREATION_KWARGS,
        )
        print(nwc)

        # Some basic tests for equivalence
        assert g1.number_of_nodes() == nwc.isel(time=-1).num_species
        assert (g1.number_of_edges() / g1.number_of_nodes() ==
                nwc.isel(time=-1).links_per_species)
        assert (g1.number_of_edges() / g1.number_of_nodes()**2 ==
                nwc.isel(time=-1).connectance)
