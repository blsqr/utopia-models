"""Tests dynamics of the mEEvo model"""

import pytest
import numpy as np

from utopya.testtools import ModelTest

# Configure the ModelTest class
mtc = ModelTest("mEEvo", test_file=__file__)


# Fixtures --------------------------------------------------------------------
# Import some
from .test_basics import from_cfgs_dir, pspace

# Define fixtures


# Tests -----------------------------------------------------------------------

def test_cep():
    """Tests that for strong enough competition, species become exinct"""
    mv, dm = mtc.create_run_load(from_cfg="cep.yml", print_tree=False)

    for uni in dm["multiverse"].values():
        print("Universe: ", uni.name)
        print(uni.tree_condensed)

        data = uni["data/mEEvo"]
        cfg = uni["cfg"]["mEEvo"]

        c_food = cfg["system"]["c_food"]
        print("c_food:", c_food)

        # Get the number of species
        n_species = data["global/n_species"].data
        print("n_species:", n_species)

        if c_food < 1.:
            assert n_species.isel(time=-1) == n_species.isel(time=0)

        elif c_food < 3.:
            assert n_species.isel(time=-1) < n_species.isel(time=0)

        else:
            assert n_species.isel(time=-1) == 1



# -----------------------------------------------------------------------------

@pytest.mark.skip(reason="Error to fix: NaN in quantile() computation") # FIXME
def test_envmod():
    """Tests the basics of the environment modification scenarios"""
    mv, dm = mtc.create_run_load(from_cfg="envmod.yml", print_tree=False)

    for uni in dm["multiverse"].values():
        print("Universe: ", uni.name)
        print(uni.tree_condensed)

        data = uni["data/mEEvo"]
        cfg = uni["cfg"]["mEEvo"]

        weight = cfg["system"]["environment"]["weight"]

        # Some non-zero signature matrices develop
        assert data["envmod/mean_signature_density"].max() > 0.
        assert data["envmod/mean_signature_density"].min() == 0.

        # Check instantaneous modification effect
        bflow = data["global/biomass_flow"]
        bflow_unmod = data["global/biomass_flow_unmodified"]

        if weight < 0.:
            assert (bflow/bflow_unmod <= 1.).all()

        elif weight == 0.:
            assert np.allclose(bflow_unmod, bflow)

        else:
            assert (bflow/bflow_unmod >= 1.).all()
