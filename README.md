# Yunus' Utopia Models

This repository contains evolutionary ecosystem models, implemented in [Utopia].
I developed and investigated these as part of my PhD project.
Specifically:

* `mEEvo`: an allometric food web model with continuous species turnover
    * This is the model I studied in the second part of my [PhD thesis](https://doi.org/10.11588/heidok.00030750)
    * Originally, this is by [Korinna Allhoff *et al.*](https://doi.org/10.1038/srep10955). The implementation here has many extensions.
* `EEcosy`: a generalised model of ecosystem evolution, still *WIP*
* `mesonet`: a Utopia reimplementation of the consumer-resource network I designed and studied in my MSc thesis.

For models of collective dynamics, see [`blsqr/collective-dynamics`](https://gitlab.com/blsqr/collective-dynamics).

This repository was created using the [Utopia Models Template](https://gitlab.com/utopia-project/models_template); see there on information on how to set up such a standalone repository for Utopia models.


#### Contents of this Readme
* [Installation](#installation)
* [Model Documentation](#model-documentation)
* [Quickstart](#quickstart)
* [Information for Developers](#information-for-developers)
* [Dependencies](#dependencies)
* [Copyright Notice](#copyright-notice)


---

## Installation
The following instructions assume that you built Utopia in a development environment, as indicated in the framework repository's [`README.md`](https://gitlab.com/utopia-project/utopia/blob/master/README.md)

**Note:** If not mentioned explicitly, all instructions and considerations from the [Utopia framework repository][Utopia] also apply here. ☝️


### Step-by-step Instructions
These instructions are intended for **[homebrew]-based** Ubuntu or macOS setups with Utopia already _built_ by your user.

⚠️ Be aware that this repository has some additional [dependencies](#dependencies) which might not be satisfiable easily on Ubuntu _without_ [homebrew].
If you don't use homebrew, you will have to fulfil the required dependency versions in some other way.


#### 0 — Setup Utopia
Make sure to `git checkout` your desired branch of the Utopia framework repository, and to properly configure and build the package as described in its [README](https://gitlab.com/utopia-project/utopia#how-to-install).


#### 1 — Cloning This Repository
Enter the `Utopia` directory into which you previously cloned the framework repository.

```bash
git clone --recurse-submodules https://gitlab.com/blsqr/evo-eco-models.git
```

*Note:* git submodules are used for some [dependencies](#dependencies).


#### 2 — Install dependencies
Install the third-party dependencies using [homebrew].

If you use some other package manager, make sure to fulfil the [dependencies requirements](#dependencies).
Additionally, consider the notes given in the section on installing dependencies in the Utopia framework repository.

```bash
brew update
brew install armadillo fmt spdlog yaml-cpp graphviz
```

If you want, install the optional dependencies:

```bash
brew install doxygen
```


#### 3 — Configure and build
Enter the repository and create your desired build directory:

```bash
cd models
mkdir build
```

Now, enter the build directory and invoke CMake:

```bash
cd build
cmake ..
```

The terminal output will show the configuration steps, which includes the installation of further Python dependencies and the creation of a virtual environment.

After this, you can build a specific or all Utopia models using:

```bash
make mesonet   # builds only the mesonet model
make -j4 all   # builds all models, using 4 CPUs
```


#### 4 — Run a model :tada:
You should now be able to run Utopia models from both the framework and this repository via the Utopia CLI.

Upon configuration, CMake creates symlinks to the Python virtual environment of the Utopia repository.
You can enter it by navigating to either of the build directories and calling

```bash
source ./activate
```

You can now execute the Utopia frontend as you are used to, only now the models of this repository should be available.


### Troubleshooting
* If the `cmake ..` command fails because it cannot locate the Utopia main
    repository, something went wrong with the CMake User Package Registry.
    To address this, enter the Utopia *framework* repository and export *its*configuration using the following command:

    ```bash
    cmake -DCMAKE_EXPORT_PACKAGE_REGISTRY=On ..
    ```

    Alternatively, you can always specify the location of Utopia's *build* directory when configuring *this* package, circumventing this CMake feature.
    To do so, specify the CMake variable `Utopia_DIR` during configuration:

    ```bash
    cmake -DUtopia_DIR=<path/to/>utopia/build ..
    ```

* If your model requires a certain branch of the Utopia framework, make sure
    to re-configure and re-build the framework repository after switching
    the branch.



## Model Documentation
Unlike the [main Utopia documentation](https://docs.utopia-project.org/), the models included in this repository come with their own documentation which has to be built locally.
It is *not* available online.

To build these docs locally, navigate to the `build` directory and execute

```bash
make doc
```

The [Sphinx](http://www.sphinx-doc.org/en/master/index.html)-built user documentation will then be located at `build/doc/html/`, and the C++ [doxygen](http://www.stack.nl/~dimitri/doxygen/)-documentation can be found at `build/doc/doxygen/html/`.
Open the respective `index.html` files to browse the documentation.



## Quickstart
### How to run a model?
The Utopia command line interface (CLI) is, by default, only available in a Python virtual environment, in which [`utopya`][utopya] (the Utopia frontend) and its dependencies are installed.
This virtual environment is located in the **main** (`utopia`) repository's build directory.
However, symlinks to the `activate` and `run-in-utopia-env` scripts are provided within the build directory of this project.
You can enter it by specifying the correct path:

```bash
source <path/to/{utopia,models}>/build/activate
```

Now, your shell should be prefixed with `(utopia-env)`.
All the following should take place inside this virtual environment.

As you have already done with the `dummy` model, the basic command to run a model named `SomeModel` is:

```bash
utopia run SomeModel
```

You can list all models registered in the frontend with

```bash
utopia model ls
```



## Information for Developers
### New to Utopia? How do I implement a model?
Please refer to the [documentation of the main repository](https://docs.utopia-project.org/).


### Testing
Not all test groups of the main project are available in this repository.

| Identifier        | Test description   |
| ----------------- | ------------------ |
| `model_<name>`    | The C++ model tests of model with name `<name>` |
| `models`†         | The C++ and Python tests for _all_ models |
| `models_python`†‡ | All python model tests (from `python/model_tests`) |
| `all`             | All of the above. (Go make yourself a hot beverage, when invoking this.) |

_Note:_
* Identifiers marked with `†` require all models to be built (by running `make all`).
* Identifiers marked with `‡` do _not_ have a corresponding `build_tests_*` target.
* Notice that you cannot execute tests for models of the main project in this repository.

#### Evaluating Test Code Coverage
Code coverage is useful information when writing and evaluating tests.
The coverage percentage of the C++ code is reported via the GitLab CI pipeline.
Check the [`README.md` in the main repository](https://gitlab.com/utopia-project/utopia#c-code-coverage) for information on how to compile your code with coverage report flags and how to retrieve the coverage information.


## Dependencies
| Software                  | Version  | Comments |
| ------------------------- | -------- | -------- |
| [model-building-utils]    | >= 9.900 | included as git submodule |
| [armadillo]               | >= 9.900 | |
| [spdlog]                  | >= 1.7   | |
| [fmt]                     | >= 7.0   | typically installed by spdlog |
| [yaml-cpp]                | >= 0.6.3 | |
| [graphviz]                | >= 2.40  | required for `pygraphviz` |
| [doxygen] _(optional)_    |          | for building the documentation |

These dependencies come on top of [Utopia]'s dependencies.
For some dependencies, a more recent version number is required than by the Utopia framework; the numbers given here take precedence and are enforced.
Note that the homebrew-based CI/CD testing image might use more recent versions, so it's actually best to have those installed.

:warning: There seem to be some weird errors with boost v1.74, observed only in the pipeline, thus the testing image uses v1.71.


### Additional Python Dependencies
If your model requires additional Python packages (e.g., for plotting and testing), they can be added to the `python/requirements.txt` file.
All packages listed there will be automatically installed into the Python virtual environment as part of the CMake configuration.
Note that these might require some additional dependencies to be installed via the package manager; those are listed above.
If such an installation fails, it will _not_ lead to a failing CMake configuration.


### Background Information on Version History
These models were moved to this separate repository to speed up development and increase flexibility.
The repository was (manually) forked from the original (group-internal) [`utopia/models`][models] repository from commit [`e7d4823d`](https://gitlab.com/utopia-project/models/-/commit/e7d4823dd7465f344bd87805e6019fa55d09f12b) and only contains models that were implemented by @blsqr.

Notable differences to the `utopia/models` repository:
* Additional dependencies
* Testing image is [homebrew-based Ubuntu 20.04](https://hub.docker.com/r/homebrew/ubuntu20.04), thus has more recent versions of dependencies




## Copyright Notice

    Copyright (C) 2018 – 2022  Yunus Sevinchan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

A copy of the [GNU General Public License Version 3][GPLv3], and the [GNU Lesser General Public License Version 3][LGPLv3] extending it, is distributed with the source code of this program; see the `COPYING` and `COPYING.LESSER` files in the root directory of this project, respectively.

For questions or contributions, please open an issue or contact me via [`yunus.sevinchan@iup.uni-heidelberg.de`][devmail].




[Utopia]: https://gitlab.com/utopia-project/utopia
[utopya]: https://gitlab.com/utopia-project/utopya
[model-building-utils]: https://gitlab.com/utopia-project/model-building-utils
[models]: https://gitlab.com/utopia-project/models
[devmail]: mailto:yunus.sevinchan@iup.uni-heidelberg.de

[homebrew]: https://brew.sh
[armadillo]: http://arma.sourceforge.net/
[fmt]: https://fmt.dev/latest/index.html
[spdlog]: https://github.com/gabime/spdlog
[yaml-cpp]: https://github.com/jbeder/yaml-cpp
[graphviz]: https://www.graphviz.org/

[doxygen]: http://www.doxygen.nl
