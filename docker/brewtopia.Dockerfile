# =============================================================================
# This is a homebrew-based setup of the Utopia framework.
# It installs all necessary dependencies using homebrew, clones the repository,
# and sets it up in working condition.
# =============================================================================

ARG BASE_IMAGE="homebrew/ubuntu22.04:latest"
FROM ${BASE_IMAGE}

LABEL maintainer="Yunus Sevinchan <ysevinch@iup.uni-heidelberg.de>"


# .. Install dependencies .....................................................
# Get ready ...
RUN brew update && brew install hello

# Install Utopia framework dependencies
RUN brew install --display-times gcc llvm pkg-config cmake python@3.10
RUN brew install --display-times armadillo boost hdf5 fmt spdlog yaml-cpp

# Optional dependencies
RUN brew install --display-times doxygen
RUN brew install --display-times texlive
RUN brew install --display-times graphviz
RUN brew install --display-times fftw
RUN brew install --display-times ffmpeg

# Make sure the correct gcc is linked
RUN brew link gcc --overwrite


# .. Set up Utopia ............................................................
RUN mkdir utopia
WORKDIR /home/linuxbrew/utopia

ARG UTOPIA_REPO="https://gitlab.com/utopia-project/utopia.git"
ARG UTOPIA_BRANCH="master"

RUN    git clone ${UTOPIA_REPO} \
    && cd utopia \
    && git checkout ${UTOPIA_BRANCH} \
    && mkdir -p build

WORKDIR /home/linuxbrew/utopia/utopia/build

ENV CC=gcc CXX=g++
RUN cmake -DCMAKE_BUILD_TYPE=Release -DHDF5_ROOT=$(brew --prefix hdf5) ..

# Hack to get around a linker error for libdl ...
RUN sudo ln -s libdl.so.2 /usr/lib/x86_64-linux-gnu/libdl.so

# Can now make the model and perform a test run
RUN make dummy && ./run-in-utopia-env utopia run dummy


# .. Get ready for using Utopia ...............................................
# TODO Could install the ipython stuff here as well ...

# Communicate to cmake where to find UtopiaConfig.cmake
ENV Utopia_DIR=/home/linuxbrew/utopia/utopia/build

# As entrypoint, enter the shell from within the utopia-env
WORKDIR /home/linuxbrew
ENTRYPOINT [ "/home/linuxbrew/utopia/utopia/build/run-in-utopia-env", \
             "/bin/bash" ]
