# TODO Consider moving all this to Utopia to have it more widely available


# Initializes a directory as a submodule if it was not initialized yet
function(initialize_submodule)
    # Parse function arguments
    set(OPTION SHOW_IN_SUMMARY SHOW_AS_REQUIRED)
    set(SINGLE NAME DIRECTORY)
    set(MULTI)
    include(CMakeParseArguments)
    cmake_parse_arguments(ARG "${OPTION}" "${SINGLE}" "${MULTI}" "${ARGN}")

    if(ARG_UNPARSED_ARGUMENTS)
        message(WARNING "Unparsed arguments in initialize_submodule!")
    endif()

    # Check if we need to do something
    if(NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${ARG_DIRECTORY}/.git)
        message(
            STATUS
                "Couldn't find git submodule ${ARG_NAME} in ${ARG_DIRECTORY}! "
                "Initializing now ..."
        )

        find_package(Git QUIET REQUIRED)
        execute_process(
            COMMAND ${GIT_EXECUTABLE} submodule update --init ${ARG_DIRECTORY}
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
            RESULT_VARIABLE GIT_EXIT_CODE
        )
        if (NOT GIT_EXIT_CODE EQUAL "0")
            message(
                FATAL_ERROR
                    "git submodule update --init external/${ARG_DIRECTORY}"
                    "failed with exit code ${GIT_EXIT_CODE}."
                    "Please checkout the submodule manually."
            )
        endif()
    endif()

    # Optionally, make it appear in the summary of required packages
    if (ARG_SHOW_IN_SUMMARY)
        set_property(
            GLOBAL APPEND PROPERTY
                PACKAGES_FOUND ${ARG_NAME}
        )
        if (ARG_SHOW_AS_REQUIRED)
            set_property(
                GLOBAL APPEND PROPERTY
                    _CMAKE_${ARG_NAME}_TYPE REQUIRED
            )
        endif()
    endif()
endfunction(initialize_submodule)


function(include_package_from_submodule)
    # Parse function arguments
    set(OPTION)
    set(SINGLE NAME DIRECTORY RELATIVE_INCLUDE_DIR)
    set(MULTI)
    include(CMakeParseArguments)
    cmake_parse_arguments(ARG "${OPTION}" "${SINGLE}" "${MULTI}" "${ARGN}")

    if(ARG_UNPARSED_ARGUMENTS)
        message(
            WARNING "Unparsed arguments in include_package_from_submodule!"
        )
    endif()

    message(
        STATUS
            "Including ${ARG_NAME} package "
            "from submodule at ${ARG_DIRECTORY} ..."
    )
    initialize_submodule(
        NAME ${ARG_NAME}
        DIRECTORY ${ARG_DIRECTORY}
        SHOW_IN_SUMMARY SHOW_AS_REQUIRED  # TODO Parametrize
    )

    # Prepare paths
    # TODO Consider exporting?!
    set(${ARG_NAME}_DIR ${CMAKE_CURRENT_SOURCE_DIR}/${ARG_DIRECTORY})
    set(${ARG_NAME}_INCLUDE_DIR ${${ARG_NAME}_DIR}/${ARG_RELATIVE_INCLUDE_DIR})

    include_directories(${${ARG_NAME}_INCLUDE_DIR} SYSTEM)
endfunction(include_package_from_submodule)
