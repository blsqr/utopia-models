Welcome to the Utopia Models documentation!
===========================================

This documentation only covers the models included in this repository, i.e. models by `Yunus Sevinchan <https://ts-gitlab.iup.uni-heidelberg.de/yunus>`_.
For the documentation of `Utopia <https://ts-gitlab.iup.uni-heidelberg.de/utopia/utopia>`_ itself, please refer to its `online documentation <https://hermes.iup.uni-heidelberg.de/utopia_doc/master/html/>`_.

.. toctree::
   :maxdepth: 1
   :glob:

   The README <README>
   C++ Documentation <cpp_doc>

.. toctree::
   :caption: Models
   :maxdepth: 1
   :glob:

   models/*
