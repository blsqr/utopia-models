
``EEcosy`` - Model for the Investigation of the Evolution of Ecosystems
=======================================================================

*WIP*

.. contents::
   :local:
   :depth: 2

----

Default Configurations
----------------------
Below are the default model and plot configuration parameters for the ``EEcosy`` model.

Default Model Configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. literalinclude:: ../../src/models/EEcosy/EEcosy_cfg.yml
   :language: yaml
   :start-after: ---

Default Plot Configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^
.. literalinclude:: ../../src/models/EEcosy/EEcosy_plots.yml
   :language: yaml
   :start-after: ---

Base Plot Configuration
^^^^^^^^^^^^^^^^^^^^^^^
.. literalinclude:: ../../src/models/EEcosy/EEcosy_base_plots.yml
   :language: yaml
   :start-after: ---

Note that some of these plots are based on the utopya base plot configurations, which are not part of this documentation.
